/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

(function () {
  'use strict';

  angular
    .module('boardMembersPasswords', [])
    .controller('boardMembersPasswordsController', BoardMembersPasswordsController);

  function BoardMembersPasswordsController(
    $mdDialog,
    endpoints,
    $http,
    $interval,
    $timeout,
    $mdToast,
    gettextCatalog,
    toastCustom,
    $scope
  ) {
    const bmpCtrl = this;
    bmpCtrl.config = $scope.bmpConfig;

	const BOARD_PASSWORD_MIN_LENGTH = 24;
	const BOARD_PASSWORD_MAX_LENGTH = 64;
    const membersPasswordsValues = [];
    let numberOfPasswordsSet = 0;

    bmpCtrl.currentMember = {};
    bmpCtrl.membersStatusDone = {};
    bmpCtrl.isPasswordHasBeenValidated = false;
    bmpCtrl.isPasswordVisible = false;
    bmpCtrl.isPasswordLengthError = false;
    bmpCtrl.isPasswordDigitError = false;
    bmpCtrl.isPasswordSpecialError = false;
    bmpCtrl.isPasswordLowercaseError = false;
    bmpCtrl.isPasswordUppercaseError = false;
    bmpCtrl.isMemberInSelectionProcess = false;

    // Two steps dialog closing handling
    bmpCtrl.twoStepsClosing = false;
    bmpCtrl.closeDialog = () => bmpCtrl.twoStepsClosing = true;
    bmpCtrl.exitDialog = () => $mdDialog.cancel();
    bmpCtrl.discardClose = () => bmpCtrl.twoStepsClosing = false;

    bmpCtrl.selectMember = function (member) {
      resetPasswords();
      bmpCtrl.currentMember = member;
      bmpCtrl.isMemberInSelectionProcess = false;
      setFocusTo('mainPasswordInput', 500);
    };

    // Main password input keyup
    bmpCtrl.mainPasswordKeyup = function (ev) {
      if (isMainPasswordEmpty()) {
        return;
      }
      // Password is checked and user press enter
      if (checkMainPasswordPolicy() && ev && ev.which === 13) {
        if (bmpCtrl.isConstitutionMode()) {
          // In constitution mode, go to confirm password input
          setFocusTo('confirmPasswordInput');
        } else {
          // In activation mode, validate the password.
          bmpCtrl.validatePassword();
        }
      }
    };

    // Confirm password input keyup
    bmpCtrl.confirmPasswordKeyup = function (ev) {
      const confirm = bmpCtrl.confirmPassword.value;
      if (isMainPasswordEmpty() || confirm == null || confirm.length === 0) {
        return;
      }
      if (checkPasswordsMatch() && ev && ev.which === 13) {
        bmpCtrl.validatePassword();
      }
    };

    bmpCtrl.togglePasswordVisibility = function () {
      bmpCtrl.isPasswordVisible = !bmpCtrl.isPasswordVisible;
    }

    // Password validation
    bmpCtrl.validatePassword = function () {
      if (isMainPasswordEmpty() || bmpCtrl.isMemberInSelectionProcess) {
        return;
      }
      bmpCtrl.isMemberInSelectionProcess = true;
      if (bmpCtrl.isConstitutionMode()) {
        if (checkMainPasswordPolicy() && checkPasswordsMatch()) {
          setCurrentMemberStatusToDone();
        }
      } else {
        validateMainPasswordHash();
      }
    }

    bmpCtrl.isConstitutionMode = function () {
      return bmpCtrl.config.membersPasswordsMode === 'constitution';
    }
    bmpCtrl.isActivationMode = function () {
      return bmpCtrl.config.membersPasswordsMode === 'activation';
    }

    bmpCtrl.isAdministrationBoardType = function () {
      return bmpCtrl.config.boardType === 'administration';
    }

    bmpCtrl.isElectoralBoardType = function () {
      return bmpCtrl.config.boardType === 'electoral';
    }

    bmpCtrl.isValidatePasswordButtonDisabled = function () {
      return bmpCtrl.mainPassword.value.length < BOARD_PASSWORD_MIN_LENGTH
        || bmpCtrl.isMemberInSelectionProcess
        || (bmpCtrl.isConstitutionMode() && bmpCtrl.confirmPassword.value.length < BOARD_PASSWORD_MIN_LENGTH)
        || (bmpCtrl.isConstitutionMode() && !bmpCtrl.isCurrentMemberPasswordMatch);
    }

    bmpCtrl.isSubmitPasswordsDisabled = function () {
      // Disable submit password button if the minimum of completed members is not reached.
      return !isMinimumCompletedMembersReached() || bmpCtrl.isMemberInSelectionProcess;
    }

    bmpCtrl.submitPasswords = function () {
      if (isMinimumCompletedMembersReached()) {
        $mdDialog.hide(membersPasswordsValues)
      }
    }

    function isMinimumCompletedMembersReached() {
      // In constitution mode, all members must be completed
      if (bmpCtrl.isConstitutionMode()) {
        return numberOfPasswordsSet === bmpCtrl.config.selectedBoardMembers.length;
      }
      // In activation mode, a minimum number (threshold) of members must be completed
      if (bmpCtrl.isActivationMode()) {
        return numberOfPasswordsSet >= bmpCtrl.config.membersPasswordsThreshold;
      }
      return false;
    }

    function initialize() {
      // Select first member
      bmpCtrl.selectMember(bmpCtrl.config.selectedBoardMembers[0]);
      // Initialize members status
      bmpCtrl.config.selectedBoardMembers.forEach(function (member) {
        membersPasswordsValues[bmpCtrl.config.selectedBoardMembers.indexOf(member)] = [];
        bmpCtrl.membersStatusDone[member] = false;
      });
    }

    // Initializing the status of each member
    initialize();

    // Check the password against the policies.
    function checkMainPasswordPolicy() {
      const pwd = bmpCtrl.mainPassword.value;
      let isValid = true;

      // Length validation
      const lengthValid = new RegExp(`^(.{${BOARD_PASSWORD_MIN_LENGTH},${BOARD_PASSWORD_MAX_LENGTH }}$)`, "gm").test(pwd);
      isValid &&= lengthValid;
      bmpCtrl.isPasswordLengthError = !lengthValid;

      // Digit validation
      const digitValid = /^(?=.*\d).+$/gm.test(pwd);
      isValid &&= digitValid;
      bmpCtrl.isPasswordDigitError = !digitValid;

      // Lowercase validation
      const lowerValid = /^(?=.*[a-z]).+$/gm.test(pwd);
      isValid &&= lowerValid;
      bmpCtrl.isPasswordLowercaseError = !lowerValid;

      // Uppercase validation
      const upperValid = /^(?=.*[A-Z]).+$/gm.test(pwd);
      isValid &&= upperValid;
      bmpCtrl.isPasswordUppercaseError = !upperValid;

      // Special validation
      const specialValid = /^(?=.*[^a-zA-Z0-9]).+$/gm.test(pwd);
      isValid &&= specialValid;
      bmpCtrl.isPasswordSpecialError = !specialValid;

      bmpCtrl.isCurrentMemberPasswordValid = isValid;
      bmpCtrl.isPasswordHasBeenValidated = true;
      return isValid;
    }

    function checkPasswordsMatch() {
      if (bmpCtrl.mainPassword.value === bmpCtrl.confirmPassword.value) {
        bmpCtrl.isCurrentMemberPasswordMatch = true;
        return true;
      } else {
        bmpCtrl.isCurrentMemberPasswordMatch = false;
        return false;
      }
    }

    function validateMainPasswordHash() {
      const passwordValidationEndpoint = `${bmpCtrl.config.membersPasswordsValidationUrl}?memberIndex=${getIndexOfCurrentMember().toString()}`;

      bmpCtrl.isMemberInSelectionProcess = true;
      $http.put(passwordValidationEndpoint, [...bmpCtrl.mainPassword.value]).then(
        () => setCurrentMemberStatusToDone(),
        () => {
          $mdToast.show(
              toastCustom.topCenter(
                  gettextCatalog.getString('The password is wrong.'),
                  'error'
              )
          );
          bmpCtrl.isMemberInSelectionProcess = false;
        }
      );
    }

    function setCurrentMemberStatusToDone() {
      if (isMainPasswordEmpty()) {
        return;
      }
      membersPasswordsValues[getIndexOfCurrentMember()] = bmpCtrl.mainPassword.value;
      bmpCtrl.membersStatusDone[bmpCtrl.currentMember] = true;
      numberOfPasswordsSet++;

      $timeout(() => {
        if (isMinimumCompletedMembersReached()) {
          resetPasswords();
          bmpCtrl.isMemberInSelectionProcess = false;
          setFocusTo('submitPasswordsButton');
        } else {
          selectNextMember();
        }
      }, 500);
    }

    function selectNextMember() {
      // Find next not done member
      for (const member of bmpCtrl.config.selectedBoardMembers) {
        if (!bmpCtrl.membersStatusDone[member]) {
          bmpCtrl.selectMember(member);
          break;
        }
      }
    }

    function getIndexOfCurrentMember() {
      return bmpCtrl.config.selectedBoardMembers.indexOf(bmpCtrl.currentMember);
    }

    function setFocusTo(elementId, delay) {
      $timeout(() => document.getElementById(elementId).focus(), delay || 100);
    }

    function isMainPasswordEmpty() {
      const pwd = bmpCtrl.mainPassword.value;
      return pwd == null || pwd.length === 0;
    }

    function resetPasswords() {
      // Clear passwords
      bmpCtrl.mainPassword = {
        value: '',
      };
      bmpCtrl.confirmPassword = {
        value: '',
      };
      // Reset password flags
      bmpCtrl.isCurrentMemberPasswordValid = false;
      bmpCtrl.isCurrentMemberPasswordMatch = false;
      bmpCtrl.isPasswordVisible = false;
      bmpCtrl.isPasswordHasBeenValidated = false;
      bmpCtrl.isPasswordLengthError = false;
      bmpCtrl.isPasswordDigitError = false;
      bmpCtrl.isPasswordSpecialError = false;
      bmpCtrl.isPasswordLowercaseError = false;
      bmpCtrl.isPasswordUppercaseError = false;
    }

  }

})();
