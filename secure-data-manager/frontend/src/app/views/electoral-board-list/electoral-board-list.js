/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

import dialogShowMembersTemplate from '../dialogs/dialog-show-members.html';
import boardMembersPasswordsTemplate from '../board-members-passwords/board-members-passwords.html';

export default angular
  .module('electoral-board-list', [])
  .controller('electoral-board-list', function (
    $scope,
    $rootScope,
    $http,
    endpoints,
    sessionService,
    $mdDialog,
    $mdToast,
    CustomDialog,
    gettextCatalog,
    toastCustom,
    adminBoardActivationService
  ) {
    'use strict';

    const electionEventIdPattern = '{electionEventId}';
    const electoralBoardIdPattern = '{electoralBoardId}';

    const ELECTORAL_BOARD_SIGNING_MSG_ID = 'Electoral board signing';
    const ELECTORAL_BOARD_SIGNED_MSG_ID = 'Electoral board signed!';
    const ELECTORAL_BOARD_MSG_ID = 'Electoral Board';
    const ELECTORAL_BOARD_CONSTITUTE_MSG_ID = 'Constitute electoral board';
    const ELECTORAL_BOARD_CONSTITUTE_SUCCESS_MSG_ID = 'Electoral board successfully constituted';
    const ELECTORAL_BOARD_MEMBERS_MSG_ID = 'Electoral board members';
    const ADMIN_BOARD_ACTIVATE_MSG_ID = 'Please, activate the administration board.';
    const ADMIN_BOARD_ACTIVATE_OK_MSG_ID = 'Activate now';
    const ADMIN_BOARD_WRONG_ACTIVATED_MSG_ID = 'Wrong Administration Board activated';
    const ADMIN_BOARD_WRONG_ACTIVATED_EXPLANATION_MSG_ID = 'The active Administration Board does not belong to the Election Event that you are' +
      ' trying to operate. Please deactivate it and activate the corresponding Administration Board for the Election Event';

    // initialize & populate view
    $scope.selectedElectionEventId = sessionService.getSelectedElectionEvent().id;
    $scope.electoralBoardsList = [];
    $scope.showMembersDialogTitle = '';
    $scope.isConstitutionInProgress = false;
    $scope.isSigningInProgress = false;

    // List the electoral boards
    $scope.listElectoralBoards = function () {
      const listEndpoint = endpoints.host() + endpoints.electoralBoardList.replace(electionEventIdPattern, $scope.selectedElectionEventId);
      $http.get(listEndpoint).then((response) => {
          const data = response.data;
          $rootScope.safeApply(() => {
            $scope.electoralBoardsList = data.result;
            sessionService.setElectoralBoards(data.result);
          });
        }
      );
    };

    $scope.listElectoralBoards();

    // Refresh listener
    $scope.$on('refresh-electoral-board-list', function () {
      $scope.listElectoralBoards();
    });

    // Members popup display
    $scope.showMembers = function (electoralBoard, ev) {
      $scope.showMembersDialogTitle = ELECTORAL_BOARD_MEMBERS_MSG_ID;
      $scope.showMembersDialogIconText = ELECTORAL_BOARD_MEMBERS_MSG_ID;
      $scope.listOfMembers = electoralBoard.boardMembers;
      $scope.closeDialog = () => $mdDialog.cancel();
      $mdDialog.show({
        template: dialogShowMembersTemplate,
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        scope: $scope,
        escapeToClose: true,
        preserveScope: true,
      });
    };

    /***
     * Electoral Board constitution
     */
    $scope.constituteElectoralBoard = function () {
      const selectedElectoralBoard = getSelectedElectoralBoard();

      // Only perform constitution if entity is LOCKED
      if (!selectedElectoralBoard || selectedElectoralBoard.status !== 'LOCKED') {
        new CustomDialog()
          .title(getText(ELECTORAL_BOARD_CONSTITUTE_MSG_ID))
          .cannotPerform(getText(ELECTORAL_BOARD_MSG_ID))
          .show();
        return;
      }

      $scope.bmpConfig = {
        membersPasswordsMode: 'constitution',
        selectedBoard: selectedElectoralBoard,
        selectedBoardMembers: selectedElectoralBoard.boardMembers,
        boardType: 'electoral'
      }

      // Members passwords modal display
      $mdDialog.show({
        controller: 'boardMembersPasswordsController',
        controllerAs: 'ctrl',
        template: boardMembersPasswordsTemplate,
        parent: angular.element(document.body),
        clickOutsideToClose: false,
        escapeToClose: false,
        preserveScope: true,
        scope: $scope
      }).then(
        (membersPasswords) => {
          if (membersPasswords.length === selectedElectoralBoard.boardMembers.length) {
            const passwords = [];
            membersPasswords.forEach(pwd => passwords.push([...pwd]));

            const constituteEndpoint = (endpoints.host() + endpoints.electoralBoardConstitute)
              .replace(electionEventIdPattern, $scope.selectedElectionEventId)
              .replace(electoralBoardIdPattern, selectedElectoralBoard.id);

            $scope.isConstitutionInProgress = true;

            $http.post(constituteEndpoint, passwords).then(constituteSuccess, constituteError);
          }
        },
        () => null
      );
    };

    function constituteSuccess() {
      $scope.listElectoralBoards();

      $scope.isConstitutionInProgress = false;

      $mdToast.show(
        toastCustom.topCenter(
          getText(ELECTORAL_BOARD_CONSTITUTE_SUCCESS_MSG_ID),
          'success',
        ),
      );
    }

    function constituteError() {
      $scope.isConstitutionInProgress = false;

      new CustomDialog()
        .title(getText(ELECTORAL_BOARD_CONSTITUTE_MSG_ID))
        .error()
        .show();
    }

    $scope.isBoardReadyToConstitute = function() {
      const selectedElectoralBoard = getSelectedElectoralBoard();
      return selectedElectoralBoard.status === 'LOCKED';
    }

    $scope.isBoardReadyToSign = function() {
      const selectedElectoralBoard = getSelectedElectoralBoard();
      return selectedElectoralBoard.status === 'READY';
    }

    $scope.isSetupMode = function () {
      return sessionService.isSetupMode();
    };

    /***
     * Electoral Board signing
     */
    $scope.signElectoralBoard = function () {
      const selectedElectoralBoard = getSelectedElectoralBoard();
      // Only perform signing if entity is READY
      if (selectedElectoralBoard.status !== 'READY') {
        new CustomDialog()
          .title(getText(ELECTORAL_BOARD_SIGNING_MSG_ID))
          .cannotPerform(getText(ELECTORAL_BOARD_MSG_ID))
          .show();
        return;
      }

      if (!adminBoardActivationService.isActivated()) {
        const p = $mdDialog.show(
          $mdDialog.customConfirm({
            locals: {
              title: getText(ELECTORAL_BOARD_SIGNING_MSG_ID),
              content: getText(ADMIN_BOARD_ACTIVATE_MSG_ID),
              ok: getText(ADMIN_BOARD_ACTIVATE_OK_MSG_ID),
            },
          }),
        );
        p.then(
          () => adminBoardActivationService.activate($scope, 'setup').then(() => $scope.signElectoralBoard()),
          function (error) {
            //Not possible to open the $mdDialog
          }
        );
        return;
      } else {
        const electionEvent = sessionService.getSelectedElectionEvent();
        if (!sessionService.doesActivatedABBelongToSelectedEE()) {
          $mdDialog.show(
            $mdDialog.customAlert({
              locals: {
                title: getText(ADMIN_BOARD_WRONG_ACTIVATED_MSG_ID),
                content: `${getText(ADMIN_BOARD_WRONG_ACTIVATED_EXPLANATION_MSG_ID)} ${electionEvent.defaultTitle}.`
              },
            }),
          );
          return;
        }
      }

      const url = (endpoints.host() + endpoints.electoralBoardSign)
        .replace(electionEventIdPattern, $scope.selectedElectionEventId)
        .replace(electoralBoardIdPattern, selectedElectoralBoard.id);

      $scope.isSigningInProgress = true;

      $http.put(url).then(
        function () {
          $rootScope.safeApply(function () {
            $scope.isSigningInProgress = false;
            $mdToast.show(
              toastCustom.topCenter(
                getText(ELECTORAL_BOARD_SIGNED_MSG_ID),
                'success',
              ),
            );
            $scope.listElectoralBoards();
          });
        },
        function () {
          $scope.isSigningInProgress = false;
          showSigningError();
        },
      );
    };

    $scope.capitalizeFirstLetter = function (string) {
      const lowerCaseString = string.toLowerCase();
      return lowerCaseString.charAt(0).toUpperCase() + lowerCaseString.slice(1);
    };


    function showSigningError() {
      new CustomDialog()
        .title(ELECTORAL_BOARD_SIGNING_MSG_ID)
        .error()
        .show();
    }

    function getSelectedElectoralBoard() {
      return $scope.electoralBoardsList[0];
    }

    function getText(textId) {
      return gettextCatalog.getString(textId);
    }

  });
