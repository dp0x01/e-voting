/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

import dialogCustomProgressTemplateTemplate from '../dialogs/dialog-custom-progress-template.html';

export default angular
  .module('election-event-list', [
    'ui.router',
    'app.sessionService',
    'endpoints',
  ])
  .controller('election-event-list', function (
    $scope,
    $rootScope,
    sessionService,
    endpoints,
    $http,
    $state,
    $mdDialog,
    $q,
    $mdToast,
    toastCustom,
    CustomDialog,
    gettextCatalog,
    ErrorsDict
  ) {
    const ELECTION_EVENT_MSG_ID = 'Election Event';
    const REQUEST_CC_KEYS_MSG_ID = 'Control Components keys';
    const REQUEST_CC_KEYS_REQUESTED_MSG_ID = 'Control Components keys requested...';
    const REQUEST_CC_KEYS_NOT_EXIST_MSG_ID = 'Control Components keys do not exist. Please request them first';
    const REQUEST_CC_KEYS_EXIST_MSG_ID = 'Control Components keys already exist. Please secure the election event';
    const REQUEST_CC_KEYS_PROGRESS_MSG_ID = 'Control Components keys request for this election event in progress';
    const REQUEST_CC_KEYS_SUCCESS_MSG_ID = 'Control Components keys successfully requested';
    const ELECTION_EVENT_SECURE_MSG_ID = 'Secure election event';
    const ELECTION_EVENT_SECURE_STARTED_MSG_ID = 'Election Event securization started...';
    const ELECTION_EVENT_SECURE_PROGRESS_MSG_ID = 'Securization of this election event is in progress';
    const ELECTION_EVENT_SECURED_MSG_ID = 'Election Event secured';
    const ELECTION_EVENT_SELECT_FIRST_MSG_ID = 'Please select first an Election event with a constituted Administration board';
    const ELECTION_EVENT_WITH_CONSTITUTED_AB_MSG_ID = 'Only election events with a constituted administration board can be secured';
    const ELECTION_EVENT_VIEW_MSG_ID = 'View Election Event(s)';
    const ELECTION_EVENT_VIEW_EXPLANATION_MSG_ID = 'To manage election events you have to secure them first. To do this, select election' +
      ' events from the list and click on the "SECURE" action button above.';
    const EXPORTING_MSG_ID = 'Exporting...';
    const EXPORT_MSG_ID = 'Export';
    const EXPORT_SUCCESS_MSG_ID = 'Data exported successfully';
    const EXPORT_ERROR_MSG_ID = 'Data has not been exported';
    const CONTACT_SUPPORT_MSG_ID = 'Something went wrong. Contact with Support';

    const electionEventIdPattern = '{electionEventId}';

    const progressValue = {};

    $scope.data = {};
    $scope.isRequestCCKeysInProgress = false;
    $scope.isSecureInProgress = false;

    $scope.selectEEText = function () {
      return gettextCatalog.getString(ELECTION_EVENT_SELECT_FIRST_MSG_ID);
    };
    $scope.listElectionEvents = function () {
      $scope.errors = {};

      $http.get(endpoints.host() + endpoints.electionEvents).then(
        function (response) {
          const data = response.data;
          $scope.selectedElectionEvent = null;
          try {
            $scope.electionEvents = data;
          } catch (e) {
            $scope.data.message = e.message;
            $scope.errors.electionEventsFailed = true;
          }
        },
        function () {
          $scope.errors.electionEventsFailed = true;
        },
      );
    };

    $scope.isABConstituted = function (adminBoardId) {
      let result;
      sessionService.getAdminBoards().forEach(function (adminBoard) {
        if (adminBoard.id === adminBoardId) {
          result = adminBoard.status === 'CONSTITUTED';
        }
      });
      return result;
    };

    $scope.isRequestCCKeysEnabled = function () {
      return sessionService.isOnlineMode();
    };

    $scope.isSecureEnabled = function () {
      return sessionService.isSetupMode();
    };

    /**
     * Requests the Control Components keys for the selected election event.
     */
    $scope.requestCCKeys = function () {
      // Only available if election event status is LOCKED
      if ($scope.selectedElectionEvent.status !== 'LOCKED') {
        new CustomDialog()
          .title(gettextCatalog.getString(REQUEST_CC_KEYS_MSG_ID))
          .cannotPerform(gettextCatalog.getString(ELECTION_EVENT_MSG_ID))
          .show();
        return;
      }

      $scope.errors = {};

      // Must have an election event selected
      if ($scope.selectedElectionEvent) {
        if (!$scope.progressContains($scope.selectedElectionEvent.id)) {
          $scope.progressInit($scope.selectedElectionEvent.id);
          $mdToast.show(
            toastCustom.topCenter(gettextCatalog.getString(REQUEST_CC_KEYS_REQUESTED_MSG_ID), 'success')
          );

          $scope.isRequestCCKeysInProgress = true;

          // Prepare request keys url
          const requestKeysUrl = endpoints.host() + endpoints.requestKeys.replace('{electionEventId}', $scope.selectedElectionEvent.id);
          // Post call
          $http.post(requestKeysUrl).then(
            // Success handler
            () => {
              $scope.progressFinish($scope.selectedElectionEvent.id);
              $mdToast.show(
                toastCustom.topCenter(gettextCatalog.getString(REQUEST_CC_KEYS_SUCCESS_MSG_ID), 'success')
              );
              $scope.isRequestCCKeysInProgress = false;
            },
            // Error handler
            (response) => requestCCKeysErrorHandler(response)
          );
        } else {
          // Already in progress
          $mdDialog.show($mdDialog.customAlert({
            locals: {
              title: gettextCatalog.getString(REQUEST_CC_KEYS_MSG_ID),
              content: gettextCatalog.getString(REQUEST_CC_KEYS_PROGRESS_MSG_ID)
            }
          }));
        }
      }
    };

    const requestCCKeysErrorHandler = function (response) {
      $scope.progressFinish($scope.selectedElectionEvent.id);
      if (response.status === 400 && response.data.error && response.data.error.code === 'KEYS_ALREADY_EXIST') {
        $mdDialog.show($mdDialog.customAlert({
          locals: {
            title: gettextCatalog.getString(REQUEST_CC_KEYS_MSG_ID),
            content: gettextCatalog.getString(REQUEST_CC_KEYS_EXIST_MSG_ID)
          }
        }));
      } else {
        new CustomDialog()
          .title(gettextCatalog.getString(REQUEST_CC_KEYS_MSG_ID))
          .error()
          .show();
      }
      $scope.isRequestCCKeysInProgress = false;
    };

    /**
     * Launches the endpoint responsible for generating the configuration of a given election event
     */
    $scope.secureElectionEvent = function () {
      if ($scope.selectedElectionEvent.status !== 'LOCKED') {
        new CustomDialog()
          .title(gettextCatalog.getString(ELECTION_EVENT_SECURE_MSG_ID))
          .cannotPerform(gettextCatalog.getString(ELECTION_EVENT_MSG_ID))
          .show();
        return;
      }

      if (
        !$scope.isABConstituted(
          $scope.selectedElectionEvent.administrationBoard.id,
        )
      ) {
        $mdDialog.show(
          $mdDialog.customAlert({
            locals: {
              title: gettextCatalog.getString(ELECTION_EVENT_SECURE_MSG_ID),
              content: gettextCatalog.getString(ELECTION_EVENT_WITH_CONSTITUTED_AB_MSG_ID),
            },
          })
        );
        return;
      }

      $scope.errors = {};

      if ($scope.selectedElectionEvent) {
        if (!$scope.progressContains($scope.selectedElectionEvent.id)) {
          $scope.progressInit($scope.selectedElectionEvent.id);

          $scope.isSecureInProgress = true;

          try {
            const url = endpoints.electionEvent.replace(
              '{electionEventId}',
              $scope.selectedElectionEvent.id,
            );
            $mdToast.show(
              toastCustom.topCenter(
                gettextCatalog.getString(ELECTION_EVENT_SECURE_STARTED_MSG_ID),
                'success',
              ),
            );
            $http.post(endpoints.host() + url).then(
              function () {
                $http.get(endpoints.host() + url).then(function () {
                  $scope.progressFinish($scope.selectedElectionEvent.id);
                  $mdToast.show(
                    toastCustom.topCenter(
                      gettextCatalog.getString(ELECTION_EVENT_SECURED_MSG_ID),
                      'success'
                    )
                  );
                  $rootScope.$broadcast('refresh-election-events');
                });
                $scope.isSecureInProgress = false;
              },
              function (response) {
                $scope.progressFinish($scope.selectedElectionEvent.id);
                if (response.status === 400 && response.data.error && response.data.error.code === 'KEYS_NOT_EXIST') {
                  $mdDialog.show($mdDialog.customAlert({
                    locals: {
                      title: gettextCatalog.getString(ELECTION_EVENT_SECURE_MSG_ID),
                      content: gettextCatalog.getString(REQUEST_CC_KEYS_NOT_EXIST_MSG_ID)
                    }
                  }));
                } else {
                  new CustomDialog()
                    .title(gettextCatalog.getString(ELECTION_EVENT_SECURE_MSG_ID))
                    .error()
                    .show();
                }
                $scope.isSecureInProgress = false;
              }
            );
          } catch (e) {
            $scope.progressFinish($scope.selectedElectionEvent.id);
            $scope.data.message = e;
            new CustomDialog()
              .title(gettextCatalog.getString(ELECTION_EVENT_SECURE_MSG_ID))
              .error()
              .show();
          }
        } else {
          $mdDialog.show(
            $mdDialog.customAlert({
              locals: {
                title: gettextCatalog.getString(ELECTION_EVENT_SECURE_MSG_ID),
                content: gettextCatalog.getString(ELECTION_EVENT_SECURE_PROGRESS_MSG_ID),
              },
            })
          );
        }
      }
    };

    $scope.goToElectionEvent = function (electionEvent) {
      $scope.errors = {};
      if (electionEvent.status === 'READY') {
        // Get electoralBoards
        const electoralBoardsURL =
          endpoints.host() +
          endpoints.electoralBoardList.replace(electionEventIdPattern, electionEvent.id);

        $http.get(electoralBoardsURL).then(
          function (response) {
            sessionService.setElectoralBoards(response.data.result);
            sessionService.setSelectedElectionEvent(electionEvent);
            $state.go('ballots');
          }
        );

      } else {
        $mdDialog.show(
          $mdDialog.customAlert({
            locals: {
              title: gettextCatalog.getString(ELECTION_EVENT_VIEW_MSG_ID),
              content: gettextCatalog.getString(ELECTION_EVENT_VIEW_EXPLANATION_MSG_ID),
            },
          })
        );

      }
    };

    $scope.uniqueChoice = function (electionEvents, electionEvent) {
      $scope.errors = {};

      let selectedEE = null;

      if (electionEvent.chosen) {
        electionEvents.forEach(function (o) {
          if (o.id !== electionEvent.id) {
            o.chosen = false;
          } else {
            selectedEE = o;
          }
        });
      }

      if (electionEvent.chosen) {
        $scope.selectedElectionEvent = selectedEE;
      } else {
        $scope.selectedElectionEvent = null;
      }

      sessionService.setSelectedElectionEvent(selectedEE);
    };

    $scope.checkSelected = function () {
      return (
        !$scope.selectedElectionEvent ||
        $scope.selectedElectionEvent.status !== 'LOCKED'
      );
    };

    $scope.getTextIsEESelected = function () {
      const check = $scope.checkSelected();
      return check ? $scope.selectEEText : '';
    };

    $scope.getAdminBoardTitle = function (adminBoardId) {
      let adminBoardTitle = '';
      (sessionService.getAdminBoards() || []).forEach(function (adminBoard) {
        if (adminBoard.id === adminBoardId) {
          adminBoardTitle = adminBoard.defaultTitle;
        }
      });

      return adminBoardTitle;
    };

    //Not used now, but will be used as other US defines
    $scope.isNotReadyToNavigate = function (electionEvent) {
      return electionEvent.status !== 'READY';
    };

    $scope.capitalizeFirstLetter = function (string) {
      const lowerCaseString = string.toLowerCase();
      return (
        lowerCaseString.charAt(0).toUpperCase() + lowerCaseString.slice(1)
      );
    };

    $scope.checkOneSelected = function () {
      return !$scope.selectedElectionEvent;
    };

    const exportConfigFunction = function () {
      const url =
        endpoints.host() +
        endpoints.export.replace(
          '{electionEventId}',
          $scope.selectedElectionEvent.id
        );

      $scope.title = gettextCatalog.getString(EXPORTING_MSG_ID);

      $mdDialog.show({
        scope: $scope,
        preserveScope: true,
        template: dialogCustomProgressTemplateTemplate,
        escapeToClose: false,
        parent: angular.element(document.body),
      });

      $http
        .get(url, {responseType: 'blob'})
        .then(function (res) {
          $mdDialog.hide();
          $mdToast.show(
            toastCustom.topCenter(
              gettextCatalog.getString(EXPORT_SUCCESS_MSG_ID),
              'success'
            )
          );

          const saveData = (function () {
            const a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            return function (data, filename) {
              const blob = new Blob([data], {type: "application/octet-stream"}),
                blobUrl = window.URL.createObjectURL(blob);
              a.href = blobUrl;
              a.download = filename;
              a.click();
              window.URL.revokeObjectURL(blobUrl);
            };
          }());

          let fileName = "export.sdm";
          const contentDispositionHeader = res.headers('content-Disposition');
          const index = contentDispositionHeader.indexOf('filename=');
          if (index !== -1) {
            fileName = contentDispositionHeader.substring(index + 9);
          }
          saveData(res.data, fileName);
        })
        .catch(function (e) {
          $mdDialog.hide();
          $mdToast.show(
            toastCustom.topCenter(
              `${gettextCatalog.getString(EXPORT_MSG_ID)}: ${gettextCatalog.getString(CONTACT_SUPPORT_MSG_ID)}`, 'error',));
          if (e.status === 400) {
            $mdDialog.show(
              $mdDialog.customAlert({
                locals: {
                  title: gettextCatalog.getString(EXPORT_ERROR_MSG_ID),
                  content: gettextCatalog.getString(ErrorsDict(4009))
                }
              })
            );
          } else {
            $mdToast.show(
              toastCustom.topCenter(
                `${gettextCatalog.getString(EXPORT_MSG_ID)}: ${gettextCatalog.getString(CONTACT_SUPPORT_MSG_ID)}`,
                'error'
              )
            );
          }
        });
    };

    $scope.exportConfig = function () {
      exportConfigFunction();
    };

    $scope.progressContains = function (id) {
      return progressValue[id];
    };

    $scope.progressInit = function (id) {
      progressValue[id] = true;
    };

    $scope.progressFinish = function (id) {
      delete progressValue[id];
    };

    $scope.listElectionEvents();

    $scope.$on('refresh-election-events', function () {
      $scope.listElectionEvents();
    });


  });
