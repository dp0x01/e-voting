/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
/* jshint maxlen: 666 */
/* global process */

angular
	.module('endpoints', [])

	.factory('endpoints', function () {

		function isElectron() {
			'use strict';
			const userAgent = navigator.userAgent.toLowerCase();
			return userAgent.indexOf(' electron/') > -1;
		}

		function host() {
			'use strict';
			return isElectron() ? 'http://localhost:8090/sdm-backend/' : '/sdm-backend/';
		}

		return {
			host: host,
			status: 'status',
			close: 'close',
			sdmConfig: 'sdm-config',
			electionEvents: 'electionevents',

			// Admin Board
			adminBoardList: 'adminboard-management/adminboards', // GET
			adminBoardConstitution: 'setup/adminboard-management/adminboards/{adminBoardId}', // POST
			adminBoardActivationSetup: 'setup/adminboard-management/adminboards/{adminBoardId}', // PUT
			adminBoardActivationTally: 'tally/adminboard-management/adminboards/{adminBoardId}', // PUT
			adminBoardPasswordValidateSetup: 'setup/adminboardpasswords/adminboards/{adminBoardId}', // PUT
			adminBoardPasswordValidateTally: 'tally/adminboardpasswords/adminboards/{adminBoardId}', // PUT

			electionEvent: 'electionevents/{electionEventId}',
			requestKeys: 'electionevents/{electionEventId}/keys',
			preconfiguration: 'preconfiguration',
			ballots: 'ballots/electionevent/{electionEventId}',
			ballottexts:
				'ballottexts/electionevent/{electionEventId}/ballottext/{ballotId}',
			ballotUpdate: 'ballots/electionevent/{electionEventId}/ballot/{ballotId}', // PUT
			votingCardSets: 'votingcardsets/electionevent/{electionEventId}',
			votingCardSetsPrecompute:
				'setup/votingcardsets/electionevent/{electionEventId}/precompute',
			votingCardSetCompute:
				'online/votingcardsets/electionevent/{electionEventId}/votingcardset/{votingCardSetId}/compute',
			votingCardSetComputationStatus:
				'online/votingcardsets/electionevent/{electionEventId}/status',
			votingCardSetDownload:
				'online/votingcardsets/electionevent/{electionEventId}/votingcardset/{votingCardSetId}/download',
			votingCardSetsSign:
				'setup/votingcardsets/electionevent/{electionEventId}/sign', // PUT
			votingCardSet:
				'votingcardsets/electionevent/{electionEventId}/votingcardset/{votingCardSetId}',
			electoralBoardList: 'electoralboard-management/electionevents/{electionEventId}/electoralboards',
			electoralBoardSign: 'electoralboard-management/electionevents/{electionEventId}/electoralboards/{electoralBoardId}', // PUT
			electoralBoardConstitute: 'electoralboard-management/electionevents/{electionEventId}/electoralboards/{electoralBoardId}', // POST
			electoralBoardPasswordValidate: 'electoralboardpasswords/electionevents/{electionEventId}/electoralboards/{electoralBoardId}', // PUT
			ballotboxes: 'ballotboxes/electionevent/{electionEventId}',
			ballotBoxSign: 'ballotboxes/electionevent/{electionEventId}/ballotbox/{ballotBoxId}', // PUT
			mixingOnline: 'mixing/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/mixonline',
			updateMixingStatus: 'mixing/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/updateStatus',
			downloadBallotBox: 'mixing/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/download',
			mixingOffline: 'mixing/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/mixoffline',
			uploadConfigurations: 'configurations/electionevent/{electionEventId}',
			export: 'operation/export/{electionEventId}',
			import: 'operation/import',
			generateTallyFiles:
				'operation/electionevent/{electionEventId}/tally-files/generate',
			generatePrintFiles:
				'operation/electionevent/{electionEventId}/print-files/generate',
			languages: 'sdm-config/langs/'
		};
	});
