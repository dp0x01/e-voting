/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

import splashTemplate from './views/splash/splash.html';
import homeManageTemplate from './views/home-manage/home-manage.html';
import electionEventListTemplate from './views/election-event-list/election-event-list.html';
import administrationBoardListTemplate from './views/administration-board-list/administration-board-list.html';
import electionEventManageTemplate from './views/election-event-manage/election-event-manage.html';
import ballotsTemplate from './views/ballots/ballots.html';
import votingCardsTemplate from './views/voting-cards/voting-cards.html';
import electoralBoardListTemplate from './views/electoral-board-list/electoral-board-list.html';
import ballotBoxesTemplate from './views/ballot-boxes/ballot-boxes.html';

angular
    .module('routes', ['ui.router'])
    .config(function ($urlRouterProvider, $stateProvider, $locationProvider) {
        'use strict';

        // Keep the URL without prefix '!'
        $locationProvider.hashPrefix('');

        $urlRouterProvider.otherwise('/splash');

        $stateProvider

            .state('body', {
                abstract: true,
            })

            .state('splash', {
                parent: 'body',
                url: '/splash',
                views: {
                    'view@body': {
                        template: splashTemplate,
                        controller: 'splash',
                    },
                },
                resolve: {
                    lazyLoad: function($ocLazyLoad) {
                        return import('./views/splash/splash')
                            .then(module => $ocLazyLoad.load({name: module.default.name}));
                    }
                },
            })

            .state('home-manage', { // NOSONAR Rule javascript:S1192 - False positive
                parent: 'body',
                views: {
                    'view@body': {
                        template: homeManageTemplate,
                        controller: 'home-manage', // NOSONAR Rule javascript:S1192 - False positive
                    },
                },
                resolve: {
                    lazyLoad: function($ocLazyLoad) {
                        return import('./views/home-manage/home-manage')
                            .then(module => $ocLazyLoad.load({name: module.default.name}));
                    }
                },
            })

            // home-manage - election-event-list
            .state('election-event-list', {
                parent: 'home-manage', // NOSONAR Rule javascript:S1192 - False positive
                url: '/election-event-list',
                views: {
                    'election-event-list': {
                        template: electionEventListTemplate,
                        controller: 'election-event-list',
                    },
                },
                resolve: {
                    lazyLoad: function($ocLazyLoad) {
                        return import('./views/election-event-list/election-event-list')
                            .then(module => $ocLazyLoad.load({name: module.default.name}));
                    }
                },
            })

            // home-manage - administration-board-list
            .state('administration-board-list', {
                parent: 'home-manage', // NOSONAR Rule javascript:S1192 - False positive
                url: '/administration-board-list',
                views: {
                    'administration-board-list': {
                        template: administrationBoardListTemplate,
                        controller: 'administration-board-list',
                    },
                },
                resolve: {
                    lazyLoad: function($ocLazyLoad) {
                        return import('./views/administration-board-list/administration-board-list')
                            .then(module => $ocLazyLoad.load({name: module.default.name}));
                    }
                },
            })

            // election-event-manage ========================================
            // ==============================================================
            .state('election-event-manage', { // NOSONAR Rule javascript:S1192 - False positive
                parent: 'body',
                url: '/election-event-manage',
                views: {
                    'view@body': {
                        template: electionEventManageTemplate,
                        controller: 'election-event-manage', // NOSONAR Rule javascript:S1192 - False positive
                    },
                },
                resolve: {
                    lazyLoad: function($ocLazyLoad) {
                        return import('./views/election-event-manage/election-event-manage')
                            .then(module => $ocLazyLoad.load({name: module.default.name}));
                    }
                },
            })

            // election-event-manage - ballots
            .state('ballots', {
                parent: 'election-event-manage', // NOSONAR Rule javascript:S1192 - False positive
                url: '/ballots',
                views: {
                    ballots: {
                        template: ballotsTemplate,
                        controller: 'ballots',
                    },
                },
                resolve: {
                    lazyLoad: function($ocLazyLoad) {
                        return import('./views/ballots/ballots')
                            .then(module => $ocLazyLoad.load({name: module.default.name}));
                    }
                },
            })

            // election-event-manage - voting-cards
            .state('voting-cards', {
                parent: 'election-event-manage', // NOSONAR Rule javascript:S1192 - False positive
                url: '/voting-cards',
                views: {
                    'voting-cards': {
                        template: votingCardsTemplate,
                        controller: 'voting-cards',
                    },
                },
                resolve: {
                    lazyLoad: function($ocLazyLoad) {
                        return import('./views/voting-cards/voting-cards')
                            .then(module => $ocLazyLoad.load({name: module.default.name}));
                    }
                },
            })

            // election-event-manage - electoral-board-list
            .state('electoral-board-list', {
                parent: 'election-event-manage', // NOSONAR Rule javascript:S1192 - False positive
                url: '/electoral-board-list',
                views: {
                    'electoral-board-list': {
                        template: electoralBoardListTemplate,
                        controller: 'electoral-board-list',
                    },
                },
                resolve: {
                    lazyLoad: function($ocLazyLoad) {
                        return import('./views/electoral-board-list/electoral-board-list')
                            .then(module => $ocLazyLoad.load({name: module.default.name}));
                    }
                },
            })

            // election-event-manage - ballots-boxes
            .state('ballot-boxes', {
                parent: 'election-event-manage', // NOSONAR Rule javascript:S1192 - False positive
                url: '/ballot-boxes',
                views: {
                    'ballot-boxes': {
                        template: ballotBoxesTemplate,
                        controller: 'ballot-boxes',
                    },
                },
                resolve: {
                    lazyLoad: function($ocLazyLoad) {
                        return import('./views/ballot-boxes/ballot-boxes')
                            .then(module => $ocLazyLoad.load({name: module.default.name}));
                    }
                },
            });
    });
