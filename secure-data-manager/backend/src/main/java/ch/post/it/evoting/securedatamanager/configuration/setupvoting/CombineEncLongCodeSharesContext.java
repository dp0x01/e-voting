/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Regroups the context values needed by the CombineEncLongCodeShares algorithm.
 *
 * <ul>
 *     <li>encryptionGroup, the encryption group. Must be non-null.</li>
 *     <li>ee, the election event ID. Must be non-null and a valid UUID.</li>
 *     <li>vcs, the verification card set ID. Must be non-null and a valid UUID.</li>
 *     <li>sk<sub>setup</sub>, the setup secret key. Must be non-null.</li>
 *     <li>n, the number of voting options for the verification card set. Must be strictly positive.</li>
 *     <li>N<sub>E</sub>, the number of eligible voters for the verification card set. Must be strictly positive.</li>
 * </ul>
 */
public class CombineEncLongCodeSharesContext {

	private final GqGroup encryptionGroup;
	private final String electionEventId;
	private final String verificationCardSetId;
	private final ElGamalMultiRecipientPrivateKey setupSecretKey;
	private final int numberOfVotingOptions;
	private final int numberOfEligibleVoters;

	private CombineEncLongCodeSharesContext(final GqGroup encryptionGroup, final String electionEventId, final String verificationCardSetId,
			final ElGamalMultiRecipientPrivateKey setupSecretKey, final int numberOfVotingOptions, final int numberOfEligibleVoters) {
		this.encryptionGroup = encryptionGroup;
		this.electionEventId = electionEventId;
		this.verificationCardSetId = verificationCardSetId;
		this.numberOfVotingOptions = numberOfVotingOptions;
		this.numberOfEligibleVoters = numberOfEligibleVoters;
		this.setupSecretKey = setupSecretKey;
	}

	public GqGroup getEncryptionGroup() {
		return encryptionGroup;
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public String getVerificationCardSetId() {
		return verificationCardSetId;
	}

	public ElGamalMultiRecipientPrivateKey getSetupSecretKey() {
		return setupSecretKey;
	}

	public int getNumberOfVotingOptions() {
		return numberOfVotingOptions;
	}

	public int getNumberOfEligibleVoters() {
		return numberOfEligibleVoters;
	}

	/**
	 * Builder performing input validations before constructing a {@link CombineEncLongCodeSharesContext}.
	 */
	public static class Builder {

		private GqGroup encryptionGroup;
		private String electionEventId;
		private String verificationCardSetId;
		private ElGamalMultiRecipientPrivateKey setupSecretKey;
		private int numberOfVotingOptions;
		private int numberOfEligibleVoters;

		public Builder setEncryptionGroup(final GqGroup encryptionGroup) {
			this.encryptionGroup = encryptionGroup;
			return this;
		}

		public Builder setElectionEventId(final String electionEventId) {
			this.electionEventId = electionEventId;
			return this;
		}

		public Builder setVerificationCardSetId(final String verificationCardSetId) {
			this.verificationCardSetId = verificationCardSetId;
			return this;
		}

		public Builder setSetupSecretKey(final ElGamalMultiRecipientPrivateKey setupSecretKey) {
			this.setupSecretKey = setupSecretKey;
			return this;
		}

		public Builder setNumberOfVotingOptions(final int numberOfVotingOptions) {
			this.numberOfVotingOptions = numberOfVotingOptions;
			return this;
		}

		public Builder setNumberOfEligibleVoters(final int numberOfEligibleVoters) {
			this.numberOfEligibleVoters = numberOfEligibleVoters;
			return this;
		}

		/**
		 * Creates the CombineEncLongCodeSharesContext. All fields must have been set and be non-null.
		 *
		 * @return a new CombineEncLongCodeSharesContext.
		 * @throws NullPointerException      if any of the fields is null.
		 * @throws FailedValidationException if {@code electionEventId} or {@code verificationCardSetId} are invalid UUID.
		 */
		public CombineEncLongCodeSharesContext build() {

			checkNotNull(encryptionGroup);
			validateUUID(electionEventId);
			validateUUID(verificationCardSetId);
			checkNotNull(setupSecretKey);

			checkArgument(setupSecretKey.getGroup().hasSameOrderAs(encryptionGroup),
					"The setup secret key must have the same order as the encryption group.");
			checkArgument(numberOfVotingOptions > 0, "The number of voting options must be strictly positive.");
			checkArgument(numberOfEligibleVoters > 0, "The number of eligible voters must be strictly positive.");

			return new CombineEncLongCodeSharesContext(encryptionGroup, electionEventId, verificationCardSetId, setupSecretKey, numberOfVotingOptions,
					numberOfEligibleVoters);
		}
	}
}
