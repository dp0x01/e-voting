/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static com.google.common.base.Preconditions.checkNotNull;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.google.common.primitives.Bytes;

import ch.post.it.evoting.cryptoprimitives.hashing.Argon2;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Hash;

@Service
public class BoardPasswordHashService {

	private final Argon2 argon2;

	public BoardPasswordHashService(
			@Qualifier("argon2Standard")
			final Argon2 argon2) {
		this.argon2 = argon2;
	}

	/**
	 * Hashes the passwords of the board members.
	 *
	 * @param boardMembersPasswords the passwords of the board members. Must be a valid BoardPassword and non-null.
	 * @return the hashes of the passwords.
	 * @throws NullPointerException     if the passwords are null.
	 * @throws IllegalArgumentException if any password is invalid.
	 */
	public List<byte[]> hashPasswords(final List<char[]> boardMembersPasswords) {
		checkNotNull(boardMembersPasswords);
		boardMembersPasswords.forEach(BoardPasswordValidation::validate);

		return boardMembersPasswords.stream().parallel()
				.map(this::hashPassword)
				.map(byte[]::clone)
				.toList();
	}

	/**
	 * Verifies that member password corresponds to the member password hash.
	 *
	 * @param boardMemberPassword  the password of the board member. Must be a valid board password and non-null.
	 * @param boardMemberArgonHash the password hash of the board member. Must be non-null.
	 * @return true if password corresponds to the password hash.
	 * @throws NullPointerException     if password or password hash is null.
	 * @throws IllegalArgumentException if password is invalid.
	 */
	public boolean verifyPassword(final char[] boardMemberPassword, final byte[] boardMemberArgonHash) {
		BoardPasswordValidation.validate(boardMemberPassword);
		checkNotNull(boardMemberArgonHash);
		final byte[] boardMemberHashTag = new byte[32];
		final byte[] boardMemberHashSalt = new byte[16];

		System.arraycopy(boardMemberArgonHash, 0, boardMemberHashTag, 0, 32);
		System.arraycopy(boardMemberArgonHash, 32, boardMemberHashSalt, 0, 16);

		final byte[] boardMemberHash = argon2.getArgon2id(toBytes(boardMemberPassword), boardMemberHashSalt);

		return Arrays.equals(boardMemberHashTag, boardMemberHash);
	}

	/**
	 * Hashes a password using the Argon2id algorithm.
	 *
	 * @param password the password of a board member. Must be non-null.
	 * @return the hash of the board member's password.
	 */
	private byte[] hashPassword(final char[] password) {
		checkNotNull(password);
		final Argon2Hash hash = argon2.genArgon2id(toBytes(password));
		return Bytes.concat(hash.getTag(), hash.getSalt());
	}

	private byte[] toBytes(final char[] chars) {
		final CharBuffer charBuffer = CharBuffer.wrap(chars);
		final ByteBuffer byteBuffer = StandardCharsets.UTF_8.encode(charBuffer);
		final byte[] bytes = Arrays.copyOfRange(byteBuffer.array(),
				byteBuffer.position(), byteBuffer.limit());
		Arrays.fill(byteBuffer.array(), (byte) 0); // clear sensitive data
		return bytes;

	}
}
