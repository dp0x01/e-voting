/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.domain.model.votingcardset;

public record VotingCardSetsData(String adminBoardId) {
}
