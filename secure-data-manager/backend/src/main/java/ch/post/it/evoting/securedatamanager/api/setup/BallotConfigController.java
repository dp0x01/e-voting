/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api.setup;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.BallotConfigService;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/sdm-backend/ballots")
@Api(value = "Ballot REST API")
@ConditionalOnProperty("role.isSetup")
public class BallotConfigController {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotConfigController.class);

	private final BallotConfigService ballotConfigService;

	public BallotConfigController(final BallotConfigService ballotConfigService) {
		this.ballotConfigService = ballotConfigService;
	}

	/**
	 * Change the state of the ballot from locked to updated for a given election event and ballot id.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @param ballotId        the ballot id. Must be non-null and a valid UUID.
	 * @return HTTP status code 200 - If the ballot is successfully signed. HTTP status code 404 - If the resource is not found. HTTP status code 412
	 * - If the ballot is already updated.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if any input is not a valid UUID.
	 */
	@PutMapping(value = "/electionevent/{electionEventId}/ballot/{ballotId}")
	@ApiOperation(value = "Update ballot", notes = "Service to change the state of the ballot from locked to updated "
			+ "for a given election event and ballot id..", response = Void.class)
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 412, message = "Precondition Failed") })
	public ResponseEntity<Void> updateBallot(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String ballotId
	) {

		validateUUID(electionEventId);
		validateUUID(ballotId);

		try {

			ballotConfigService.updateBallot(electionEventId, ballotId);

		} catch (final ResourceNotFoundException e) {
			LOGGER.error("An error occurred while fetching the ballot to sign", e);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}
}
