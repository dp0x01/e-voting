/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.task;

import java.io.UncheckedIOException;
import java.util.Optional;
import java.util.concurrent.ExecutorService;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;

@Service
public class ExecutionTaskService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionTaskService.class);

	private final ObjectMapper objectMapper;
	private final ExecutorService executorService;
	private final ExecutionTaskRepository executionTaskRepository;

	public ExecutionTaskService(
			final ObjectMapper objectMapper,
			final ExecutorService executorService,
			final ExecutionTaskRepository executionTaskRepository) {
		this.objectMapper = objectMapper;
		this.executorService = executorService;
		this.executionTaskRepository = executionTaskRepository;
	}

	public void executeTask(final String businessId, final ExecutionTaskType type, final String description, final Runnable fn) {
		final ExecutionTask task = new ExecutionTask.Builder()
				.setType(type)
				.setBusinessId(businessId)
				.setDescription(description)
				.setStatus(ExecutionTaskStatus.STARTED)
				.build();

		// Check if task already exists
		final Optional<ExecutionTask> taskExists = findById(task.getId());
		if (taskExists.isPresent()) {
			LOGGER.warn("{} already exists. [taskId: {}]", task.getDescription(), task.getId());
			return;
		}

		// Save the new task
		save(task);

		// Submit task to executor
		executorService.submit(() -> taskExecution(fn, task));

		LOGGER.info("{} submitted to executor service. taskId: {}]", task.getDescription(), task.getId());
	}

	private void taskExecution(final Runnable fn, final ExecutionTask task) {
		// Execute the task function.
		try {
			fn.run();
			task.setStatus(ExecutionTaskStatus.COMPLETED);
		} catch (Exception e) {
			task.setStatus(ExecutionTaskStatus.FAILED);
			LOGGER.error("An error occurred during task execution.", e);
		}

		// Update task
		update(task);
		LOGGER.info("{} finished. [taskId: {}, taskStatus: {}]", task.getDescription(), task.getId(), task.getStatus());
	}

	public ExecutionTaskStatus getStatus(final String taskId) {
		// Check if task exists
		final Optional<ExecutionTask> taskExists = findById(taskId);
		if (taskExists.isPresent()) {
			return taskExists.get().getStatus();
		}
		return ExecutionTaskStatus.UNKNOWN;
	}

	private Optional<ExecutionTask> findById(final String entityId) {
		final String executionTaskJson = executionTaskRepository.find(entityId);
		if (StringUtils.isEmpty(executionTaskJson) || JsonConstants.EMPTY_OBJECT.equals(executionTaskJson)) {
			return Optional.empty();
		}

		try {
			return Optional.of(objectMapper.readValue(executionTaskJson, ExecutionTask.class));
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(e);
		}
	}

	private void save(final ExecutionTask executionTask) {
		try {
			final String executionTaskJson = objectMapper.writeValueAsString(executionTask);
			executionTaskRepository.save(executionTaskJson);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(e);
		}
	}

	private void update(final ExecutionTask executionTask) {
		try {
			final String executionTaskJson = objectMapper.writeValueAsString(executionTask);
			executionTaskRepository.update(executionTaskJson);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(e);
		}
	}

}
