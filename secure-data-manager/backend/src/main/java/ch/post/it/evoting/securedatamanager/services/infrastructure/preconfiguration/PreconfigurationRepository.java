/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration;

import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.orientechnologies.orient.core.storage.ORecordDuplicatedException;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.EncryptionParametersPayload;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.securedatamanager.EncryptionParametersPayloadService;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.commons.JsonUtils;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.CantonConfigConfigFileRepository;
import ch.post.it.evoting.securedatamanager.services.domain.model.EntityRepository;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;
import ch.post.it.evoting.securedatamanager.services.infrastructure.administrationboard.AdministrationBoardRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballot.BallotRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballotbox.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballottext.BallotTextRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electionevent.ElectionEventRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electoralboard.ElectoralBoardRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

/**
 * Saves the election configuration information to the corresponding repositories.
 */
@Repository
@ConditionalOnProperty("role.isSetup")
public class PreconfigurationRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(PreconfigurationRepository.class);

	private static final String EMPTY_SIGNATURE = "";
	private static final String ERROR_SAVING_DUPLICATED = "Error saving entity: {0}. It is duplicated.";

	private final ElectionEventRepository electionEventRepository;
	private final BallotBoxRepository ballotBoxRepository;
	private final BallotRepository ballotRepository;
	private final BallotTextRepository ballotTextRepository;
	private final VotingCardSetRepository votingCardSetRepository;
	private final ElectoralBoardRepository electoralBoardRepository;
	private final AdministrationBoardRepository administrationBoardRepository;
	private final ObjectReader jsonReader;
	private final CantonConfigConfigFileRepository cantonConfigConfigFileRepository;
	private final EncryptionParametersPayloadService encryptionParametersPayloadService;
	private final ObjectMapper objectMapper;
	private final PathResolver pathResolver;

	@Autowired
	public PreconfigurationRepository(final ElectionEventRepository electionEventRepository,
			final BallotBoxRepository ballotBoxRepository,
			final BallotRepository ballotRepository,
			final BallotTextRepository ballotTextRepository,
			final VotingCardSetRepository votingCardSetRepository,
			final ElectoralBoardRepository electoralBoardRepository,
			final AdministrationBoardRepository administrationBoardRepository,
			final ObjectReader jsonReader,
			final CantonConfigConfigFileRepository cantonConfigConfigFileRepository,
			final EncryptionParametersPayloadService encryptionParametersPayloadService,
			final ObjectMapper objectMapper,
			final PathResolver pathResolver) {
		this.electionEventRepository = electionEventRepository;
		this.ballotBoxRepository = ballotBoxRepository;
		this.ballotRepository = ballotRepository;
		this.ballotTextRepository = ballotTextRepository;
		this.votingCardSetRepository = votingCardSetRepository;
		this.electoralBoardRepository = electoralBoardRepository;
		this.administrationBoardRepository = administrationBoardRepository;
		this.jsonReader = jsonReader;
		this.cantonConfigConfigFileRepository = cantonConfigConfigFileRepository;
		this.encryptionParametersPayloadService = encryptionParametersPayloadService;
		this.objectMapper = objectMapper;
		this.pathResolver = pathResolver;
	}

	/**
	 * Creates the elections_config using the configuration-anonymized and saves it. Saves the customer input files: configuration-anonymized and
	 * encryptionParameters.
	 *
	 * @return the generated election event id.
	 */
	public String createElectionsConfig() throws IOException {
		LOGGER.debug("Creating the elections_config...");

		// Get input files (configuration-anonymized.xml and encryptionParameters.json)
		final Configuration configuration = cantonConfigConfigFileRepository.loadPreconfiguration()
				.orElseThrow(() -> new IllegalArgumentException(
						"A configuration-anonymized.xml file in sdm/input is needed for the election event pre-configuration."));
		checkState(configuration.getHeader().getVoterTotal() == configuration.getRegister().getVoter().size(),
				"The counted number of voters in the configuration-anonymized does not equal the voter total.");

		final EncryptionParametersPayload encryptionParametersPayload = encryptionParametersPayloadService.loadPreconfiguration();

		// Create and save elections_config
		final ObjectNode electionsConfig = ElectionsConfigFactory.createElectionsConfig(configuration);
		final Path electionsConfigPath = pathResolver.resolveSdmConfigPath().resolve(Constants.SDM_CONFIG_FILE_NAME_ELECTIONS_CONFIG);
		try {
			final byte[] electionsConfigBytes = objectMapper.writeValueAsBytes(electionsConfig);
			Files.write(electionsConfigPath, electionsConfigBytes);
			LOGGER.info("Successfully persisted the elections_config. [path: {}]", electionsConfigPath);
		} catch (final IOException e) {
			throw new UncheckedIOException("Failed to serialize elections_config.", e);
		}

		// Copy the input files to sdm/{ee}/CUSTOMER/input
		final String electionEventId = electionsConfig.get(JsonConstants.ELECTION_EVENTS).get(0).get(JsonConstants.ID).asText();
		cantonConfigConfigFileRepository.save(electionEventId, configuration);
		encryptionParametersPayloadService.save(electionEventId, encryptionParametersPayload);

		// Rename the input files to {ee}-configuration-anonymized and {ee}-encryptionParameters
		cantonConfigConfigFileRepository.renameConfigurationFile(electionEventId);
		encryptionParametersPayloadService.renameEncryptionParametersFile(electionEventId);

		LOGGER.info("Election pre-configuration completed.");

		return electionEventId;
	}

	/**
	 * Reads a json of configuration and saves the data related with each contests.
	 *
	 * @return The ids of the created election events.
	 * @throws IOException if there are any problem during json parsing.
	 */
	public String readFromFileAndSave() throws IOException {
		// result
		final JsonObjectBuilder jsonBuilderResult = Json.createObjectBuilder();

		// load json from file
		final Path electionsConfigPath = pathResolver.resolveSdmConfigPath().resolve(Constants.SDM_CONFIG_FILE_NAME_ELECTIONS_CONFIG);
		try (final InputStream is = Files.newInputStream(electionsConfigPath)) {

			final JsonNode rootNode = jsonReader.readTree(is);

			// save election events
			final JsonNode enrichedElectionEvents = enrichElectionEvents(rootNode);
			saveElectionEvents(jsonBuilderResult, enrichedElectionEvents);

			// save ballots
			saveBallots(jsonBuilderResult, rootNode);

			// save ballot texts
			final JsonNode enrichedBallotTexts = enrichBallotTexts(rootNode);
			saveBallotTexts(enrichedBallotTexts);

			// save ballot boxes
			saveBallotBoxes(jsonBuilderResult, rootNode);

			// save voting card sets
			saveVotingCardSets(jsonBuilderResult, rootNode);

			// save electoral boards
			saveElectoralBoards(jsonBuilderResult, rootNode);

			// save admin boards
			saveAdminBoards(jsonBuilderResult, rootNode);

			// because the relations ballot/ballotbox, votingcardset/ballot,
			// electoralboard/ballotbox we needed to put them
			// in the json
			final JsonObject result = jsonBuilderResult.build();
			final JsonArray arrayBallots = result.getJsonArray(JsonConstants.BALLOTS);
			if (!arrayBallots.isEmpty()) {
				updateBallots(arrayBallots.getValuesAs(JsonString.class));
			}
			final JsonArray arrayVotingCardSets = result.getJsonArray(JsonConstants.VOTING_CARD_SETS);
			if (!arrayVotingCardSets.isEmpty()) {
				updateVotingCardSets(arrayVotingCardSets.getValuesAs(JsonString.class));
			}
			final JsonArray arrayElectoralBoards = result.getJsonArray(JsonConstants.ELECTORAL_BOARDS);
			if (!arrayElectoralBoards.isEmpty()) {
				updateElectoralBoards(arrayElectoralBoards.getValuesAs(JsonString.class));
			}
			final JsonArray arrayBallotBoxes = result.getJsonArray(JsonConstants.BALLOT_BOXES);
			if (!arrayBallotBoxes.isEmpty()) {
				updateBallotBoxes(arrayBallotBoxes.getValuesAs(JsonString.class));
			}
			return result.toString();
		}
	}

	// save admin boards
	private void saveAdminBoards(final JsonObjectBuilder jsonBuilderResult, final JsonNode rootNode) {
		final JsonArrayBuilder jsonArraySaved = saveFromTree(rootNode.path(JsonConstants.ADMINISTRATION_BOARDS),
				administrationBoardRepository, Status.class);
		jsonBuilderResult.add(JsonConstants.ADMINISTRATION_BOARDS, jsonArraySaved);
	}

	// save electoral boards
	private void saveElectoralBoards(final JsonObjectBuilder jsonBuilderResult, final JsonNode rootNode) {
		final JsonArrayBuilder jsonArraySaved = saveFromTree(rootNode.path(JsonConstants.ELECTORAL_BOARDS), electoralBoardRepository,
				Status.class);
		jsonBuilderResult.add(JsonConstants.ELECTORAL_BOARDS, jsonArraySaved);
	}

	// save voting card sets
	private void saveVotingCardSets(final JsonObjectBuilder jsonBuilderResult, final JsonNode rootNode) {
		final JsonArrayBuilder jsonArraySaved = saveFromTree(rootNode.path(JsonConstants.VOTING_CARD_SETS), votingCardSetRepository, Status.class);
		jsonBuilderResult.add(JsonConstants.VOTING_CARD_SETS, jsonArraySaved);
	}

	// save ballot boxes
	private void saveBallotBoxes(final JsonObjectBuilder jsonBuilderResult, final JsonNode rootNode) {
		final JsonArrayBuilder jsonArraySaved = saveFromTree(rootNode.path(JsonConstants.BALLOT_BOXES), ballotBoxRepository, BallotBoxStatus.class);
		jsonBuilderResult.add(JsonConstants.BALLOT_BOXES, jsonArraySaved);
	}

	// save ballot boxes
	private void saveBallotTexts(final JsonNode rootNode) {
		saveFromTree(rootNode.path(JsonConstants.TRANSLATIONS), ballotTextRepository, Status.class);
	}

	// save ballot boxes
	private JsonNode enrichBallotTexts(final JsonNode rootNode) throws IOException {
		final JsonArrayBuilder jsonArrayProcessed = Json.createArrayBuilder();
		for (final JsonNode node : rootNode.path(JsonConstants.TRANSLATIONS)) {
			try {
				// add id attribute to the ballot text with a concatenation
				// between ballot id and locale
				final String json = node.toString();
				final JsonObject object = JsonUtils.getJsonObject(json);
				final String id = node.path(JsonConstants.BALLOT).path(JsonConstants.ID).textValue() + node.path(JsonConstants.LOCALE).textValue();
				final JsonObject jsonObjectWithUpdatedId = JsonUtils.jsonObjectToBuilder(object).add(JsonConstants.ID, id)
						.add(JsonConstants.SIGNED_OBJECT, EMPTY_SIGNATURE).build();
				jsonArrayProcessed.add(jsonObjectWithUpdatedId);
			} catch (final ORecordDuplicatedException e) {
				// duplicated error
				final String entityName = ballotTextRepository.getClass().getName();
				LOGGER.error(MessageFormat.format(ERROR_SAVING_DUPLICATED, entityName, e));
			}
		}

		final String json = Json.createObjectBuilder().add(JsonConstants.TRANSLATIONS, jsonArrayProcessed).build().toString();

		return jsonReader.readTree(json);
	}

	// save election events
	private void saveElectionEvents(final JsonObjectBuilder jsonBuilderResult, final JsonNode rootNode) {
		final JsonArrayBuilder jsonArraySaved = saveFromTree(rootNode.path(JsonConstants.ELECTION_EVENTS), electionEventRepository, Status.class);
		jsonBuilderResult.add(JsonConstants.ELECTION_EVENTS, jsonArraySaved);

	}

	// save election events
	private JsonNode enrichElectionEvents(final JsonNode rootNode) throws IOException {
		final JsonArrayBuilder jsonArrayProcessed = Json.createArrayBuilder();
		for (final JsonNode electionEventNode : rootNode.path(JsonConstants.ELECTION_EVENTS)) {

			final String electionEventId = electionEventNode.get(JsonConstants.ID).textValue();
			((ObjectNode) electionEventNode).put(JsonConstants.STATUS, Status.LOCKED.name());

			for (final JsonNode settingNode : rootNode.path(JsonConstants.SETTINGS)) {
				// add settings and status
				if (electionEventId.equals(settingNode.get(JsonConstants.ELECTION_EVENT).get(JsonConstants.ID).asText())) {
					final JsonObject electionEvent = JsonUtils.getJsonObject(electionEventNode.toString());
					final JsonObject setting = JsonUtils.getJsonObject(settingNode.toString());
					final JsonObject electionEventWithSettings = JsonUtils.jsonObjectToBuilder(electionEvent).add(JsonConstants.SETTINGS, setting)
							.build();
					jsonArrayProcessed.add(electionEventWithSettings);
				}
			}
		}

		final String json = Json.createObjectBuilder().add(JsonConstants.ELECTION_EVENTS, jsonArrayProcessed).build().toString();

		return jsonReader.readTree(json);
	}

	// save ballot
	private void saveBallots(final JsonObjectBuilder jsonBuilderResult, final JsonNode rootNode) {
		final JsonArrayBuilder jsonArraySaved = saveFromTree(rootNode.path(JsonConstants.BALLOTS), ballotRepository, Status.class);
		jsonBuilderResult.add(JsonConstants.BALLOTS, jsonArraySaved);
	}

	// Save from a set of entities taking into account status
	private <E extends Enum<E>> JsonArrayBuilder saveFromTree(final JsonNode path, final EntityRepository repository, final Class<E> enumClazz) {
		final JsonArrayBuilder jsonArraySaved = Json.createArrayBuilder();
		for (final JsonNode node : path) {
			try {
				// add status attribute to the object with the current status as value
				final String json = node.toString();
				final JsonObject object = JsonUtils.getJsonObject(json);
				final JsonObject jsonObjectWithStatus = JsonUtils.jsonObjectToBuilder(object)
						.add(JsonConstants.DETAILS, LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")))
						.add(JsonConstants.SYNCHRONIZED, Boolean.TRUE.toString()).build();
				final Optional<String> optionalId = saveOrUpdate(jsonObjectWithStatus, repository, enumClazz);
				optionalId.ifPresent(jsonArraySaved::add);
			} catch (final ORecordDuplicatedException e) {
				// duplicated error
				final String entityName = repository.getClass().getName();
				LOGGER.error(MessageFormat.format(ERROR_SAVING_DUPLICATED, entityName, e));
			}
		}
		return jsonArraySaved;
	}

	private <E extends Enum<E>> Optional<String> saveOrUpdate(final JsonObject entityObject, final EntityRepository repository,
			final Class<E> enumClazz) {
		final String id = entityObject.getString(JsonConstants.ID);
		final String foundEntityString = repository.find(id);
		final JsonObject foundEntityObject = JsonUtils.getJsonObject(foundEntityString);
		Optional<String> resultId = Optional.empty();
		if (foundEntityObject.isEmpty()) {
			repository.save(entityObject.toString());
			resultId = Optional.of(id);
		} else if (!foundEntityObject.containsKey(JsonConstants.STATUS) || !entityObject.containsKey(JsonConstants.STATUS)) {
			repository.update(entityObject.toString());
			resultId = Optional.of(id);
		} else {

			try {

				final String entityStatus = entityObject.getString(JsonConstants.STATUS);
				final E entityStatusEnumValue = Enum.valueOf(enumClazz, entityStatus);
				final String foundEntityStatus = foundEntityObject.getString(JsonConstants.STATUS);
				final E foundEntityStatusEnumValue = Enum.valueOf(enumClazz, foundEntityStatus);

				if (foundEntityStatusEnumValue.compareTo(entityStatusEnumValue) < 0) {
					repository.delete(id);
					repository.save(entityObject.toString());
					resultId = Optional.of(id);
				} else {
					// Entity status is before or equal to current status.
					LOGGER.debug(
							"Entity status is before or equal to current status, must not be updated. Entity {}, Entity status {}, Current status {}",
							id, entityStatusEnumValue, foundEntityStatusEnumValue);
				}
			} catch (final IllegalArgumentException e) {
				LOGGER.error("Not supported entity status found. You might need a new version of this tool that supports such type.", e);
			}
		}
		return resultId;
	}

	private void updateBallots(final List<JsonString> ids) {
		final List<String> list = ids.stream().map(JsonString::getString).toList();
		ballotRepository.updateRelatedBallotBox(list);
	}

	private void updateVotingCardSets(final List<JsonString> ids) {
		final List<String> list = ids.stream().map(JsonString::getString).toList();
		votingCardSetRepository.updateRelatedBallot(list);
	}

	private void updateBallotBoxes(final List<JsonString> ids) {
		final List<String> list = ids.stream().map(JsonString::getString).toList();
		ballotBoxRepository.updateRelatedBallotAlias(list);
	}

	private void updateElectoralBoards(final List<JsonString> ids) {
		final List<String> list = ids.stream().map(JsonString::getString).toList();
		electoralBoardRepository.updateRelatedBallotBox(list);
	}

}
