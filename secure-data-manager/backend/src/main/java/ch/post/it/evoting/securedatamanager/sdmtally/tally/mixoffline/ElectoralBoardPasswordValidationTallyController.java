/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The REST endpoint for accessing electoral board data in Tally phase.
 */
@RestController
@RequestMapping("/sdm-backend/electoralboardpasswords")
@Api(value = "Electoral board password validation REST API")
@ConditionalOnProperty("role.isTally")
public class ElectoralBoardPasswordValidationTallyController {
	private final ElectoralBoardTallyService electoralBoardTallyService;

	public ElectoralBoardPasswordValidationTallyController(final ElectoralBoardTallyService electoralBoardTallyService) {
		this.electoralBoardTallyService = electoralBoardTallyService;
	}

	/**
	 * Validates an electoral board member's password against its persisted hash.
	 *
	 * @param electionEventId        the election event id.
	 * @param electoralBoardId       the electoral board id.
	 * @param memberIndex            the index of the member in the member list.
	 * @param electoralBoardPassword the member's password.
	 */
	@PutMapping(value = "electionevents/{electionEventId}/electoralboards/{electoralBoardId}")
	@ApiOperation(value = "Validate electoral board password", notes = "Service to validate the electoral board password against its hash.")
	public ResponseEntity<Void> validatePassword(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electoralBoardId,
			@ApiParam(value = "index", required = true)
			@RequestParam
			final int memberIndex,
			@ApiParam(value = "electoralBoardPassword", required = true)
			@RequestBody
			final char[] electoralBoardPassword) {

		validateUUID(electionEventId);
		validateUUID(electoralBoardId);
		checkNotNull(electoralBoardPassword);

		if (electoralBoardTallyService.verifyElectoralBoardMemberPassword(electionEventId, memberIndex, electoralBoardPassword)) {
			return new ResponseEntity<>(HttpStatus.OK);
		}

		return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
	}
}
