/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.importexport;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.hashing.Argon2;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Hash;
import ch.post.it.evoting.cryptoprimitives.math.Random;

@Service
public class StreamedEncryptionService {

	private static final String AES = "AES";
	private static final int AES_GCM_TAG_BYTE_LENGTH = 16;
	private static final int SALT_BYTE_LENGTH = 16;
	private static final int NONCE_BYTE_LENGTH = 12;

	private static final String ALGORITHM_NAME = "AES/GCM/NoPadding";
	private final Random random;
	private final Argon2 argon2;

	public StreamedEncryptionService(final Random random,
			@Qualifier("argon2Standard")
			final Argon2 argon2) {

		this.random = random;
		this.argon2 = argon2;
	}

	public InputStream decrypt(final InputStream inputStream, final char[] password, final byte[] associatedData) {
		try {
			final byte[] salt = inputStream.readNBytes(SALT_BYTE_LENGTH);
			final byte[] nonce = inputStream.readNBytes(NONCE_BYTE_LENGTH);
			final byte[] derivedKey = argon2.getArgon2id(toBytes(password), salt);
			return decryptionStream(derivedKey, nonce, associatedData, inputStream);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	public void encrypt(final OutputStream outputStream, final InputStream inputStream, final char[] password,
			final byte[] associatedData) {
		try {
			final Argon2Hash argon2Hash = argon2.genArgon2id(toBytes(password));
			final byte[] derivedKey = argon2Hash.getTag();
			final byte[] salt = argon2Hash.getSalt();
			final byte[] nonce = random.genRandomBase16String(NONCE_BYTE_LENGTH).getBytes(StandardCharsets.UTF_8);

			try (final CipherOutputStream cipherOutputStream = encryptionStream(derivedKey, nonce, associatedData, outputStream)) {
				outputStream.write(salt);
				outputStream.write(nonce);
				inputStream.transferTo(cipherOutputStream);
			}
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	private byte[] toBytes(final char[] chars) {
		final CharBuffer charBuffer = CharBuffer.wrap(chars);
		final ByteBuffer byteBuffer = StandardCharsets.UTF_8.encode(charBuffer);
		final byte[] bytes = Arrays.copyOfRange(byteBuffer.array(), byteBuffer.position(), byteBuffer.limit());
		Arrays.fill(byteBuffer.array(), (byte) 0);
		return bytes;
	}

	private CipherOutputStream encryptionStream(final byte[] secretKey, final byte[] nonce, final byte[] associatedData, final OutputStream output) {
		final Cipher cipher = getCipher(secretKey, nonce, Cipher.ENCRYPT_MODE);
		cipher.updateAAD(associatedData);

		return new CipherOutputStream(output, cipher);
	}

	private CipherInputStream decryptionStream(final byte[] secretKey, final byte[] nonce, final byte[] associatedData, final InputStream input) {
		final Cipher cipher = getCipher(secretKey, nonce, Cipher.DECRYPT_MODE);
		cipher.updateAAD(associatedData);

		return new CipherInputStream(input, cipher);
	}

	private Cipher getCipher(final byte[] encryptionKey, final byte[] nonce, final int opmode) {
		// Get Cipher Instance
		final Cipher cipher;
		try {
			cipher = Cipher.getInstance(ALGORITHM_NAME, new BouncyCastleProvider());
		} catch (final NoSuchAlgorithmException | NoSuchPaddingException e) {
			throw new IllegalStateException(
					"Requested cryptographic algorithm or padding in the algorithm is not available in the environment.", e);
		}

		// Create the encryptionKey
		final Key key = new SecretKeySpec(encryptionKey, AES);

		// Create the algorithm used for the authentication
		final AlgorithmParameterSpec params = new GCMParameterSpec(AES_GCM_TAG_BYTE_LENGTH * 8, nonce);

		// Initialize Cipher for the authentication
		try {
			cipher.init(opmode, key, params);
		} catch (final InvalidKeyException e) {
			throw new IllegalArgumentException("Error with the given encryptionKey during Cipher initialization", e);
		} catch (final InvalidAlgorithmParameterException e) {
			throw new IllegalStateException("Configured algorithm parameters are invalid or inappropriate.", e);
		}

		return cipher;
	}
}