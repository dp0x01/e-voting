/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.VoterReturnCodes;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingprint.VotingCardList;
import ch.post.it.evoting.securedatamanager.configuration.PrimesMappingTableService;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.VoterInitialCodesPayloadService.VoterInitialCodesByVcs;
import ch.post.it.evoting.securedatamanager.services.application.service.BallotBoxService;

@Service
@ConditionalOnProperty("role.isSetup")
public class SetupComponentEvotingPrintService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentEvotingPrintService.class);

	private final BallotBoxService ballotBoxService;
	private final BallotConfigService ballotConfigService;
	private final PrimesMappingTableService primesMappingTableService;
	private final VotingCardSetConfigService votingCardSetConfigService;
	private final CantonConfigConfigService cantonConfigConfigService;
	private final SetupComponentEvotingPrintFileRepository setupComponentEvotingPrintFileRepository;
	private final VoterInitialCodesPayloadService voterInitialCodesPayloadService;
	private final VoterReturnCodesPayloadService voterReturnCodesPayloadService;

	public SetupComponentEvotingPrintService(
			final BallotBoxService ballotBoxService,
			final BallotConfigService ballotConfigService,
			final PrimesMappingTableService primesMappingTableService,
			final VotingCardSetConfigService votingCardSetConfigService,
			final CantonConfigConfigService cantonConfigConfigService,
			final SetupComponentEvotingPrintFileRepository setupComponentEvotingPrintFileRepository,
			final VoterInitialCodesPayloadService voterInitialCodesPayloadService,
			final VoterReturnCodesPayloadService voterReturnCodesPayloadService) {
		this.ballotBoxService = ballotBoxService;
		this.ballotConfigService = ballotConfigService;
		this.primesMappingTableService = primesMappingTableService;
		this.votingCardSetConfigService = votingCardSetConfigService;
		this.cantonConfigConfigService = cantonConfigConfigService;
		this.setupComponentEvotingPrintFileRepository = setupComponentEvotingPrintFileRepository;
		this.voterInitialCodesPayloadService = voterInitialCodesPayloadService;
		this.voterReturnCodesPayloadService = voterReturnCodesPayloadService;
	}

	/**
	 * Generates and persists the setup component evoting print file.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @throws NullPointerException      if the election event id is null.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	public void generate(final String electionEventId) {
		validateUUID(electionEventId);

		LOGGER.debug("Generating setup component evoting print... [electionEventId: {}]", electionEventId);

		// Gathering all data to generate evoting print file
		final Configuration configuration = cantonConfigConfigService.load(electionEventId);

		final Map<String, VoterInitialCodesByVcs> voterInitialCodesMap = voterInitialCodesPayloadService.loadVoterInitialCodesMap(electionEventId);
		final Map<String, VoterReturnCodes> voterReturnCodesMap = voterReturnCodesPayloadService.loadVoterReturnCodesMap(electionEventId);
		checkState(voterInitialCodesMap.size() == voterReturnCodesMap.size(),
				"The voter initial codes and return codes map must have the same size. [voterInitialCodesMap: %s, voterReturnCodesMap: %s]",
				voterInitialCodesMap.size(), voterReturnCodesMap.size());

		final Map<String, PrimesMappingTable> primesMappingTableMap = primesMappingTableService.loadAll(electionEventId);
		final List<String> verificationCardSetIds = new ArrayList<>(primesMappingTableMap.keySet());
		final Map<String, Ballot> ballotMap = verificationCardSetIds.stream().parallel()
				.map(verificationCardSetId -> {
					final String ballotBoxId = votingCardSetConfigService.getBallotBoxIdFromVerificationCardSetId(verificationCardSetId);
					final String ballotId = ballotBoxService.getBallotId(ballotBoxId);
					final Ballot ballot = ballotConfigService.getBallot(electionEventId, ballotId);
					return new VerificationCardSetBallot(verificationCardSetId, ballot);
				}).collect(Collectors.toMap(
						VerificationCardSetBallot::verificationCardSetId,
						VerificationCardSetBallot::ballot
				));

		// Map data to the voting card list
		final VotingCardList votingCardList = VotingCardListMapper.toVotingCardList(configuration, voterInitialCodesMap, voterReturnCodesMap,
				primesMappingTableMap, ballotMap);

		setupComponentEvotingPrintFileRepository.save(electionEventId, votingCardList);

		LOGGER.info("Setup component evoting print file successfully generated. [electionEventId: {}]", electionEventId);
	}

	private record VerificationCardSetBallot(String verificationCardSetId, Ballot ballot) {
	}
}
