/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmonline.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.commons.JsonUtils;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.SetupComponentVoterAuthenticationPayloadService;
import ch.post.it.evoting.securedatamanager.sdmonline.configuration.setupvoting.LongVoteCastReturnCodesAllowListUploadService;
import ch.post.it.evoting.securedatamanager.sdmonline.configuration.setupvoting.SetupComponentCMTablePayloadUploadService;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.SynchronizeStatus;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

/**
 * Service which will upload the information related to the voting card sets.
 */
@Service
public class VotingCardSetUploadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetUploadService.class);

	private final VotingCardSetRepository votingCardSetRepository;
	private final SetupComponentVoterAuthenticationPayloadService setupComponentVoterAuthenticationPayloadService;
	private final SetupComponentCMTablePayloadUploadService setupComponentCMTablePayloadUploadService;
	private final LongVoteCastReturnCodesAllowListUploadService longVoteCastReturnCodesAllowListUploadService;
	private final boolean isVotingPortalEnabled;

	public VotingCardSetUploadService(
			final VotingCardSetRepository votingCardSetRepository,
			final SetupComponentVoterAuthenticationPayloadService setupComponentVoterAuthenticationPayloadService,
			final SetupComponentCMTablePayloadUploadService setupComponentCMTablePayloadUploadService,
			final LongVoteCastReturnCodesAllowListUploadService longVoteCastReturnCodesAllowListUploadService,
			@Value("${voting.portal.enabled}")
			final boolean isVotingPortalEnabled) {
		this.votingCardSetRepository = votingCardSetRepository;
		this.setupComponentVoterAuthenticationPayloadService = setupComponentVoterAuthenticationPayloadService;
		this.setupComponentCMTablePayloadUploadService = setupComponentCMTablePayloadUploadService;
		this.longVoteCastReturnCodesAllowListUploadService = longVoteCastReturnCodesAllowListUploadService;
		this.isVotingPortalEnabled = isVotingPortalEnabled;
	}

	/**
	 * Uploads to the voter portal:
	 * <ul>
	 *     <li>the voter information.</li>
	 *     <li>the return codes mapping tables.</li>
	 *     <li>the long Vote Cast Return Codes allow lists.</li>
	 * </ul>
	 */
	public void uploadSynchronizableVotingCardSets(final String electionEventId) {
		validateUUID(electionEventId);
		checkState(isVotingPortalEnabled, "The voting-portal connection is not enabled.");

		final Map<String, Object> votingCardSetsParams = new HashMap<>();
		addSigned(votingCardSetsParams);
		addPendingToSynchronize(votingCardSetsParams);
		votingCardSetsParams.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);

		final String votingCardSetDocuments = votingCardSetRepository.list(votingCardSetsParams);
		final JsonArray votingCardSets = JsonUtils.getJsonObject(votingCardSetDocuments).getJsonArray(JsonConstants.RESULT);

		for (int i = 0; i < votingCardSets.size(); i++) {
			final JsonObject votingCardSetInArray = votingCardSets.getJsonObject(i);

			checkArgument(electionEventId.equals(votingCardSetInArray.getJsonObject(JsonConstants.ELECTION_EVENT).getString(JsonConstants.ID)));

			final String verificationCardSetId = votingCardSetInArray.getString(JsonConstants.VERIFICATION_CARD_SET_ID);

			LOGGER.debug("Uploading voter authentication... [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
					verificationCardSetId);
			setupComponentVoterAuthenticationPayloadService.upload(electionEventId, verificationCardSetId);

			LOGGER.debug("Uploading return codes mapping table... [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
					verificationCardSetId);
			setupComponentCMTablePayloadUploadService.upload(electionEventId, verificationCardSetId);

			LOGGER.debug("Uploading long Vote Cast Return Codes allow list... [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
					verificationCardSetId);
			longVoteCastReturnCodesAllowListUploadService.upload(electionEventId, verificationCardSetId);

			LOGGER.info("The voting card and verification card sets where successfully uploaded. [electionEventId: {}, verificationCardSetId: {}]",
					electionEventId, verificationCardSetId);

			final String votingCardSetId = votingCardSetInArray.getString(JsonConstants.ID);
			final JsonObjectBuilder builder = Json.createObjectBuilder();
			builder.add(JsonConstants.ID, votingCardSetId);
			builder.add(JsonConstants.SYNCHRONIZED, SynchronizeStatus.SYNCHRONIZED.getIsSynchronized().toString());
			builder.add(JsonConstants.DETAILS, SynchronizeStatus.SYNCHRONIZED.getStatus());

			votingCardSetRepository.update(builder.build().toString());
			LOGGER.info("Changed the state of the voting card set.");
		}
	}

	private void addSigned(final Map<String, Object> votingCardSetsParams) {
		votingCardSetsParams.put(JsonConstants.STATUS, Status.SIGNED.name());
	}

	private void addPendingToSynchronize(final Map<String, Object> votingCardSetsParams) {
		votingCardSetsParams.put(JsonConstants.SYNCHRONIZED, SynchronizeStatus.PENDING.getIsSynchronized().toString());
	}

}
