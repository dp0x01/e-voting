/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.importexport;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.securedatamanager.commons.JsonUtils;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.domain.model.EntityRepository;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ElectionEventContextRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.administrationboard.AdministrationBoardRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballot.BallotRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballotbox.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballottext.BallotTextRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electionevent.ElectionEventRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electoralboard.ElectoralBoardRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

@Service
public class ImportExportOrientDbService {

	public static final String SDM = "sdm_";

	private static final Logger LOGGER = LoggerFactory.getLogger(ImportExportOrientDbService.class);

	private final ElectionEventRepository electionEventRepository;
	private final BallotBoxRepository ballotBoxRepository;
	private final BallotRepository ballotRepository;
	private final BallotTextRepository ballotTextRepository;
	private final VotingCardSetRepository votingCardSetRepository;
	private final ElectoralBoardRepository electoralBoardRepository;
	private final AdministrationBoardRepository administrationBoardRepository;
	private final ElectionEventContextRepository electionEventContextRepository;

	@SuppressWarnings("java:S107")
	public ImportExportOrientDbService(final ElectionEventRepository electionEventRepository,
			final BallotBoxRepository ballotBoxRepository,
			final BallotRepository ballotRepository,
			final BallotTextRepository ballotTextRepository,
			final VotingCardSetRepository votingCardSetRepository,
			final ElectoralBoardRepository electoralBoardRepository,
			final AdministrationBoardRepository administrationBoardRepository,
			final ElectionEventContextRepository electionEventContextRepository) {
		this.electionEventRepository = electionEventRepository;
		this.ballotBoxRepository = ballotBoxRepository;
		this.ballotRepository = ballotRepository;
		this.ballotTextRepository = ballotTextRepository;
		this.votingCardSetRepository = votingCardSetRepository;
		this.electoralBoardRepository = electoralBoardRepository;
		this.administrationBoardRepository = administrationBoardRepository;
		this.electionEventContextRepository = electionEventContextRepository;
	}

	public void exportOrientDb(final Path dbDump, final String eeid) throws ResourceNotFoundException, IOException {
		final JsonObjectBuilder dumpJsonBuilder = Json.createObjectBuilder();

		final String adminBoards = administrationBoardRepository.list();
		dumpJsonBuilder.add(JsonConstants.ADMINISTRATION_BOARDS, JsonUtils.getJsonObject(adminBoards).getJsonArray(JsonConstants.RESULT));

		final String electionEvent = electionEventRepository.find(eeid);

		if (StringUtils.isEmpty(electionEvent) || JsonConstants.EMPTY_OBJECT.equals(electionEvent)) {
			throw new ResourceNotFoundException("Election Event not found");
		}

		final JsonArray electionEvents = Json.createArrayBuilder().add(JsonUtils.getJsonObject(electionEvent)).build();
		dumpJsonBuilder.add(JsonConstants.ELECTION_EVENTS, electionEvents);

		final String ballots = ballotRepository.listByElectionEvent(eeid);
		final JsonArray ballotsArray = JsonUtils.getJsonObject(ballots).getJsonArray(JsonConstants.RESULT);
		dumpJsonBuilder.add(JsonConstants.BALLOTS, ballotsArray);

		final JsonArrayBuilder ballotBoxTextsArrayBuilder = Json.createArrayBuilder();
		for (final JsonValue ballotValue : ballotsArray) {
			final JsonObject ballotObject = (JsonObject) ballotValue;
			final String id = ballotObject.getString(JsonConstants.ID);
			final String ballotTexts = ballotTextRepository.list(Collections.singletonMap(JsonConstants.BALLOT_DOT_ID, id));
			final JsonArray ballotTextsForBallot = JsonUtils.getJsonObject(ballotTexts).getJsonArray(JsonConstants.RESULT);
			for (final JsonValue ballotText : ballotTextsForBallot) {
				ballotBoxTextsArrayBuilder.add(ballotText);
			}
		}
		dumpJsonBuilder.add(JsonConstants.TEXTS, ballotBoxTextsArrayBuilder.build());

		final String ballotBoxes = ballotBoxRepository.listByElectionEvent(eeid);
		dumpJsonBuilder.add(JsonConstants.BALLOT_BOXES, JsonUtils.getJsonObject(ballotBoxes).getJsonArray(JsonConstants.RESULT));

		final String votingCardSets = votingCardSetRepository.listByElectionEvent(eeid);
		dumpJsonBuilder.add(JsonConstants.VOTING_CARD_SETS, JsonUtils.getJsonObject(votingCardSets).getJsonArray(JsonConstants.RESULT));

		final String electoralBoards = electoralBoardRepository.listByElectionEvent(eeid);
		dumpJsonBuilder.add(JsonConstants.ELECTORAL_BOARDS, JsonUtils.getJsonObject(electoralBoards).getJsonArray(JsonConstants.RESULT));

		final String electionEventContext = electionEventContextRepository.find(eeid);
		if (!StringUtils.isEmpty(electionEventContext) && !JsonConstants.EMPTY_OBJECT.equals(electionEventContext)) {
			dumpJsonBuilder.add(JsonConstants.ELECTION_EVENT_CONTEXT, JsonUtils.getJsonObject(electionEventContext));
		}

		try {
			Files.writeString(dbDump, dumpJsonBuilder.build().toString());

			LOGGER.info("Database export to dump file has been completed successfully: {}", dbDump);
		} catch (final IOException e) {
			LOGGER.error("An error occurred writing DB dump to: {}", dbDump, e);
		}
	}

	public void importOrientDb(final Path dbDump) throws IOException {

		if (Files.notExists(dbDump)) {
			LOGGER.warn("There is no dump database to import");
			return;
		}

		final String dump;
		try {
			dump = Files.readString(dbDump);
		} catch (final IOException e) {
			throw new IOException("Error reading import file ", e);
		}

		final JsonObject dumpJson = JsonUtils.getJsonObject(dump);
		final JsonArray adminBoards = dumpJson.getJsonArray(JsonConstants.ADMINISTRATION_BOARDS);
		for (final JsonValue adminBoard : adminBoards) {
			saveOrUpdate(adminBoard, administrationBoardRepository, Status.class);
		}

		final JsonArray electionEvents = dumpJson.getJsonArray(JsonConstants.ELECTION_EVENTS);
		for (final JsonValue electionEvent : electionEvents) {
			saveOrUpdate(electionEvent, electionEventRepository, Status.class);
		}

		final JsonArray ballots = dumpJson.getJsonArray(JsonConstants.BALLOTS);
		for (final JsonValue ballot : ballots) {
			saveOrUpdate(ballot, ballotRepository, Status.class);
		}

		final JsonArray ballotTexts = dumpJson.getJsonArray(JsonConstants.TEXTS);
		for (final JsonValue ballotText : ballotTexts) {
			saveOrUpdate(ballotText, ballotTextRepository, Status.class);
		}

		final JsonArray ballotBoxes = dumpJson.getJsonArray(JsonConstants.BALLOT_BOXES);
		for (final JsonValue ballotBox : ballotBoxes) {
			saveOrUpdate(ballotBox, ballotBoxRepository, BallotBoxStatus.class);
		}

		final JsonArray votingCardSets = dumpJson.getJsonArray(JsonConstants.VOTING_CARD_SETS);
		for (final JsonValue votingCardSet : votingCardSets) {
			saveOrUpdate(votingCardSet, votingCardSetRepository, Status.class);
		}

		final JsonArray electoralBoards = dumpJson.getJsonArray(JsonConstants.ELECTORAL_BOARDS);
		for (final JsonValue electoralBoard : electoralBoards) {
			saveOrUpdate(electoralBoard, electoralBoardRepository, Status.class);
		}

		final JsonObject electionEventContext = dumpJson.getJsonObject(JsonConstants.ELECTION_EVENT_CONTEXT);
		if (electionEventContext != null) {
			saveOrUpdate(electionEventContext, electionEventContextRepository, Status.class);
		}
	}

	private static <E extends Enum<E>> void saveOrUpdate(final JsonValue entity, final EntityRepository repository, final Class<E> enumClazz) {
		final JsonObject entityObject = (JsonObject) entity;
		final String id = entityObject.getString(JsonConstants.ID);
		final String foundEntityString = repository.find(id);
		final JsonObject foundEntityObject = JsonUtils.getJsonObject(foundEntityString);

		if (foundEntityObject.isEmpty()) {

			repository.save(entity.toString());
		} else if (!foundEntityObject.containsKey(JsonConstants.STATUS) || !entityObject.containsKey(JsonConstants.STATUS)) {

			repository.update(entity.toString());
		} else {

			try {

				final String entityStatus = entityObject.getString(JsonConstants.STATUS);
				final E entityStatusEnumValue = Enum.valueOf(enumClazz, entityStatus);
				final String foundEntityStatus = foundEntityObject.getString(JsonConstants.STATUS);
				final E foundEntityStatusEnumValue = Enum.valueOf(enumClazz, foundEntityStatus);

				if (foundEntityStatusEnumValue.compareTo(entityStatusEnumValue) < 0) {
					repository.delete(id);
					repository.save(entity.toString());
				} else {
					LOGGER.debug("Entity can't be updated. [id: {}, class: {}]", id, enumClazz.getName());
				}
			} catch (final IllegalArgumentException e) {
				LOGGER.error("Not supported entity status found. You might need a new version of this tool that supports such type.", e);
			}
		}
	}
}
