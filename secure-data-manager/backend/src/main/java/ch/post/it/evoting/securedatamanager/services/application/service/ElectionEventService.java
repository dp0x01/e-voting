/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants;
import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.domain.configuration.ControlComponentKeyGenerationRequestPayload;
import ch.post.it.evoting.evotinglibraries.domain.configuration.ControlComponentPublicKeysPayload;
import ch.post.it.evoting.securedatamanager.ControlComponentPublicKeysService;
import ch.post.it.evoting.securedatamanager.EncryptionParametersPayloadService;
import ch.post.it.evoting.securedatamanager.SetupKeyPairService;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.commons.RemoteService;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.GenSetupEncryptionKeysAlgorithm;
import ch.post.it.evoting.securedatamanager.services.application.exception.CCKeysAlreadyExistException;
import ch.post.it.evoting.securedatamanager.services.application.exception.CCKeysNotExistException;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.domain.service.ElectionEventDataGeneratorService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.MessageBrokerOrchestratorClient;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electionevent.ElectionEventRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

import retrofit2.Response;

/**
 * This is a service for handling election event entities.
 */
@Service
public class ElectionEventService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectionEventService.class);

	private final ObjectMapper mapper;
	private final PathResolver pathResolver;
	private final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;
	private final SetupKeyPairService setupKeyPairService;
	private final ElectionEventRepository electionEventRepository;
	private final EncryptionParametersPayloadService encryptionParametersPayloadService;
	private final GenSetupEncryptionKeysAlgorithm genSetupEncryptionKeysAlgorithm;
	private final ConfigurationEntityStatusService configurationEntityStatusService;
	private final ElectionEventDataGeneratorService electionEventDataGeneratorService;
	private final ControlComponentPublicKeysService controlComponentPublicKeysService;
	private final boolean isVotingPortalEnabled;

	public ElectionEventService(
			final ObjectMapper mapper,
			final PathResolver pathResolver,
			final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient,
			final SetupKeyPairService setupKeyPairService,
			final ElectionEventRepository electionEventRepository,
			final EncryptionParametersPayloadService encryptionParametersPayloadService,
			final GenSetupEncryptionKeysAlgorithm genSetupEncryptionKeysAlgorithm,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final ElectionEventDataGeneratorService electionEventDataGeneratorService,
			final ControlComponentPublicKeysService controlComponentPublicKeysService,
			@Value("${voting.portal.enabled}")
			final boolean isVotingPortalEnabled) {
		this.mapper = mapper;
		this.pathResolver = pathResolver;
		this.messageBrokerOrchestratorClient = messageBrokerOrchestratorClient;
		this.setupKeyPairService = setupKeyPairService;
		this.electionEventRepository = electionEventRepository;
		this.encryptionParametersPayloadService = encryptionParametersPayloadService;
		this.genSetupEncryptionKeysAlgorithm = genSetupEncryptionKeysAlgorithm;
		this.configurationEntityStatusService = configurationEntityStatusService;
		this.electionEventDataGeneratorService = electionEventDataGeneratorService;
		this.controlComponentPublicKeysService = controlComponentPublicKeysService;
		this.isVotingPortalEnabled = isVotingPortalEnabled;
	}

	/**
	 * Creates an election event based on the given id and if everything ok, it sets its status to ready.
	 *
	 * @param electionEventId identifies the election event to be created.
	 * @return a boolean.
	 */
	public boolean create(final String electionEventId) throws IOException, CCKeysNotExistException {

		// Check if Control Components keys exists
		if (!controlComponentPublicKeysService.exist(electionEventId)) {
			throw new CCKeysNotExistException(
					String.format("The Control Components keys do not exist for this election event. [electionEventId: %s]", electionEventId));
		}

		// Load the election encryption parameters.
		final GqGroup group = encryptionParametersPayloadService.loadEncryptionGroup(electionEventId);

		// Generate the setup key pair
		final ElGamalMultiRecipientKeyPair setupKeyPair = genSetupEncryptionKeysAlgorithm.genSetupEncryptionKeys(group);

		final Path offlinePath = pathResolver.resolveOfflinePath(electionEventId);
		Files.createDirectories(offlinePath);

		// Persist the setup key pair.
		setupKeyPairService.save(electionEventId, setupKeyPair);

		// Persist the setup secret key.
		mapper.writeValue(offlinePath.resolve(Constants.SETUP_SECRET_KEY_FILE_NAME).toFile(), setupKeyPair.getPrivateKey());

		// Create election event data.
		if (electionEventDataGeneratorService.generate(electionEventId)) {
			configurationEntityStatusService.update(Status.READY.name(), electionEventId, electionEventRepository);
		} else {
			return false;
		}

		return true;
	}

	/**
	 * Requests the Control Components keys for an election event based on the given id.
	 *
	 * @param electionEventId identifies the election event for which Control Components keys are requested.
	 * @throws CCKeysAlreadyExistException if Control Components keys already exist.
	 */
	@Transactional
	public void requestCCKeys(final String electionEventId) throws CCKeysAlreadyExistException {

		checkState(isVotingPortalEnabled, "The voting-portal connection is not enabled.");

		if (controlComponentPublicKeysService.exist(electionEventId)) {
			throw new CCKeysAlreadyExistException(
					String.format("The Control Components keys already exist for this election event. [electionEventId: %s]", electionEventId));
		}

		final GqGroup gqGroup = encryptionParametersPayloadService.loadEncryptionGroup(electionEventId);
		final ControlComponentKeyGenerationRequestPayload requestPayload = new ControlComponentKeyGenerationRequestPayload(electionEventId, gqGroup);

		final Response<List<ControlComponentPublicKeysPayload>> response =
				RemoteService.call(messageBrokerOrchestratorClient.generateCCKeys(electionEventId, requestPayload))
						.networkErrorMessage("Failed to communicate with message broker orchestrator.")
						.unsuccessfulResponseErrorMessage("Request for the Control Components keys failed. [electionEventId: %s]", electionEventId)
						.execute();

		checkState(response.body() != null, "Response body of the request for the Control Components keys body is null. [electionEventId: %s]",
				electionEventId);

		LOGGER.info("Successfully retrieved control component public keys payloads. [electionEventId: {}]", electionEventId);

		final List<ControlComponentPublicKeysPayload> controlComponentPublicKeysPayloads = response.body();

		checkState(controlComponentPublicKeysPayloads.size() == ControlComponentConstants.NODE_IDS.size(),
				"There number of control component public keys payloads expected is incorrect. [received: %s, expected: %s, electionEventId: %s]",
				controlComponentPublicKeysPayloads.size(), ControlComponentConstants.NODE_IDS.size(), electionEventId);

		final Set<Integer> payloadNodeIds = controlComponentPublicKeysPayloads.stream()
				.map(ControlComponentPublicKeysPayload::getControlComponentPublicKeys)
				.map(ControlComponentPublicKeys::nodeId)
				.collect(Collectors.toSet());

		checkState(payloadNodeIds.equals(ControlComponentConstants.NODE_IDS),
				"The control component public keys payloads node ids do not match the expected node ids. [received: %s, expected: %s, electionEventId: %s]",
				payloadNodeIds, ControlComponentConstants.NODE_IDS, electionEventId);

		controlComponentPublicKeysPayloads.forEach(payload -> checkState(payload.getElectionEventId().equals(electionEventId),
				"The controlComponentPublicKeysPayload's election event id must correspond to the election event id of the request"));
		controlComponentPublicKeysPayloads.forEach(payload -> checkState(payload.getEncryptionGroup().equals(gqGroup),
				"The controlComponentPublicKeysPayload's group must correspond to the group stored by the secure-data-manager."));

		controlComponentPublicKeysPayloads.forEach(controlComponentPublicKeysService::save);
		LOGGER.info("Successfully saved control component public keys payloads. [electionEventId: {}]", electionEventId);
	}

	/**
	 * @return the election event alias based on the given id.
	 */
	public String getElectionEventAlias(final String electionEventId) {
		validateUUID(electionEventId);

		return electionEventRepository.getElectionEventAlias(electionEventId);
	}

	/**
	 * @return the election event description based on the given id.
	 */
	public String getElectionEventDescription(final String electionEventId) {
		validateUUID(electionEventId);

		return electionEventRepository.getElectionEventDescription(electionEventId);
	}

	public LocalDateTime getDateFrom(final String electionEventId) {
		validateUUID(electionEventId);

		return LocalDateTime.parse(electionEventRepository.getDateFrom(electionEventId), DateTimeFormatter.ISO_DATE_TIME);
	}

	public LocalDateTime getDateTo(final String electionEventId) {
		validateUUID(electionEventId);

		return LocalDateTime.parse(electionEventRepository.getDateTo(electionEventId), DateTimeFormatter.ISO_DATE_TIME);
	}

	public boolean exists(final String electionEventId) {
		validateUUID(electionEventId);

		return electionEventRepository.exists(electionEventId);
	}

}
