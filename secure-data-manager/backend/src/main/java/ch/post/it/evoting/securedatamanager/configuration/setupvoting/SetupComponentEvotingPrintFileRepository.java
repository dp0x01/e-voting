/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.commons.Constants.SETUP_COMPONENT_EVOTING_PRINT_XML;
import static com.google.common.base.Preconditions.checkNotNull;

import java.nio.file.Path;
import java.security.SignatureException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.xml.XmlFileRepository;
import ch.post.it.evoting.evotinglibraries.xml.XsdConstants;
import ch.post.it.evoting.evotinglibraries.xml.hashable.HashableSetupComponentEvotingPrintFactory;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingprint.VotingCardList;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@Repository
@ConditionalOnProperty("role.isSetup")
public class SetupComponentEvotingPrintFileRepository extends XmlFileRepository<VotingCardList> {
	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentEvotingPrintFileRepository.class);

	private final PathResolver pathResolver;
	private final SignatureKeystore<Alias> signatureKeystoreServiceSdmConfig;

	public SetupComponentEvotingPrintFileRepository(
			final PathResolver pathResolver,
			@Qualifier("keystoreServiceSdmConfig")
			final SignatureKeystore<Alias> signatureKeystoreServiceSdmConfig) {
		this.pathResolver = pathResolver;
		this.signatureKeystoreServiceSdmConfig = signatureKeystoreServiceSdmConfig;
	}

	/**
	 * Saves the given voting card list in the {@value ch.post.it.evoting.securedatamanager.commons.Constants#SETUP_COMPONENT_EVOTING_PRINT_XML} file
	 * while validating it against the related {@value XsdConstants#SETUP_COMPONENT_EVOTING_PRINT_XSD}.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @param votingCardList  the voting card list. Must be non-null.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	public void save(final String electionEventId, final VotingCardList votingCardList) {
		validateUUID(electionEventId);
		checkNotNull(votingCardList);

		LOGGER.debug("Signing setup component evoting print... [electionEventId: {}]", electionEventId);

		final byte[] signature = getSignature(votingCardList, electionEventId);
		votingCardList.setSignature(signature);

		LOGGER.debug("Setup component evoting print successfully signed. [electionEventId: {}]", electionEventId);

		LOGGER.debug("Saving file {}... [electionEventId: {}]", SETUP_COMPONENT_EVOTING_PRINT_XML, electionEventId);

		final String extendedFilename = String.format(SETUP_COMPONENT_EVOTING_PRINT_XML, votingCardList.getContest().getContestIdentification());
		final Path xmlFilePath = pathResolver.resolvePrintingPath(electionEventId).resolve(extendedFilename);

		final Path writePath = write(votingCardList, XsdConstants.SETUP_COMPONENT_EVOTING_PRINT_XSD, xmlFilePath);

		LOGGER.debug("File successfully saved. [electionEventId: {}, path: {}]", electionEventId, writePath);
	}

	private byte[] getSignature(final VotingCardList votingCardList, final String electionEventId) {
		final Hashable hashable = HashableSetupComponentEvotingPrintFactory.fromVotingCardList(votingCardList);
		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentEvotingPrint();

		try {
			return signatureKeystoreServiceSdmConfig.generateSignature(hashable, additionalContextData);
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format("Could not sign setup component evoting print. [electionEventId: %s]", electionEventId));
		}
	}

}
