/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmonline.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.configuration.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.services.application.service.BallotBoxService;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The configuration upload end-point.
 */
@RestController
@RequestMapping("/sdm-backend/configurations")
@Api(value = "Configurations REST API")
public class ConfigurationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationController.class);

	private final BallotBoxService ballotBoxService;
	private final BallotUploadService ballotUploadService;
	private final VotingCardSetRepository votingCardSetRepository;
	private final VotingCardSetUploadService votingCardSetUploadService;
	private final ElectoralBoardsUploadService electoralBoardsUploadService;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;
	private final boolean isVotingPortalEnabled;

	public ConfigurationController(
			final BallotUploadService ballotUploadService,
			final BallotBoxService ballotBoxService,
			final VotingCardSetRepository votingCardSetRepository,
			final VotingCardSetUploadService votingCardSetUploadService,
			final ElectoralBoardsUploadService electoralBoardsUploadService,
			final ElectionEventContextPayloadService electionEventContextPayloadService,
			@Value("${voting.portal.enabled}")
			final boolean isVotingPortalEnabled) {
		this.ballotBoxService = ballotBoxService;
		this.ballotUploadService = ballotUploadService;
		this.votingCardSetRepository = votingCardSetRepository;
		this.votingCardSetUploadService = votingCardSetUploadService;
		this.electoralBoardsUploadService = electoralBoardsUploadService;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
		this.isVotingPortalEnabled = isVotingPortalEnabled;
	}

	/**
	 * Uploads data specific to one election event
	 */
	@PostMapping(value = "/electionevent/{electionEventId}", produces = "application/json")
	@ResponseStatus(value = HttpStatus.OK)
	@ApiOperation(value = "Election event upload configuration", notes = "Service to upload the configuration for a given election event.")
	public void uploadElectionEventConfigurationConfiguration(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final
			String electionEventId) {
		validateUUID(electionEventId);

		if (isVotingPortalEnabled) {
			uploadElectionConfiguration(electionEventId);
		} else {
			LOGGER.warn("The application is configured to not have connectivity to Voter Portal, check if this is the expected behavior");
		}
	}

	private void uploadElectionConfiguration(final String electionEventId) {
		// The control-components need all voting card sets to be computed before uploading the election event context.
		final List<String> signedVotingCardSetIds = votingCardSetRepository.findAllVotingCardSetIdsByStatus(electionEventId, Status.SIGNED);
		checkState(signedVotingCardSetIds.size() == votingCardSetRepository.findAllVotingCardSetIds(electionEventId).size(),
				"Some voting card sets are not signed yet. Uploading nothing. [electionEventId: %s]", electionEventId);
		checkState(electionEventContextPayloadService.exist(electionEventId), "Election event context does not exist yet. [electionEventId: {}]");

		LOGGER.debug("Uploading the election configuration. [electionEventId: {}]", electionEventId);

		if (!electionEventContextPayloadService.isElectionEventContextUploaded(electionEventId)) {
			electionEventContextPayloadService.uploadElectionEventContextData(electionEventId);
			upload(electionEventId);
		} else {
			upload(electionEventId);
		}
	}

	private void upload(final String electionEventId) {
		LOGGER.debug("Uploading ballots");
		ballotUploadService.uploadSynchronizableBallots(electionEventId);
		LOGGER.debug("Uploading ballot boxes");
		ballotBoxService.updateSynchronizationStatuses(electionEventId);
		LOGGER.debug("Uploading voting card sets");
		votingCardSetUploadService.uploadSynchronizableVotingCardSets(electionEventId);
		LOGGER.debug("Uploading electoral board");
		electoralBoardsUploadService.uploadSynchronizableElectoralBoards(electionEventId);
	}

}
