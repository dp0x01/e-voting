/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.domain.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.common.annotations.VisibleForTesting;

import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electionevent.ElectionEventRepository;

/**
 * This implementation generates the election event data.
 */
@Service
public class ElectionEventDataGeneratorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectionEventDataGeneratorService.class);

	private final ElectionEventRepository electionEventRepository;
	private final PathResolver pathResolver;

	public ElectionEventDataGeneratorService(final ElectionEventRepository electionEventRepository, final PathResolver pathResolver) {
		this.electionEventRepository = electionEventRepository;
		this.pathResolver = pathResolver;
	}

	/**
	 * This method creates the input necessary for the configuration to work and calls it to generate the election event data for the one identified
	 * by the given id. It simulates the use from command line by calling directly the code. One of the inputs being a properties file, this is
	 * created and saved on disk.
	 *
	 * @param electionEventId The identifier of the election event for whom to generate the data.
	 * @return a boolean.
	 */
	public boolean generate(final String electionEventId) {

		// basic validation of the input
		if (StringUtils.isBlank(electionEventId)) {
			return false;
		}

		// just in case the directory is not created
		final Path configPath = pathResolver.resolveConfigPath();
		try {
			makePath(configPath);
		} catch (final IOException e2) {
			LOGGER.error("Failed to create the config path. [configPath: {}]", configPath);
			return false;
		}

		final String electionEvent = electionEventRepository.find(electionEventId);
		return !JsonConstants.EMPTY_OBJECT.equals(electionEvent);
	}

	/**
	 * Creates all the directories from the path if they don't exist yet. This is wrapper over a static method in order to make the class testable.
	 *
	 * @param path The path to be created.
	 * @return a Path representing the directory created.
	 * @throws IOException in case there is a I/O problem.
	 */
	@VisibleForTesting
	Path makePath(final Path path) throws IOException {
		return Files.createDirectories(path);
	}

}
