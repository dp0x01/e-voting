/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/sdm-backend/operation")
@Api(value = "SDM Operations REST API for Tally phase")
@ConditionalOnProperty("role.isTally")
public class OperationsTallyController {

	private final TallyComponentFilesService tallyComponentFilesService;

	public OperationsTallyController(final TallyComponentFilesService tallyComponentFilesService) {
		this.tallyComponentFilesService = tallyComponentFilesService;
	}

	/**
	 * Generates the tally files.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @throws NullPointerException      if the election event id is null.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	@PostMapping(value = "/electionevent/{electionEventId}/tally-files/generate")
	@ApiOperation(value = "Generate tally files", notes = "Service to generate the tally files.")
	public ResponseEntity<Void> generateTallyFiles(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId) {

		validateUUID(electionEventId);

		tallyComponentFilesService.generate(electionEventId);

		return new ResponseEntity<>(HttpStatus.OK);
	}
}
