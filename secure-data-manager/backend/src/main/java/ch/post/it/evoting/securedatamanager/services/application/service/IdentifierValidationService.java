/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;

@Service
public class IdentifierValidationService {

	private final ElectionEventService electionEventService;
	private final BallotBoxService ballotBoxService;

	public IdentifierValidationService(final ElectionEventService electionEventService, final BallotBoxService ballotBoxService) {
		this.electionEventService = electionEventService;
		this.ballotBoxService = ballotBoxService;
	}

	/**
	 * Validates that the given election event ID exists and that the given ballot box ID exists for the given election event ID.
	 *
	 * @param electionEventId the identifier of the election event. Must be a valid UUID.
	 * @param ballotBoxId the identifier of the ballot box. Must be a valid UUID.
	 * @throws NullPointerException if any of the IDs is null.
	 * @throws FailedValidationException if any of the IDs is not a valid UUID.
	 * @throws IllegalArgumentException if
	 * <ul>
	 *     <li>the election event ID does not exist in the data base</li>
	 *     <li>the ballot box ID does not exist in the database for the given election event ID</li>
	 * </ul>
	 */
	public void validateBallotBoxRelatedIds(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);
		checkArgument(electionEventService.exists(electionEventId), "The given election event ID does not exist. [electionEventId: %s]",
				electionEventId);
		checkArgument(ballotBoxService.getBallotBoxesId(electionEventId).contains(ballotBoxId),
				"The given ballot box ID does not belong to the given election event ID. [ballotBoxId: %s, electionEventId: %s]", ballotBoxId,
				electionEventId);
	}

	/**
	 * Validates that the given ballot ID corresponds to the given ballot box ID.
	 *
	 * @param ballotId the identifier of the ballot. Must be a valid UUID.
	 * @param ballotBoxId the identifier of the ballot box. Must be a valid UUID.
	 * @throws NullPointerException if any of the IDs is null.
	 * @throws FailedValidationException if any of the IDs is not a valid UUID.
	 * @throws IllegalArgumentException if the ballot ID does not exist in the database for the given ballot box ID.
	 */
	public void validateBallotId(final String ballotId, final String ballotBoxId) {
		validateUUID(ballotId);
		validateUUID(ballotBoxId);
		checkArgument(ballotBoxService.getBallotId(ballotBoxId).equals(ballotId),
				"The given ballot ID does not belong to the given ballot box ID. [ballotId: %s, ballotBoxId: %s]", ballotId, ballotBoxId);
	}
}
