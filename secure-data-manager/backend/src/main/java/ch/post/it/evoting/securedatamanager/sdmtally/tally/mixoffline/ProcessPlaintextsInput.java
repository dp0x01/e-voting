/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientMessage;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;

/**
 * The input of the ProcessPlaintexts algorithm.
 *
 * <ul>
 *     <li>m, the list of plaintext votes. Non-null.</li>
 *     <li>p&#126;<sub>w</sub>, the write-in voting options. Non-null.</li>
 *     <li>&Psi;, the number of selectable voting options. Strictly positive.</li>
 *     <li>&delta;&#770;, the number of allowed write-ins plus one. Strictly positive.</li>
 * </ul>
 */
public class ProcessPlaintextsInput {

	private final GroupVector<ElGamalMultiRecipientMessage, GqGroup> plaintextVotes;
	private final GroupVector<PrimeGqElement, GqGroup> writeInVotingOptions;
	private final int numberOfSelectableVotingOptions;
	private final int numberOfAllowedWriteInsPlusOne;

	private ProcessPlaintextsInput(final GroupVector<ElGamalMultiRecipientMessage, GqGroup> plaintextVotes,
			final GroupVector<PrimeGqElement, GqGroup> writeInVotingOptions, final int numberOfSelectableVotingOptions,
			final int numberOfAllowedWriteInsPlusOne) {
		this.plaintextVotes = plaintextVotes;
		this.writeInVotingOptions = writeInVotingOptions;
		this.numberOfSelectableVotingOptions = numberOfSelectableVotingOptions;
		this.numberOfAllowedWriteInsPlusOne = numberOfAllowedWriteInsPlusOne;
	}

	public GroupVector<ElGamalMultiRecipientMessage, GqGroup> getPlaintextVotes() {
		return plaintextVotes;
	}

	public GroupVector<PrimeGqElement, GqGroup> getWriteInVotingOptions() {
		return writeInVotingOptions;
	}

	public int getNumberOfSelectableVotingOptions() {
		return numberOfSelectableVotingOptions;
	}

	public int getNumberOfAllowedWriteInsPlusOne() {
		return numberOfAllowedWriteInsPlusOne;
	}

	public static class Builder {
		private GroupVector<ElGamalMultiRecipientMessage, GqGroup> plaintextVotes;
		private GroupVector<PrimeGqElement, GqGroup> writeInVotingOptions;
		private int numberOfSelectableVotingOptions;
		private int numberOfAllowedWriteInsPlusOne;

		public Builder setPlaintextVotes(
				GroupVector<ElGamalMultiRecipientMessage, GqGroup> plaintextVotes) {
			this.plaintextVotes = plaintextVotes;
			return this;
		}

		public Builder setWriteInVotingOptions(
				GroupVector<PrimeGqElement, GqGroup> writeInVotingOptions) {
			this.writeInVotingOptions = writeInVotingOptions;
			return this;
		}

		public Builder setNumberOfSelectableVotingOptions(int numberOfSelectableVotingOptions) {
			this.numberOfSelectableVotingOptions = numberOfSelectableVotingOptions;
			return this;
		}

		public Builder setNumberOfAllowedWriteInsPlusOne(int numberOfAllowedWriteInsPlusOne) {
			this.numberOfAllowedWriteInsPlusOne = numberOfAllowedWriteInsPlusOne;
			return this;
		}

		/**
		 *
		 *
		 * @throws NullPointerException if the plaintext votes or write-in voting options are null
		 * @throws IllegalArgumentException if
		 * <ul>
		 *     <li>the number of selectable voting options is not strictly greater than 0</li>
		 *     <li>the number of allowed write-ins + 1 is not strictly greater than 0</li>
		 *     <li>the write-in options are not empty and have a different group than the plaintext votes</li>
		 * </ul>
		 */
		public ProcessPlaintextsInput build() {
			checkNotNull(plaintextVotes);
			checkNotNull(writeInVotingOptions);

			checkArgument(numberOfSelectableVotingOptions > 0, "The number of selectable voting options must be greater or equal to 1. [psi: %s]",
					numberOfSelectableVotingOptions);
			checkArgument(numberOfAllowedWriteInsPlusOne > 0, "The number of allowed write-ins + 1 must be strictly greater than 0. [delta_hat: %s]",
					numberOfAllowedWriteInsPlusOne);

			if (!writeInVotingOptions.isEmpty()) {
				checkArgument(plaintextVotes.getGroup().equals(writeInVotingOptions.getGroup()),
						"The plaintext votes and the write-in voting options must have the same group.");
			}

			return new ProcessPlaintextsInput(plaintextVotes, writeInVotingOptions, numberOfSelectableVotingOptions, numberOfAllowedWriteInsPlusOne);
		}
	}
}
