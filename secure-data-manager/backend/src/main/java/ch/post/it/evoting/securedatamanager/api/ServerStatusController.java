/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.OrientManager;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * The server status end-point.
 */
@RestController
@RequestMapping("/sdm-backend")
@Api(value = "Server status REST API")
public class ServerStatusController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ServerStatusController.class);

	private static final String OPEN_STATUS = "OPEN";
	private static final String CLOSED_STATUS = "CLOSED";

	private final OrientManager manager;
	private final boolean isVotingPortalEnabled;
	private final boolean isTally;
	private final boolean isSetup;
	private final String serverModeName;

	public ServerStatusController(
			final OrientManager manager,

			@Value("${voting.portal.enabled}")
			final boolean isVotingPortalEnabled,

			@Value("${role.isTally}")
			final boolean isTally,

			@Value("${role.isSetup}")
			final boolean isSetup) {

		this.manager = manager;
		this.isVotingPortalEnabled = isVotingPortalEnabled;
		this.isTally = isTally;
		this.isSetup = isSetup;

		this.serverModeName = getServerMode().name();
		final String serverModeTesting = ServerMode.SERVER_MODE_TESTING.name();
		if (this.serverModeName.equals(serverModeTesting)) {
			LOGGER.warn("Server mode is {}. This should only be used for testing purposes. Ensure this is the expected running mode before going any "
							+ "further.", serverModeTesting);
		} else {
			LOGGER.info("Server mode is {}.", serverModeName);
		}
	}

	/**
	 * Returns the server status [{@value ServerStatusController#OPEN_STATUS} | {@value ServerStatusController#CLOSED_STATUS}] and server mode
	 * [{@link ServerMode#SERVER_MODE_ONLINE} | {@link ServerMode#SERVER_MODE_SETUP} |	 {@link ServerMode#SERVER_MODE_TALLY} |
	 * {@link ServerMode#SERVER_MODE_TESTING}].
	 */
	@GetMapping(value = "/status", produces = "application/json")
	@ResponseStatus(value = HttpStatus.OK)
	@ApiOperation(value = "Health Check Service", response = String.class, notes = "Service to validate application is up & running.")
	public String getStatus() {
		final JsonObjectBuilder builder = Json.createObjectBuilder();
		final String status = manager.isActive() ? OPEN_STATUS : CLOSED_STATUS;
		builder.add(JsonConstants.STATUS, status);
		builder.add(JsonConstants.SERVER_MODE, serverModeName);
		return builder.build().toString();
	}

	/**
	 * Closes the database.
	 */
	@PostMapping(value = "/close", produces = "application/json")
	@ResponseStatus(value = HttpStatus.OK)
	@ApiOperation(value = "Close Database", response = String.class, notes = "Service to close the database.")
	public String closeDatabase() {
		manager.shutdown();
		final JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add(JsonConstants.STATUS, CLOSED_STATUS);

		return builder.build().toString();
	}

	private ServerMode getServerMode() {
		if (isTally && isSetup) {
			return ServerMode.SERVER_MODE_TESTING;
		}
		if (isTally && !isVotingPortalEnabled) {
			return ServerMode.SERVER_MODE_TALLY;
		}
		if (isSetup && !isVotingPortalEnabled) {
			return ServerMode.SERVER_MODE_SETUP;
		}
		if (!isTally && !isSetup && isVotingPortalEnabled) {
			return ServerMode.SERVER_MODE_ONLINE;
		}

		throw new IllegalStateException(String.format(
				"Application has an unexpected configuration. [isVotingPortalEnabled: %s, isTally: %s, isSetup: %s]", isVotingPortalEnabled, isTally,
				isSetup));
	}
}
