/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.UncheckedIOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationDataPayload;
import ch.post.it.evoting.securedatamanager.commons.ChunkedRequestBodyCreator;
import ch.post.it.evoting.securedatamanager.commons.RemoteService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.VoteVerificationClient;

import okhttp3.RequestBody;

@Service
public class SetupComponentVoterAuthenticationPayloadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentVoterAuthenticationPayloadService.class);

	private final ObjectMapper objectMapper;
	private final boolean isVotingPortalEnabled;
	private final VoteVerificationClient voteVerificationClient;
	private final SetupComponentVoterAuthenticationPayloadFileRepository setupComponentVoterAuthenticationPayloadFileRepository;

	public SetupComponentVoterAuthenticationPayloadService(
			final ObjectMapper objectMapper,
			@Value("${voting.portal.enabled}")
			final boolean isVotingPortalEnabled,
			final VoteVerificationClient voteVerificationClient,
			final SetupComponentVoterAuthenticationPayloadFileRepository setupComponentVoterAuthenticationPayloadFileRepository) {
		this.objectMapper = objectMapper;
		this.isVotingPortalEnabled = isVotingPortalEnabled;
		this.voteVerificationClient = voteVerificationClient;
		this.setupComponentVoterAuthenticationPayloadFileRepository = setupComponentVoterAuthenticationPayloadFileRepository;
	}

	/**
	 * Saves a setup component voter authentication payload in the corresponding verification card set folder.
	 *
	 * @param setupComponentVoterAuthenticationDataPayload the voter authentication payload to save.
	 * @throws NullPointerException if {@code setupComponentVoterAuthenticationDataPayload} is null.
	 */
	public void save(final SetupComponentVoterAuthenticationDataPayload setupComponentVoterAuthenticationDataPayload) {
		checkNotNull(setupComponentVoterAuthenticationDataPayload);

		final String electionEventId = setupComponentVoterAuthenticationDataPayload.getElectionEventId();
		final String verificationCardSetId = setupComponentVoterAuthenticationDataPayload.getVerificationCardSetId();

		setupComponentVoterAuthenticationPayloadFileRepository.save(setupComponentVoterAuthenticationDataPayload);
		LOGGER.info("Saved setup component voter authentication payload. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);
	}

	/**
	 * Loads the setup component voter authentication payload for the given election event id and verification card set id.
	 *
	 * @param electionEventId       the election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId the verification card set id. Must be non-null and a valid UUID.
	 * @return the setup component voter authentication payload for this {@code electionEventId} and {@code verificationCardSetId}.
	 * @throws NullPointerException      if {@code electionEventId} or {@code verificationCardSetId} is null.
	 * @throws FailedValidationException if {@code electionEventId} or {@code verificationCardSetId} is not a valid UUID.
	 */
	public SetupComponentVoterAuthenticationDataPayload load(final String electionEventId, final String verificationCardSetId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		return setupComponentVoterAuthenticationPayloadFileRepository.findByElectionEventIdAndVerificationCardSetId(electionEventId,
						verificationCardSetId)
				.orElseThrow(() -> new IllegalStateException(String.format(
						"Requested setup component voter authentication payload is not present. [electionEventId: %s, verificationCardSetId: %s]",
						electionEventId, verificationCardSetId)));
	}

	/**
	 * Uploads the setup component voter authentication payload corresponding to the given election event id and verification card set id to the
	 * control components through the message broker orchestrator. If the election event id is empty, the upload is not done.
	 *
	 * @param electionEventId       the election event id. Must be non-null. If the election event id is not empty, it must be a valid UUID.
	 * @param verificationCardSetId the verification card set id. Must be non-null and a valid UUID.
	 * @throws NullPointerException      if {@code electionEventId} or {@code verificationCardSetId} is null.
	 * @throws FailedValidationException if {@code electionEventId} is not empty and not a valid UUID or if {@code verificationCardSetId} is not a
	 *                                   valid UUID.
	 */
	public void upload(final String electionEventId, final String verificationCardSetId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkState(isVotingPortalEnabled, "The voting-portal connection is not enabled.");

		final SetupComponentVoterAuthenticationDataPayload setupComponentVoterAuthenticationDataPayload = load(electionEventId,
				verificationCardSetId);
		final byte[] payloadBytes;
		try {
			payloadBytes = objectMapper.writeValueAsBytes(setupComponentVoterAuthenticationDataPayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(
					String.format("Failed to serialize voter authentication payload. [electionEventId: %s, verificationCardSetId: %s]",
							electionEventId, verificationCardSetId), e);
		}

		final RequestBody requestBody = ChunkedRequestBodyCreator.forJsonPayload(payloadBytes);

		RemoteService.call(voteVerificationClient.uploadVoterAuthenticationData(electionEventId, verificationCardSetId, requestBody))
				.networkErrorMessage("Failed to communicate with vote verification.")
				.unsuccessfulResponseErrorMessage(
						"Request for uploading voter authentication payloads failed. [electionEventId: %s, verificationCardSetId: %s]",
						electionEventId, verificationCardSetId)
				.execute();

		LOGGER.info("Successfully uploaded voter authentication payload. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);
	}

}
