/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

/**
 * Allows saving and retrieving the primes mapping table associated to an election event and verification card set.
 */
@Repository
class PrimesMappingTableFileRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(PrimesMappingTableFileRepository.class);
	private static final String PRIMES_MAPPING_TABLE_FILE_NAME = "primesMappingTable.json";

	private final ObjectMapper objectMapper;
	private final PathResolver pathResolver;

	PrimesMappingTableFileRepository(
			final ObjectMapper objectMapper,
			final PathResolver pathResolver) {
		this.objectMapper = objectMapper;
		this.pathResolver = pathResolver;
	}

	/**
	 * Persists a primes mapping table to the file system, in the {@code OFFLINE/primesMappingTables/{verificationCardSetId}} path.
	 *
	 * @param primesMappingTableFileEntity the primes mapping table to save.
	 * @return the path where the primes mapping table was successfully saved.
	 * @throws NullPointerException if {@code primesMappingTableFileEntity} is null.
	 * @throws UncheckedIOException if
	 *                              <ul>
	 *                                  <li>creating the save directory fails for the serialization of the primes mapping table fails.</li>
	 *                                  <li>the serialization of the primes mapping table fails.</li>
	 *                              </ul>
	 */
	Path save(final PrimesMappingTableFileEntity primesMappingTableFileEntity) {
		checkNotNull(primesMappingTableFileEntity);

		final String electionEventId = primesMappingTableFileEntity.electionEventId();
		final String verificationCardSetId = primesMappingTableFileEntity.verificationCardSetId();

		final Path verificationCardSetIdPath = pathResolver.resolvePrimesMappingTablesPath(electionEventId).resolve(verificationCardSetId);
		try {
			Files.createDirectories(verificationCardSetIdPath);
		} catch (final IOException e) {
			throw new UncheckedIOException(String.format("Failed to create primes mapping table directory. [path: %s]", verificationCardSetIdPath),
					e);
		}

		final Path primesMappingTablePath = verificationCardSetIdPath.resolve(PRIMES_MAPPING_TABLE_FILE_NAME);
		try {
			final byte[] primesMappingTableEntityBytes = objectMapper.writeValueAsBytes(primesMappingTableFileEntity);

			final Path writePath = Files.write(primesMappingTablePath, primesMappingTableEntityBytes);
			LOGGER.debug("Successfully persisted primes mapping table. [electionEventId: {}, verificationCardSetId: {}, path: {}]", electionEventId,
					verificationCardSetId, primesMappingTablePath);

			return writePath;
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to serialize primes mapping table entity. [electionEventId: %s, verificationCardSetId: %s, path: %s]",
							electionEventId, verificationCardSetId, primesMappingTablePath), e);
		}
	}

	/**
	 * Retrieves from the file system the primes mapping table for this {@code electionEventId} and {@code verificationCardSetId}.
	 *
	 * @param electionEventId       the corresponding election event id.
	 * @param verificationCardSetId the corresponding verificationCardSetId.
	 * @return the primes mapping table for the given ids or {@link Optional#empty} if none found.
	 * @throws FailedValidationException if either {@code electionEventId} or {@code verificationCardSetId}.
	 * @throws UncheckedIOException      if the deserialization of the primes mapping table fails.
	 */
	Optional<PrimesMappingTableFileEntity> findByVerificationCardSetId(final String electionEventId, final String verificationCardSetId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		final Path primesMappingTablePath = pathResolver.resolvePrimesMappingTablesPath(electionEventId).resolve(verificationCardSetId)
				.resolve(PRIMES_MAPPING_TABLE_FILE_NAME);

		if (!Files.exists(primesMappingTablePath)) {
			LOGGER.debug("Requested primes mapping table does not exist. [electionEventId: {}, verificationCardSetId: {}, path: {}]", electionEventId,
					verificationCardSetId, primesMappingTablePath);
			return Optional.empty();
		}

		try {
			return Optional.of(objectMapper.readValue(primesMappingTablePath.toFile(), PrimesMappingTableFileEntity.class));
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to deserialize primes mapping table entity. [electionEventId: %s, verificationCardSetId: %s, path: %s]",
							electionEventId, verificationCardSetId, primesMappingTablePath), e);
		}
	}

	/**
	 * Retrieves from the file system all the primes mapping table for this {@code electionEventId}.
	 *
	 * @param electionEventId the corresponding election event id.
	 * @return the list of primes mapping table for the given id or an empty list if none found.
	 * @throws FailedValidationException if either {@code electionEventId} or {@code verificationCardSetId}.
	 * @throws UncheckedIOException      if the deserialization of the primes mapping table fails.
	 */
	List<PrimesMappingTableFileEntity> findAll(final String electionEventId) {
		validateUUID(electionEventId);

		final List<Path> verificationCarSetIdPaths;
		final Path primesMappingTablesPath = pathResolver.resolvePrimesMappingTablesPath(electionEventId);

		try (final Stream<Path> paths = Files.walk(primesMappingTablesPath, 1).parallel()) {
			verificationCarSetIdPaths = paths
					.filter(path -> !primesMappingTablesPath.equals(path))
					.filter(Files::isDirectory)
					.toList();
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Unable to find primes mapping table entity. [electionEventId: %s, path: %s]", electionEventId,
							primesMappingTablesPath), e);
		}

		return verificationCarSetIdPaths.stream().parallel()
				.map(verificationCarSetIdPath -> findByVerificationCardSetId(electionEventId,
						verificationCarSetIdPath.getFileName().toString()))
				.flatMap(Optional::stream)
				.toList();
	}

}
