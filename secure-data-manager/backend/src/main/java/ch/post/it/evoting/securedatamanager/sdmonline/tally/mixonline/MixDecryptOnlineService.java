/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmonline.tally.mixonline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.UncheckedIOException;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.domain.tally.MixDecryptOnlinePayload;
import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;
import ch.post.it.evoting.securedatamanager.commons.RemoteService;
import ch.post.it.evoting.securedatamanager.sdmcommon.tally.ControlComponentBallotBoxPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.sdmcommon.tally.ControlComponentShufflePayloadFileRepository;
import ch.post.it.evoting.securedatamanager.services.application.service.BallotBoxService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.MessageBrokerOrchestratorClient;

import retrofit2.Response;

@Service
public class MixDecryptOnlineService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MixDecryptOnlineService.class);
	private static final String COMMUNICATION_FAILURE_MESSAGE = "Failed to communicate with orchestrator.";
	private static final String VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE = "The voting-portal connection is not enabled.";
	private final BallotBoxService ballotBoxService;
	private final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;
	private final ControlComponentShufflePayloadFileRepository controlComponentShufflePayloadFileRepository;
	private final ControlComponentBallotBoxPayloadFileRepository controlComponentBallotBoxPayloadFileRepository;
	private final boolean isVotingPortalEnabled;

	public MixDecryptOnlineService(
			final BallotBoxService ballotBoxService,
			final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient,
			final ControlComponentShufflePayloadFileRepository controlComponentShufflePayloadFileRepository,
			final ControlComponentBallotBoxPayloadFileRepository controlComponentBallotBoxPayloadFileRepository,
			@Value("${voting.portal.enabled}")
			final boolean isVotingPortalEnabled) {
		this.ballotBoxService = ballotBoxService;
		this.messageBrokerOrchestratorClient = messageBrokerOrchestratorClient;
		this.controlComponentShufflePayloadFileRepository = controlComponentShufflePayloadFileRepository;
		this.controlComponentBallotBoxPayloadFileRepository = controlComponentBallotBoxPayloadFileRepository;
		this.isVotingPortalEnabled = isVotingPortalEnabled;
	}

	/**
	 * Requests the control-components to mix the ballot box with given id {@code ballotBoxId}.
	 *
	 * @param electionEventId the election event id of the ballot box.
	 * @param ballotBoxId     the id of the ballot box to mix.
	 * @return {@code true} if the mixing was started and {@code false} if it hasn't because the ballot box was not yet closed.
	 * @throws UncheckedIOException if the communication with the orchestrator fails.
	 */
	public boolean startOnlineMixing(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		final boolean isTestBallotBox = ballotBoxService.isTestBallotBox(ballotBoxId);
		final LocalDateTime ballotBoxFinishDate = ballotBoxService.getDateTo(ballotBoxId);

		if (!isTestBallotBox && LocalDateTime.now().isBefore(ballotBoxFinishDate)) {
			LOGGER.error("The ballot box is not yet closed and cannot be mixed. [electionEventId: {}, ballotBoxId: {}]", electionEventId,
					ballotBoxId);
			return false;
		}

		RemoteService.call(messageBrokerOrchestratorClient.createMixDecryptOnline(electionEventId, ballotBoxId))
				.networkErrorMessage(COMMUNICATION_FAILURE_MESSAGE)
				.unsuccessfulResponseErrorMessage("Failed to start online mixing. [electionEventId: {}, ballotBoxId: {}]", electionEventId,
						ballotBoxId)
				.execute();

		ballotBoxService.updateBallotBoxStatus(ballotBoxId, BallotBoxStatus.MIXING);
		LOGGER.info("Ballot box status updated. [ballotBoxId: {}, status: {}", ballotBoxId, BallotBoxStatus.MIXING);

		return true;
	}

	/**
	 * Gets the status of the ballot box with id {@code ballotBoxId} from the control-components.
	 *
	 * @param electionEventId the election event id of the ballot box.
	 * @param ballotBoxId     the id of the ballot box to get the status.
	 * @return the status of the ballot box.
	 * @throws UncheckedIOException  if the communication with the orchestrator fails.
	 * @throws IllegalStateException if the response from the orchestrator was unsuccessful.
	 */
	public BallotBoxStatus getOnlineStatus(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		final Response<BallotBoxStatus> response =
				RemoteService.call(messageBrokerOrchestratorClient.checkMixDecryptOnlineStatus(electionEventId, ballotBoxId))
						.networkErrorMessage(COMMUNICATION_FAILURE_MESSAGE)
						.unsuccessfulResponseErrorMessage("Get online status unsuccessful. [ballotBoxId: %s]", ballotBoxId)
						.execute();

		checkNotNull(response.body(), "Get online status returned a null response. [ballotBoxId: %s]", ballotBoxId);

		return response.body();
	}

	/**
	 * Downloads and persists the outputs generated when mixing the requested ballot box on the online control component nodes.
	 *
	 * @param electionEventId the election event id.
	 * @param ballotBoxId     the ballot box id.
	 * @throws IllegalArgumentException         if the ballot box is not {@link BallotBoxStatus#MIXED}.
	 * @throws UncheckedIOException             if the communication with the orchestrator fails.
	 * @throws IllegalStateException            if the response from the orchestrator was unsuccessful.
	 * @throws InvalidPayloadSignatureException if the signature of the response's content is invalid.
	 */
	@Transactional
	public void downloadOnlineMixnetPayloads(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		LOGGER.info("Requesting payloads for ballot box... [ballotBoxId: {}]", ballotBoxId);

		// Check that the full ballot box has been mixed.
		if (!ballotBoxService.hasStatus(ballotBoxId, BallotBoxStatus.MIXED)) {
			throw new IllegalArgumentException(String.format("Ballot box is not mixed [ballotBoxId : %s]", ballotBoxId));
		}

		final Response<MixDecryptOnlinePayload> response =
				RemoteService.call(messageBrokerOrchestratorClient.downloadMixDecryptOnline(electionEventId, ballotBoxId))
						.networkErrorMessage(COMMUNICATION_FAILURE_MESSAGE)
						.unsuccessfulResponseErrorMessage("Download unsuccessful. [ballotBoxId: {}]", ballotBoxId)
						.execute();

		checkNotNull(response.body(), "Downloaded content is null. [ballotBoxId: {}]", ballotBoxId);

		final String ballotId = ballotBoxService.getBallotId(ballotBoxId);
		final List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads = response.body().controlComponentBallotBoxPayloads();
		final List<ControlComponentShufflePayload> controlComponentShufflePayloads = response.body().controlComponentShufflePayloads();

		// Check consistency of ballot box ID and election event ID
		controlComponentBallotBoxPayloads.forEach(ballotBoxPayload -> checkState(ballotBoxPayload.getBallotBoxId().equals(ballotBoxId),
				"The controlComponentBallotBoxPayload's ballot box id must correspond to the ballot box id of the request"));
		controlComponentBallotBoxPayloads.forEach(ballotBoxPayload -> checkState(ballotBoxPayload.getElectionEventId().equals(electionEventId),
				"The controlComponentBallotBoxPayload's election event id must correspond to the election event id of the request"));

		controlComponentShufflePayloads.forEach(shufflePayload -> checkState(shufflePayload.getBallotBoxId().equals(ballotBoxId),
				"The controlComponentShufflePayload's ballot box id must correspond to the ballot box id of the request"));
		controlComponentShufflePayloads.forEach(shufflePayload -> checkState(shufflePayload.getElectionEventId().equals(electionEventId),
				"The controlComponentShufflePayload's election event id must correspond to the election event id of the request"));

		// Save mixnet payloads.
		controlComponentBallotBoxPayloadFileRepository.saveAll(ballotId, controlComponentBallotBoxPayloads);
		LOGGER.info("Confirmed encrypted votes payloads successfully stored. [electionEventId:{}, ballotId:{}, ballotBoxId:{}]", electionEventId,
				ballotId, ballotBoxId);

		controlComponentShufflePayloads.forEach(payload -> controlComponentShufflePayloadFileRepository.savePayload(ballotId, payload));
		LOGGER.info("Control component shuffle payloads successfully stored. [electionEventId:{}, ballotId:{}, ballotBoxId:{}]", electionEventId,
				ballotId, ballotBoxId);

		ballotBoxService.updateBallotBoxStatus(ballotBoxId, BallotBoxStatus.DOWNLOADED);
		LOGGER.info("Ballot box status updated. [electionEventId: {}, ballotId: {}, ballotBoxId: {}, status: {}]", BallotBoxStatus.DOWNLOADED,
				electionEventId, ballotId, ballotBoxId);
	}

}
