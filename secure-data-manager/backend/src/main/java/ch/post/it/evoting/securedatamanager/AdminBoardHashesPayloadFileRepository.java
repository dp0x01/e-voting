/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.VisibleForTesting;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.AdminBoardHashesPayload;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

/**
 * Allows performing operations with the admin board hashes payload. The admin board hashes payload is persisted/retrieved to/from the file system of
 * the SDM, in its workspace.
 */
@Repository
public class AdminBoardHashesPayloadFileRepository {
	@VisibleForTesting
	static final String PAYLOAD_FILE_NAME = Constants.CONFIG_SETUP_COMPONENT_ADMIN_BOARD_HASHES_PAYLOAD;

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminBoardHashesPayloadFileRepository.class);

	private final ObjectMapper objectMapper;
	private final PathResolver pathResolver;

	public AdminBoardHashesPayloadFileRepository(final ObjectMapper objectMapper, final PathResolver pathResolver) {
		this.objectMapper = objectMapper;
		this.pathResolver = pathResolver;
	}

	/**
	 * Persists an admin board hashes payload to the file system.
	 *
	 * @param adminBoardHashesPayload the admin board hashes payload to persist. Must be non-null.
	 * @return the path where the admin board hashes payload has been successfully persisted.
	 * @throws NullPointerException if {@code adminBoardHashesPayload} is null.
	 * @throws UncheckedIOException if the serialization of the admin board hashes payload fails.
	 */
	public Path save(final AdminBoardHashesPayload adminBoardHashesPayload) {
		checkNotNull(adminBoardHashesPayload);

		final String adminBoardId = adminBoardHashesPayload.getAdminBoardId();

		final Path adminBoardPath = pathResolver.resolveAdminBoardPath(adminBoardId);
		createAdminBoardsDirectory(adminBoardPath);

		final Path payloadPath = adminBoardPath.resolve(PAYLOAD_FILE_NAME);

		try {
			final byte[] payloadBytes = objectMapper.writeValueAsBytes(adminBoardHashesPayload);

			final Path writePath = Files.write(payloadPath, payloadBytes);
			LOGGER.debug("Successfully persisted admin board hashes payload. [adminBoardId: {}, path: {}]", adminBoardId, payloadPath);

			return writePath;
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to serialize admin board hashes payload. [adminBoardId: %s, path: %s]", adminBoardId, payloadPath),
					e);
		}
	}

	/**
	 * Checks if the admin board hashes payload file exists for the given {@code adminBoardId}.
	 *
	 * @param adminBoardId the admin board id to check. Must be non-null and a valid UUID.
	 * @return {@code true} if the admin board hashes payload file exists, {@code false} otherwise.
	 * @throws FailedValidationException if {@code adminBoardId} is null or not a valid UUID.
	 */
	public boolean existsById(final String adminBoardId) {
		validateUUID(adminBoardId);

		final Path payloadPath = pathResolver.resolveAdminBoardPath(adminBoardId).resolve(PAYLOAD_FILE_NAME);
		LOGGER.debug("Checking admin board hashes payload file existence. [adminBoardId: {}, path: {}]", adminBoardId, payloadPath);

		return Files.exists(payloadPath);
	}

	/**
	 * Retrieves from the file system an admin board hashes payload by {@code adminBoardId}.
	 *
	 * @param adminBoardId the admin board id. Must be non-null and a valid UUID.
	 * @return the admin board hashes payload with the given id or {@link Optional#empty} if none found.
	 * @throws FailedValidationException if {@code adminBoardId} is null or not a valid UUID.
	 * @throws UncheckedIOException      if the deserialization of the admin board hashes payload fails.
	 */
	public Optional<AdminBoardHashesPayload> findById(final String adminBoardId) {
		validateUUID(adminBoardId);

		final Path payloadPath = pathResolver.resolveAdminBoardPath(adminBoardId).resolve(PAYLOAD_FILE_NAME);

		if (!Files.exists(payloadPath)) {
			LOGGER.debug("Requested admin board hashes payload does not exist. [adminBoardId: {}, path: {}]", adminBoardId, payloadPath);
			return Optional.empty();
		}

		try {
			return Optional.of(objectMapper.readValue(payloadPath.toFile(), AdminBoardHashesPayload.class));
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to deserialize admin board hashes payload. [adminBoardId: %s, path: %s]", adminBoardId,
							payloadPath), e);
		}
	}

	private void createAdminBoardsDirectory(final Path path) {
		try {
			Files.createDirectories(path);
		} catch (final IOException e) {
			throw new UncheckedIOException(String.format("Unable to create the adminBoard directory. [path: %s]", path), e);
		}
	}

}
