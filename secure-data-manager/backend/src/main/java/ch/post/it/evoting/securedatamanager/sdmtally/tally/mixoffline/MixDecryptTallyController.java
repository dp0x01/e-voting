/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.services.application.exception.CheckedIllegalStateException;
import ch.post.it.evoting.securedatamanager.services.application.service.IdleStatusService;
import ch.post.it.evoting.securedatamanager.services.domain.common.IdleState;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/sdm-backend/mixing")
@Api(value = "Mixing REST API")
@ConditionalOnProperty("role.isTally")
public class MixDecryptTallyController {
	private static final Logger LOGGER = LoggerFactory.getLogger(MixDecryptTallyController.class);

	private final MixOfflineFacade mixOfflineFacade;
	private final IdleStatusService idleStatusService;
	private final ElectoralBoardTallyService electoralBoardTallyService;

	public MixDecryptTallyController(
			final MixOfflineFacade mixOfflineFacade,
			final IdleStatusService idleStatusService,
			final ElectoralBoardTallyService electoralBoardTallyService) {
		this.mixOfflineFacade = mixOfflineFacade;
		this.idleStatusService = idleStatusService;
		this.electoralBoardTallyService = electoralBoardTallyService;
	}

	/**
	 * Performs the offline mixing of a specific ballot box
	 */
	@PutMapping(value = "/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/mixoffline", produces = "application/json")
	@ResponseBody
	@ApiOperation(value = "Mix a ballot box offline", notes = "Service to mix a given ballot box offline.", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 403, message = "Forbidden"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	public ResponseEntity<IdleState> mixOffline(
			@ApiParam(
					name = "electionEventId",
					type = "String",
					value = "Unique identifier for election event",
					example = "bf731ed5b4f24cf8a926d744015118ae",
					required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(
					name = "ballotBoxId",
					type = "String",
					value = "Unique identifier for ballot box",
					example = "51a21bbc884749fca630b8037cd29424",
					required = true)
			@PathVariable
			final String ballotBoxId,
			@ApiParam(value = "electoralBoardPasswords", required = true)
			@RequestBody
			final List<char[]> electoralBoardPasswords) {

		try {
			validateUUID(electionEventId);
			validateUUID(ballotBoxId);
			checkNotNull(electoralBoardPasswords);
			checkArgument(electoralBoardPasswords.size() >= 2);

			IntStream.range(0, electoralBoardPasswords.size())
					.forEach(memberIndex -> checkArgument(
							electoralBoardTallyService.verifyElectoralBoardMemberPassword(electionEventId, memberIndex,
									electoralBoardPasswords.get(memberIndex))));
		} catch (final NullPointerException | IllegalArgumentException | FailedValidationException e) {
			LOGGER.warn("Some of the received parameters are invalid.");
			return ResponseEntity.badRequest().build();
		}

		try {
			// If the ballot box is already being operated on, exit immediately.
			final IdleState idleState = new IdleState();
			if (!(idleStatusService.getIdLock(ballotBoxId))) {
				idleState.setIdle(true);
				return ResponseEntity.ok(idleState);
			}

			mixOfflineFacade.mixOffline(electionEventId, ballotBoxId, electoralBoardPasswords);
		} catch (final CheckedIllegalStateException e) {
			LOGGER.error("The ballot box cannot be mixed", e);
			idleStatusService.freeIdLock(ballotBoxId);
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} finally {
			idleStatusService.freeIdLock(ballotBoxId);
		}

		return ResponseEntity.ok().build();
	}
}
