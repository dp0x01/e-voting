/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.domain.service;

public class ElectionConfigDefaults {

   static final Integer CERTIFICATES_VALIDITY_PERIOD_DEFAULT = 1;
   static final String CHALLENGE_LENGTH_DEFAULT = "16";
   static final String CHALLENGE_RESPONSE_EXPIRATION_TIME_DEFAULT = "2000";
   static final String NUMBER_VOTES_PER_VOTING_CARD_DEFAULT = "1";
   static final String NUMBER_VOTES_PER_AUTH_TOKEN_DEFAULT = "1" ;
   static final String NAME_MAXIMUM_NUMBER_OF_ATTEMPTS_DEFAULT = "5";
   static final String AUTH_TOKEN_EXPIRATION_TIME_DEFAULT = "72000"; // 20 hours in seconds

   private ElectionConfigDefaults() {
      super();
   }
}
