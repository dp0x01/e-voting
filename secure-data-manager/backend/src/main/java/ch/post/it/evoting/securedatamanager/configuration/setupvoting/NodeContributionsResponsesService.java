/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.nio.file.Path;
import java.security.SignatureException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;

/**
 * Service loading the chunk-wise contributions of all control component nodes. For performance reasons, the GenVerDat algorithm splits the entire
 * verification card set into smaller pieces (a process called chunking).
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class NodeContributionsResponsesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(NodeContributionsResponsesService.class);

	private final SignatureKeystore<Alias> signatureKeystoreService;

	private final NodeContributionsResponsesFileRepository nodeContributionsResponsesFileRepository;

	public NodeContributionsResponsesService(
			@Qualifier("keystoreServiceSdmConfig")
			final SignatureKeystore<Alias> signatureKeystoreService,
			final NodeContributionsResponsesFileRepository nodeContributionsResponsesFileRepository) {
		this.signatureKeystoreService = signatureKeystoreService;
		this.nodeContributionsResponsesFileRepository = nodeContributionsResponsesFileRepository;
	}

	/**
	 * Loads all node contributions response chunk paths for the given {@code electionEventId} and {@code verificationCardSetId}.
	 *
	 * @param electionEventId       the node contributions responses' election event id.
	 * @param verificationCardSetId the node contributions responses' verification card set id.
	 * @return all node contributions response chunk paths.
	 * @throws FailedValidationException if {@code electionEventId} or {@code verificationCardSetId} is invalid.
	 */
	public List<Path> loadAllChunkPaths(final String electionEventId, final String verificationCardSetId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		LOGGER.debug("Loading all node contributions response chunk paths. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);

		return nodeContributionsResponsesFileRepository.findAllPathsOrderByChunkId(electionEventId, verificationCardSetId);
	}

	public int getChunkId(final Path path) {
		return nodeContributionsResponsesFileRepository.getChunkId(path);
	}

	/**
	 * Loads a node contributions response chunk for the given {@code path}.
	 *
	 * @param path the node contributions response chunk's path. Must be not null.
	 * @return a node contributions response chunk.
	 */
	public NodeContributionsChunk loadNodeContributionsChunk(final Path path) {
		checkNotNull(path);

		LOGGER.debug("Loading a node contributions response chunk. [path: {}]", path);

		final List<ControlComponentCodeSharesPayload> controlComponentCodeSharesPayloads = nodeContributionsResponsesFileRepository.getControlComponentCodeSharesPayloads(
				path);
		final int chunkId = controlComponentCodeSharesPayloads.get(0).getChunkId();
		final String electionEventId = controlComponentCodeSharesPayloads.get(0).getElectionEventId();
		final String verificationCardSetId = controlComponentCodeSharesPayloads.get(0).getVerificationCardSetId();
		checkState(controlComponentCodeSharesPayloads.parallelStream().allMatch(this::verifySignature),
				"All return code generation response payloads must have a valid signature. [electionEventId: {}, verificationCardSetId: {}, chunkId: {}]",
				electionEventId, verificationCardSetId, chunkId);
		return new NodeContributionsChunk(controlComponentCodeSharesPayloads, chunkId);
	}

	private boolean verifySignature(final ControlComponentCodeSharesPayload payload) {
		final int nodeId = payload.getNodeId();
		final String electionEventId = payload.getElectionEventId();
		final String verificationCardSetId = payload.getVerificationCardSetId();
		final int chunkId = payload.getChunkId();

		final CryptoPrimitivesSignature signature = payload.getSignature();

		checkState(signature != null,
				"The signature of the control component code shares payload is null. [nodeId: %s, electionEventId: %s, verificationCardSetId: %s, chunkId: %s]",
				nodeId, electionEventId, verificationCardSetId, chunkId);

		try {
			return signatureKeystoreService.verifySignature(Alias.getControlComponentByNodeId(nodeId), payload,
					ChannelSecurityContextData.controlComponentCodeShares(nodeId, electionEventId, verificationCardSetId),
					signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format(
					"Cannot verify the signature of the control component code shares payload. [nodeId: %s, electionEventId: %s, verificationCardSetId: %s, chunkId: %s]",
					nodeId, electionEventId, verificationCardSetId, chunkId), e);
		}
	}

	record NodeContributionsChunk(List<ControlComponentCodeSharesPayload> contributions, int chunkId) {
	}

}
