/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.hasNoDuplicates;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionEventContext;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.TallyComponentShufflePayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.configuration.SetupComponentTallyDataPayload;
import ch.post.it.evoting.securedatamanager.SetupComponentTallyDataPayloadService;
import ch.post.it.evoting.securedatamanager.configuration.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.configuration.SetupComponentPublicKeysPayloadService;
import ch.post.it.evoting.securedatamanager.services.application.exception.CheckedIllegalStateException;
import ch.post.it.evoting.securedatamanager.services.application.service.BallotBoxService;

/**
 * Handles the offline mixing steps.
 */
@Service
@ConditionalOnProperty("role.isTally")
public class MixOfflineFacade {

	private static final Logger LOGGER = LoggerFactory.getLogger(MixOfflineFacade.class);

	private final BallotBoxService ballotBoxService;
	private final BallotTallyService ballotTallyService;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;
	private final SetupComponentPublicKeysPayloadService setupComponentPublicKeysPayloadService;
	private final MixDecOfflineService mixDecOfflineService;
	private final ProcessPlaintextsService processPlaintextsService;
	private final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService;
	private final SignatureKeystore<Alias> signatureKeystoreServiceSdmTally;
	private final VerifyMixOfflineService verifyMixOfflineService;
	private final EncryptionParametersTallyService encryptionParametersTallyService;

	@Autowired
	MixOfflineFacade(final BallotBoxService ballotBoxService,
			final BallotTallyService ballotTallyService,
			final ElectionEventContextPayloadService electionEventContextPayloadService,
			final SetupComponentPublicKeysPayloadService setupComponentPublicKeysPayloadService,
			final MixDecOfflineService mixDecOfflineService,
			final ProcessPlaintextsService processPlaintextsService,
			final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService,
			@Qualifier("keystoreServiceSdmTally")
			final SignatureKeystore<Alias> signatureKeystoreServiceSdmTally,
			final VerifyMixOfflineService verifyMixOfflineService,
			final EncryptionParametersTallyService encryptionParametersTallyService) {
		this.ballotBoxService = ballotBoxService;
		this.ballotTallyService = ballotTallyService;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
		this.setupComponentPublicKeysPayloadService = setupComponentPublicKeysPayloadService;
		this.mixDecOfflineService = mixDecOfflineService;
		this.processPlaintextsService = processPlaintextsService;
		this.setupComponentTallyDataPayloadService = setupComponentTallyDataPayloadService;
		this.signatureKeystoreServiceSdmTally = signatureKeystoreServiceSdmTally;
		this.verifyMixOfflineService = verifyMixOfflineService;
		this.encryptionParametersTallyService = encryptionParametersTallyService;
	}

	/**
	 * Coordinates the offline mixing: mixing, decryption, factorisation and persistence.
	 *
	 * @param electionEventId                the id of the election event for which we want to mix a ballot box. Must be non-null and a valid UUID.
	 * @param ballotBoxId                    the id of the ballot box to mix. Must be non-null and a valid UUID.
	 * @param electoralBoardMembersPasswords the list of electoral board members' password. Not null
	 * @throws CheckedIllegalStateException if the ballot box has not been downloaded prior.
	 */
	public void mixOffline(final String electionEventId, final String ballotBoxId, final List<char[]> electoralBoardMembersPasswords)
			throws CheckedIllegalStateException {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);
		checkNotNull(electoralBoardMembersPasswords);
		checkArgument(electoralBoardMembersPasswords.size() >= 2);

		final String ballotId = ballotBoxService.getBallotId(ballotBoxId);
		final ElectionEventContextPayload electionEventContextPayload = loadElectionEventContextPayload(electionEventId);

		validateConsistency(electionEventId, ballotId, ballotBoxId);

		final ElectionEventContext electionEventContext = electionEventContextPayload.getElectionEventContext();
		final VerificationCardSetContext verificationCardSetContext = electionEventContext.verificationCardSetContexts().stream()
				.filter(vcsContext -> vcsContext.ballotBoxId().equals(ballotBoxId))
				.collect(MoreCollectors.onlyElement());

		final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload = loadSetupComponentPublicKeysPayload(electionEventId);
		final SetupComponentPublicKeys setupComponentPublicKeys = setupComponentPublicKeysPayload.getSetupComponentPublicKeys();

		final Ballot ballot = ballotTallyService.getBallot(electionEventId, ballotId);
		final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(ballot);
		final int numberOfSelectableVotingOptions = combinedCorrectnessInformation.getTotalNumberOfSelections();

		final int numberOfAllowedWriteInsPlusOne = verificationCardSetContext.numberOfWriteInFields() + 1;

		final String verificationCardSetId = verificationCardSetContext.verificationCardSetId();
		final SetupComponentTallyDataPayload setupComponentTallyDataPayload = loadSetupComponentTallyDataPayload(electionEventId,
				verificationCardSetId);

		final ControlComponentShufflePayload lastNodePayload = verifyMixOfflineService.verifyMixDecrypt(electionEventId, ballotId, ballotBoxId,
				numberOfSelectableVotingOptions, numberOfAllowedWriteInsPlusOne, electionEventContext, setupComponentPublicKeys,
				setupComponentTallyDataPayload);

		ballotBoxService.updateBallotBoxStatus(ballotBoxId, BallotBoxStatus.DECRYPTING);
		LOGGER.info("Mixing and decrypting. [electionEventId: {}, ballotId: {}, ballotBoxId: {}]", electionEventId, ballotId, ballotBoxId);

		final TallyComponentShufflePayload tallyComponentShufflePayload = mixDecOfflineService.mixDecrypt(electionEventId, ballotId, ballotBoxId,
				numberOfAllowedWriteInsPlusOne, lastNodePayload, electoralBoardMembersPasswords);
		LOGGER.info("Successfully mixed the votes and performed the final decryption. [electionEventId: {}, ballotId: {}, ballotBoxId: {}]",
				electionEventId, ballotId, ballotBoxId);

		final PrimesMappingTable primesMappingTable = verificationCardSetContext.primesMappingTable();

		final GqGroup encryptionGroup = encryptionParametersTallyService.loadEncryptionGroup(electionEventId);
		final GroupVector<PrimeGqElement, GqGroup> writeInVotingOptions = combinedCorrectnessInformation.getTotalListOfWriteInOptions().stream()
				.map(option -> PrimeGqElement.PrimeGqElementFactory.fromValue(option.intValue(), encryptionGroup))
				.collect(GroupVector.toGroupVector());

		processPlaintextsService.processPlaintexts(electionEventId, ballotId, ballotBoxId, numberOfSelectableVotingOptions,
				numberOfAllowedWriteInsPlusOne, tallyComponentShufflePayload, primesMappingTable, writeInVotingOptions);
		LOGGER.info("Voter selections factorized. [electionEventId: {}, ballotId: {}, ballotBoxId: {}]", electionEventId, ballotId, ballotBoxId);
	}

	private void validateConsistency(final String electionEventId, final String ballotId, final String ballotBoxId)
			throws CheckedIllegalStateException {

		if (!ballotBoxService.isDownloaded(ballotBoxId)) {
			throw new CheckedIllegalStateException(String.format(
					"Ballot box has not been downloaded, hence it cannot be mixed. [electionEventId: %s, ballotId: %s, ballotBoxId: %S]",
					electionEventId, ballotId, ballotBoxId));
		}

		validateMixIsAllowed(electionEventId, ballotBoxId, LocalDateTime::now);
		validateBallotBoxConsistency(electionEventId);
	}

	@VisibleForTesting
	void validateMixIsAllowed(final String electionEventId, final String ballotBoxId, final Supplier<LocalDateTime> now) {
		final int gracePeriod = ballotBoxService.getGracePeriod(ballotBoxId);
		final boolean isTestBallotBox = ballotBoxService.isTestBallotBox(ballotBoxId);
		final ElectionEventContext electionEventContext = electionEventContextPayloadService.load(electionEventId).getElectionEventContext();

		final boolean afterGracePeriod = now.get().isAfter(electionEventContext.finishTime().plusSeconds(gracePeriod));

		// Test ballot boxes can be mixed and decrypted at any time. Real ballot boxes can be mixed and decrypted only after the election event period ended.
		checkState(isTestBallotBox || afterGracePeriod,
				"The ballot box can not be mixed. [isTestBallotBox: %s, finishTime: %s, electionEventId: %s, ballotBoxId: %s]",
				isTestBallotBox, electionEventContext.finishTime(), electionEventId, ballotBoxId);
	}

	@VisibleForTesting
	void validateBallotBoxConsistency(final String electionEventId) {
		final List<String> ballotBoxIdsFromContext = electionEventContextPayloadService.load(electionEventId).getElectionEventContext()
				.verificationCardSetContexts().stream()
				.map(VerificationCardSetContext::ballotBoxId)
				.toList();

		final List<String> ballotBoxIdsFromDb = ballotBoxService.getBallotBoxesId(electionEventId);

		final boolean isCountEquals = ballotBoxIdsFromDb.size() == ballotBoxIdsFromContext.size();
		checkState(isCountEquals,
				"The number of ballot boxes in the DB and in the context mismatch. [electionEventId: %s, dbCount: %s, contextCount: %s]",
				electionEventId, ballotBoxIdsFromDb.size(), ballotBoxIdsFromContext.size());

		checkState(hasNoDuplicates(ballotBoxIdsFromDb), "There are duplicate values. [electionEventId: %s, dbContent: %s]", electionEventId,
				ballotBoxIdsFromDb);
		checkState(hasNoDuplicates(ballotBoxIdsFromContext), "There are duplicate values. [electionEventId: %s, contextContent: %s]", electionEventId,
				ballotBoxIdsFromContext);

		final Set<String> uniqueBallotBoxIdsFromContext = new HashSet<>(ballotBoxIdsFromContext);
		final Set<String> uniqueBallotBoxIdsFromDb = new HashSet<>(ballotBoxIdsFromDb);
		final boolean isIdsStrictlyEqual = uniqueBallotBoxIdsFromContext.equals(uniqueBallotBoxIdsFromDb);
		checkState(isIdsStrictlyEqual,
				"The ballot boxes are not the same in the DB and in the context. [electionEventId: %s, dbContent: %s, contextContent: %s]",
				electionEventId, uniqueBallotBoxIdsFromDb, uniqueBallotBoxIdsFromContext);
	}

	private ElectionEventContextPayload loadElectionEventContextPayload(final String electionEventId) {
		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadService.load(electionEventId);

		final CryptoPrimitivesSignature signature = electionEventContextPayload.getSignature();

		checkState(signature != null, "The signature of the election event context payload is null. [electionEventId: %s]", electionEventId);

		final Hashable additionalContextData = ChannelSecurityContextData.electionEventContext(electionEventId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreServiceSdmTally.verifySignature(Alias.SDM_CONFIG, electionEventContextPayload,
					additionalContextData, signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of the election event context. [electionEventId: %s]", electionEventId));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(ElectionEventContextPayload.class,
					String.format("[electionEventId: %s]", electionEventId));
		}
		return electionEventContextPayload;
	}

	private SetupComponentPublicKeysPayload loadSetupComponentPublicKeysPayload(final String electionEventId) {
		final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload = setupComponentPublicKeysPayloadService.load(electionEventId);

		final CryptoPrimitivesSignature signature = setupComponentPublicKeysPayload.getSignature();

		checkState(signature != null, "The signature of the setup component public keys payload is null. [electionEventId: %s]", electionEventId);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentPublicKeys(electionEventId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreServiceSdmTally.verifySignature(Alias.SDM_CONFIG, setupComponentPublicKeysPayload,
					additionalContextData, signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of the setup component public keys. [electionEventId: %s]", electionEventId));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(SetupComponentPublicKeysPayload.class,
					String.format("[electionEventId: %s]", electionEventId));
		}
		return setupComponentPublicKeysPayload;
	}

	private SetupComponentTallyDataPayload loadSetupComponentTallyDataPayload(final String electionEventId, final String verificationCardSetId) {
		final SetupComponentTallyDataPayload setupComponentTallyDataPayload =
				setupComponentTallyDataPayloadService.load(electionEventId, verificationCardSetId);

		final CryptoPrimitivesSignature signature = setupComponentTallyDataPayload.getSignature();

		checkState(signature != null,
				"The signature of the setup component tally data payload is null. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentTallyData(electionEventId, verificationCardSetId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreServiceSdmTally.verifySignature(Alias.SDM_CONFIG, setupComponentTallyDataPayload,
					additionalContextData, setupComponentTallyDataPayload.getSignature().signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format(
							"Could not verify the signature of the setup component tally data payload. [electionEventId: %s, verificationCardSetId: %s]",
							electionEventId, verificationCardSetId));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(SetupComponentTallyDataPayload.class,
					String.format("[electionEventId: %s, verificationCardSetId: %s]", electionEventId, verificationCardSetId));
		}

		return setupComponentTallyDataPayload;
	}
}
