/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.VoterReturnCodes;
import ch.post.it.evoting.domain.configuration.VoterReturnCodesPayload;

/**
 * Allows to generate and persist {@link VoterReturnCodesPayload}.
 */
@Service
public class VoterReturnCodesPayloadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VoterReturnCodesPayloadService.class);

	private final VoterReturnCodesPayloadFileRepository voterReturnCodesPayloadFileRepository;

	public VoterReturnCodesPayloadService(final VoterReturnCodesPayloadFileRepository voterReturnCodesPayloadFileRepository) {
		this.voterReturnCodesPayloadFileRepository = voterReturnCodesPayloadFileRepository;
	}

	/**
	 * Persists a {@link VoterReturnCodesPayload}.
	 *
	 * @param payload         the payload to be saved. Must be non-null.
	 * @param votingCardSetId the voting card set id. Must be non-null and a valid UUID.
	 * @throws NullPointerException      if the {@code payload} or {@code votingCardSetId} is null.
	 * @throws FailedValidationException if the {@code votingCardSetId} format is not valid.
	 */
	public void save(final VoterReturnCodesPayload payload, final String votingCardSetId) {
		checkNotNull(payload);
		validateUUID(votingCardSetId);

		voterReturnCodesPayloadFileRepository.save(payload, votingCardSetId);
		final String electionEventId = payload.electionEventId();
		LOGGER.info("Voter return codes payload successfully saved. [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId);
	}

	/**
	 * Loads the {@link VoterReturnCodesPayload} for the given the election event and voting card set.
	 *
	 * @param electionEventId, the election event id. Must be non-null and a valid UUID.
	 * @param votingCardSetId, the voting card set id. Must be non-null and a valid UUID.
	 * @return a {@link VoterReturnCodesPayload}.
	 * @throws FailedValidationException if {@code electionEventId} or {@code votingCardSetId} is invalid.
	 * @throws IllegalStateException     if the requested voter return codes payload is not present.
	 */
	public VoterReturnCodesPayload load(final String electionEventId, final String votingCardSetId) {
		validateUUID(electionEventId);
		validateUUID(votingCardSetId);

		return voterReturnCodesPayloadFileRepository.findByElectionEventIdAndVotingCardSetId(
						electionEventId, votingCardSetId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Requested voter return codes payload is not present. [electionEventId: %s, votingCardSetId: %s]",
								electionEventId, votingCardSetId)));
	}

	/**
	 * Loads all the voter return codes payloads for the given {@code electionEventId} and collects them as a map by verification card id.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @return the voter return codes map for this {@code electionEventId}.
	 * @throws NullPointerException      if the election event id is null.
	 * @throws FailedValidationException if the event id is not a valid UUID.
	 */
	@Cacheable("VoterReturnCodesMap")
	public Map<String, VoterReturnCodes> loadVoterReturnCodesMap(final String electionEventId) {
		validateUUID(electionEventId);

		final List<VoterReturnCodesPayload> voterReturnCodesPayloads = voterReturnCodesPayloadFileRepository.findAllByElectionEventId(
				electionEventId);
		checkState(!voterReturnCodesPayloads.isEmpty(), "Requested voter return codes payload is not present. [electionEventId: %s]",
				electionEventId);

		final Map<String, VoterReturnCodes> voterReturnCodesMap = voterReturnCodesPayloads.stream()
				.map(VoterReturnCodesPayload::voterReturnCodes)
				.flatMap(List::stream)
				.collect(Collectors.toMap(VoterReturnCodes::verificationCardId, Function.identity()));

		LOGGER.info("Loaded all voter return codes payloads. [electionEventId: {}]", electionEventId);

		return voterReturnCodesMap;
	}

}
