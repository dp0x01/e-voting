/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static com.google.common.base.Preconditions.checkState;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.SynchronizeStatus;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

/**
 * This is an application service that manages voting card sets.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class VotingCardSetSignService extends BaseVotingCardSetService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetSignService.class);

	final ElectionEventContextPersistenceService electionEventContextPersistenceService;
	private final ConfigurationEntityStatusService configurationEntityStatusService;

	public VotingCardSetSignService(
			final ElectionEventContextPersistenceService electionEventContextPersistenceService,
			final ConfigurationEntityStatusService configurationEntityStatusService) {
		this.electionEventContextPersistenceService = electionEventContextPersistenceService;
		this.configurationEntityStatusService = configurationEntityStatusService;
	}

	/**
	 * Persist the election event context and change the state of the voting card sets from generated to SIGNED for a given election event.
	 *
	 * @param electionEventId the election event id.
	 */
	public void sign(final String electionEventId) {
		final List<String> votingCardSetIds = votingCardSetRepository.findAllVotingCardSetIdsByStatus(electionEventId, Status.GENERATED);
		checkState(votingCardSetRepository.findAllVotingCardSetIds(electionEventId).size() == votingCardSetIds.size(),
				"The voting card sets are not all generated. [electionEventId: %s, votingCardSetIds: %s]", electionEventId, votingCardSetIds);

		electionEventContextPersistenceService.persist(electionEventId);
		LOGGER.info("Successfully persisted election event context. [electionEventId: {}]", electionEventId);

		votingCardSetIds.stream().parallel()
				.forEach(votingCardSetId -> {
					configurationEntityStatusService.updateWithSynchronizedStatus(Status.SIGNED.name(), votingCardSetId, votingCardSetRepository,
							SynchronizeStatus.PENDING);
					LOGGER.info("Changed the status of the voting card set. [votingCardSetId: {}]", votingCardSetId);
				});
	}
}
