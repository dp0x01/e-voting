/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.hasNoDuplicates;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateBase64Encoded;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.securedatamanager.commons.Constants;

/**
 * Holds the output of the genVerDat algorithm.
 */
@SuppressWarnings("java:S115")
public class GenVerDatOutput {

	private final int size;
	private final GqGroup gqGroup;

	private final List<String> verificationCardIds;
	private final List<String> startVotingKeys;
	private final List<ElGamalMultiRecipientKeyPair> verificationCardKeyPairs;
	private final List<String> partialChoiceReturnCodesAllowList;
	private final List<String> ballotCastingKeys;
	private final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedPartialChoiceReturnCodes;
	private final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedConfirmationKeys;
	private final PrimesMappingTable pTable;

	private GenVerDatOutput(
			final List<String> verificationCardIds,
			final List<String> startVotingKeys,
			final List<ElGamalMultiRecipientKeyPair> verificationCardKeyPairs,
			final List<String> partialChoiceReturnCodesAllowList,
			final List<String> ballotCastingKeys,
			final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedPartialChoiceReturnCodes,
			final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedConfirmationKeys,
			final PrimesMappingTable pTable) {

		checkNotNull(verificationCardIds);
		checkNotNull(startVotingKeys);
		checkNotNull(verificationCardKeyPairs);
		checkNotNull(partialChoiceReturnCodesAllowList);
		checkNotNull(ballotCastingKeys);
		checkNotNull(encryptedHashedConfirmationKeys);
		checkNotNull(pTable);
		final List<String> verificationCardIdsCopy = List.copyOf(verificationCardIds);
		final List<String> startVotingKeysCopy = List.copyOf(startVotingKeys);
		final List<ElGamalMultiRecipientKeyPair> verificationCardKeyPairsCopy = List.copyOf(verificationCardKeyPairs);
		final List<String> partialChoiceReturnCodesAllowListCopy = List.copyOf(partialChoiceReturnCodesAllowList);
		final List<String> ballotCastingKeysCopy = List.copyOf(ballotCastingKeys);

		checkArgument(!verificationCardIdsCopy.isEmpty(), "The output must not be empty.");
		checkArgument(!startVotingKeysCopy.isEmpty(), "The start voting keys must not be empty.");

		this.size = verificationCardIdsCopy.size();
		checkArgument(this.size == verificationCardKeyPairsCopy.size() && this.size == startVotingKeysCopy.size()
				&& this.size == ballotCastingKeysCopy.size() && this.size == encryptedHashedPartialChoiceReturnCodes.size()
				&& this.size == encryptedHashedConfirmationKeys.size(), "All vectors must have the same size.");

		final int n = encryptedHashedPartialChoiceReturnCodes.getElementSize();
		checkArgument(partialChoiceReturnCodesAllowListCopy.size() == n * this.size,
				String.format("There must be %d elements in the allow list.", n * this.size));

		partialChoiceReturnCodesAllowListCopy.forEach(
				element -> checkArgument(validateBase64Encoded(element).length() == Constants.BASE64_ENCODED_HASH_OUTPUT_LENGTH,
						String.format("Elements in allowList must be of length %s.", Constants.BASE64_ENCODED_HASH_OUTPUT_LENGTH)));

		final GqGroup group = verificationCardKeyPairsCopy.get(0).getGroup();
		checkArgument(group.equals(encryptedHashedPartialChoiceReturnCodes.get(0).getGroup()) && group
				.equals(encryptedHashedConfirmationKeys.get(0).getGroup()), "All vectors must belong to the same group.");

		checkArgument(hasNoDuplicates(verificationCardIdsCopy), "The vector of verification card ids must not contain any duplicated element.");

		checkArgument(pTable.size() == n, "The primes mapping table must be of size n.");

		this.gqGroup = group;
		this.verificationCardIds = verificationCardIdsCopy;
		this.startVotingKeys = startVotingKeysCopy;
		this.verificationCardKeyPairs = verificationCardKeyPairsCopy;
		this.partialChoiceReturnCodesAllowList = partialChoiceReturnCodesAllowListCopy;
		this.ballotCastingKeys = ballotCastingKeysCopy;
		this.encryptedHashedPartialChoiceReturnCodes = encryptedHashedPartialChoiceReturnCodes;
		this.encryptedHashedConfirmationKeys = encryptedHashedConfirmationKeys;
		this.pTable = pTable;
	}

	public List<String> getVerificationCardIds() {
		return List.copyOf(verificationCardIds);
	}

	public List<String> getStartVotingKeys() {
		return List.copyOf(startVotingKeys);
	}

	public List<ElGamalMultiRecipientKeyPair> getVerificationCardKeyPairs() {
		return List.copyOf(verificationCardKeyPairs);
	}

	public List<String> getPartialChoiceReturnCodesAllowList() {
		return List.copyOf(partialChoiceReturnCodesAllowList);
	}

	public List<String> getBallotCastingKeys() {
		return List.copyOf(ballotCastingKeys);
	}

	public List<ElGamalMultiRecipientCiphertext> getEncryptedHashedPartialChoiceReturnCodes() {
		return List.copyOf(encryptedHashedPartialChoiceReturnCodes);
	}

	public List<ElGamalMultiRecipientCiphertext> getEncryptedHashedConfirmationKeys() {
		return List.copyOf(encryptedHashedConfirmationKeys);
	}

	public PrimesMappingTable getPTable() {
		return pTable;
	}

	public int size() {
		return this.size;
	}

	public GqGroup getGroup() {
		return this.gqGroup;
	}

	public static class Builder {
		private List<String> verificationCardIds;
		private List<String> startVotingKeys;
		private List<ElGamalMultiRecipientKeyPair> verificationCardKeyPairs;
		private List<String> partialChoiceReturnCodesAllowList;
		private List<String> ballotCastingKeys;
		private GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedPartialChoiceReturnCodes;
		private GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedConfirmationKeys;
		private PrimesMappingTable pTable;

		public Builder setVerificationCardIds(final List<String> verificationCardIds) {
			this.verificationCardIds = verificationCardIds;
			return this;
		}

		public Builder setStartVotingKeys(final List<String> startVotingKeys) {
			this.startVotingKeys = startVotingKeys;
			return this;
		}

		public Builder setVerificationCardKeyPairs(final List<ElGamalMultiRecipientKeyPair> verificationCardKeyPairs) {
			this.verificationCardKeyPairs = verificationCardKeyPairs;
			return this;
		}

		public Builder setPartialChoiceReturnCodesAllowList(final List<String> partialChoiceReturnCodesAllowList) {
			this.partialChoiceReturnCodesAllowList = partialChoiceReturnCodesAllowList;
			return this;
		}

		public Builder setBallotCastingKeys(final List<String> ballotCastingKeys) {
			this.ballotCastingKeys = ballotCastingKeys;
			return this;
		}

		public Builder setEncryptedHashedPartialChoiceReturnCodes(
				final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedPartialChoiceReturnCodes) {
			this.encryptedHashedPartialChoiceReturnCodes = encryptedHashedPartialChoiceReturnCodes;
			return this;
		}

		public Builder setEncryptedHashedConfirmationKeys(
				final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedConfirmationKeys) {
			this.encryptedHashedConfirmationKeys = encryptedHashedConfirmationKeys;
			return this;
		}

		public Builder setPTable(final PrimesMappingTable pTable) {
			this.pTable = pTable;
			return this;
		}

		public GenVerDatOutput build() {
			return new GenVerDatOutput(verificationCardIds, startVotingKeys, verificationCardKeyPairs, partialChoiceReturnCodesAllowList,
					ballotCastingKeys, encryptedHashedPartialChoiceReturnCodes, encryptedHashedConfirmationKeys, pTable);
		}
	}

}
