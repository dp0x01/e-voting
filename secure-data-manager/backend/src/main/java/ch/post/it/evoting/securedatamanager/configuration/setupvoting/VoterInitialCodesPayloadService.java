/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.setupvoting.VoterInitialCodes;
import ch.post.it.evoting.domain.configuration.setupvoting.VoterInitialCodesPayload;

/**
 * Allows saving and retrieving voter initial codes payloads.
 */
@Service
public class VoterInitialCodesPayloadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VoterInitialCodesPayloadService.class);

	private final VoterInitialCodesPayloadFileRepository voterInitialCodesPayloadFileRepository;

	public VoterInitialCodesPayloadService(final VoterInitialCodesPayloadFileRepository voterInitialCodesPayloadFileRepository) {
		this.voterInitialCodesPayloadFileRepository = voterInitialCodesPayloadFileRepository;
	}

	/**
	 * Saves a voter initial codes payload in the corresponding election event folder.
	 *
	 * @param voterInitialCodesPayload the voter initial codes payload to save. Must be non-null.
	 * @param votingCardSetId          the voting card set id. Must be non-null and a valid UUID.
	 * @throws NullPointerException      if any parameter is null.
	 * @throws FailedValidationException if the voting card set id is not a valid UUID.
	 */
	public void save(final VoterInitialCodesPayload voterInitialCodesPayload, final String votingCardSetId) {
		checkNotNull(voterInitialCodesPayload);
		validateUUID(votingCardSetId);

		final String electionEventId = voterInitialCodesPayload.electionEventId();

		voterInitialCodesPayloadFileRepository.save(voterInitialCodesPayload, votingCardSetId);

		LOGGER.info("Saved voter initial codes payload. [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId);
	}

	/**
	 * Loads the voter initial codes payload for the given {@code electionEventId} and {@code votingCardSetId}.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @param votingCardSetId the voting card set id. Must be non-null and a valid UUID.
	 * @return the voter initial codes payload for this {@code electionEventId} and {@code votingCardSetId}.
	 * @throws NullPointerException      if any parameter is null.
	 * @throws FailedValidationException if any parameter is not a valid UUID.
	 */
	public VoterInitialCodesPayload load(final String electionEventId, final String votingCardSetId) {
		validateUUID(electionEventId);
		validateUUID(votingCardSetId);

		final VoterInitialCodesPayload voterInitialCodesPayload = voterInitialCodesPayloadFileRepository.findByElectionEventIdAndVotingCardSetId(
						electionEventId, votingCardSetId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Requested voter initial codes payload is not present. [electionEventId: %s, votingCardSetId: %s]",
								electionEventId, votingCardSetId)));

		LOGGER.info("Loaded voter initial codes payload. [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId);

		return voterInitialCodesPayload;
	}

	/**
	 * Loads all the voter initial codes payloads for the given {@code electionEventId} and collects them as a map by voter identification.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @return the voter initial codes map for this {@code electionEventId}.
	 * @throws NullPointerException      if the election event id is null.
	 * @throws FailedValidationException if the event id is not a valid UUID.
	 */
	@Cacheable("VoterInitialCodesMap")
	public Map<String, VoterInitialCodesByVcs> loadVoterInitialCodesMap(final String electionEventId) {
		validateUUID(electionEventId);

		final List<VoterInitialCodesPayload> voterInitialCodesPayloads = voterInitialCodesPayloadFileRepository.findAllByElectionEventId(
				electionEventId);
		checkState(!voterInitialCodesPayloads.isEmpty(), "Requested voter initial codes payloads are not present. [electionEventId: %s]",
				electionEventId);

		final Map<String, VoterInitialCodesByVcs> voterInitialCodesMap = voterInitialCodesPayloads.stream()
				.flatMap(voterInitialCodesPayload -> voterInitialCodesPayload.voterInitialCodes().stream()
						.map(voterInitialCodes -> new VoterInitialCodesByVcs(voterInitialCodesPayload.verificationCardSetId(), voterInitialCodes)))
				.collect(Collectors.toMap(voterInitialCodesByVcs -> voterInitialCodesByVcs.voterInitialCodes.voterIdentification(),
						Function.identity()));

		LOGGER.info("Loaded all voter initial codes payloads. [electionEventId: {}]", electionEventId);

		return voterInitialCodesMap;
	}

	public record VoterInitialCodesByVcs(String verificationCardSetId, VoterInitialCodes voterInitialCodes) {
	}
}
