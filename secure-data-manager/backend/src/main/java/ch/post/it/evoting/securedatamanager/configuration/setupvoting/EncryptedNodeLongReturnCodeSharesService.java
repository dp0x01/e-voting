/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeShare;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationData;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.NodeContributionsResponsesService.NodeContributionsChunk;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.PayloadStorageException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.SetupComponentVerificationDataPayloadFileRepository;

/**
 * Allows to combine the node contributions responses.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class EncryptedNodeLongReturnCodeSharesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EncryptedNodeLongReturnCodeSharesService.class);

	private final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository;

	public EncryptedNodeLongReturnCodeSharesService(
			final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository) {
		this.setupComponentVerificationDataPayloadFileRepository = setupComponentVerificationDataPayloadFileRepository;
	}

	/**
	 * Converts a node contributions response chunk into an {@link EncryptedNodeLongReturnCodeSharesChunk} for the given {@code electionEventId},
	 * {@code verificationCardSetId} and {@code nodeContributionsChunk}.
	 *
	 * @param electionEventId        the node contributions responses' election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId  the node contributions responses' verification card set id. Must be non-null and a valid UUID.
	 * @param nodeContributionsChunk the node contributions response chunk to convert. Must be non-null.
	 * @return an {@link EncryptedNodeLongReturnCodeSharesChunk}
	 * @throws FailedValidationException if {@code electionEventId}, {@code verificationCardSetId} is invalid.
	 */
	public EncryptedNodeLongReturnCodeSharesChunk convertNodeContributionsChunk(final String electionEventId, final String verificationCardSetId,
			final NodeContributionsChunk nodeContributionsChunk) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(nodeContributionsChunk);

		LOGGER.debug("Converting a node contributions chunk. [electionEventId: {}, verificationCardSetId: {}, chunkId: {}]",
				electionEventId, verificationCardSetId, nodeContributionsChunk.chunkId());

		verifyConsistencyChunk(electionEventId, verificationCardSetId, nodeContributionsChunk);

		final List<EncryptedSingleNodeLongReturnCodeSharesChunk> encryptedSingleNodeLongReturnCodeSharesChunks =
				nodeContributionsChunk.contributions().stream()
						.map(controlComponentCodeSharesPayload -> new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
								.setNodeId(controlComponentCodeSharesPayload.getNodeId())
								.setChunkId(controlComponentCodeSharesPayload.getChunkId())
								.setVerificationCardIds(controlComponentCodeSharesPayload.getControlComponentCodeShares().stream()
										.map(ControlComponentCodeShare::verificationCardId)
										.toList())
								.setExponentiatedEncryptedPartialChoiceReturnCodes(
										controlComponentCodeSharesPayload.getControlComponentCodeShares().stream()
												.map(ControlComponentCodeShare::exponentiatedEncryptedPartialChoiceReturnCodes)
												.toList())
								.setExponentiatedEncryptedConfirmationKeys(
										controlComponentCodeSharesPayload.getControlComponentCodeShares().stream()
												.map(ControlComponentCodeShare::exponentiatedEncryptedConfirmationKey)
												.toList())
								.build())
						.toList();

		return new EncryptedNodeLongReturnCodeSharesChunk.Builder()
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setVerificationCardIds(encryptedSingleNodeLongReturnCodeSharesChunks.get(0).getVerificationCardIds())
				.setChunkId(encryptedSingleNodeLongReturnCodeSharesChunks.get(0).getChunkId())
				.setNodeReturnCodesValues(encryptedSingleNodeLongReturnCodeSharesChunks)
				.build();
	}

	/**
	 * Verifies:
	 * <ul>
	 *     <li>there is at least one node contribution response.</li>
	 *     <li>there is no missing node id.</li>
	 *     <li>the order of ControlComponentCodeSharesPayloads is sorted by node id.</li>
	 *     <li>the election event id verification card set id and chunk id of all the ControlComponentCodeSharesPayloads are correct.</li>
	 *     <li>the chunk count is the same for the SetupComponentVerificationDataPayloads and ControlComponentCodeSharesPayloads.</li>
	 *     <li>the ControlComponentCodeSharesPayloads have the same encryption group as the SetupComponentVerificationDataPayloads.</li>
	 *     <li>the verification card ids are unique among the ControlComponentCodeSharesPayload chunks.</li>
	 *     <li>the ControlComponentCodeSharesPayloads' verification card ids have the same content and order across all nodes.</li>
	 *     <li>the verification card ids of the SetupComponentVerificationDataPayload's chunks have the same content and order than the verification card ids of the ControlComponentCodeSharesPayload's chunks.</li>
	 * </ul>
	 */
	private void verifyConsistencyChunk(final String electionEventId, final String verificationCardSetId,
			final NodeContributionsChunk nodeContributionsChunk) {

		final int chunkId = nodeContributionsChunk.chunkId();

		// At least one node contribution response
		checkState(!nodeContributionsChunk.contributions().isEmpty(),
				"No node contributions responses. [electionEventId: %s, verificationCardSetId: %s, chunkId: %s]", electionEventId,
				verificationCardSetId, chunkId);

		// Missing node id
		checkState(nodeContributionsChunk.contributions().size() == NODE_IDS.size(),
				"The node ID sequence is incomplete. [electionEventId: %s, verificationCardSetId: %s, chunkId: %s]", electionEventId,
				verificationCardSetId, chunkId);

		// Node id order
		checkState(IntStream.range(0, NODE_IDS.size() - 1)
						.allMatch(i -> nodeContributionsChunk.contributions().get(i).getNodeId() == i + 1),
				"The node ID sequence is not in the correct order. [electionEventId: %s, verificationCardSetId: %s, chunkId: %s]", electionEventId,
				verificationCardSetId, chunkId);

		// ElectionEventId, VerificationCardSetId and ChunkId are consistent
		checkState(nodeContributionsChunk.contributions().stream()
						.allMatch(payload -> electionEventId.equals(payload.getElectionEventId())
								&& verificationCardSetId.equals(payload.getVerificationCardSetId())
								&& chunkId == payload.getChunkId()),
				"All return code generation response payloads must be related to the correct election event id,"
						+ " verification card set id and chunkId. [electionEventId: %s, verificationCardSetId: %s, chunkId: %s]", electionEventId,
				verificationCardSetId, chunkId);

		final SetupComponentVerificationDataPayloadContent setupComponentVerificationDataPayloadContent = getSetupComponentVerificationDataPayloadContent(
				electionEventId, verificationCardSetId, chunkId);

		nodeContributionsChunk.contributions().forEach(controlComponentCodeSharesPayload -> {
			final int nodeId = controlComponentCodeSharesPayload.getNodeId();

			// ChunkIds are consistent
			checkState(chunkId == setupComponentVerificationDataPayloadContent.chunkId,
					"The ControlComponentCodeSharesPayload does not have the same chunk id as the SetupComponentVerificationDataPayload."
							+ " [controlComponentChunkId: %s, setupComponentChunkId: %s]", chunkId,
					setupComponentVerificationDataPayloadContent.chunkId);

			// Encryption groups are equals
			checkState(
					setupComponentVerificationDataPayloadContent.encryptionGroup()
							.equals(controlComponentCodeSharesPayload.getEncryptionGroup()),
					"The ControlComponentCodeSharesPayload does not have the same encryption group as the "
							+ "SetupComponentVerificationDataPayload. [electionEventId: %s, verificationCardSetId: %s, chunkId: %s, nodeId: %s]",
					electionEventId, verificationCardSetId, chunkId, nodeId);

			final List<String> controlComponentVerificationCardIds = controlComponentCodeSharesPayload.getControlComponentCodeShares()
					.stream()
					.parallel()
					.map(ControlComponentCodeShare::verificationCardId)
					.toList();
			// The ControlComponentCodeShare payload ensures no verificationCardId duplicates.

			// The verification card ids are equals between payloads
			checkState(setupComponentVerificationDataPayloadContent.verificationCardIds().equals(controlComponentVerificationCardIds),
					"The ControlComponentCodeSharesPayload does not have the same verification card ids as the "
							+ "SetupComponentVerificationDataPayload. [electionEventId: %s, verificationCardSetId: %s, chunkId: %s, nodeId: %s]",
					electionEventId, verificationCardSetId, chunkId, nodeId);
		});

	}

	private SetupComponentVerificationDataPayloadContent getSetupComponentVerificationDataPayloadContent(final String electionEventId,
			final String verificationCardSetId, final int chunkId) {
		final SetupComponentVerificationDataPayload setupComponentVerificationDataPayload;
		try {
			setupComponentVerificationDataPayload = setupComponentVerificationDataPayloadFileRepository.retrieve(electionEventId,
					verificationCardSetId, chunkId);
		} catch (final PayloadStorageException e) {
			throw new IllegalStateException(
					String.format("No SetupComponentVerificationDataPayload found. [electionEventId: %s, verificationCardSetId: %s, chunkId: %s]",
							electionEventId, verificationCardSetId, chunkId), e);
		}

		checkState(electionEventId.equals(setupComponentVerificationDataPayload.getElectionEventId()),
				"The electionEventId in SetupComponentVerificationDataPayload is not correct. [expected: %s, actual: %s]", electionEventId,
				setupComponentVerificationDataPayload.getElectionEventId());
		checkState(verificationCardSetId.equals(setupComponentVerificationDataPayload.getVerificationCardSetId()),
				"The verificationCardSetId in SetupComponentVerificationDataPayload is not correct. [expected: %s, actual: %s]",
				verificationCardSetId, setupComponentVerificationDataPayload.getVerificationCardSetId());
		checkState(chunkId == setupComponentVerificationDataPayload.getChunkId(),
				"The chunkId in SetupComponentVerificationDataPayload is not correct. [expected: %s, actual: %s]",
				chunkId, setupComponentVerificationDataPayload.getChunkId());

		final List<String> setupComponentVerificationCardIds = setupComponentVerificationDataPayload.getSetupComponentVerificationData()
				.stream()
				.parallel()
				.map(SetupComponentVerificationData::verificationCardId)
				.toList();

		return new SetupComponentVerificationDataPayloadContent(
				setupComponentVerificationDataPayload.getChunkId(),
				setupComponentVerificationDataPayload.getEncryptionGroup(),
				setupComponentVerificationCardIds
		);
	}

	record SetupComponentVerificationDataPayloadContent(int chunkId, GqGroup encryptionGroup, List<String> verificationCardIds) {
	}

}
