/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballot.BallotRepository;

/**
 * Service for operates with ballots.
 */
@Service
@ConditionalOnProperty("role.isTally")
public class BallotTallyService {

	private final BallotRepository ballotRepository;
	private final ObjectMapper objectMapper;

	public BallotTallyService(final BallotRepository ballotRepository,
			final ObjectMapper objectMapper) {
		this.ballotRepository = ballotRepository;
		this.objectMapper = objectMapper;
	}

	public Ballot getBallot(final String electionEventId, final String ballotId) {
		final Map<String, Object> attributeValueMap = new HashMap<>();
		attributeValueMap.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);
		attributeValueMap.put(JsonConstants.ID, ballotId);
		final String ballotAsJson = ballotRepository.find(attributeValueMap);
		try {
			return objectMapper.readValue(ballotAsJson, Ballot.class);
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot deserialize the ballot box json string to a valid Ballot object.", e);
		}
	}
}
