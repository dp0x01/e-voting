/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmonline.tally.mixonline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.securedatamanager.services.application.service.BallotBoxService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/sdm-backend/mixing")
@Api(value = "Mixing REST API")
public class MixDecryptOnlineController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MixDecryptOnlineController.class);

	private final MixDecryptOnlineService mixDecryptOnlineService;
	private final BallotBoxService ballotBoxService;

	public MixDecryptOnlineController(
			final MixDecryptOnlineService mixDecryptOnlineService,
			final BallotBoxService ballotBoxService) {
		this.mixDecryptOnlineService = mixDecryptOnlineService;
		this.ballotBoxService = ballotBoxService;
	}

	@PutMapping(value = "/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/mixonline", produces = "application/json")
	@ResponseBody
	public ResponseEntity<Void> mixOnline(
			@ApiParam(
					name = "electionEventId",
					type = "String",
					value = "Unique identifier for election event",
					example = "bf731ed5b4f24cf8a926d744015118ae",
					required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(
					name = "ballotBoxId",
					type = "String",
					value = "Unique identifier for ballot box",
					example = "51a21bbc884749fca630b8037cd29424",
					required = true)
			@PathVariable
			final String ballotBoxId) {

		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		LOGGER.info("Received request to mix. [ballotBoxId: {}].", ballotBoxId);

		if (mixDecryptOnlineService.startOnlineMixing(electionEventId, ballotBoxId)) {
			LOGGER.info("Started mixing ballot box. [ballotBoxId: {}]", ballotBoxId);
			return ResponseEntity.ok().build();
		} else {
			LOGGER.error("Mixing of ballot box not started. [ballotBoxId: {}]", ballotBoxId);
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		}
	}

	@PutMapping(value = "/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/updateStatus", produces = "application/json")
	@ResponseBody
	public ResponseEntity<BallotBoxStatus> onlineMixingStatus(
			@ApiParam(
					name = "electionEventId",
					type = "String",
					value = "Unique identifier for election event",
					example = "bf731ed5b4f24cf8a926d744015118ae",
					required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(
					name = "ballotBoxId",
					type = "String",
					value = "Unique identifier for ballot box",
					example = "51a21bbc884749fca630b8037cd29424",
					required = true)
			@PathVariable
			final String ballotBoxId) {

		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		final BallotBoxStatus localStatus = ballotBoxService.getBallotBoxStatus(ballotBoxId);
		if (BallotBoxStatus.MIXING.equals(localStatus) || BallotBoxStatus.MIXING_ERROR.equals(localStatus)) {
			// Ask the status from the orchestrator.
			final BallotBoxStatus onlineStatus = mixDecryptOnlineService.getOnlineStatus(electionEventId, ballotBoxId);

			// The sdm already triggered the mixing and doesn't manage the MIXING_NOT_STARTED status.
			if (BallotBoxStatus.MIXING_NOT_STARTED.equals(onlineStatus)) {
				return ResponseEntity.ok(BallotBoxStatus.MIXING);
			}

			// Update status in the local database.
			final BallotBoxStatus newStatus = ballotBoxService.updateBallotBoxStatus(ballotBoxId, onlineStatus);
			LOGGER.info("Ballot box status updated. [ballotBoxId: {}, status: {}]", ballotBoxId, newStatus);

			return ResponseEntity.ok(newStatus);
		} else {
			return ResponseEntity.ok(localStatus);
		}

	}

	@GetMapping(value = "/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/download", produces = "application/json")
	@ResponseBody
	public ResponseEntity<Void> downloadMixedOnlineBallots(
			@ApiParam(
					name = "electionEventId",
					type = "String",
					value = "Unique identifier for election event",
					example = "bf731ed5b4f24cf8a926d744015118ae",
					required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(
					name = "ballotBoxId",
					type = "String",
					value = "Unique identifier for ballot box",
					example = "51a21bbc884749fca630b8037cd29424",
					required = true)
			@PathVariable
			final String ballotBoxId) {

		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		mixDecryptOnlineService.downloadOnlineMixnetPayloads(electionEventId, ballotBoxId);
		LOGGER.info("Successfully downloaded ballot box. [ballotBoxId: {}]", ballotBoxId);

		return ResponseEntity.ok().build();
	}
}
