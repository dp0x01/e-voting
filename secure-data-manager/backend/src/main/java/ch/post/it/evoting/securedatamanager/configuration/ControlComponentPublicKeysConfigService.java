/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.configuration.ControlComponentPublicKeysPayload;

/**
 * Allows retrieving existing control component public keys.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class ControlComponentPublicKeysConfigService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ControlComponentPublicKeysConfigService.class);
	private final ControlComponentPublicKeysPayloadFileRepository controlComponentPublicKeysPayloadFileRepository;
	private final SignatureKeystore<Alias> signatureKeystoreService;

	public ControlComponentPublicKeysConfigService(
			final ControlComponentPublicKeysPayloadFileRepository controlComponentPublicKeysPayloadFileRepository,
			@Qualifier("keystoreServiceSdmConfig")
			final SignatureKeystore<Alias> signatureKeystoreService) {
		this.controlComponentPublicKeysPayloadFileRepository = controlComponentPublicKeysPayloadFileRepository;
		this.signatureKeystoreService = signatureKeystoreService;
	}

	/**
	 * Loads all {@link ControlComponentPublicKeys} for the given {@code electionEventId}. Upon retrieving the public keys, the signatures of their
	 * respective payloads are first verified. The result of this method is cached.
	 *
	 * @param electionEventId the election event id for which to get the public keys.
	 * @return the control component public keys for this {@code electionEventId}.
	 * @throws FailedValidationException if {@code electionEventId} is invalid.
	 * @throws IllegalStateException     if
	 *                                   <ul>
	 *                                       <li>There is not the correct number of payloads.</li>
	 *                                       <li>If any verification of signature fails.</li>
	 *                                       <li>If any signature is invalid.</li>
	 *                                   </ul>
	 */
	@Cacheable("controlComponentPublicKeys")
	public List<ControlComponentPublicKeys> loadOrderByNodeId(final String electionEventId) {
		validateUUID(electionEventId);

		final List<ControlComponentPublicKeysPayload> controlComponentPublicKeysPayloads =
				controlComponentPublicKeysPayloadFileRepository.findAllOrderByNodeId(electionEventId);

		LOGGER.debug("Retrieved all control component public keys payloads are valid. [electionEventId: {}]", electionEventId);

		// Ensure we received all payloads corresponds to the node ids.
		final List<Integer> payloadsNodeIds = controlComponentPublicKeysPayloads.stream().parallel()
				.map(ControlComponentPublicKeysPayload::getControlComponentPublicKeys)
				.map(ControlComponentPublicKeys::nodeId)
				.toList();

		checkState(NODE_IDS.size() == payloadsNodeIds.size() && NODE_IDS.containsAll(payloadsNodeIds),
				"Wrong number of control component public keys payloads. [required node ids: %s, found: %s]", NODE_IDS,
				payloadsNodeIds);

		// Check signatures of payloads.
		controlComponentPublicKeysPayloads
				.forEach(controlComponentPublicKeysPayload -> validateSignature(controlComponentPublicKeysPayload, electionEventId));

		LOGGER.debug("Signature of all control component public keys payloads are valid. [electionEventId: {}]", electionEventId);

		return controlComponentPublicKeysPayloads.stream().parallel()
				.map(ControlComponentPublicKeysPayload::getControlComponentPublicKeys)
				.sorted(Comparator.comparingInt(ControlComponentPublicKeys::nodeId))
				.toList();
	}

	private void validateSignature(final ControlComponentPublicKeysPayload controlComponentPublicKeysPayload, final String electionEventId) {

		final CryptoPrimitivesSignature signature = controlComponentPublicKeysPayload.getSignature();
		final int nodeId = controlComponentPublicKeysPayload.getControlComponentPublicKeys().nodeId();

		checkState(signature != null, "The signature of the control component public keys payload is null. [electionEventId: %s, nodeId: %s]",
				electionEventId, nodeId);

		final Hashable additionalContextData = ChannelSecurityContextData.controlComponentPublicKeys(nodeId, electionEventId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.getControlComponentByNodeId(nodeId),
					controlComponentPublicKeysPayload, additionalContextData, signature.signatureContents());

		} catch (final SignatureException e) {
			throw new IllegalStateException("Cannot verify the signature.", e);
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(ControlComponentPublicKeysPayload.class,
					String.format("[electionEventId: %s, nodeId: %s]", electionEventId, nodeId));
		}
	}
}
