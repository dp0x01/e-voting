/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.AdminBoardHashesPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.securedatamanager.AdminBoardHashesPayloadService;
import ch.post.it.evoting.securedatamanager.BoardPasswordHashService;

@Service
@ConditionalOnProperty("role.isTally")
public class AdminBoardTallyService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminBoardTallyService.class);

	private final BoardPasswordHashService boardPasswordHashService;
	private final SignatureKeystore<Alias> signatureKeystoreServiceSdmConfig;
	private final AdminBoardHashesPayloadService adminBoardHashesPayloadService;

	public AdminBoardTallyService(
			final BoardPasswordHashService boardPasswordHashService,
			@Qualifier("keystoreServiceSdmTally")
			final SignatureKeystore<Alias> signatureKeystoreServiceSdmConfig,
			final AdminBoardHashesPayloadService adminBoardHashesPayloadService) {
		this.boardPasswordHashService = boardPasswordHashService;
		this.signatureKeystoreServiceSdmConfig = signatureKeystoreServiceSdmConfig;
		this.adminBoardHashesPayloadService = adminBoardHashesPayloadService;
	}

	/**
	 * Activates the Admin board.
	 * <p>
	 * The passwords are hashed and compared to the ones saved in a AdminBoardHashesPayload. The number of provided passwords must be greater than or
	 * equal to the threshold.
	 *
	 * @param adminBoardId        the admin board id.
	 * @param adminBoardPasswords the admin board passwords.
	 */
	public void activate(final String adminBoardId, final List<char[]> adminBoardPasswords) {
		LOGGER.debug("Activating Admin board... [adminBoardId: {}]", adminBoardId);

		validateUUID(adminBoardId);
		final AdminBoardHashesPayload adminBoardHashesPayload = adminBoardHashesPayloadService.load(adminBoardId);
		checkArgument(adminBoardId.equals(adminBoardHashesPayload.getAdminBoardId()));

		// Verify signature
		if (!verifySignature(adminBoardHashesPayload)) {
			throw new InvalidPayloadSignatureException(
					AdminBoardHashesPayload.class, String.format("[adminBoardId: %s]", adminBoardId));
		}
		LOGGER.info("Validated signature of admin board hashes payload. [adminBoardId: {}]", adminBoardId);

		// Verify password with hash
		final List<byte[]> adminBoardMembersHashes = adminBoardHashesPayload.getAdminBoardHashes();
		checkArgument(adminBoardMembersHashes.size() == adminBoardPasswords.size());

		final long countValidPassword = IntStream.range(0, adminBoardMembersHashes.size())
				.filter(i -> adminBoardPasswords.get(i) != null && adminBoardPasswords.get(i).length > 0 && boardPasswordHashService.verifyPassword(
						adminBoardPasswords.get(i), adminBoardMembersHashes.get(i)))
				.count();

		checkArgument(countValidPassword >= adminBoardHashesPayload.getThreshold());

		// Wipe the passwords after usage
		adminBoardPasswords.stream().filter(Objects::nonNull).forEach(pw -> Arrays.fill(pw, '\u0000'));
	}

	/**
	 * Verifies the given electoral board member's password.
	 *
	 * @param adminBoardId             the identifier of the admin board. Must be non-null and a valid UUID.
	 * @param adminBoardMemberIdx      the index of the admin board member. Must be positive and strictly smaller than the number of stored ABHashes.
	 * @param adminBoardMemberPassword the password of the admin board member. Must be non-null and a valid EBPassword.
	 * @return true if the given password matches the stored hash of the admin board member, false otherwise.
	 * @throws NullPointerException             if any input is null.
	 * @throws FailedValidationException        if {@code adminBoardId} is not a valid UUID.
	 * @throws IllegalArgumentException         if the {@code adminBoardMemberIdx} is negative or greater than the number of stored ABHashes.
	 * @throws InvalidPayloadSignatureException if the signature of the admin board hashes payload is invalid.
	 * @throws IllegalStateException            if the signature verification of the admin board hashes was not possible.
	 */
	public boolean verifyAdminBoardMemberPassword(final String adminBoardId, final int adminBoardMemberIdx,
			final char[] adminBoardMemberPassword) {
		validateUUID(adminBoardId);
		checkArgument(adminBoardMemberIdx >= 0);
		checkNotNull(adminBoardMemberPassword);

		final AdminBoardHashesPayload adminBoardHashesPayload = adminBoardHashesPayloadService.load(adminBoardId);

		// Verify signature
		if (!verifySignature(adminBoardHashesPayload)) {
			throw new InvalidPayloadSignatureException(
					AdminBoardHashesPayload.class, String.format("[adminBoardId: %s]", adminBoardId));
		}
		LOGGER.info("Validated signature of admin board hashes payload. [adminBoardId: {}]", adminBoardId);

		final List<byte[]> adminBoardMembersHashes = adminBoardHashesPayload.getAdminBoardHashes();
		checkArgument(adminBoardMemberIdx < adminBoardMembersHashes.size());
		final byte[] adminBoardMemberArgonHash = adminBoardMembersHashes.get(adminBoardMemberIdx);

		return boardPasswordHashService.verifyPassword(adminBoardMemberPassword, adminBoardMemberArgonHash);
	}

	private boolean verifySignature(final AdminBoardHashesPayload adminBoardHashesPayload) {
		final String adminBoardId = adminBoardHashesPayload.getAdminBoardId();
		final CryptoPrimitivesSignature signature = adminBoardHashesPayload.getSignature();

		checkState(signature != null, "The signature of the admin board hashes payload is null. [adminBoardId: %s]",
				adminBoardHashesPayload.getAdminBoardId());

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentAdminBoardHashes(adminBoardId);

		try {
			return signatureKeystoreServiceSdmConfig.verifySignature(Alias.SDM_CONFIG, adminBoardHashesPayload, additionalContextData,
					signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of the admin board hashes payload. [adminBoardId: %s]", adminBoardId));
		}
	}
}
