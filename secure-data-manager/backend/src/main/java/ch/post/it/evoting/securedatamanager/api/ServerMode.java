/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api;

enum ServerMode {
	SERVER_MODE_ONLINE,
	SERVER_MODE_SETUP,
	SERVER_MODE_TALLY,
	SERVER_MODE_TESTING
}
