/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.commons;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import okhttp3.RequestBody;
import okhttp3.internal.Util;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;

public class ChunkedRequestBodyCreator {
	private ChunkedRequestBodyCreator() {
		//intentionally left blank
	}

	/**
	 * Creates a {@link RequestBody} without a content size to enforce chunked transfer encoding.
	 */
	public static RequestBody forJsonPayload(final byte[] payload) {
		return new RequestBody() {
			@Override
			public okhttp3.MediaType contentType() {
				return okhttp3.MediaType.get(javax.ws.rs.core.MediaType.APPLICATION_JSON);
			}

			@Override
			public void writeTo(final BufferedSink bufferedSink) throws IOException {
				try (final Source source = Okio.source(new ByteArrayInputStream(payload))) {
					bufferedSink.writeAll(source);
					Util.closeQuietly(source);
				}
			}
		};
	}
}
