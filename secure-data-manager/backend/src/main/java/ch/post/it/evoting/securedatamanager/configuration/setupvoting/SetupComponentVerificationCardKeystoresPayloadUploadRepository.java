/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.UncheckedIOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.domain.configuration.SetupComponentVerificationCardKeystoresPayload;
import ch.post.it.evoting.securedatamanager.commons.ChunkedRequestBodyCreator;
import ch.post.it.evoting.securedatamanager.commons.RemoteService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.VoteVerificationClient;

import okhttp3.RequestBody;

@Repository
public class SetupComponentVerificationCardKeystoresPayloadUploadRepository {

	private final ObjectMapper objectMapper;
	private final VoteVerificationClient voteVerificationClient;

	private final boolean isVotingPortalEnabled;

	public SetupComponentVerificationCardKeystoresPayloadUploadRepository(
			final ObjectMapper objectMapper,
			final VoteVerificationClient voteVerificationClient,
			@Value("${voting.portal.enabled}")
			final boolean isVotingPortalEnabled) {

		this.objectMapper = objectMapper;
		this.voteVerificationClient = voteVerificationClient;
		this.isVotingPortalEnabled = isVotingPortalEnabled;
	}

	/**
	 * Uploads the setup component verification card keystores payload to the vote verification.
	 *
	 * @param setupComponentVerificationCardKeystoresPayload the {@link SetupComponentVerificationCardKeystoresPayload} to upload. Must be non-null.
	 * @throws NullPointerException if the input is null.
	 */
	public void upload(final SetupComponentVerificationCardKeystoresPayload setupComponentVerificationCardKeystoresPayload) {

		checkNotNull(setupComponentVerificationCardKeystoresPayload);

		checkState(isVotingPortalEnabled, "The voting-portal connection is not enabled.");

		final String electionEventId = setupComponentVerificationCardKeystoresPayload.getElectionEventId();
		final String verificationCardSetId = setupComponentVerificationCardKeystoresPayload.getVerificationCardSetId();
		final RequestBody requestBody;
		try {
			requestBody = ChunkedRequestBodyCreator.forJsonPayload(objectMapper.writeValueAsBytes(setupComponentVerificationCardKeystoresPayload));
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(
					String.format(
							"Failed to serialize setupComponentVerificationCardKeystoresPayload. [electionEventId: %s, verificationCardSetId: %s]",
							electionEventId, verificationCardSetId), e);
		}

		RemoteService.call(voteVerificationClient.uploadSetupComponentVerificationCardKeystores(electionEventId, verificationCardSetId, requestBody))
				.networkErrorMessage("Failed to communicate with vote verification.")
				.unsuccessfulResponseErrorMessage("Request for uploading setup component verification card keystores payload failed. "
								+ "[electionEventId: %s, verificationCardSetId: %s]", electionEventId, verificationCardSetId)
				.execute();
	}

}
