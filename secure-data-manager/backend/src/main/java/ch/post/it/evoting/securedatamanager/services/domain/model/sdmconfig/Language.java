/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.domain.model.sdmconfig;

public record Language(String code, String name) {

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Language{");
		sb.append("code='").append(code).append('\'');
		sb.append(", name='").append(name).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
