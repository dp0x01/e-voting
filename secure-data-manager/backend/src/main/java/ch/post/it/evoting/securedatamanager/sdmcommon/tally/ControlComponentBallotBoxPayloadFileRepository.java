/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmcommon.tally;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.cryptoprimitives.utils.Validations.allEqual;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@Repository
public class ControlComponentBallotBoxPayloadFileRepository {

	private static final String FILE_PREFIX = "controlComponentBallotBoxPayload_";

	private final ObjectMapper objectMapper;
	private final PathResolver payloadResolver;

	public ControlComponentBallotBoxPayloadFileRepository(final ObjectMapper objectMapper, final PathResolver payloadResolver) {
		this.objectMapper = objectMapper;
		this.payloadResolver = payloadResolver;
	}

	public ControlComponentBallotBoxPayload getPayload(final String electionEventId, final String ballotId, final String ballotBoxId,
			final int nodeId) {
		validateUUID(electionEventId);
		validateUUID(ballotId);
		validateUUID(ballotBoxId);
		checkArgument(nodeId >= NODE_IDS.first() && nodeId <= NODE_IDS.last());

		final Path payloadPath = payloadPath(electionEventId, ballotId, ballotBoxId, nodeId);

		try {
			return objectMapper.readValue(payloadPath.toFile(), ControlComponentBallotBoxPayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format(
							"Unable to read the control component ballot box payload file. [electionEventId: %s, ballotId: %s, ballotBoxId: %s, nodeId: %d]",
							electionEventId, ballotId, ballotBoxId, nodeId), e);
		}
	}

	/**
	 * Saves the control component ballot box payload to the filesystem for the given election event, ballot, ballot box, control component
	 * combination.
	 *
	 * @param ballotId                          the corresponding ballot id.
	 * @param controlComponentBallotBoxPayloads the payloads to save.
	 * @return the path of the saved file.
	 * @throws NullPointerException     if any of the inputs is null.
	 * @throws IllegalArgumentException if any of the inputs is not valid.
	 * @see PathResolver to get the resolved file Path.
	 */
	public List<Path> saveAll(final String ballotId, final List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads) {
		checkNotNull(controlComponentBallotBoxPayloads);

		final int numberOfPayloads = controlComponentBallotBoxPayloads.size();
		final int expectedNumberOfPayloads = NODE_IDS.size();
		checkArgument(numberOfPayloads == expectedNumberOfPayloads, "Wrong number of payload to save. [actual: %s, expected: %s]", numberOfPayloads,
				expectedNumberOfPayloads);

		checkArgument(allEqual(controlComponentBallotBoxPayloads.stream(), ControlComponentBallotBoxPayload::getEncryptionGroup),
				"All payloads must have the same encryption group.");
		checkArgument(allEqual(controlComponentBallotBoxPayloads.stream(), ControlComponentBallotBoxPayload::getElectionEventId),
				"All payloads must have the same election event id.");
		checkArgument(allEqual(controlComponentBallotBoxPayloads.stream(), ControlComponentBallotBoxPayload::getBallotBoxId),
				"All payloads must have the same ballot box id.");

		return controlComponentBallotBoxPayloads.stream()
				.map(controlComponentBallotBoxPayload -> {
					final String electionEventId = controlComponentBallotBoxPayload.getElectionEventId();
					final String ballotBoxId = controlComponentBallotBoxPayload.getBallotBoxId();
					final int nodeId = controlComponentBallotBoxPayload.getNodeId();

					final Path ballotBoxPath = payloadResolver.resolveBallotBoxPath(electionEventId, ballotId, ballotBoxId);
					final Path payloadPath = ballotBoxPath.resolve(FILE_PREFIX + nodeId + Constants.JSON);

					try {
						Files.createDirectories(ballotBoxPath);
						final Path filePath = Files.createFile(payloadPath);
						objectMapper.writeValue(filePath.toFile(), controlComponentBallotBoxPayload);
						return filePath;
					} catch (final IOException e) {
						throw new UncheckedIOException("Failed to write payload to file system.", e);
					}
				})
				.toList();
	}

	private Path payloadPath(final String electionEventId, final String ballotId, final String ballotBoxId, final int nodeId) {
		final Path ballotBoxPath = payloadResolver.resolveBallotBoxPath(electionEventId, ballotId, ballotBoxId);
		return ballotBoxPath.resolve(FILE_PREFIX + nodeId + Constants.JSON);
	}
}
