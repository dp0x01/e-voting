/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setuptally;

import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_WRITE_IN_OPTIONS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.securedatamanager.BoardPasswordValidation;

/**
 * Regroups the inputs needed by the SetupTallyEB algorithm. The inputs are :
 * <ul>
 * <li>EL<sub>pk,1</sub>, EL<sub>pk,2</sub> and EL<sub>pk,3</sub>, the CCM election public keys.</li>
 * <li>π<sub>EL<sub>pk, j</sub></sub>, the schnorr proofs.</li>
 * <li>δ - 1, the maximum number of write-ins in all verification card sets.</li>
 * <li>(PW<sub>0</sub>, ... , PW<sub>k-1</sub>), the passwords of k electoral board members.</li>
 * </ul>
 */
public record SetupTallyEBInput(GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys,
								GroupVector<GroupVector<SchnorrProof, ZqGroup>, ZqGroup> schnorrProofs,
								int maxWriteInsInAllVerificationCardSets,
								List<char[]> electoralBoardMembersPasswords) {

	private static final int MU = MAXIMUM_NUMBER_OF_WRITE_IN_OPTIONS + 1;

	/**
	 * Constructor for a SetupTallyEBInput.
	 *
	 * @param ccmElectionPublicKeys                (EL<sub>pk,1</sub>, EL<sub>pk,2</sub>, EL<sub>pk,3</sub>, EL<sub>pk,4</sub>), the CCM election
	 *                                             public keys as a {@link GroupVector}. Non-null.
	 * @param schnorrProofs                        π<sub>EL<sub>pk, j</sub></sub> , the schnorr proofs as a {@link GroupVector}. Non-null.
	 * @param maxWriteInsInAllVerificationCardSets δ - 1, the maximum number of write-ins in all verification card sets. Non-negative.
	 * @param electoralBoardMembersPasswords       (PW<sub>0</sub>, ... , PW<sub>k-1</sub>), the passwords of k electoral board members. Non-null.
	 *                                             Each password must be at least 12 characters long, must contain at least one upper case letter, one
	 *                                             lower case letter, one special character and one digit.
	 * @throws NullPointerException     if any of the input parameters is null.
	 * @throws IllegalArgumentException if
	 *                                  <ul>
	 *                                      <li>there are not exactly 4 CCM election public keys provided in the ccmElectionPublicKeys vector.</li>
	 *                                      <li>there are not exactly 4 schnorr proofs provided.</li>
	 *                                      <li>EL<sub>pk,1</sub>, EL<sub>pk,2</sub> and EL<sub>pk,3</sub> are not of size μ.</li>
	 *                                      <li>δ - 1 is strictly negative.</li>
	 *                                      <li>k is strictly smaller than 2.</li>
	 *                                      <li>the passwords do not fulfill the policy.</li>
	 *                                  </ul>
	 */
	public SetupTallyEBInput {
		checkNotNull(ccmElectionPublicKeys);
		checkNotNull(schnorrProofs);
		checkNotNull(electoralBoardMembersPasswords);
		electoralBoardMembersPasswords.forEach(Preconditions::checkNotNull);

		checkArgument(ccmElectionPublicKeys.getGroup().hasSameOrderAs(schnorrProofs.getGroup()),
				"The CCM election public keys and the schnorr proofs must have the same group order.");

		checkArgument(ccmElectionPublicKeys.size() == 4, "There must be exactly 4 CCM election public keys.");
		checkArgument(schnorrProofs.size() == 4, "There must be exactly 4 Schnorr proofs.");
		checkArgument(ccmElectionPublicKeys.getElementSize() == MU, "The CCM election public keys must all be of size mu.");
		checkArgument(maxWriteInsInAllVerificationCardSets >= 0,
				"The maximum number of write-ins in all verification card sets must be a non-negative number.");
		final List<char[]> passwords = electoralBoardMembersPasswords.stream().map(char[]::clone).toList();
		checkArgument(passwords.size() >= 2, "There must be at least 2 electoral board members.");
		passwords.forEach(BoardPasswordValidation::validate);
	}
}
