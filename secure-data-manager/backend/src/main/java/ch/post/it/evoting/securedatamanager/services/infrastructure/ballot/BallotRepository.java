/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.ballot;

import static java.util.Collections.singletonMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.orientechnologies.common.exception.OException;
import com.orientechnologies.orient.core.record.impl.ODocument;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.securedatamanager.services.application.exception.DatabaseException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.AbstractEntityRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.DatabaseManager;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballotbox.BallotBoxRepository;

/**
 * Implementation of the Ballot Repository
 */
@Repository
public class BallotRepository extends AbstractEntityRepository {

	@Autowired
	@Lazy
	BallotBoxRepository ballotBoxRepository;

	/**
	 * Constructor
	 *
	 * @param databaseManager the injected database manager
	 */
	@Autowired
	public BallotRepository(final DatabaseManager databaseManager) {
		super(databaseManager);
	}

	@PostConstruct
	@Override
	public void initialize() {
		super.initialize();
	}

	/**
	 * Lists aliases of the specified ballot.
	 *
	 * @param id the ballot identifier
	 * @return the aliases
	 * @throws DatabaseException failed to list aliases.
	 */
	public List<String> listAliases(final String id) {
		final String sql = "select alias from " + entityName() + " where id=:id";
		final Map<String, Object> parameters = singletonMap(JsonConstants.ID, id);
		final List<ODocument> documents;
		try {
			documents = selectDocuments(sql, parameters, -1);
		} catch (final OException e) {
			throw new DatabaseException("Failed to list aliases.", e);
		}
		final List<String> aliases = new ArrayList<>(documents.size());
		for (final ODocument document : documents) {
			aliases.add(document.field("alias", String.class));
		}
		return aliases;
	}

	/**
	 * Update the related ballot box to a given list of ballots
	 *
	 * @param ids - the list of ballots ids to be updated
	 */
	public void updateRelatedBallotBox(final List<String> ballotIds) {
		try {
			for (final String id : ballotIds) {
				final ODocument ballot = getDocument(id);
				final List<String> relatedIds = ballotBoxRepository.listAliases(id);
				// to maintain compatibility with FE, save as comma-separated
				// string
				ballot.field(JsonConstants.BALLOT_BOXES, StringUtils.join(relatedIds, ","));
				saveDocument(ballot);
			}
		} catch (final OException e) {
			throw new DatabaseException("Failed to update related ballot box.", e);
		}
	}

	/**
	 * Updates the ballot options for every contest.
	 *
	 * @param ballotId              - identifier of the ballot to be updated
	 * @param newBallotContestsJson - contests of the ballot
	 */
	public void updateBallotContests(final String ballotId, final String newBallotContestsJson) {
		try {
			final ODocument ballot = getDocument(ballotId);
			final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
			final JsonNode ballotContests = objectMapper.readTree(ballot.toJSON()).get(JsonConstants.CONTESTS);
			final JsonNode newBallotContests = objectMapper.readTree(newBallotContestsJson);

			IntStream.range(0, ballotContests.size()).forEach(i -> {
				final JsonNode newBallotOptions = newBallotContests.get(i).get(JsonConstants.OPTIONS);
				((ObjectNode) ballotContests.get(i)).replace(JsonConstants.OPTIONS, newBallotOptions);

				final JsonNode newBallotAttributes = newBallotContests.get(i).get(JsonConstants.ATTRIBUTES);
				((ObjectNode) ballotContests.get(i)).replace(JsonConstants.ATTRIBUTES, newBallotAttributes);
			});

			final String updatedBallotJson = ((ObjectNode) objectMapper.readTree(ballot.toJSON()))
					.set(JsonConstants.CONTESTS, ballotContests).toString();
			final ODocument updatedBallot = new ODocument().fromJSON(updatedBallotJson);
			saveDocument(updatedBallot);
		} catch (final OException | JsonProcessingException e) {
			throw new DatabaseException("Failed to update ballot contests.", e);
		}
	}

	/**
	 * Lists ballots which belong to a given election event in JSON format.
	 *
	 * @param electionEventId the election event identifier
	 * @return the ballots.
	 */
	public String listByElectionEvent(final String electionEventId) {
		return list(singletonMap("electionEvent.id", electionEventId));
	}

	@Override
	protected String entityName() {
		return Ballot.class.getSimpleName();
	}
}
