/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.integerToString;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.stream.IntStream;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;

/**
 * Implements the GenVerCardSetKeys algorithm.
 */
@Service
public class GenVerCardSetKeysAlgorithm {

	private static final int PHI = MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;

	private final ElGamal elGamal;
	private final ZeroKnowledgeProof zeroKnowledgeProof;

	public GenVerCardSetKeysAlgorithm(
			final ElGamal elGamal,
			final ZeroKnowledgeProof zeroKnowledgeProof) {
		this.elGamal = elGamal;
		this.zeroKnowledgeProof = zeroKnowledgeProof;
	}

	/**
	 * Generates the verification card set keys by combining the CCR<sub>j</sub> Choice Return Codes encryption public keys pk<sub>CCR_j</sub>. Also
	 * verifies the Schnorr proofs associated to each CCR<sub>j</sub> public key.
	 *
	 * @param electionEventId                           the current election event id. Must be valid and non-null.
	 * @param ccrjChoiceReturnCodesEncryptionPublicKeys the Choice Return Codes encryption public keys (pk<sub>CCR_1</sub>, pk<sub>CCR_2</sub>,
	 *                                                  pk<sub>CCR_3</sub>, pk<sub>CCR_4</sub>) as a {@link GroupVector}. Must be non-null.
	 * @param ccrjSchnorrProofs                         the Schnorr proofs, (&pi;<sub>pkCCR_1</sub>, &pi;<sub>pkCCR_2</sub>, &pi;<sub>pkCCR_3</sub>,
	 *                                                  &pi;<sub>pkCCR_4</sub>) corresponding to the
	 *                                                  {@code ccrjChoiceReturnCodesEncryptionPublicKeys}. Must be non-null.
	 * @return the combined Choice Return Codes encryption public key <b>pk</b><sub>CCR</sub>
	 * @throws NullPointerException     if {@code ccrjChoiceReturnCodesEncryptionPublicKeys} or {@code ccrjSchnorrProofs} is null.
	 * @throws IllegalArgumentException if
	 *                                  <ul>
	 *                                      <li>There is not the expected number of CCR_j Choice Return Codes encryption public keys.</li>
	 *                                      <li>There is not the xepcted number of CCR_j Schnorr proofs.</li>
	 *                                      <li>The CCR_j Choice Return Codes encryption public keys don't have the expected element size.</li>
	 *                                      <li>The CCR_j Schnorr proofs don't have the expected element size.</li>
	 *                                      <li>The CCR_j Choice Return Codes encryption public keys and Schnorr proofs have a different group.</li>
	 *                                  </ul>
	 */
	@SuppressWarnings("java:S117")
	public ElGamalMultiRecipientPublicKey genVerCardSetKeys(final String electionEventId,
			final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccrjChoiceReturnCodesEncryptionPublicKeys,
			final GroupVector<GroupVector<SchnorrProof, ZqGroup>, ZqGroup> ccrjSchnorrProofs) {

		validateUUID(electionEventId);
		checkNotNull(ccrjChoiceReturnCodesEncryptionPublicKeys);
		checkNotNull(ccrjSchnorrProofs);

		// Variables.
		final String ee = electionEventId;

		// Size checks.
		final int keysNumber = ccrjChoiceReturnCodesEncryptionPublicKeys.size();
		final int nodesNumber = NODE_IDS.size();
		checkArgument(keysNumber == nodesNumber,
				"Wrong number of CCR_j Choice Return Codes encryption public keys. [expected: %s, found :%s]", nodesNumber, keysNumber);
		final int proofsNumber = ccrjSchnorrProofs.size();
		checkArgument(proofsNumber == nodesNumber, "Wrong number of CCR_j Schnorr proofs. [expected: %s, found :%s]", nodesNumber, proofsNumber);

		checkArgument(ccrjChoiceReturnCodesEncryptionPublicKeys.getElementSize() == PHI,
				"The CCR_j Choice Return Codes encryption public keys must be of size %s", PHI);
		checkArgument(ccrjSchnorrProofs.getElementSize() == PHI, "The CCR_j Schnorr proofs must be of size %s", PHI);

		// Cross group check.
		checkArgument(ccrjChoiceReturnCodesEncryptionPublicKeys.getGroup().hasSameOrderAs(ccrjSchnorrProofs.getGroup()),
				"The CCR_j Choice Return Codes encryption public keys must have the same group order as the CCR_j Schnorr proofs.");

		// Operation.
		final List<List<String>> i_aux = IntStream.range(0, nodesNumber)
				.mapToObj(j -> List.of(ee, "GenKeysCCR", integerToString(j + 1))) // Due to zero indexing.
				.toList();

		IntStream.range(0, PHI)
				.parallel()
				.forEach(i -> {
					final List<Boolean> verifSch_j = IntStream.range(0, nodesNumber)
							.mapToObj(j -> {
								final SchnorrProof pi_pkCCR_j_i = ccrjSchnorrProofs.get(j).get(i);
								final GqElement pkCCR_j_i = ccrjChoiceReturnCodesEncryptionPublicKeys.get(j).get(i);
								final List<String> i_aux_j = i_aux.get(j);

								return zeroKnowledgeProof.verifySchnorrProof(pi_pkCCR_j_i, pkCCR_j_i, i_aux_j);
							})
							.toList();

					final boolean verifSch_1 = verifSch_j.get(0);
					final boolean verifSch_2 = verifSch_j.get(1);
					final boolean verifSch_3 = verifSch_j.get(2);
					final boolean verifSch_4 = verifSch_j.get(3);

					if (!(verifSch_1 && verifSch_2 && verifSch_3 && verifSch_4)) {
						throw new IllegalStateException(
								String.format("Schnorr proofs verification failed. [verifSch_1: %s, verifSch_2: %s, verifSch_3: %s, verifSch_4: %s]",
										verifSch_1, verifSch_2, verifSch_3, verifSch_4));
					}
				});

		return elGamal.combinePublicKeys(ccrjChoiceReturnCodesEncryptionPublicKeys);
	}

}
