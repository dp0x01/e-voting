/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.hasNoDuplicates;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.json.JsonArray;
import javax.json.JsonObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.evotinglibraries.domain.tally.TallyComponentVotesPayload;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AuthorizationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.securedatamanager.commons.JsonUtils;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballotbox.BallotBoxRepository;

@Service
@ConditionalOnProperty("role.isTally")
public class TallyComponentFilesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TallyComponentFilesService.class);

	private final TallyComponentDecryptService tallyComponentDecryptService;
	private final TallyComponentEch0110Service tallyComponentEch0110Service;
	private final TallyComponentEch0222Service tallyComponentEch0222Service;
	private final CantonConfigTallyService cantonConfigTallyService;
	private final BallotBoxRepository ballotBoxRepository;
	private final TallyComponentVotesService tallyComponentVotesService;

	public TallyComponentFilesService(
			final TallyComponentDecryptService tallyComponentDecryptService,
			final TallyComponentEch0110Service tallyComponentEch0110Service,
			final TallyComponentEch0222Service tallyComponentEch0222Service,
			final CantonConfigTallyService cantonConfigTallyService,
			final BallotBoxRepository ballotBoxRepository,
			final TallyComponentVotesService tallyComponentVotesService) {
		this.tallyComponentDecryptService = tallyComponentDecryptService;
		this.tallyComponentEch0110Service = tallyComponentEch0110Service;
		this.tallyComponentEch0222Service = tallyComponentEch0222Service;
		this.cantonConfigTallyService = cantonConfigTallyService;
		this.ballotBoxRepository = ballotBoxRepository;
		this.tallyComponentVotesService = tallyComponentVotesService;
	}

	/**
	 * Generates and persists the tally output files.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @throws NullPointerException      if the election event id is null.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	public void generate(final String electionEventId) {
		validateUUID(electionEventId);

		LOGGER.debug("Generating tally files... [electionEventId: {}]", electionEventId);

		final Map<AuthorizationType, TallyComponentVotesPayload> authorizationToTallyComponentVotesPayloadMap =
				getAuthorizationToTallyComponentVotesPayloadMap(electionEventId);

		tallyComponentDecryptService.generate(electionEventId, authorizationToTallyComponentVotesPayloadMap);
		tallyComponentEch0110Service.generate(electionEventId, authorizationToTallyComponentVotesPayloadMap.keySet());
		tallyComponentEch0222Service.generate(electionEventId);

		LOGGER.info("Tally files successfully generated. [electionEventId: {}]", electionEventId);
	}

	/**
	 * @return the map of configuration authorizations to corresponding tally component votes payload for all ballot boxes that are decrypted and
	 * synchronized. The mapping is done through the field {@link AuthorizationType#getAuthorizationName()}.
	 */
	private Map<AuthorizationType, TallyComponentVotesPayload> getAuthorizationToTallyComponentVotesPayloadMap(final String electionEventId) {

		final Configuration configuration = cantonConfigTallyService.load(electionEventId);
		final List<AuthorizationType> allAuthorizations = configuration.getAuthorizations().getAuthorization();

		final JsonArray ballotBoxesJsonArray = JsonUtils.getJsonObject(ballotBoxRepository.listByElectionEvent(electionEventId))
				.getJsonArray(JsonConstants.RESULT);

		final Predicate<JsonObject> isDecrypted = bb -> BallotBoxStatus.DECRYPTED.name().equals(bb.getString(JsonConstants.STATUS));

		record TallyComponentVotesTuple(AuthorizationType authorization, TallyComponentVotesPayload tallyComponentVotesPayload) {
		}

		final Map<AuthorizationType, TallyComponentVotesPayload> authorizationToTallyComponentVotesPayloadMap = ballotBoxesJsonArray.stream()
				.parallel()
				.map(JsonObject.class::cast)
				.filter(isDecrypted)
				.map(ballotBoxJsonObject -> {

					final String ballotId = ballotBoxJsonObject.getJsonObject(JsonConstants.BALLOT).getString(JsonConstants.ID);
					final String ballotBoxId = ballotBoxJsonObject.getString(JsonConstants.ID);

					// Ballot box default title corresponds to the authorization name.
					final String ballotBoxDefaultTitle = ballotBoxJsonObject.getString(JsonConstants.DEFAULT_TITLE);

					final AuthorizationType authorization = allAuthorizations.stream().parallel()
							.filter(authorizationType -> authorizationType.getAuthorizationName().equals(ballotBoxDefaultTitle))
							.collect(MoreCollectors.onlyElement());

					return new TallyComponentVotesTuple(authorization, tallyComponentVotesService.load(electionEventId, ballotId, ballotBoxId));
				})
				.collect(Collectors.toMap(TallyComponentVotesTuple::authorization, TallyComponentVotesTuple::tallyComponentVotesPayload));

		// Ensure that the authorization names are unique since this is the field we use for matching.
		checkState(hasNoDuplicates(authorizationToTallyComponentVotesPayloadMap.keySet().stream().parallel()
				.map(AuthorizationType::getAuthorizationName)
				.toList()), "There cannot be duplicated authorization names. [electionEventId: %s]", electionEventId);

		return authorizationToTallyComponentVotesPayloadMap;
	}

}
