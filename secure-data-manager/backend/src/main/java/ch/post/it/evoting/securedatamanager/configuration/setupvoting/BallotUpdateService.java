/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.StringReader;
import java.io.UncheckedIOException;
import java.util.Collection;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.Contest;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionOption;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.EncryptionParametersPayload;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.securedatamanager.EncryptionParametersPayloadService;

/**
 * Service for updating information of the ballots for a given election event.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class BallotUpdateService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotUpdateService.class);

	private final ObjectMapper objectMapper;
	private final EncryptionParametersPayloadService encryptionParametersPayloadService;
	private final Map<String, SmallPrimesTuple> electionEventRepresentationMap = new ConcurrentHashMap<>();
	private final Map<String, String> optionIdRepresentationMap = new ConcurrentHashMap<>();

	public BallotUpdateService(final EncryptionParametersPayloadService encryptionParametersPayloadService, final ObjectMapper objectMapper) {
		this.encryptionParametersPayloadService = encryptionParametersPayloadService;
		this.objectMapper = objectMapper;
	}

	/**
	 * Update the options' representation value of the ballot.
	 *
	 * @param electionEventId The electionEventId
	 * @param ballot          The ballot to be updated.
	 * @return Return a JsonObject with the updated ballot.
	 */
	public JsonObject updateOptionsRepresentation(final String electionEventId, final JsonObject ballot) {
		validateUUID(electionEventId);
		checkNotNull(ballot);

		final SmallPrimesTuple smallPrimesTuple = getSmallPrimesTuple(electionEventId);

		final JsonObject updateBallot = updateBallotOptions(ballot, smallPrimesTuple);
		LOGGER.info("Update ballot options' representation. [electionEventId: {}, ballotId: {}]", electionEventId, ballot.getString("id"));

		return updateBallot;
	}

	private SmallPrimesTuple getSmallPrimesTuple(final String electionEventId) {
		return electionEventRepresentationMap
				.computeIfAbsent(electionEventId, k -> {
					final EncryptionParametersPayload encryptionParametersPayload = encryptionParametersPayloadService.load(electionEventId);
					final List<PrimeGqElement> primeGqElementList = encryptionParametersPayload.getSmallPrimes().stream().toList();
					final DequeBasedSynchronizedStack<PrimeGqElement> smallPrimesDeque = new DequeBasedSynchronizedStack<>();
					smallPrimesDeque.addAll(primeGqElementList);
					return new SmallPrimesTuple(smallPrimesDeque, primeGqElementList.size());
				});
	}

	private JsonObject updateBallotOptions(final JsonObject ballotObject, final SmallPrimesTuple smallPrimesTuple) {

		final Ballot ballot = toBallot(ballotObject);

		final int orderedElectionOptionsSize = ballot.getOrderedElectionOptions().size();
		checkArgument(orderedElectionOptionsSize <= smallPrimesTuple.originalSize,
				"The number of ballot options must be smaller than or equal to the available primes. [#options:%s, #primes:%s]",
				orderedElectionOptionsSize, smallPrimesTuple.originalSize);

		ballot.contests().stream()
				.map(Contest::options)
				.flatMap(Collection::stream)
				.forEach(electionOption -> electionOption.setRepresentation(
						getRepresentationValue(electionOption, smallPrimesTuple.smallPrimesDeque)));

		return toJsonObject(ballot);
	}

	private String getRepresentationValue(final ElectionOption option, final DequeBasedSynchronizedStack<PrimeGqElement> representationIds) {
		return optionIdRepresentationMap.computeIfAbsent(option.getId(), k -> {
			final PrimeGqElement primeGqElement = representationIds.pop();
			return primeGqElement.getValue().toString();
		});
	}

	private JsonObject toJsonObject(final Ballot ballot) {
		final JsonObject updatedBallot;
		try {
			final String ballotJson = objectMapper.writeValueAsString(ballot);

			final JsonReader jsonReader = Json.createReader(new StringReader(ballotJson));
			updatedBallot = jsonReader.readObject();
			jsonReader.close();

		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot deserialize the Ballot object to a ballot JsonObject.", e);
		}
		return updatedBallot;
	}

	private Ballot toBallot(final JsonObject ballotObject) {
		final Ballot ballot;
		try {
			ballot = objectMapper.readValue(ballotObject.toString(), Ballot.class);
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot deserialize the ballot json string to a valid Ballot object.", e);
		}
		return ballot;
	}

	record SmallPrimesTuple(DequeBasedSynchronizedStack<PrimeGqElement> smallPrimesDeque, int originalSize) {
	}

	private static class DequeBasedSynchronizedStack<T> {

		// Internal Deque which gets decorated for synchronization.
		private final Deque<T> dequeStore;

		public DequeBasedSynchronizedStack() {
			this.dequeStore = new LinkedList<>();
		}

		public synchronized T pop() {
			return this.dequeStore.pop();
		}

		public synchronized boolean addAll(final Collection<T> collection) {
			return this.dequeStore.addAll(collection);
		}

		public synchronized int size() {
			return this.dequeStore.size();
		}
	}

}
