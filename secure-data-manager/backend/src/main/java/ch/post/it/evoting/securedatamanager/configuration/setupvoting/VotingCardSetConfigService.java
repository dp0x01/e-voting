/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

/**
 * This is an application service that manages voting card sets.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class VotingCardSetConfigService {

	private final VotingCardSetRepository votingCardSetRepository;

	public VotingCardSetConfigService(
			final VotingCardSetRepository votingCardSetRepository) {
		this.votingCardSetRepository = votingCardSetRepository;
	}

	public String getBallotBoxIdFromVerificationCardSetId(final String verificationCardSetId) {
		validateUUID(verificationCardSetId);
		final String votingCardSetId = votingCardSetRepository.getVotingCardSetId(verificationCardSetId);
		return votingCardSetRepository.getBallotBoxId(votingCardSetId);
	}
}
