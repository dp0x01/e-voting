/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import ch.ech.xmlns.ech_0222._1.Delivery;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.xml.XmlNormalizer;
import ch.post.it.evoting.evotinglibraries.xml.mapper.RawDataDeliveryMapper;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingdecrypt.Results;

@Service
@ConditionalOnProperty("role.isTally")
public class TallyComponentEch0222Service {

	private static final Logger LOGGER = LoggerFactory.getLogger(TallyComponentEch0222Service.class);

	private final TallyComponentDecryptService tallyComponentDecryptService;
	private final CantonConfigTallyService cantonConfigTallyService;
	private final TallyComponentEch0222FileRepository tallyComponentEch0222FileRepository;
	private final XmlNormalizer xmlNormalizer;

	public TallyComponentEch0222Service(
			final TallyComponentDecryptService tallyComponentDecryptService,
			final CantonConfigTallyService cantonConfigTallyService,
			final TallyComponentEch0222FileRepository tallyComponentEch0222FileRepository,
			final XmlNormalizer xmlNormalizer) {
		this.tallyComponentDecryptService = tallyComponentDecryptService;
		this.cantonConfigTallyService = cantonConfigTallyService;
		this.tallyComponentEch0222FileRepository = tallyComponentEch0222FileRepository;
		this.xmlNormalizer = xmlNormalizer;
	}

	/**
	 * Generates and persists the eCH-0222 tally file.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @throws NullPointerException      if the election event id is null.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	public void generate(final String electionEventId) {
		validateUUID(electionEventId);

		LOGGER.debug("Generating tally component eCH-0222 file... [electionEventId: {}]", electionEventId);

		final Configuration configuration = cantonConfigTallyService.load(electionEventId);
		final Results results = tallyComponentDecryptService.load(electionEventId, configuration.getContest().getContestIdentification());

		final Delivery delivery = RawDataDeliveryMapper.createECH0222(electionEventId, configuration, results);
		final Delivery normalizedDelivery = xmlNormalizer.normalizeWriteInsEch0220(delivery);

		tallyComponentEch0222FileRepository.save(normalizedDelivery, electionEventId);

		LOGGER.info("Tally component eCH-0222 file successfully generated. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Loads the tally component eCH-0222 for the given election event id and validates its signature.
	 * <p>
	 * The result of this method is cached.
	 *
	 * @param electionEventId       the election event id. Must be non-null and a valid UUID.
	 * @param contestIdentification the contest identification. Must be non-null and non-blank.
	 * @return the tally component eCH-0222 as {@link Delivery}.
	 * @throws NullPointerException      if the election event id or the contest identification is null.
	 * @throws IllegalArgumentException  if the contest identification is blank.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	@Cacheable("tallyComponentEch0222s")
	public Delivery load(final String electionEventId, final String contestIdentification) {
		validateUUID(electionEventId);
		checkNotNull(contestIdentification);
		checkArgument(!contestIdentification.isBlank(), "The contest identification cannot be blank.");

		return tallyComponentEch0222FileRepository.load(electionEventId, contestIdentification)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Could not find the requested tally component eCH-0222 file. [electionEventId: %s, contestIdentification: %s]",
								electionEventId, contestIdentification)));
	}
}
