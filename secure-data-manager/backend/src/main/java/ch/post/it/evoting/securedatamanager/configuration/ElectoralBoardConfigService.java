/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;
import java.util.List;

import javax.json.JsonObject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.securedatamanager.BoardPasswordHashService;
import ch.post.it.evoting.securedatamanager.commons.JsonUtils;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.SynchronizeStatus;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electoralboard.ElectoralBoardRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

@Service
@ConditionalOnProperty("role.isSetup")
public class ElectoralBoardConfigService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ElectoralBoardConfigService.class);

	private final BoardPasswordHashService boardPasswordHashService;
	private final ElectoralBoardRepository electoralBoardRepository;
	private final ConfigurationEntityStatusService configurationEntityStatusService;
	private final ElectoralBoardConstitutionService electoralBoardConstitutionService;

	public ElectoralBoardConfigService(
			final BoardPasswordHashService boardPasswordHashService,
			final ElectoralBoardRepository electoralBoardRepository,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final ElectoralBoardConstitutionService electoralBoardConstitutionService) {
		this.boardPasswordHashService = boardPasswordHashService;
		this.electoralBoardRepository = electoralBoardRepository;
		this.configurationEntityStatusService = configurationEntityStatusService;
		this.electoralBoardConstitutionService = electoralBoardConstitutionService;
	}

	/**
	 * Constitutes the electoral board and updates the electoral board status.
	 *
	 * @param electionEventId                the election event id
	 * @param electoralBoardId               the electoral board id
	 * @param electoralBoardMembersPasswords the electoral board members' passwords
	 * @return true if constitution and update operations were successful, false otherwise.
	 */
	public boolean constitute(final String electionEventId, final String electoralBoardId, final List<char[]> electoralBoardMembersPasswords) {
		validateUUID(electionEventId);
		validateUUID(electoralBoardId);

		checkNotNull(electoralBoardMembersPasswords);
		electoralBoardMembersPasswords.forEach(Preconditions::checkNotNull);
		final List<char[]> passwords = electoralBoardMembersPasswords.stream().map(char[]::clone).toList();
		checkArgument(passwords.size() >= 2);
		// Wipe the passwords after usage
		electoralBoardMembersPasswords.forEach(pw -> Arrays.fill(pw, '\u0000'));

		LOGGER.debug("Constituting electoral board... [electionEventId: {}, electoralBoardId: {}]", electionEventId, electoralBoardId);

		final List<byte[]> hashes = boardPasswordHashService.hashPasswords(passwords);

		LOGGER.debug("Hashed electoral board members passwords. [electionEventId: {}, electoralBoardId: {}]", electionEventId, electoralBoardId);

		electoralBoardConstitutionService.constitute(electionEventId, passwords, hashes);

		if (updateElectoralBoardStatus(electoralBoardId)) {
			LOGGER.info("Constitute electoral board successful. [electionEventId: {}, electoralBoardId: {}]", electionEventId, electoralBoardId);
			return true;
		} else {
			LOGGER.error("Constitute electoral board unsuccessful. [electionEventId: {}, electoralBoardId: {}]", electionEventId, electoralBoardId);
			return false;
		}
	}

	/**
	 * Change the state of the electoral board from constituted to {@link Status#SIGNED} for a given election event and electoral board id.
	 *
	 * @param electionEventId  the election event id.
	 * @param electoralBoardId the electoral board unique id.
	 * @return true if the status is successfully changed to signed. Otherwise, false.
	 * @throws ResourceNotFoundException if the electoral board is not found.
	 */
	public boolean sign(final String electionEventId, final String electoralBoardId) throws ResourceNotFoundException {

		final JsonObject electoralBoardJson = loadElectoralBoardJsonObject(electoralBoardId);

		if (electoralBoardJson != null && electoralBoardJson.containsKey(JsonConstants.STATUS)) {
			final String status = electoralBoardJson.getString(JsonConstants.STATUS);
			if (Status.READY.name().equals(status)) {
				LOGGER.debug("Changing the status of the electoral board... [electionEventId: {}, electoralBoardId: {}]", electionEventId,
						electoralBoardId);
				configurationEntityStatusService.updateWithSynchronizedStatus(Status.SIGNED.name(), electoralBoardId, electoralBoardRepository,
						SynchronizeStatus.PENDING);

				LOGGER.info("The electoral board is successfully signed. [electionEventId: {}, electoralBoardId: {}]", electionEventId,
						electoralBoardId);

				return true;
			}
		}

		return false;
	}

	/**
	 * Updates the electoral board status from {@link Status#LOCKED} to {@link Status#READY}.
	 *
	 * @param electoralBoardId the electoral board id
	 */
	private boolean updateElectoralBoardStatus(final String electoralBoardId) {
		final String status;
		try {
			status = loadElectoralBoardJsonObject(electoralBoardId).getString("status");
		} catch (final ResourceNotFoundException e) {
			LOGGER.warn(String.format("Could not load the electoral board JSON object. [electoralBoardId: %s]", electoralBoardId), e);
			return false;
		}
		if (!Status.LOCKED.name().equals(status)) {
			LOGGER.warn("Status of electoral board is not LOCKED. [electoralBoardId: {}, status: {}]", electoralBoardId, status);
			return false;
		}

		configurationEntityStatusService.update(Status.READY.name(), electoralBoardId, electoralBoardRepository);
		LOGGER.debug("Updated status of electoral board from LOCKED to READY. [electoralBoardId: {}]", electoralBoardId);
		return true;
	}

	/**
	 * Loads the electoral board json object.
	 *
	 * @param electoralBoardId the electoral board id
	 * @return the electoral board json object
	 * @throws ResourceNotFoundException if the electoral board json object could not be retrieved.
	 */
	private JsonObject loadElectoralBoardJsonObject(final String electoralBoardId) throws ResourceNotFoundException {
		final String electoralBoardJSON = electoralBoardRepository.find(electoralBoardId);
		if (StringUtils.isEmpty(electoralBoardJSON) || JsonConstants.EMPTY_OBJECT.equals(electoralBoardJSON)) {
			throw new ResourceNotFoundException(String.format("Electoral Board not found. [electoralBoardId: %s]", electoralBoardId));
		}
		return JsonUtils.getJsonObject(electoralBoardJSON);
	}

}
