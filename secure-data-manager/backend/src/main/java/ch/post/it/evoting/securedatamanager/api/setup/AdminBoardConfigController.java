/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api.setup;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.configuration.AdminBoardConfigService;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;

/**
 * The admin board end-point.
 */
@RestController
@RequestMapping("/sdm-backend/setup/adminboard-management")
@Api(value = "Administration Board REST API")
@ConditionalOnProperty("role.isSetup")
public class AdminBoardConfigController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminBoardConfigController.class);

	private final AdminBoardConfigService adminBoardConfigService;

	public AdminBoardConfigController(final AdminBoardConfigService adminBoardConfigService) {
		this.adminBoardConfigService = adminBoardConfigService;
	}

	/**
	 * Constitutes the adminBoards.
	 *
	 * @param adminBoardId        the admin board authority id.
	 * @param adminBoardPasswords the list of members' password.
	 */
	@PostMapping(value = "adminboards/{adminBoardId}")
	@ResponseStatus(HttpStatus.CREATED)
	public void constitute(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String adminBoardId,
			@ApiParam(value = "adminBoardPasswords", required = true)
			@RequestBody
			final List<char[]> adminBoardPasswords) throws ResourceNotFoundException {

		validateUUID(adminBoardId);
		checkNotNull(adminBoardPasswords);
		checkArgument(adminBoardPasswords.size() >= 2);

		LOGGER.info("Received request to constitute administration board. [adminBoardId: {}]", adminBoardId);

		adminBoardConfigService.constitute(adminBoardId, adminBoardPasswords);
	}

	/**
	 * Activates the adminBoards.
	 */
	@PutMapping(value = "adminboards/{adminBoardId}")
	@ResponseStatus(HttpStatus.OK)
	public void activate(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String adminBoardId,
			@ApiParam(value = "adminBoardPasswords", required = true)
			@RequestBody
			final List<char[]> adminBoardPasswords) {

		validateUUID(adminBoardId);
		checkNotNull(adminBoardPasswords);

		LOGGER.info("Received request to constitute administration board. [adminBoardId: {}]", adminBoardId);

		adminBoardConfigService.activate(adminBoardId, adminBoardPasswords);
	}
}
