/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.securedatamanager.commons.Constants.CONFIG_DIR_NAME_ONLINE;
import static ch.post.it.evoting.securedatamanager.commons.Constants.CONFIG_DIR_NAME_VOTEVERIFICATION;
import static ch.post.it.evoting.securedatamanager.configuration.setupvoting.SetupComponentLVCCAllowListPayloadFileRepository.PAYLOAD_FILE_NAME;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.Collections;
import java.util.Locale;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@DisplayName("A SetupComponentLVCCAllowListPayloadService")
class SetupComponentLVCCAllowListPayloadServiceTest {
	private static final Random random = RandomFactory.createRandom();
	private static final String ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String NON_EXISTING_VERIFICATION_CARD_SET_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
	private static final ch.post.it.evoting.cryptoprimitives.hashing.Hash hash = HashFactory.createHash();

	private static PathResolver pathResolver;
	private static SetupComponentLVCCAllowListPayloadService setupComponentLVCCAllowListPayloadService;

	@BeforeAll
	static void setUpAll(
			@TempDir
			final Path tempDir) throws IOException {

		Files.createDirectories(
				tempDir.resolve("sdm/config").resolve(ELECTION_EVENT_ID).resolve(CONFIG_DIR_NAME_ONLINE).resolve(CONFIG_DIR_NAME_VOTEVERIFICATION)
						.resolve(VERIFICATION_CARD_SET_ID));

		final PathResolver pathResolver = new PathResolver(tempDir.toString());

		final SetupComponentLVCCAllowListPayloadFileRepository setupComponentLVCCAllowListPayloadFileRepository =
				new SetupComponentLVCCAllowListPayloadFileRepository(objectMapper, pathResolver);

		setupComponentLVCCAllowListPayloadFileRepository.save(validLongVoteCastReturnCodesAllowListPayload());

		setupComponentLVCCAllowListPayloadService = new SetupComponentLVCCAllowListPayloadService(
				setupComponentLVCCAllowListPayloadFileRepository);
	}

	private static SetupComponentLVCCAllowListPayload validLongVoteCastReturnCodesAllowListPayload() {
		final String lVCC = Base64.getEncoder().encodeToString(new byte[] { 2, 65, 35 });
		final SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload =
				new SetupComponentLVCCAllowListPayload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, Collections.singletonList(lVCC));

		byte[] payloadHash = hash.recursiveHash(setupComponentLVCCAllowListPayload);

		setupComponentLVCCAllowListPayload.setSignature(new CryptoPrimitivesSignature(payloadHash));

		return setupComponentLVCCAllowListPayload;
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private SetupComponentLVCCAllowListPayloadService setupComponentLVCCAllowListPayloadServiceTemp;

		private SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload;

		@BeforeAll
		void setUpAll(
				@TempDir
				final Path tempDir) throws IOException {

			Files.createDirectories(
					tempDir.resolve("sdm/config").resolve(ELECTION_EVENT_ID).resolve(CONFIG_DIR_NAME_ONLINE).resolve(CONFIG_DIR_NAME_VOTEVERIFICATION)
							.resolve(VERIFICATION_CARD_SET_ID));

			pathResolver = new PathResolver(tempDir.toString());
			final SetupComponentLVCCAllowListPayloadFileRepository setupComponentLVCCAllowListPayloadFileRepositoryTemp =
					new SetupComponentLVCCAllowListPayloadFileRepository(objectMapper, pathResolver);

			setupComponentLVCCAllowListPayloadServiceTemp = new SetupComponentLVCCAllowListPayloadService(
					setupComponentLVCCAllowListPayloadFileRepositoryTemp);
		}

		@BeforeEach
		void setUp() {
			// Create payload.
			setupComponentLVCCAllowListPayload = validLongVoteCastReturnCodesAllowListPayload();
		}

		@Test
		@DisplayName("a valid payload does not throw")
		void saveValidPayload() {
			assertDoesNotThrow(() -> setupComponentLVCCAllowListPayloadServiceTemp.save(setupComponentLVCCAllowListPayload));

			assertTrue(Files.exists(
					pathResolver.resolveVerificationCardSetPath(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID).resolve(PAYLOAD_FILE_NAME)));
		}

		@Test
		@DisplayName("a null payload throws NullPointerException")
		void saveNullPayload() {
			assertThrows(NullPointerException.class, () -> setupComponentLVCCAllowListPayloadServiceTemp.save(null));
		}
	}

	@Nested
	@DisplayName("calling exist")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class ExistTest {

		@Test
		@DisplayName("for valid verification card set returns true")
		void existValid() {
			assertTrue(setupComponentLVCCAllowListPayloadService.exist(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
		}

		@Test
		@DisplayName("with invalid ids throws an exception")
		void existInvalidIdThrows() {
			assertAll(
					() -> assertThrows(NullPointerException.class,
							() -> setupComponentLVCCAllowListPayloadService.exist(null, VERIFICATION_CARD_SET_ID)),
					() -> assertThrows(FailedValidationException.class,
							() -> setupComponentLVCCAllowListPayloadService.exist("invalidElectionEventId", VERIFICATION_CARD_SET_ID)),
					() -> assertThrows(NullPointerException.class,
							() -> setupComponentLVCCAllowListPayloadService.exist(ELECTION_EVENT_ID, null)),
					() -> assertThrows(FailedValidationException.class,
							() -> setupComponentLVCCAllowListPayloadService.exist(ELECTION_EVENT_ID, "invalidVerificationCardSetId"))
			);
		}

		@Test
		@DisplayName("for non existing verification card set returns false")
		void existNonExistingVerificationCardSet() {
			assertFalse(setupComponentLVCCAllowListPayloadService.exist(ELECTION_EVENT_ID, NON_EXISTING_VERIFICATION_CARD_SET_ID));
		}

	}

	@Nested
	@DisplayName("loading")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class LoadTest {

		@Test
		@DisplayName("existing verification card set returns expected setup component LVCC allow list payload")
		void loadExistingVerificationCardSet() {

			final SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload =
					setupComponentLVCCAllowListPayloadService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);

			assertNotNull(setupComponentLVCCAllowListPayload);
		}

		@Test
		@DisplayName("with invalid ids throws an exception")
		void loadInvalidIdThrows() {
			assertAll(
					() -> assertThrows(NullPointerException.class,
							() -> setupComponentLVCCAllowListPayloadService.load(null, VERIFICATION_CARD_SET_ID)),
					() -> assertThrows(FailedValidationException.class,
							() -> setupComponentLVCCAllowListPayloadService.load("invalidElectionEventId", VERIFICATION_CARD_SET_ID)),
					() -> assertThrows(NullPointerException.class,
							() -> setupComponentLVCCAllowListPayloadService.load(ELECTION_EVENT_ID, null)),
					() -> assertThrows(FailedValidationException.class,
							() -> setupComponentLVCCAllowListPayloadService.load(ELECTION_EVENT_ID, "invalidVerificationCardSetId"))
			);
		}

		@Test
		@DisplayName("existing verification card set with missing payload throws")
		void loadMissingPayload() {

			final IllegalStateException illegalStateException = assertThrows(IllegalStateException.class, () ->
					setupComponentLVCCAllowListPayloadService.load(ELECTION_EVENT_ID, NON_EXISTING_VERIFICATION_CARD_SET_ID));

			assertEquals(String.format(
					"Requested setup component LVCC allow list payload is not present. [electionEventId: %s, verificationCardSetId: %s]",
					ELECTION_EVENT_ID, NON_EXISTING_VERIFICATION_CARD_SET_ID), Throwables.getRootCause(illegalStateException).getMessage());
		}

	}

}
