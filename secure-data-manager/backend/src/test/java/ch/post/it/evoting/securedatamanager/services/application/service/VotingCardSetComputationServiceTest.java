/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static java.nio.file.Files.write;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationData;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.securedatamanager.VotingCardSetServiceTestBase;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.PayloadStorageException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.SetupComponentVerificationDataPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

@ExtendWith(MockitoExtension.class)
class VotingCardSetComputationServiceTest extends VotingCardSetServiceTestBase {

	private static final String ELECTION_EVENT_ID = "a3d790fd1ac543f9b0a05ca79a20c9e2";
	private static final String VERIFICATION_CARD_SET_ID = "e8c82c55701c4bbdbef2cd8a675549d1";
	private static final String VERIFICATION_CARD_ID_1 = "bef2cd8a675549d1e8c82c55701c4bbd";
	private static final String VERIFICATION_CARD_ID_2 = "a675549d1e8c82c557bef2cd801c4bbd";
	private static final String PRECOMPUTED_VALUES_PATH = "computeTest";

	private static Ballot ballot;
	@Mock
	VotingCardSetRepository votingCardSetRepository;
	@InjectMocks
	private VotingCardSetComputationService votingCardSetComputationService;
	@Mock
	private IdleStatusService idleStatusServiceMock;
	@Mock
	private VotingCardSetRepository votingCardSetRepositoryMock;
	@Mock
	private ConfigurationEntityStatusService configurationEntityStatusServiceMock;
	@Mock
	private EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesServiceMock;
	@Mock
	private SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepositoryMock;
	@Mock
	private SignatureKeystore<Alias> signatureKeystoreService;

	@Test
	void compute() throws IOException, URISyntaxException, PayloadStorageException {
		ballot = DomainObjectMapper.getNewInstance()
				.readValue(VotingCardSetComputationServiceTest.class.getClassLoader().getResource("ballot.json"), Ballot.class);

		final GqGroup gqGroup = new GqGroup(BigInteger.valueOf(11), BigInteger.valueOf(5), BigInteger.valueOf(3));

		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
		final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(ballot);

		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> ciphertextsConfirmationKey = elGamalGenerator.genRandomCiphertextVector(2, 1);

		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> ciphertexts = elGamalGenerator.genRandomCiphertextVector(
				2, combinedCorrectnessInformation.getTotalNumberOfVotingOptions());

		final ElGamalMultiRecipientPublicKey verificationCardPublicKey = new ElGamalMultiRecipientPublicKey(
				GroupVector.of(GqElement.GqElementFactory.fromValue(BigInteger.valueOf(4), gqGroup)));

		final List<SetupComponentVerificationData> setupComponentVerificationData = List.of(
				new SetupComponentVerificationData(VERIFICATION_CARD_ID_1, ciphertextsConfirmationKey.get(0), ciphertexts.get(1),
						verificationCardPublicKey),
				new SetupComponentVerificationData(VERIFICATION_CARD_ID_2, ciphertextsConfirmationKey.get(0), ciphertexts.get(1),
						verificationCardPublicKey));

		final List<String> partialChoiceReturnCodesAllowList = IntStream.range(0,
						combinedCorrectnessInformation.getTotalNumberOfVotingOptions() * setupComponentVerificationData.size())
				.mapToObj(Integer::toString)
				.map(value -> value.getBytes(StandardCharsets.UTF_8))
				.map(Base64.getEncoder()::encodeToString)
				.toList();

		final SetupComponentVerificationDataPayload payload = new SetupComponentVerificationDataPayload(ELECTION_EVENT_ID,
				VERIFICATION_CARD_SET_ID, partialChoiceReturnCodesAllowList, 1, gqGroup, setupComponentVerificationData,
				combinedCorrectnessInformation);
		payload.setSignature(new CryptoPrimitivesSignature(new byte[] {}));

		when(configurationEntityStatusServiceMock.update(anyString(), anyString(), any())).thenReturn("");
		when(votingCardSetRepository.getVerificationCardSetId(VOTING_CARD_SET_ID)).thenReturn(VERIFICATION_CARD_SET_ID);
		when(setupComponentVerificationDataPayloadFileRepositoryMock.getCount(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)).thenReturn(1);
		when(setupComponentVerificationDataPayloadFileRepositoryMock.retrieve(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, 0)).thenReturn(
				payload);
		when(idleStatusServiceMock.getIdLock(anyString())).thenReturn(true);

		final Path representationsFile = getPathOfFileInResources(Paths.get(PRECOMPUTED_VALUES_PATH)).resolve(ELECTION_EVENT_ID)
				.resolve(Constants.CONFIG_DIR_NAME_ONLINE).resolve(Constants.CONFIG_DIR_NAME_VOTEVERIFICATION).resolve(VERIFICATION_CARD_SET_ID)
				.resolve(Constants.CONFIG_FILE_NAME_ELECTION_OPTION_REPRESENTATIONS);
		write(representationsFile, singletonList("2"));

		assertDoesNotThrow(() -> votingCardSetComputationService.compute(VOTING_CARD_SET_ID, ELECTION_EVENT_ID));

		verify(encryptedLongReturnCodeSharesServiceMock, times(1)).computeGenEncLongCodeShares(payload);
		verify(configurationEntityStatusServiceMock, times(1)).update(anyString(), anyString(), any());
	}

	@Test
	void computeInvalidParams() {
		when(idleStatusServiceMock.getIdLock(anyString())).thenReturn(true);

		assertThrows(FailedValidationException.class, () -> votingCardSetComputationService.compute("", ELECTION_EVENT_ID));
	}

	@Test
	void computeInvalidSignature() throws ResourceNotFoundException, IOException {
		when(idleStatusServiceMock.getIdLock(anyString())).thenReturn(true);

		assertDoesNotThrow(() -> votingCardSetComputationService.compute(VOTING_CARD_SET_ID, ELECTION_EVENT_ID));

		verify(encryptedLongReturnCodeSharesServiceMock, times(0)).computeGenEncLongCodeShares(any());
	}
}
