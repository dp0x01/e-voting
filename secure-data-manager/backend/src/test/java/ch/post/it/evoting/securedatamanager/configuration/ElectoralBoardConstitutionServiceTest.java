/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SignatureException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionEventContext;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.evotinglibraries.domain.configuration.ControlComponentPublicKeysPayload;
import ch.post.it.evoting.securedatamanager.ElectoralBoardHashesPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.ElectoralBoardHashesPayloadService;
import ch.post.it.evoting.securedatamanager.configuration.setuptally.SetupTallyEBAlgorithm;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.GenVerCardSetKeysAlgorithm;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.SetupComponentVerificationCardKeystoresPayloadGenerationService;
import ch.post.it.evoting.securedatamanager.services.application.service.ElectionEventService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@DisplayName("An ElectoralBoardConstitutionService")
@ExtendWith(MockitoExtension.class)
class ElectoralBoardConstitutionServiceTest {

	private static final Random random = RandomFactory.createRandom();

	private static final String ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);

	private static final String CONTROL_COMPONENT_PUBLIC_KEYS_PAYLOAD_1 =
			ElectoralBoardConstitutionServiceTest.class.getSimpleName() + "/controlComponentPublicKeysPayload.1.json";
	private static final String CONTROL_COMPONENT_PUBLIC_KEYS_PAYLOAD_2 =
			ElectoralBoardConstitutionServiceTest.class.getSimpleName() + "/controlComponentPublicKeysPayload.2.json";
	private static final String CONTROL_COMPONENT_PUBLIC_KEYS_PAYLOAD_3 =
			ElectoralBoardConstitutionServiceTest.class.getSimpleName() + "/controlComponentPublicKeysPayload.3.json";
	private static final String CONTROL_COMPONENT_PUBLIC_KEYS_PAYLOAD_4 =
			ElectoralBoardConstitutionServiceTest.class.getSimpleName() + "/controlComponentPublicKeysPayload.4.json";
	private static final List<char[]> PASSWORDS = List.of("Password_ElectoralBoard1".toCharArray(), "Password_ElectoralBoard2".toCharArray());
	private static final List<byte[]> HASHES = PASSWORDS.stream().map(pw -> CharBuffer.wrap(pw).toString().getBytes(StandardCharsets.UTF_8)).toList();

	private static final ControlComponentPublicKeysConfigService controlComponentPublicKeysConfigServiceMock = mock(
			ControlComponentPublicKeysConfigService.class);
	private static final ZeroKnowledgeProof zeroKnowledgeProofServiceMock = mock(ZeroKnowledgeProof.class);
	private static final GenVerCardSetKeysAlgorithm GENERATE_VERIFICATION_CARD_SET_KEYS_ALGORITHM =
			new GenVerCardSetKeysAlgorithm(ElGamalFactory.createElGamal(), zeroKnowledgeProofServiceMock);
	private static final SetupTallyEBAlgorithm SETUP_TALLY_EB_ALGORITHM = new SetupTallyEBAlgorithm(HashFactory.createHash(),
			ElGamalFactory.createElGamal(), zeroKnowledgeProofServiceMock);

	private static final ElectionEventService electionEventServiceMock = mock(ElectionEventService.class);
	private static final SignatureKeystore<Alias> signatureKeystoreServiceSdmConfigEBMock = mock(SignatureKeystore.class);
	private static final EncryptionParametersConfigService encryptionParametersConfigServiceMock = mock(EncryptionParametersConfigService.class);

	private static final SetupComponentVerificationCardKeystoresPayloadGenerationService setupComponentVerificationCardKeystoresPayloadGenerationServiceMock =
			mock(SetupComponentVerificationCardKeystoresPayloadGenerationService.class);

	private static PathResolver pathResolver;
	private static ElectoralBoardConstitutionService electoralBoardConstitutionService;

	@BeforeAll
	static void setUp(
			@TempDir
			final Path tempDir) throws IOException, URISyntaxException, SignatureException {

		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();

		final Path controlComponentPublicKeysPayload1 =
				Paths.get(Objects.requireNonNull(
						ElectoralBoardConstitutionServiceTest.class.getClassLoader().getResource(CONTROL_COMPONENT_PUBLIC_KEYS_PAYLOAD_1)).toURI());
		final Path controlComponentPublicKeysPayload2 =
				Paths.get(Objects.requireNonNull(
						ElectoralBoardConstitutionServiceTest.class.getClassLoader().getResource(CONTROL_COMPONENT_PUBLIC_KEYS_PAYLOAD_2)).toURI());
		final Path controlComponentPublicKeysPayload3 =
				Paths.get(Objects.requireNonNull(
						ElectoralBoardConstitutionServiceTest.class.getClassLoader().getResource(CONTROL_COMPONENT_PUBLIC_KEYS_PAYLOAD_3)).toURI());
		final Path controlComponentPublicKeysPayload4 =
				Paths.get(Objects.requireNonNull(
						ElectoralBoardConstitutionServiceTest.class.getClassLoader().getResource(CONTROL_COMPONENT_PUBLIC_KEYS_PAYLOAD_4)).toURI());

		final List<ControlComponentPublicKeysPayload> controlComponentPublicKeysPayloadList = Arrays.asList(
				objectMapper.readValue(controlComponentPublicKeysPayload1.toFile(), ControlComponentPublicKeysPayload.class),
				objectMapper.readValue(controlComponentPublicKeysPayload2.toFile(), ControlComponentPublicKeysPayload.class),
				objectMapper.readValue(controlComponentPublicKeysPayload3.toFile(), ControlComponentPublicKeysPayload.class),
				objectMapper.readValue(controlComponentPublicKeysPayload4.toFile(), ControlComponentPublicKeysPayload.class));

		final List<ControlComponentPublicKeys> controlComponentPublicKeysList = controlComponentPublicKeysPayloadList.stream()
				.map(ControlComponentPublicKeysPayload::getControlComponentPublicKeys)
				.toList();

		when(controlComponentPublicKeysConfigServiceMock.loadOrderByNodeId(ELECTION_EVENT_ID)).thenReturn(controlComponentPublicKeysList);
		when(electionEventServiceMock.getDateFrom(any())).thenReturn(LocalDateTime.now());
		when(electionEventServiceMock.getDateTo(any())).thenReturn(LocalDateTime.now());
		when(electionEventServiceMock.exists(ELECTION_EVENT_ID)).thenReturn(true);

		final ZqGroup zqGroup = controlComponentPublicKeysList.get(0).ccmjSchnorrProofs().getGroup();
		when(zeroKnowledgeProofServiceMock.genSchnorrProof(any(), any(), anyList())).thenReturn(
				new SchnorrProof(ZqElement.create(3, zqGroup), ZqElement.create(2, zqGroup)));
		when(zeroKnowledgeProofServiceMock.verifySchnorrProof(any(), any(), any())).thenReturn(true);

		Files.createDirectories(tempDir.resolve("sdm/config").resolve(ELECTION_EVENT_ID).resolve("OFFLINE"));

		pathResolver = new PathResolver(tempDir.toString());

		final SetupComponentPublicKeysPayloadFileRepository setupComponentPublicKeysPayloadFileRepository = new SetupComponentPublicKeysPayloadFileRepository(
				objectMapper, pathResolver);

		final ElectoralBoardHashesPayloadFileRepository electoralBoardHashesPayloadFileRepository = new ElectoralBoardHashesPayloadFileRepository(
				objectMapper, pathResolver);

		final ElectionEventContextPayloadService electionEventContextPayloadService = mock(ElectionEventContextPayloadService.class);
		final ElectionEventContextPayload electionEventContextPayload = mock(ElectionEventContextPayload.class);
		final ElectionEventContext electionEventContext = mock(ElectionEventContext.class);
		when(electionEventContext.getMaxNumberOfWriteInFields()).thenReturn(2);
		when(electionEventContextPayload.getElectionEventContext()).thenReturn(electionEventContext);
		when(electionEventContextPayloadService.load(any())).thenReturn(electionEventContextPayload);

		final SetupComponentPublicKeysPayloadService setupComponentPublicKeysPayloadService = new SetupComponentPublicKeysPayloadService(
				setupComponentPublicKeysPayloadFileRepository);

		final ElectoralBoardHashesPayloadService electoralBoardHashesPayloadService = new ElectoralBoardHashesPayloadService(
				electoralBoardHashesPayloadFileRepository);

		final PrimesMappingTableService primesMappingTableServiceMock = mock(PrimesMappingTableService.class);
		final GroupVector<PrimeGqElement, GqGroup> smallPrimeGroupMembers = PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(
				controlComponentPublicKeysList.get(0).ccmjElectionPublicKey().getGroup(), 1);
		final PrimesMappingTable primesMappingTable = PrimesMappingTable.from(
				List.of(new PrimesMappingTableEntry("actualVotingOption", smallPrimeGroupMembers.get(0), "semantic")));
		when(primesMappingTableServiceMock.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)).thenReturn(primesMappingTable);

		final ElectoralBoardPersistenceService electoralBoardPersistenceService = new ElectoralBoardPersistenceService(
				setupComponentPublicKeysPayloadService, signatureKeystoreServiceSdmConfigEBMock, electoralBoardHashesPayloadService);
		when(signatureKeystoreServiceSdmConfigEBMock.generateSignature(any(), any())).thenReturn(new byte[] { 1, 2, 3 });

		when(encryptionParametersConfigServiceMock.loadEncryptionGroup(ELECTION_EVENT_ID)).thenReturn(
				controlComponentPublicKeysList.get(0).ccmjElectionPublicKey().getGroup());

		electoralBoardConstitutionService = new ElectoralBoardConstitutionService(controlComponentPublicKeysConfigServiceMock,
				GENERATE_VERIFICATION_CARD_SET_KEYS_ALGORITHM, SETUP_TALLY_EB_ALGORITHM, electoralBoardPersistenceService,
				setupComponentVerificationCardKeystoresPayloadGenerationServiceMock, electionEventServiceMock,
				encryptionParametersConfigServiceMock, electionEventContextPayloadService);
	}

	@Test
	@DisplayName("constitutes successfully and persists the payloads.")
	void constituteHappyPath() {
		assertDoesNotThrow(() -> electoralBoardConstitutionService.constitute(ELECTION_EVENT_ID, PASSWORDS, HASHES));

		assertTrue(Files.exists(
				pathResolver.resolveOfflinePath(ELECTION_EVENT_ID)
						.resolve("setupComponentElectoralBoardHashesPayload.json")));
	}

	@Test
	@DisplayName("provided with non-valid election event id or null arguments throws.")
	void nonValidArguments() {
		assertAll(
				() -> assertThrows(NullPointerException.class, () -> electoralBoardConstitutionService.constitute(null, PASSWORDS, HASHES)),
				() -> assertThrows(FailedValidationException.class, () -> electoralBoardConstitutionService.constitute("invalid", PASSWORDS, HASHES)),
				() -> assertThrows(NullPointerException.class,
						() -> electoralBoardConstitutionService.constitute(ELECTION_EVENT_ID, null, HASHES)),
				() -> assertThrows(NullPointerException.class,
						() -> electoralBoardConstitutionService.constitute(ELECTION_EVENT_ID, PASSWORDS, null))
		);
	}

}
