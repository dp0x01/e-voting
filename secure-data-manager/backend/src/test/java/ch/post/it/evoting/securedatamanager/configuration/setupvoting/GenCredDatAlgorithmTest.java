/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.SecureRandom;
import java.util.List;
import java.util.Locale;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Factory;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Profile;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.symmetric.Symmetric;
import ch.post.it.evoting.cryptoprimitives.symmetric.SymmetricFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetActualVotingOptionsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetEncodedVotingOptionsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetSemanticInformationAlgorithm;
import ch.post.it.evoting.securedatamanager.commons.Constants;

/**
 * Tests of GenCredDatAlgorithm.
 */
@DisplayName("GenCredDatAlgorithm")
class GenCredDatAlgorithmTest {

	private final static int bound = 12;
	private final static int maxSize = 4;

	private final Random rand = RandomFactory.createRandom();
	private final SecureRandom srand = new SecureRandom();
	private final int size = srand.nextInt(bound) + 1;
	private final List<String> verificationCardIds = Stream.generate(
					() -> rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase(Locale.ENGLISH))
			.limit(size).toList();
	private final List<String> startVotingKeys = Stream.generate(() -> rand.genRandomBase32String(Constants.SVK_LENGTH).toLowerCase(Locale.ENGLISH))
			.limit(size)
			.toList();
	private final int delta = srand.nextInt(bound) + 1;
	private final int psi = srand.nextInt(bound) + 1;
	private final GqGroup gqGroup = GroupTestData.getGqGroup();
	private final ZqGroup zqGroup = new ZqGroup(gqGroup.getQ());
	private final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(zqGroup);
	private final GroupVector<ZqElement, ZqGroup> verificationCardSecretKeys = Stream.generate(zqGroupGenerator::genRandomZqElementMember).limit(size)
			.collect(GroupVector.toGroupVector());
	private final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
	private final ElGamalMultiRecipientPublicKey electionPublicKey = elGamalGenerator.genRandomPublicKey(delta);
	private final GqGroupGenerator generator = new GqGroupGenerator(electionPublicKey.getGroup());
	private final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions = Stream.generate(generator::genSmallPrimeMember).limit(maxSize)
			.distinct()
			.collect(GroupVector.toGroupVector());
	private final List<String> actualVotingOptions = Stream.generate(
					() -> rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase(Locale.ENGLISH))
			.limit(encodedVotingOptions.size()).toList();
	private final PrimesMappingTable primesMappingTable = PrimesMappingTable.from(IntStream.range(0, actualVotingOptions.size())
			.mapToObj(i -> new PrimesMappingTableEntry(actualVotingOptions.get(i), encodedVotingOptions.get(i), "semantic information"))
			.toList());
	private final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey = elGamalGenerator.genRandomPublicKey(psi);
	private final String electionEventId = rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase(Locale.ENGLISH);
	private final String verificationCardSetId = rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase(Locale.ENGLISH);
	private final List<String> ciSelections = Stream.generate(
					() -> rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase(Locale.ENGLISH))
			.limit(maxSize).toList();
	private final List<String> ciVotingOptions = Stream.generate(
					() -> rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase(Locale.ENGLISH))
			.limit(maxSize).toList();
	private final Hash hash = HashFactory.createHash();
	private final Symmetric symmetric = SymmetricFactory.createSymmetric();
	private final Base64 base64 = BaseEncodingFactory.createBase64();
	private final Argon2 argon2 = Argon2Factory.createArgon2(Argon2Profile.TEST.getContext());
	private final GetEncodedVotingOptionsAlgorithm getEncodedVotingOptionsAlgorithm = new GetEncodedVotingOptionsAlgorithm();
	private final GetActualVotingOptionsAlgorithm getActualVotingOptionsAlgorithm = new GetActualVotingOptionsAlgorithm();
	private final GetSemanticInformationAlgorithm getSemanticInformationAlgorithm = new GetSemanticInformationAlgorithm();

	private GenCredDatContext context;
	private GenCredDatInput input;
	private GenCredDatAlgorithm genCredDatAlgorithm;

	@BeforeEach
	void setup() {
		context = buildCredDataContext();
		input = new GenCredDatInput(verificationCardIds, verificationCardSecretKeys, startVotingKeys);

		assertNotNull(hash, "hashService");
		assertNotNull(symmetric, "symmetricService");

		genCredDatAlgorithm = new GenCredDatAlgorithm(hash, symmetric, base64, argon2, getEncodedVotingOptionsAlgorithm,
				getActualVotingOptionsAlgorithm, getSemanticInformationAlgorithm);
	}

	@Test
	void happyPath() {
		final GenCredDatOutput output = genCredDatAlgorithm.genCredDat(context, input);

		assertNotNull(output, "output is null");
		assertNotNull(output.verificationCardKeystores(), "output.verificationCardKeystores() is null");
		assertEquals(output.verificationCardKeystores().size(), input.verificationCardIds().size());
	}

	@Test
	void nullContextArgument() {
		assertThrows(NullPointerException.class, () -> genCredDatAlgorithm.genCredDat(null, input));
	}

	@Test
	void nullInputArgument() {
		assertThrows(NullPointerException.class, () -> genCredDatAlgorithm.genCredDat(context, null));
	}

	private GenCredDatContext buildCredDataContext() {
		return new GenCredDatContext
				.Builder()
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setElectionPublicKey(electionPublicKey)
				.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
				.setPrimesMappingTable(primesMappingTable)
				.setCorrectnessInformationSelections(ciSelections)
				.setCorrectnessInformationVotingOptions(ciVotingOptions)
				.build();
	}

}
