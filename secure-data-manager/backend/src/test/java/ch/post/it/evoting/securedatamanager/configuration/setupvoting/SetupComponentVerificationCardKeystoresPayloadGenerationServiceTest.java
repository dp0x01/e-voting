/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.SignatureException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.internal.hashing.HashService;
import ch.post.it.evoting.cryptoprimitives.internal.signing.SignatureKeystoreService;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.SetupComponentVerificationCardKeystoresPayload;
import ch.post.it.evoting.domain.configuration.setupvoting.VoterInitialCodes;
import ch.post.it.evoting.domain.configuration.setupvoting.VoterInitialCodesPayload;
import ch.post.it.evoting.evotinglibraries.domain.configuration.SetupComponentTallyDataPayload;
import ch.post.it.evoting.securedatamanager.SetupComponentTallyDataPayloadService;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.configuration.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.configuration.EntityHelper;
import ch.post.it.evoting.securedatamanager.configuration.VerificationCardSecretKeyPayloadService;
import ch.post.it.evoting.securedatamanager.services.application.service.ElectionEventService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

@DisplayName("SetupComponentVerificationCardKeystoresPayloadGenerationService")
class SetupComponentVerificationCardKeystoresPayloadGenerationServiceTest {

	private static final Random random = RandomFactory.createRandom();
	private final int SIZE = 6;
	private final GqGroup gqGroup = GroupTestData.getGqGroup();
	private final GqGroupGenerator gqGroupGenerator = new GqGroupGenerator(gqGroup);
	private final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
	private final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey = elGamalGenerator.genRandomPublicKey(SIZE);
	private final ElGamalMultiRecipientPublicKey electionPublicKey = elGamalGenerator.genRandomPublicKey(SIZE);
	private final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = elGamalGenerator.genRandomPublicKey(SIZE);
	private final String electionEventId = random.genRandomBase16String(Constants.BASE16_ID_LENGTH);
	private final String verificationCardSetId = random.genRandomBase16String(Constants.BASE16_ID_LENGTH);
	private final String ballotBoxAlias = random.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase(Locale.ENGLISH);
	private final String ballotBoxId = random.genRandomBase16String(Constants.BASE16_ID_LENGTH);
	private final String votingCardSetId = random.genRandomBase16String(Constants.BASE16_ID_LENGTH);
	private final VotingCardSetRepository votingCardSetRepository = mock(VotingCardSetRepository.class);
	private final VerificationCardSecretKeyPayloadService verificationCardSecretKeyPayloadService = mock(
			VerificationCardSecretKeyPayloadService.class);
	private final GenCredDatAlgorithm genCredDatAlgorithm = mock(GenCredDatAlgorithm.class);
	private final ElectionEventService electionEventService = mock(ElectionEventService.class);
	private final SignatureKeystoreService<Alias> signatureKeystoreService = mock(SignatureKeystoreService.class);
	private final VoterInitialCodesPayloadService voterInitialCodesPayloadService = mock(VoterInitialCodesPayloadService.class);
	private final ElectionEventContextPayloadService electionEventContextPayloadService = mock(ElectionEventContextPayloadService.class,
			RETURNS_DEEP_STUBS);
	private final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService = mock(SetupComponentTallyDataPayloadService.class);
	private final EntityHelper entityHelper = mock(EntityHelper.class);
	private final SetupComponentVerificationCardKeystoresPayloadService setupComponentVerificationCardKeystoresPayloadService
			= mock(SetupComponentVerificationCardKeystoresPayloadService.class);

	private final SetupComponentVerificationCardKeystoresPayloadGenerationService sut =
			new SetupComponentVerificationCardKeystoresPayloadGenerationService(
					entityHelper,
					genCredDatAlgorithm,
					electionEventService,
					votingCardSetRepository,
					signatureKeystoreService,
					voterInitialCodesPayloadService,
					verificationCardSecretKeyPayloadService,
					electionEventContextPayloadService,
					setupComponentTallyDataPayloadService,
					setupComponentVerificationCardKeystoresPayloadService);

	@Nested
	@DisplayName("calling generate with")
	class GenerateArgumentsTest {

		@Test
		@DisplayName("an invalid election event id input throws a FailedValidationException.")
		void invalidElectionEventIdArguments() {
			final String invalidElectionEventId = "electionEventId";
			assertThrows(FailedValidationException.class,
					() -> sut.generate(invalidElectionEventId, choiceReturnCodesEncryptionPublicKey, electionPublicKey));
		}

		@Test
		@DisplayName("an election event id not in the database throws an IllegalArgumentException")
		void nonExistingElectionEventIdArgument() {
			final String nonExistingElectionEventId = "0123456789abcdef0123456789abcdef";
			assertThrows(IllegalArgumentException.class,
					() -> sut.generate(nonExistingElectionEventId, choiceReturnCodesEncryptionPublicKey, electionPublicKey));
		}

		@Test
		@DisplayName("any null input throws a NullPointerException.")
		void nullArguments() {
			assertAll(
					() -> assertThrows(NullPointerException.class,
							() -> sut.generate(null, choiceReturnCodesEncryptionPublicKey, electionPublicKey)),
					() -> assertThrows(NullPointerException.class,
							() -> sut.generate(electionEventId, null, electionPublicKey)),
					() -> assertThrows(NullPointerException.class,
							() -> sut.generate(electionEventId, choiceReturnCodesEncryptionPublicKey, null))
			);
		}

		@Test
		@DisplayName("inputs with different groups throws an IllegalArgumentException.")
		void invalidArguments() {
			when(electionEventService.exists(electionEventId)).thenReturn(true);

			final GqGroup gqGroupDifferent = GroupTestData.getDifferentGqGroup(gqGroup);
			final ElGamalGenerator elGamalGeneratorDifferentGroup = new ElGamalGenerator(gqGroupDifferent);

			final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKeyDifferentGroup = elGamalGeneratorDifferentGroup.genRandomPublicKey(
					SIZE);
			final ElGamalMultiRecipientPublicKey electionPublicKeyDifferentGroup = elGamalGeneratorDifferentGroup.genRandomPublicKey(SIZE);

			assertAll(
					() -> {
						final IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
								() -> sut.generate(electionEventId, choiceReturnCodesEncryptionPublicKeyDifferentGroup, electionPublicKey));

						assertEquals("The choice return codes encryption public key and the election public key must have the same group",
								illegalArgumentException.getMessage());
					},
					() -> {
						final IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
								() -> sut.generate(electionEventId, choiceReturnCodesEncryptionPublicKey, electionPublicKeyDifferentGroup));

						assertEquals("The choice return codes encryption public key and the election public key must have the same group",
								illegalArgumentException.getMessage());
					}
			);
		}
	}

	@Nested
	@DisplayName("executing generate")
	class GenerateExecutionTest {

		private final List<String> votingCardSetIds = List.of(votingCardSetId);
		private List<String> verificationCardIds;

		@BeforeEach
		void setup() throws SignatureException, IOException {
			final PrimeGqElement encodedVotingOption = gqGroupGenerator.genSmallPrimeMember();

			verificationCardIds =
					Stream.generate(() -> random.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase(Locale.ENGLISH)).limit(SIZE).toList();

			final List<ElGamalMultiRecipientPublicKey> verificationCardPublicKeys =
					Stream.generate(() -> elGamalGenerator.genRandomPublicKey(SIZE)).limit(SIZE).toList();

			final String actualVotingOption = "actualVotingOption";
			final String semanticInformation = "semantics";
			final PrimesMappingTable primesMappingTable = new PrimesMappingTable(
					GroupVector.of(new PrimesMappingTableEntry(actualVotingOption, encodedVotingOption, semanticInformation)));

			final SetupComponentTallyDataPayload setupComponentTallyDataPayload = new SetupComponentTallyDataPayload(electionEventId,
					verificationCardSetId, ballotBoxAlias, gqGroup, verificationCardIds, GroupVector.from(verificationCardPublicKeys));

			final byte[] payloadHash = HashService.getInstance().recursiveHash(setupComponentTallyDataPayload);
			setupComponentTallyDataPayload.setSignature(new CryptoPrimitivesSignature(payloadHash));

			final GenCredDatOutput genCredDatOutput =
					new GenCredDatOutput(Stream.generate(() -> random.genRandomBase16String(SIZE)).limit(SIZE).toList());

			// Services mocks:
			when(electionEventService.exists(electionEventId)).thenReturn(true);

			// In generate method:
			when(votingCardSetRepository.findAllVotingCardSetIds(anyString())).thenReturn(votingCardSetIds);

			final List<VerificationCardSecretKey> verificationCardSecretKeys = verificationCardIds.stream()
					.map(verificationCardId -> new VerificationCardSecretKey(verificationCardId, elGamalGenerator.genRandomPrivateKey(1)))
					.toList();
			final VerificationCardSecretKeyPayload verificationCardSecretKeyPayload = new VerificationCardSecretKeyPayload(gqGroup, electionEventId,
					verificationCardSetId, verificationCardSecretKeys);

			when(votingCardSetRepository.getVerificationCardSetId(anyString())).thenReturn(verificationCardSetId);
			when(verificationCardSecretKeyPayloadService.load(anyString(), anyString())).thenReturn(verificationCardSecretKeyPayload);

			final VoterInitialCodesPayload voterInitialCodesPayload = mock(VoterInitialCodesPayload.class);
			final VoterInitialCodes voterInitialCodes = mock(VoterInitialCodes.class);
			when(voterInitialCodes.startVotingKey()).thenReturn(random.genRandomBase32String(Constants.SVK_LENGTH).toLowerCase(Locale.ENGLISH));
			when(voterInitialCodesPayload.voterInitialCodes()).thenReturn(verificationCardIds.stream().map(id -> voterInitialCodes).toList());
			when(voterInitialCodesPayloadService.load(electionEventId, votingCardSetId)).thenReturn(voterInitialCodesPayload);

			final ClassLoader classLoader = GenerateExecutionTest.class.getClassLoader();
			final String ballotJson =
					IOUtils.toString(Objects.requireNonNull(classLoader.getResource("ballot.json")).openStream(), StandardCharsets.UTF_8);
			final Ballot ballot = DomainObjectMapper.getNewInstance().readValue(ballotJson, Ballot.class);

			when(entityHelper.getBallotFromVotingCardSet(anyString(), anyString())).thenReturn(ballot);

			when(genCredDatAlgorithm.genCredDat(any(), any())).thenReturn(genCredDatOutput);

			// In createSetupComponentVerificationCardKeystoresPayload method:
			when(signatureKeystoreService.generateSignature(any(), any())).thenReturn("signature".getBytes(StandardCharsets.UTF_8));

			// In loadSetupComponentTallyDataPayload method:
			when(setupComponentTallyDataPayloadService.load(anyString(), anyString())).thenReturn(setupComponentTallyDataPayload);
			when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);

			final String verificationCardSetAlias = verificationCardSetId + "_alias";
			final String verificationCardSetDescription = verificationCardSetId + "_description";
			final LocalDateTime startTime = LocalDateTime.now();
			final LocalDateTime finishTime = startTime.plusDays(5);
			final VerificationCardSetContext verificationCardSetContext = new VerificationCardSetContext(verificationCardSetId,
					verificationCardSetAlias, verificationCardSetDescription, ballotBoxId, startTime, finishTime, true, 0,
					10, 900, primesMappingTable);
			when(electionEventContextPayloadService.load(electionEventId).getElectionEventContext().verificationCardSetContexts()).thenReturn(
					List.of(verificationCardSetContext));
		}

		@Test
		@DisplayName("behaves as expected.")
		void happyPath() throws SignatureException {
			final List<SetupComponentVerificationCardKeystoresPayload> payloads =
					assertDoesNotThrow(() -> sut.generate(electionEventId, choiceReturnCodesEncryptionPublicKey, electoralBoardPublicKey));

			final int votingCardSetIdsSize = votingCardSetIds.size();

			verify(votingCardSetRepository, times(votingCardSetIdsSize)).findAllVotingCardSetIds(anyString());
			verify(setupComponentTallyDataPayloadService, times(votingCardSetIdsSize)).load(anyString(), anyString());
			verify(signatureKeystoreService, times(1)).verifySignature(any(), any(ElectionEventContextPayload.class), any(), any());
			verify(signatureKeystoreService, times(votingCardSetIdsSize)).verifySignature(any(), any(SetupComponentTallyDataPayload.class), any(),
					any());
			verify(votingCardSetRepository, times(votingCardSetIdsSize)).getVerificationCardSetId(anyString());
			verify(verificationCardSecretKeyPayloadService, times(1)).load(anyString(), anyString());
			verify(genCredDatAlgorithm, times(votingCardSetIdsSize)).genCredDat(any(), any());
			verify(signatureKeystoreService, times(votingCardSetIdsSize)).generateSignature(any(), any());

			final Predicate<SetupComponentVerificationCardKeystoresPayload> validatePayload = payload ->
					electionEventId.equals(payload.getElectionEventId()) && verificationCardSetId.equals(payload.getVerificationCardSetId())
							&& payload.getVerificationCardKeystores() != null;

			assertFalse(payloads.isEmpty());
			assertEquals(votingCardSetIds.size(), payloads.size());
			assertTrue(payloads.stream().allMatch(validatePayload));
		}

		@Test
		@DisplayName("with an empty voting card set found returns an empty list of SetupComponentVerificationCardKeystoresPayload.")
		void givenAnEmptylistOfVotingCardSetIdsThenNodata() throws SignatureException {
			when(votingCardSetRepository.findAllVotingCardSetIds(anyString())).thenReturn(List.of());

			final List<SetupComponentVerificationCardKeystoresPayload> payloads =
					assertDoesNotThrow(() -> sut.generate(electionEventId, choiceReturnCodesEncryptionPublicKey, electoralBoardPublicKey));

			assertTrue(payloads.isEmpty());

			verify(votingCardSetRepository, times(1)).findAllVotingCardSetIds(anyString());

			verify(setupComponentTallyDataPayloadService, times(0)).load(anyString(), anyString());
			verify(signatureKeystoreService, times(1)).verifySignature(any(), any(ElectionEventContextPayload.class), any(), any());

			verify(votingCardSetRepository, times(0)).getVerificationCardSetId(anyString());
			verify(verificationCardSecretKeyPayloadService, times(0)).load(anyString(), anyString());
			verify(voterInitialCodesPayloadService, times(0)).load(anyString(), anyString());
			verify(signatureKeystoreService, times(0)).generateSignature(any(), any());

			verify(genCredDatAlgorithm, times(0)).genCredDat(any(), any());
		}

		@Test
		@DisplayName("with an invalid signature of setupComponentTallyDataPayload throws an InvalidPayloadSignatureException.")
		void setupComponentTallyDataPayloadWithInvalidSignature() throws SignatureException {
			when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(false);

			assertThrows(InvalidPayloadSignatureException.class,
					() -> sut.generate(electionEventId, choiceReturnCodesEncryptionPublicKey, electoralBoardPublicKey));
		}

	}
}
