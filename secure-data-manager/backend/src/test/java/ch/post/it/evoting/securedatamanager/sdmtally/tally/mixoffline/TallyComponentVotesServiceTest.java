/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.evotinglibraries.domain.tally.TallyComponentVotesPayload;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@ExtendWith(MockitoExtension.class)
class TallyComponentVotesServiceTest {

	private static final String electionEventId = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String ballotId = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String ballotBoxId = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static TallyComponentVotesService tallyComponentVotesService;
	private static GroupVector<GroupVector<PrimeGqElement, GqGroup>, GqGroup> votes;
	private static List<List<String>> actualSelectedVotingOptions;

	private static List<List<String>> decodedWriteInVotes;

	@BeforeAll
	static void setUp(
			@TempDir
			final Path tempDir) {

		final GqGroup gqGroup = GroupTestData.getGroupP59();

		final int desiredNumberOfPrimes = 3;
		final GroupVector<PrimeGqElement, GqGroup> primeGroupMembers = PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(gqGroup,
				desiredNumberOfPrimes);

		votes = GroupVector.of(GroupVector.from(primeGroupMembers), GroupVector.from(primeGroupMembers));
		actualSelectedVotingOptions = votes.stream()
				.map(actualSelectedVotingOption -> actualSelectedVotingOption.stream().map(v -> "124124aa-154153").toList()).toList();
		decodedWriteInVotes = votes.stream()
				.map(actualSelectedVotingOption -> actualSelectedVotingOption.stream().map(v -> "James Bond").toList()).toList();

		final PathResolver pathResolverMock = Mockito.mock(PathResolver.class);
		when(pathResolverMock.resolveBallotBoxPath(anyString(), anyString(), anyString())).thenReturn(tempDir);

		final TallyComponentVotesFileRepository tallyComponentVotesFileRepository = new TallyComponentVotesFileRepository(pathResolverMock,
				DomainObjectMapper.getNewInstance());
		tallyComponentVotesService = new TallyComponentVotesService(tallyComponentVotesFileRepository);
	}

	@Test
	@DisplayName("load with null parameters throws a NullPointerException")
	void verifyLoadTallyComponentVotesWithNullParametersThrows() {
		assertThrows(NullPointerException.class, () -> tallyComponentVotesService.load(null, ballotId, ballotBoxId));
		assertThrows(NullPointerException.class, () -> tallyComponentVotesService.load(electionEventId, null, ballotBoxId));
		assertThrows(NullPointerException.class, () -> tallyComponentVotesService.load(electionEventId, ballotId, null));
	}

	@Test
	@DisplayName("save with null parameters throws a NullPointerException")
	void verifySaveTallyComponentVotesWithNullParametersThrows() {
		assertThrows(NullPointerException.class, () -> tallyComponentVotesService.save(null));
	}

	@Test
	@DisplayName("persist tally component votes payload")
	void testSaveAndLoad() {
		final TallyComponentVotesPayload payload = new TallyComponentVotesPayload(electionEventId, ballotId, ballotBoxId, votes.getGroup(), votes,
				actualSelectedVotingOptions, decodedWriteInVotes);
		payload.setSignature(new CryptoPrimitivesSignature("".getBytes(StandardCharsets.UTF_8)));

		final IllegalStateException ise1 = assertThrows(IllegalStateException.class,
				() -> tallyComponentVotesService.load(electionEventId, ballotId, ballotBoxId));
		assertTrue(ise1.getMessage().startsWith("Requested tally component votes payload is not present."));

		//persist
		assertDoesNotThrow(() -> tallyComponentVotesService.save(payload));

		//load
		assertDoesNotThrow(() -> tallyComponentVotesService.load(payload.getElectionEventId(), payload.getBallotId(), payload.getBallotBoxId()));

		//try to persist again
		final IllegalStateException ise2 = assertThrows(IllegalStateException.class, () -> tallyComponentVotesService.save(payload));
		assertTrue(ise2.getMessage().startsWith("Requested tally component votes payload already exists."));
	}
}