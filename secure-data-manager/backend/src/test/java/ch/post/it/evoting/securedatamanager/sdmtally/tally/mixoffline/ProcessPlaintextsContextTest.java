/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline.ProcessPlaintextsContext;

@DisplayName("Construct ProcessPlaintextsContext with")
class ProcessPlaintextsContextTest {

	private GqGroup encryptionGroup;

	@BeforeEach
	void setup() {
		encryptionGroup = GroupTestData.getGqGroup();
	}

	@Test
	@DisplayName("inputs from different groups throws an IllegalArgumentException")
	void processPlaintextsWithInputsOfDifferentGroupsThrows() {
		final GqGroup otherGqGroup = GroupTestData.getDifferentGqGroup(encryptionGroup);
		final PrimesMappingTable primesMappingTableFromOtherGroup = PrimesMappingTable.from(
				List.of(new PrimesMappingTableEntry("57a30570-1722-3a7e-a8f9-7dd643d7f33",
						PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(otherGqGroup, 1)
								.get(0), "semantics")));

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new ProcessPlaintextsContext(encryptionGroup, primesMappingTableFromOtherGroup));

		assertEquals("The primes mapping table's entries must belong to the encryption group.",
				Throwables.getRootCause(exception).getMessage());
	}
}
