/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.commons;

import static com.tngtech.archunit.core.domain.JavaAccess.Predicates.target;
import static com.tngtech.archunit.core.domain.JavaClass.Predicates.assignableTo;
import static com.tngtech.archunit.core.domain.properties.HasName.Predicates.nameMatching;
import static com.tngtech.archunit.core.domain.properties.HasOwner.Predicates.With.owner;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;
import static com.tngtech.archunit.base.DescribedPredicate.not;
import static com.tngtech.archunit.core.domain.properties.HasName.Predicates.name;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.library.freeze.FreezingArchRule;

import retrofit2.Call;

@AnalyzeClasses(packages = "ch.post.it.evoting.securedatamanager", importOptions = ImportOption.DoNotIncludeTests.class)
class RemoteServiceTestArchRule {

	@ArchTest
	static final ArchRule REMOTE_CALLS_SHOULD_GO_THROUGH_REMOTE_SERVICE = FreezingArchRule.freeze(noClasses()
			.that(not(name(RemoteService.class.getName())))
			.should().callMethodWhere(target(owner(assignableTo(Call.class))).and(target(nameMatching("execute")))));
}