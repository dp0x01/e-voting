/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionEventContext;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.SerializationUtils;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ElectionEventContextRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.VoteVerificationClient;

@DisplayName("An ElectionEventContextPayloadService")
class ElectionEventContextPayloadServiceTest {

	private static final String ELECTION_EVENT_ID = "b643acd93ccc453db5b1ed3ed910f4b2";
	private static final String WRONG_ELECTION_EVENT_ID = "414bd34dcf6e4de4b771a92fa3849d3d";
	private static final String NOT_EXISTING_ELECTION_EVENT_PAYLOAD = "614bd34dcf6e4de4b771a92fa3849d3d";
	private static final String INVALID_ID = "invalidId";
	private static final Random RANDOM = RandomFactory.createRandom();

	private static ObjectMapper objectMapper;
	private static PathResolver pathResolver;
	private static ElectionEventContextPayloadService electionEventContextPayloadService;

	@BeforeAll
	static void setUpAll() throws URISyntaxException {
		objectMapper = DomainObjectMapper.getNewInstance();
		final Path path = Paths.get(
				Objects.requireNonNull(ElectionEventContextPayloadServiceTest.class.getResource("/ElectionEventContextServiceTest/")).toURI());
		final PathResolver pathResolver = new PathResolver(path.toString());

		final ElectionEventContextPayloadFileRepository electionEventContextPayloadFileRepository = new ElectionEventContextPayloadFileRepository(
				objectMapper, pathResolver);
		final VoteVerificationClient voteVerificationClient = mock(VoteVerificationClient.class);
		final ElectionEventContextRepository electionEventContextRepository = mock(ElectionEventContextRepository.class);

		electionEventContextPayloadService = new ElectionEventContextPayloadService(objectMapper, false,
				voteVerificationClient, electionEventContextRepository, electionEventContextPayloadFileRepository);
	}

	private ElectionEventContextPayload validElectionEventContextPayload() {
		final String electionEventAlias = ELECTION_EVENT_ID + "_alias";
		final String electionEventDescription = ELECTION_EVENT_ID + "_description";
		final List<VerificationCardSetContext> verificationCardSetContexts = new ArrayList<>();

		IntStream.rangeClosed(1, 2).forEach((i) -> verificationCardSetContexts.add(generatedVerificationCardSetContext()));

		final GqGroup gqGroup = SerializationUtils.getGqGroup();

		final LocalDateTime startTime = LocalDateTime.now().minusDays(1);
		final LocalDateTime finishTime = startTime.plusWeeks(1);

		final ElectionEventContext electionEventContext = new ElectionEventContext(ELECTION_EVENT_ID, electionEventAlias, electionEventDescription,
				verificationCardSetContexts, startTime, finishTime);

		return new ElectionEventContextPayload(gqGroup, electionEventContext);
	}

	private VerificationCardSetContext generatedVerificationCardSetContext() {
		final String verificationCardSetId = RANDOM.genRandomBase16String(32);
		final String verificationCardSetAlias = verificationCardSetId + "_alias";
		final String verificationCardSetDescription = verificationCardSetId + "_description";
		final String ballotBoxId = RANDOM.genRandomBase16String(32);
		final LocalDateTime startTime = LocalDateTime.now();
		final LocalDateTime finishTime = startTime.plusDays(5);
		final boolean testBallotBox = Math.random() < 0.5;
		final int numberOfWriteInFields = 1;
		final int numberVotingCards = 10;
		final int gracePeriod = 900;
		final GroupVector<PrimeGqElement, GqGroup> smallPrimeGroupMembers = PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(
				SerializationUtils.getGqGroup(), 1);
		final PrimesMappingTable primesMappingTable = PrimesMappingTable.from(
				List.of(new PrimesMappingTableEntry("actualVotingOption", smallPrimeGroupMembers.get(0), "semantic")));

		return new VerificationCardSetContext(verificationCardSetId, verificationCardSetAlias, verificationCardSetDescription, ballotBoxId, startTime,
				finishTime, testBallotBox, numberOfWriteInFields, numberVotingCards,
				gracePeriod, primesMappingTable);
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private ElectionEventContextPayloadService electionEventContextPayloadServiceTemp;

		private ElectionEventContextPayload electionEventContextPayload;

		@BeforeAll
		void setUpAll(
				@TempDir
				final Path tempDir) throws IOException {

			Files.createDirectories(tempDir.resolve("sdm/config").resolve(ELECTION_EVENT_ID));

			pathResolver = new PathResolver(tempDir.toString());
			final ElectionEventContextPayloadFileRepository electionEventContextPayloadFileRepositoryTemp = new ElectionEventContextPayloadFileRepository(
					objectMapper, pathResolver);
			final VoteVerificationClient voteVerificationClient = mock(VoteVerificationClient.class);
			final ElectionEventContextRepository electionEventContextRepository = mock(ElectionEventContextRepository.class);

			electionEventContextPayloadServiceTemp = new ElectionEventContextPayloadService(objectMapper, false, voteVerificationClient,
					electionEventContextRepository, electionEventContextPayloadFileRepositoryTemp);
		}

		@BeforeEach
		void setUp() {
			// Create payload.
			electionEventContextPayload = validElectionEventContextPayload();
		}

		@Test
		@DisplayName("a valid payload does not throw")
		void saveValidPayload() {
			assertDoesNotThrow(() -> electionEventContextPayloadServiceTemp.save(electionEventContextPayload));

			assertTrue(Files.exists(
					pathResolver.resolveElectionEventPath(ELECTION_EVENT_ID).resolve(ElectionEventContextPayloadFileRepository.PAYLOAD_FILE_NAME)));
		}

		@Test
		@DisplayName("a null payload throws NullPointerException")
		void saveNullPayload() {
			assertThrows(NullPointerException.class, () -> electionEventContextPayloadServiceTemp.save(null));
		}

	}

	@Nested
	@DisplayName("calling exist")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class ExistTest {

		@Test
		@DisplayName("for valid election event returns true")
		void existValidElectionEvent() {
			assertTrue(electionEventContextPayloadService.exist(ELECTION_EVENT_ID));
		}

		@Test
		@DisplayName("for invalid election event id throws FailedValidationException")
		void existInvalidElectionEvent() {
			assertThrows(FailedValidationException.class, () -> electionEventContextPayloadService.exist(INVALID_ID));
		}

		@Test
		@DisplayName("for non existing election event returns false")
		void existNonExistingElectionEvent() {
			assertFalse(electionEventContextPayloadService.exist(WRONG_ELECTION_EVENT_ID));
		}

	}

	@Nested
	@DisplayName("loading")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class LoadTest {

		@Test
		@DisplayName("existing election event returns expected election event context payload")
		void loadExistingElectionEvent() {
			final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadService.load(ELECTION_EVENT_ID);

			assertNotNull(electionEventContextPayload);
		}

		@Test
		@DisplayName("invalid election event id throws FailedValidationException")
		void loadInvalidElectionEventId() {
			assertThrows(FailedValidationException.class, () -> electionEventContextPayloadService.load(INVALID_ID));
		}

		@Test
		@DisplayName("existing election event with missing payload throws IllegalStateException")
		void loadMissingPayload() {
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> electionEventContextPayloadService.load(NOT_EXISTING_ELECTION_EVENT_PAYLOAD));

			final String errorMessage = String.format("Requested election event context payload is not present. [electionEventId: %s]",
					NOT_EXISTING_ELECTION_EVENT_PAYLOAD);
			assertEquals(errorMessage, Throwables.getRootCause(exception).getMessage());
		}

	}

}
