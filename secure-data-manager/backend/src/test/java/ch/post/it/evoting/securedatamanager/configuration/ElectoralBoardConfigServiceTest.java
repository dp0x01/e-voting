/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

import javax.json.JsonObject;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.post.it.evoting.securedatamanager.BoardPasswordHashService;
import ch.post.it.evoting.securedatamanager.commons.JsonUtils;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electoralboard.ElectoralBoardRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

@ExtendWith(MockitoExtension.class)
class ElectoralBoardConfigServiceTest {

	private static final String ELECTION_EVENT_ID = "0b149cfdaad04b04b990c3b1d4ca7639";
	private static final String ELECTORAL_BOARD_ID = "16e020d934594544a6e17d1e410da513";
	private static final String ELECTORAL_BOARD_LOCKED_JSON =
			ElectoralBoardConfigServiceTest.class.getSimpleName() + "/electoralBoard_LOCKED.json";
	private static final String ELECTORAL_BOARD_READY_JSON =
			ElectoralBoardConfigServiceTest.class.getSimpleName() + "/electoralBoard_READY.json";
	private static final String ELECTORAL_BOARD_JSON_WITHOUT_STATUS = "{" +
			"    \"id\": \"16e020d934594544a6e17d1e410da513\"," +
			"    \"defaultTitle\": \"Electoral Board\"," +
			"    \"defaultDescription\": \"A  sample EA\"," +
			"    \"alias\": \"EA1\"," +
			"    \"electionEvent\": {" +
			"        \"id\": \"0b149cfdaad04b04b990c3b1d4ca7639\"" +
			"    }," +
			"    \"electoralBoard\": [" +
			"        \"John\"," +
			"        \"Peter\"" +
			"    ]" +
			"}";

	private static final BoardPasswordHashService boardPasswordHashServiceMock = mock(BoardPasswordHashService.class);
	private static final ElectoralBoardRepository ELECTORAL_BOARD_REPOSITORY_MOCK = mock(ElectoralBoardRepository.class);
	private static final ConfigurationEntityStatusService statusService = mock(ConfigurationEntityStatusService.class);
	private static final ElectoralBoardConstitutionService constituteService = mock(ElectoralBoardConstitutionService.class);

	private static JsonObject electoralBoardReadyJson;
	private static JsonObject electoralBoardLockedJson;
	private static ElectoralBoardConfigService electoralBoardConfigService;
	final List<char[]> ELECTORAL_BOARD_PASSWORDS = List.of("Password_ElectoralBoard1".toCharArray(), "Password_ElectoralBoard2".toCharArray());

	@BeforeAll
	static void setUp() throws IOException, URISyntaxException {

		final Path pathReady = Paths.get(
				Objects.requireNonNull(ElectoralBoardConfigServiceTest.class.getClassLoader().getResource(ELECTORAL_BOARD_READY_JSON)).toURI());
		electoralBoardReadyJson = JsonUtils.getJsonObject(Files.readString(pathReady));

		final Path pathLocked = Paths.get(
				Objects.requireNonNull(ElectoralBoardConfigServiceTest.class.getClassLoader().getResource(ELECTORAL_BOARD_LOCKED_JSON)).toURI());
		electoralBoardLockedJson = JsonUtils.getJsonObject(Files.readString(pathLocked));

		electoralBoardConfigService = new ElectoralBoardConfigService(
				boardPasswordHashServiceMock,
				ELECTORAL_BOARD_REPOSITORY_MOCK,
				statusService,
				constituteService);
	}

	@Test
	void notSignAnElectoralBoardWithoutStatus() throws ResourceNotFoundException {
		when(ELECTORAL_BOARD_REPOSITORY_MOCK.find(anyString())).thenReturn(ELECTORAL_BOARD_JSON_WITHOUT_STATUS);

		assertFalse(electoralBoardConfigService.sign(ELECTION_EVENT_ID, ELECTORAL_BOARD_ID));
	}

	@Test
	void signHappyPath() throws ResourceNotFoundException {
		when(ELECTORAL_BOARD_REPOSITORY_MOCK.find(anyString())).thenReturn(electoralBoardReadyJson.toString());

		assertTrue(electoralBoardConfigService.sign(ELECTION_EVENT_ID, ELECTORAL_BOARD_ID));
	}

	@Test
	void constituteHappyPath() {
		when(ELECTORAL_BOARD_REPOSITORY_MOCK.find(anyString())).thenReturn(electoralBoardLockedJson.toString());

		assertTrue(electoralBoardConfigService.constitute(ELECTION_EVENT_ID, ELECTORAL_BOARD_ID, ELECTORAL_BOARD_PASSWORDS));
	}

	@Test
	void constituteElectoralBoardReady() {
		when(ELECTORAL_BOARD_REPOSITORY_MOCK.find(anyString())).thenReturn(electoralBoardReadyJson.toString());

		assertFalse(electoralBoardConfigService.constitute(ELECTION_EVENT_ID, ELECTORAL_BOARD_ID, ELECTORAL_BOARD_PASSWORDS));
	}

	@Test
	void constituteElectoralBoardNotfound() {
		when(ELECTORAL_BOARD_REPOSITORY_MOCK.find(anyString())).thenReturn(null);

		assertFalse(electoralBoardConfigService.constitute(ELECTION_EVENT_ID, ELECTORAL_BOARD_ID, ELECTORAL_BOARD_PASSWORDS));
	}
}
