/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.importexport;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@ExtendWith(MockitoExtension.class)
class ImportExportFileSystemServiceTest {

	@Mock
	private PathResolver pathResolver;
	@TempDir
	private Path sdmDirectory;
	@TempDir
	private Path usbDirectory;
	private ImportExportFileSystemService importExportFilesystemService;

	private WhiteList whiteList;

	@BeforeEach
	void setUp() {
		whiteList = new WhiteList(pathResolver, true);
		importExportFilesystemService = new ImportExportFileSystemService(pathResolver, whiteList);
		when(pathResolver.resolveSdmPath()).thenReturn(sdmDirectory);
	}

	@Test
	void importFileSystem() throws IOException {
		// given
		initializeSourceDirectory(usbDirectory);

		// when
		importExportFilesystemService.importFileSystem(usbDirectory);

		// then
		validateTargetDirectory(sdmDirectory);
	}

	@Test
	void exportFileSystem() throws IOException {

		final String electionEventId = "ddfd6b5267534b47ba7ff1bbe8bee855";

		when(pathResolver.resolveElectionEventPath(any())).thenReturn(sdmDirectory.resolve("config").resolve(electionEventId));

		// given
		initializeSourceDirectory(sdmDirectory);

		// when
		importExportFilesystemService.exportFileSystem(electionEventId, usbDirectory);

		// then
		validateTargetDirectory(usbDirectory);
	}

	private void initializeSourceDirectory(final Path baseDirectory) throws IOException {
		final String adminBoardsDirectory = "config/admin-boards/";
		final String electionEventDirectory = "config/ddfd6b5267534b47ba7ff1bbe8bee855/";
		final String customer = electionEventDirectory + "CUSTOMER/";
		final String online = electionEventDirectory + "ONLINE/";
		final String offline = electionEventDirectory + "OFFLINE/";

		/*sdmConfig root*/
		createTestFile(baseDirectory, "sdmConfig/elections_config.json");

		/*admin-boards directory*/
		createTestFile(baseDirectory, adminBoardsDirectory + "a7ff1bbe8bee855ddfd6b5267534b47b/setupComponentAdminBoardHashesPayload.json");

		/*election root*/
		createTestFile(baseDirectory, electionEventDirectory + "controlComponentPublicKeysPayload.0.json"); // unwanted file
		createTestFile(baseDirectory, electionEventDirectory + "controlComponentPublicKeysPayload.1.json");
		createTestFile(baseDirectory, electionEventDirectory + "controlComponentPublicKeysPayload.2.json");
		createTestFile(baseDirectory, electionEventDirectory + "controlComponentPublicKeysPayload.3.json");
		createTestFile(baseDirectory, electionEventDirectory + "controlComponentPublicKeysPayload.4.json");
		createTestFile(baseDirectory, electionEventDirectory + "controlComponentPublicKeysPayload.5.json"); // unwanted file
		createTestFile(baseDirectory, electionEventDirectory + "electionEventContextPayload.json");

		/*customer directory*/
		createTestFile(baseDirectory, customer + "input/configuration-anonymized.xml");
		createTestFile(baseDirectory, customer + "input/encryptionParametersPayload.json");

		/*offline directory*/
		createTestFile(baseDirectory, offline + "setupComponentElectoralBoardHashesPayload.json");

		for (String ballotId : List.of("81edb65ff0504270816063ad7de5ea49", "73c78a58a3574db7ae1df6daa0a6f2af")) {
			final String ballotDirectory = online + "electionInformation/ballots/" + ballotId + "/";

			createTestFile(baseDirectory, ballotDirectory + "ballot.json");

			final String ballotBoxDirectory = ballotDirectory + "ballotBoxes/21b65581e63c4fa19f6c646b1fed8c9a/";

			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentBallotBoxPayload_0.json"); // unwanted file
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentBallotBoxPayload_1.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentBallotBoxPayload_2.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentBallotBoxPayload_3.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentBallotBoxPayload_4.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentBallotBoxPayload_5.json"); // unwanted file
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentShufflePayload_0.json"); // unwanted file
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentShufflePayload_1.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentShufflePayload_2.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentShufflePayload_3.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentShufflePayload_4.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentShufflePayload_5.json"); // unwanted file
			createTestFile(baseDirectory, ballotBoxDirectory + "tallyComponentShufflePayload.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "tallyComponentVotesPayload.json");
		}

		// create unwanted files
		Files.walkFileTree(baseDirectory, new SimpleFileVisitor<>() {
			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				Files.createFile(dir.resolve("unwanted"));
				return FileVisitResult.CONTINUE;
			}
		});
	}

	private void validateTargetDirectory(final Path baseDirectory) throws IOException {
		final String adminBoardsDirectory = "config/admin-boards/";
		final String electionEventDirectory = "config/ddfd6b5267534b47ba7ff1bbe8bee855/";
		final String customer = electionEventDirectory + "CUSTOMER/";
		final String online = electionEventDirectory + "ONLINE/";
		final String offline = electionEventDirectory + "OFFLINE/";

		/*sdmConfig root*/
		assertThat(baseDirectory.resolve("sdmConfig/elections_config.json")).exists();

		/*admin-boards directory*/
		assertThat(
				baseDirectory.resolve(adminBoardsDirectory + "a7ff1bbe8bee855ddfd6b5267534b47b/setupComponentAdminBoardHashesPayload.json")).exists();

		/*election root*/
		assertThat(baseDirectory.resolve(electionEventDirectory + "controlComponentPublicKeysPayload.0.json")).doesNotExist();
		assertThat(baseDirectory.resolve(electionEventDirectory + "controlComponentPublicKeysPayload.1.json")).exists();
		assertThat(baseDirectory.resolve(electionEventDirectory + "controlComponentPublicKeysPayload.2.json")).exists();
		assertThat(baseDirectory.resolve(electionEventDirectory + "controlComponentPublicKeysPayload.3.json")).exists();
		assertThat(baseDirectory.resolve(electionEventDirectory + "controlComponentPublicKeysPayload.4.json")).exists();
		assertThat(baseDirectory.resolve(electionEventDirectory + "controlComponentPublicKeysPayload.5.json")).doesNotExist();
		assertThat(baseDirectory.resolve(electionEventDirectory + "electionEventContextPayload.json")).exists();

		/*customer directory*/
		assertThat(baseDirectory.resolve(customer + "input/configuration-anonymized.xml")).exists();
		assertThat(baseDirectory.resolve(customer + "input/encryptionParametersPayload.json")).exists();

		/*offline directory*/
		assertThat(baseDirectory.resolve(offline + "setupComponentElectoralBoardHashesPayload.json")).exists();

		for (final String ballotId : List.of("81edb65ff0504270816063ad7de5ea49", "73c78a58a3574db7ae1df6daa0a6f2af")) {

			final String ballotDirectory = online + "electionInformation/ballots/" + ballotId + "/";

			final String ballotBoxDirectory = ballotDirectory + "ballotBoxes/21b65581e63c4fa19f6c646b1fed8c9a/";

			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentBallotBoxPayload_0.json")).doesNotExist();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentBallotBoxPayload_1.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentBallotBoxPayload_2.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentBallotBoxPayload_3.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentBallotBoxPayload_4.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentBallotBoxPayload_5.json")).doesNotExist();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentShufflePayload_0.json")).doesNotExist();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentShufflePayload_1.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentShufflePayload_2.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentShufflePayload_3.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentShufflePayload_4.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentShufflePayload_5.json")).doesNotExist();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "tallyComponentShufflePayload.json")).doesNotExist();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "tallyComponentVotesPayload.json")).doesNotExist();
		}

		final String sdmConfig = "sdmConfig/";
		assertThat(baseDirectory.resolve(sdmConfig + "elections_config.json")).exists();

		// check unwanted files
		Files.walkFileTree(baseDirectory, new SimpleFileVisitor<>() {
			@Override
			public FileVisitResult postVisitDirectory(final Path dir, final IOException exc) {
				assertThat(dir.resolve("unwanted")).doesNotExist();
				return FileVisitResult.CONTINUE;
			}
		});
	}

	private void createTestFile(final Path baseDirectory, final String pathName) throws IOException {
		final Path path = baseDirectory.resolve(pathName);
		Files.createDirectories(path.getParent());
		Files.createFile(path);
	}
}