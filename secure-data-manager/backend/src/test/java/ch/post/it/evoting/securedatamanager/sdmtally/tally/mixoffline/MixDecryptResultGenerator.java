/*
 *  (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.VerifiablePlaintextDecryption;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientMessage;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.mixnet.VerifiableShuffle;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.VerifiableShuffleGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.DecryptionProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.DecryptionProofGenerator;
import ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline.MixDecOfflineOutput;

public class MixDecryptResultGenerator {

	private final GqGroup group;

	MixDecryptResultGenerator(final GqGroup group) {
		this.group = group;
	}

	MixDecOfflineOutput genMixDecOfflineOutput(final int numCiphertexts, final int ciphertextSize) {
		final VerifiableShuffle shuffle = new VerifiableShuffleGenerator(group).genVerifiableShuffle(numCiphertexts, ciphertextSize);
		final VerifiablePlaintextDecryption plaintextDecryption = genVerifiablePlaintextDecryption(numCiphertexts, ciphertextSize);
		return new MixDecOfflineOutput(shuffle, plaintextDecryption.getDecryptedVotes(),
				plaintextDecryption.getDecryptionProofs());
	}

	VerifiablePlaintextDecryption genVerifiablePlaintextDecryption(final int numMessages, final int messageSize) {
		final GroupVector<ElGamalMultiRecipientMessage, GqGroup> decryptedVotes = new ElGamalGenerator(group)
				.genRandomMessageVector(numMessages, messageSize);
		final GroupVector<DecryptionProof, ZqGroup> decryptionProofs = new DecryptionProofGenerator(ZqGroup.sameOrderAs(group))
				.genDecryptionProofVector(numMessages, messageSize);
		return new VerifiablePlaintextDecryption(decryptedVotes, decryptionProofs);
	}

}
