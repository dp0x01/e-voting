/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

class KeystoreRepositoryTest {

	private static final String KEYSTORE_CONTENT_CONFIG = "keystore-content-config";
	private static final String KEYSTORE_PASSWORD_CONTENT_CONFIG = "keystore-password-content-config";
	private static final String KEYSTORE_CONTENT_TALLY = "keystore-content-tally";
	private static final String KEYSTORE_PASSWORD_CONTENT_TALLY = "keystore-password-content-tally";
	@TempDir
	static Path tempKeystorePath;

	static KeystoreRepository keystoreRepository;

	@BeforeAll
	static void setUp() {

		final String keystoreLocationConfig = tempKeystorePath.resolve("signing_keystore_test_config.p12").toString();
		final String keystorePasswordLocationConfig = tempKeystorePath.resolve("signing_pw_test_config.txt").toString();
		final String keystoreLocationTally = tempKeystorePath.resolve("signing_keystore_test_tally.p12").toString();
		final String keystorePasswordLocationTally = tempKeystorePath.resolve("signing_pw_test_tally.txt").toString();

		try {
			Files.writeString(Paths.get(keystoreLocationConfig), KEYSTORE_CONTENT_CONFIG);
			Files.writeString(Paths.get(keystorePasswordLocationConfig), KEYSTORE_PASSWORD_CONTENT_CONFIG);
			Files.writeString(Paths.get(keystoreLocationTally), KEYSTORE_CONTENT_TALLY);
			Files.writeString(Paths.get(keystorePasswordLocationTally), KEYSTORE_PASSWORD_CONTENT_TALLY);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		keystoreRepository = new KeystoreRepository(keystoreLocationConfig, keystorePasswordLocationConfig, keystoreLocationTally,
				keystorePasswordLocationTally);
	}

	@Test
	void testGetKeyStore() throws IOException {
		// given

		// when
		String keyStoreContentConfig = IOUtils.toString(keystoreRepository.getConfigKeyStore(), StandardCharsets.UTF_8);
		String keyStoreContentTally = IOUtils.toString(keystoreRepository.getTallyKeyStore(), StandardCharsets.UTF_8);

		// then
		assertThat(keyStoreContentConfig).isEqualTo(KEYSTORE_CONTENT_CONFIG);
		assertThat(keyStoreContentTally).isEqualTo(KEYSTORE_CONTENT_TALLY);
	}

	@Test
	void testGetKeystorePassword() throws IOException {
		// given

		// when
		char[] passwordContentConfig = keystoreRepository.getConfigKeystorePassword();
		char[] passwordContentTally = keystoreRepository.getTallyKeystorePassword();

		// then
		assertThat(passwordContentConfig).isEqualTo(KEYSTORE_PASSWORD_CONTENT_CONFIG.toCharArray());
		assertThat(passwordContentTally).isEqualTo(KEYSTORE_PASSWORD_CONTENT_TALLY.toCharArray());
	}
}
