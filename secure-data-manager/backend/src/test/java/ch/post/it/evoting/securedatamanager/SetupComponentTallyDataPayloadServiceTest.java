/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static ch.post.it.evoting.securedatamanager.commons.Constants.CONFIG_FILE_NAME_SETUP_COMPONENT_TALLY_DATA_PAYLOAD;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.SerializationUtils;
import ch.post.it.evoting.evotinglibraries.domain.configuration.SetupComponentTallyDataPayload;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@DisplayName("A SetupComponentTallyDataPayloadService")
class SetupComponentTallyDataPayloadServiceTest {
	private static final Random random = RandomFactory.createRandom();
	private static final Hash hashService = HashFactory.createHash();

	private static final String ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String MISSING_ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String EXISTING_ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String INVALID_ID = "invalidId";

	private static ObjectMapper objectMapper;
	private static PathResolver pathResolver;
	private static SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService;

	@BeforeAll
	static void setUpAll(
			@TempDir
			final Path tempDir) throws IOException {
		objectMapper = DomainObjectMapper.getNewInstance();

		createDirectories(tempDir, ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);

		final PathResolver pathResolver = new PathResolver(tempDir.toString());

		final SetupComponentTallyDataPayloadFileRepository setupComponentTallyDataPayloadFileRepository =
				new SetupComponentTallyDataPayloadFileRepository(objectMapper, pathResolver);

		final SetupComponentTallyDataPayload setupComponentTallyDataPayload = validSetupComponentTallyDataPayload(ELECTION_EVENT_ID,
				VERIFICATION_CARD_SET_ID);
		setupComponentTallyDataPayloadFileRepository.save(setupComponentTallyDataPayload);

		setupComponentTallyDataPayloadService = new SetupComponentTallyDataPayloadService(setupComponentTallyDataPayloadFileRepository);
	}

	private static SetupComponentTallyDataPayload validSetupComponentTallyDataPayload(final String electionEventId,
			final String verificationCardSetId) {
		final GqGroup encryptionGroup = SerializationUtils.getGqGroup();
		final List<String> verificationCardIds = List.of(
				random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH),
				random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH),
				random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH));
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> verificationCardPublicKeys = GroupVector.of(
				SerializationUtils.getPublicKey(),
				SerializationUtils.getPublicKey(),
				SerializationUtils.getPublicKey());

		final String ballotBoxAlias = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
		final SetupComponentTallyDataPayload setupComponentTallyDataPayload = new SetupComponentTallyDataPayload(electionEventId,
				verificationCardSetId, ballotBoxAlias, encryptionGroup, verificationCardIds, verificationCardPublicKeys);

		final byte[] payloadHash = hashService.recursiveHash(setupComponentTallyDataPayload);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(payloadHash);
		setupComponentTallyDataPayload.setSignature(signature);

		return setupComponentTallyDataPayload;
	}

	private static void createDirectories(final Path tempDir, final String electionEventId, final String verificationCardSetId) throws IOException {
		Files.createDirectories(
				tempDir.resolve("sdm/config").resolve(electionEventId).resolve("ONLINE").resolve("voteVerification").resolve(verificationCardSetId));
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private SetupComponentTallyDataPayload setupComponentTallyDataPayload;

		private SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadServiceTemp;

		@BeforeAll
		void setUpAll(
				@TempDir
				final Path tempDir) throws IOException {

			createDirectories(tempDir, ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);

			pathResolver = new PathResolver(tempDir.toString());

			final SetupComponentTallyDataPayloadFileRepository setupComponentTallyDataPayloadFileRepositoryTemp =
					new SetupComponentTallyDataPayloadFileRepository(objectMapper, pathResolver);

			setupComponentTallyDataPayloadServiceTemp = new SetupComponentTallyDataPayloadService(setupComponentTallyDataPayloadFileRepositoryTemp);
		}

		@BeforeEach
		void setUp() {
			setupComponentTallyDataPayload = validSetupComponentTallyDataPayload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);
		}

		@Test
		@DisplayName("a valid payload does not throw")
		void saveValidPayload() {
			assertDoesNotThrow(() -> setupComponentTallyDataPayloadServiceTemp.save(setupComponentTallyDataPayload));

			assertTrue(Files.exists(pathResolver.resolveVerificationCardSetPath(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)
					.resolve(CONFIG_FILE_NAME_SETUP_COMPONENT_TALLY_DATA_PAYLOAD)));
		}

		@Test
		@DisplayName("a null setup component tally data payload throws")
		void saveNullPayload() {
			assertThrows(NullPointerException.class, () -> setupComponentTallyDataPayloadServiceTemp.save(null));
		}
	}

	@Nested
	@DisplayName("calling exist")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class ExistTest {

		@Test
		@DisplayName("for valid election event and verification card set returns true")
		void existValidElectionEvent() {
			assertTrue(setupComponentTallyDataPayloadService.exist(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
		}

		@Test
		@DisplayName("for null input throws NullPointerException")
		void existNullInput() {
			assertThrows(NullPointerException.class, () -> setupComponentTallyDataPayloadService.exist(null, VERIFICATION_CARD_SET_ID));
			assertThrows(NullPointerException.class, () -> setupComponentTallyDataPayloadService.exist(EXISTING_ELECTION_EVENT_ID, null));
		}

		@Test
		@DisplayName("for invalid input throws FailedValidationException")
		void existInvalidInput() {
			assertThrows(FailedValidationException.class, () -> setupComponentTallyDataPayloadService.exist(INVALID_ID, VERIFICATION_CARD_SET_ID));
			assertThrows(FailedValidationException.class, () -> setupComponentTallyDataPayloadService.exist(EXISTING_ELECTION_EVENT_ID, INVALID_ID));
		}

		@Test
		@DisplayName("for non existing election event returns false")
		void existNonExistingElectionEvent() {
			assertFalse(setupComponentTallyDataPayloadService.exist(MISSING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
		}

	}

	@Nested
	@DisplayName("loading")
	@TestInstance(TestInstance.Lifecycle.PER_METHOD)
	class LoadTest {

		@Test
		@DisplayName("existing election event and verification card set returns expected setup component tally data payload")
		void loadExistingElectionEventValidSignature() {
			final SetupComponentTallyDataPayload setupComponentTallyDataPayload = setupComponentTallyDataPayloadService.load(ELECTION_EVENT_ID,
					VERIFICATION_CARD_SET_ID);

			assertEquals(ELECTION_EVENT_ID, setupComponentTallyDataPayload.getElectionEventId());
		}

		@Test
		@DisplayName("null input throws NullPointerException")
		void loadNullInput() {
			assertThrows(NullPointerException.class, () -> setupComponentTallyDataPayloadService.load(null, VERIFICATION_CARD_SET_ID));
			assertThrows(NullPointerException.class, () -> setupComponentTallyDataPayloadService.load(ELECTION_EVENT_ID, null));
		}

		@Test
		@DisplayName("invalid input throws FailedValidationException")
		void loadInvalidInput() {
			assertThrows(FailedValidationException.class, () -> setupComponentTallyDataPayloadService.load(INVALID_ID, VERIFICATION_CARD_SET_ID));
			assertThrows(FailedValidationException.class, () -> setupComponentTallyDataPayloadService.load(ELECTION_EVENT_ID, INVALID_ID));
		}

		@Test
		@DisplayName("existing election event and verification card set but with missing payload throws IllegalStateException")
		void loadMissingPayload() {
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> setupComponentTallyDataPayloadService.load(MISSING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));

			final String errorMessage = String.format(
					"Requested setup component tally data payload is not present. [electionEventId: %s, verificationCardSetId: %s]",
					MISSING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);
			assertEquals(errorMessage, Throwables.getRootCause(exception).getMessage());
		}

	}

}
