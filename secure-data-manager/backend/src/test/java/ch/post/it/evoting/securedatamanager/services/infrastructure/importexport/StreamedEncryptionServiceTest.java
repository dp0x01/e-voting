/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.importexport;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.hashing.Argon2;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Factory;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Profile;
import ch.post.it.evoting.cryptoprimitives.internal.math.RandomService;
import ch.post.it.evoting.cryptoprimitives.math.Random;

class StreamedEncryptionServiceTest {
	private final Argon2 argon2 = Argon2Factory.createArgon2(Argon2Profile.TEST.getContext());
	private final Random random = new RandomService();
	private final StreamedEncryptionService streamedEncryptionService = new StreamedEncryptionService(random, argon2);

	private final byte[] BYTES = random.genRandomBase32String(4096).getBytes(StandardCharsets.UTF_8);
	private final char[] PASSWORD = "password".toCharArray();

	private final byte[] ASSOCIATED_DATA = new byte[] {};

	@Test
	void encryptAndDecrypt() throws IOException {

		try (final ByteArrayInputStream dataStream = new ByteArrayInputStream(BYTES);
				final ByteArrayOutputStream encryptedStream = new ByteArrayOutputStream()) {

			streamedEncryptionService.encrypt(encryptedStream, dataStream, PASSWORD, ASSOCIATED_DATA);

			try (final ByteArrayInputStream is = new ByteArrayInputStream(encryptedStream.toByteArray())) {
				final InputStream decryptedStream = streamedEncryptionService.decrypt(is, PASSWORD, ASSOCIATED_DATA);
				assertArrayEquals(BYTES, decryptedStream.readAllBytes());
			}
		}
	}

	@Test
	void encryptAndDecryptWrongPassword() throws IOException {

		try (final ByteArrayInputStream dataStream = new ByteArrayInputStream(BYTES);
				final ByteArrayOutputStream encryptedStream = new ByteArrayOutputStream()) {

			streamedEncryptionService.encrypt(encryptedStream, dataStream, PASSWORD, ASSOCIATED_DATA);

			try (final ByteArrayInputStream is = new ByteArrayInputStream(encryptedStream.toByteArray())) {
				IOException exception = assertThrows(IOException.class, () -> {
					final InputStream decryptedStream = streamedEncryptionService.decrypt(is, "ANOTHER_PASSWORD".toCharArray(), ASSOCIATED_DATA);
					decryptedStream.readAllBytes();
				});
				assertEquals("javax.crypto.AEADBadTagException: mac check in GCM failed", exception.getMessage());
			}
		}
	}

	@Test
	void encryptAndDecryptWrongAssociatedData() throws IOException {

		try (final ByteArrayInputStream dataStream = new ByteArrayInputStream(BYTES);
				final ByteArrayOutputStream encryptedStream = new ByteArrayOutputStream()) {

			streamedEncryptionService.encrypt(encryptedStream, dataStream, PASSWORD, ASSOCIATED_DATA);

			try (final ByteArrayInputStream is = new ByteArrayInputStream(encryptedStream.toByteArray())) {
				IOException exception = assertThrows(IOException.class, () -> {
					final InputStream decryptedStream = streamedEncryptionService.decrypt(is, PASSWORD, new byte[] { 1, 2, 3 });
					decryptedStream.readAllBytes();
				});
				assertEquals("javax.crypto.AEADBadTagException: mac check in GCM failed", exception.getMessage());
			}
		}
	}
}