/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientMessage;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement.PrimeGqElementFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.FactorizeAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetActualVotingOptionsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetEncodedVotingOptionsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.writeins.DecodeWriteInsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.writeins.IntegerToWriteInAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.writeins.IsWriteInOptionAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.writeins.QuadraticResidueToWriteInAlgorithm;

/**
 * Tests of ProcessPlaintextsAlgorithm.
 */
@DisplayName("ProcessPlaintexts with")
class ProcessPlaintextsAlgorithmTest {

	private static final SecureRandom RANDOM = new SecureRandom();
	private static final GqGroup GQ_GROUP = GroupTestData.getLargeGqGroup();
	private static final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(GQ_GROUP);

	private static ProcessPlaintextsAlgorithm processPlaintextsAlgorithm;

	private PrimesMappingTable primesMappingTable;
	private GroupVector<ElGamalMultiRecipientMessage, GqGroup> plaintextVotes;

	private GroupVector<PrimeGqElement, GqGroup> writeInVotingOptions;
	private int numberOfSelectableVotes;
	private int numberOfAllowedWriteInsPlusOne;
	private ProcessPlaintextsContext processPlaintextsContext;

	@BeforeAll
	static void setupAll() {
		final IntegerToWriteInAlgorithm integerToWriteInAlgorithm = new IntegerToWriteInAlgorithm();
		final QuadraticResidueToWriteInAlgorithm quadraticResidueToWriteInAlgorithm = new QuadraticResidueToWriteInAlgorithm(
				integerToWriteInAlgorithm);
		final DecodeWriteInsAlgorithm decodeWriteInsAlgorithm = new DecodeWriteInsAlgorithm(new IsWriteInOptionAlgorithm(),
				quadraticResidueToWriteInAlgorithm);
		processPlaintextsAlgorithm = new ProcessPlaintextsAlgorithm(ElGamalFactory.createElGamal(), new FactorizeAlgorithm(),
				decodeWriteInsAlgorithm, new GetEncodedVotingOptionsAlgorithm(), new GetActualVotingOptionsAlgorithm());
	}

	@BeforeEach
	void setup() {
		final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions = PrimeGqElementFactory.getSmallPrimeGroupMembers(GQ_GROUP, 3);

		final List<String> actualVotingOptions = List.of(
				"57a30570-1722-3a7e-a8f9-7dd643d7f33",
				"b9c57a40-a555-35e1-972c-a1a6b7e03381",
				"1-3");

		final List<String> semantics = List.of(
				"Semantics 1",
				"Semantics 2",
				"Semantics 3");

		final GroupVector<PrimesMappingTableEntry, GqGroup> primesMappingTableEntries = IntStream.range(0, actualVotingOptions.size())
				.mapToObj(i -> new PrimesMappingTableEntry(actualVotingOptions.get(i), encodedVotingOptions.get(i), semantics.get(i)))
				.collect(GroupVector.toGroupVector());

		primesMappingTable = PrimesMappingTable.from(primesMappingTableEntries);

		numberOfSelectableVotes = RANDOM.nextInt(120) + 1; // range [1, 120]
		numberOfAllowedWriteInsPlusOne = RANDOM.nextInt(5) + 1;
		final int n = RANDOM.nextInt(4) + 2;
		plaintextVotes = elGamalGenerator.genRandomMessageVector(n, numberOfAllowedWriteInsPlusOne);
		writeInVotingOptions = numberOfAllowedWriteInsPlusOne == 1 ?
				GroupVector.of() :
				PrimeGqElementFactory.getSmallPrimeGroupMembers(GQ_GROUP, numberOfAllowedWriteInsPlusOne - 1);

		processPlaintextsContext = new ProcessPlaintextsContext(GQ_GROUP, primesMappingTable);
	}

	@DisplayName("valid inputs behaves as expected")
	@Test
	void happyPath() {
		final GqGroup gqGroup = new GqGroup(BigInteger.valueOf(47), BigInteger.valueOf(23), BigInteger.valueOf(2));
		final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions = Stream.of(3, 7, 17, 37)
				.map(value -> PrimeGqElementFactory.fromValue(value, gqGroup))
				.collect(GroupVector.toGroupVector());

		final List<String> actualVotingOptions = List.of(
				"57a30570-1722-3a7e-a8f9-7dd643d7f33",
				"b9c57a40-a555-35e1-972c-a1a6b7e03381",
				"8814c2b6-8c73-38e8-99e6-830bffdf32c6",
				"1-3");

		final List<String> semantics = List.of(
				"Semantics 1",
				"Semantics 2",
				"Semantics 3",
				"Semantics 4");

		final GroupVector<PrimesMappingTableEntry, GqGroup> primesMappingTableEntries = IntStream.range(0, actualVotingOptions.size())
				.mapToObj(i -> new PrimesMappingTableEntry(actualVotingOptions.get(i), encodedVotingOptions.get(i), semantics.get(i)))
				.collect(GroupVector.toGroupVector());

		final PrimesMappingTable pTable = PrimesMappingTable.from(primesMappingTableEntries);

		final GqElement gqElement = GqElement.GqElementFactory.fromValue(BigInteger.valueOf(21), gqGroup);
		final GroupVector<ElGamalMultiRecipientMessage, GqGroup> plaintextVotes = GroupVector.of(
				new ElGamalMultiRecipientMessage(GroupVector.of(gqElement)),
				new ElGamalMultiRecipientMessage(GroupVector.of(gqElement)));

		final ProcessPlaintextsContext context = new ProcessPlaintextsContext(gqGroup, pTable);
		final ProcessPlaintextsInput input = new ProcessPlaintextsInput.Builder()
				.setPlaintextVotes(plaintextVotes)
				.setWriteInVotingOptions(GroupVector.of())
				.setNumberOfSelectableVotingOptions(2)
				.setNumberOfAllowedWriteInsPlusOne(1).build();
		final ProcessPlaintextsAlgorithmOutput processPlaintextsAlgorithmOutput = assertDoesNotThrow(
				() -> processPlaintextsAlgorithm.processPlaintexts(context, input));

		final GroupVector<PrimeGqElement, GqGroup> encodedVotes = Stream.of(3, 7)
				.map(value -> PrimeGqElement.PrimeGqElementFactory.fromValue(value, gqGroup))
				.collect(GroupVector.toGroupVector());

		final GroupVector<GroupVector<PrimeGqElement, GqGroup>, GqGroup> L_votes = GroupVector.of(encodedVotes, encodedVotes);
		final List<List<String>> L_decodedVotes = List.of(
				List.of(actualVotingOptions.get(0), actualVotingOptions.get(1)),
				List.of(actualVotingOptions.get(0), actualVotingOptions.get(1)));

		assertEquals(L_votes, processPlaintextsAlgorithmOutput.L_votes());
		assertEquals(L_decodedVotes, processPlaintextsAlgorithmOutput.L_decodedVotes());
	}

	@Test
	@DisplayName("fewer than two mixed votes throws an IllegalArgumentException")
	void processPlaintextsWithTooSmallPlaintextVotesThrows() {

		final GroupVector<ElGamalMultiRecipientMessage, GqGroup> singlePlaintextVotes = elGamalGenerator.genRandomMessageVector(1,
				numberOfAllowedWriteInsPlusOne);
		final ProcessPlaintextsInput input = new ProcessPlaintextsInput.Builder()
				.setPlaintextVotes(singlePlaintextVotes)
				.setWriteInVotingOptions(writeInVotingOptions)
				.setNumberOfSelectableVotingOptions(numberOfSelectableVotes)
				.setNumberOfAllowedWriteInsPlusOne(numberOfAllowedWriteInsPlusOne)
				.build();
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> processPlaintextsAlgorithm.processPlaintexts(processPlaintextsContext, input));

		assertEquals(String.format("There must be at least two mixed votes. [N_c_hat: %s]", singlePlaintextVotes.size()),
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("processPlaintexts with number of selectable votes more than 120 throws an IllegalArgumentException")
	void processPlaintextsWithTooBigNumberOfSelectableVotesThrows() {
		final int tooBigNumberOfSelectableVotes = 121;
		final ProcessPlaintextsInput input = new ProcessPlaintextsInput.Builder()
				.setPlaintextVotes(plaintextVotes)
				.setWriteInVotingOptions(writeInVotingOptions)
				.setNumberOfSelectableVotingOptions(tooBigNumberOfSelectableVotes)
				.setNumberOfAllowedWriteInsPlusOne(numberOfAllowedWriteInsPlusOne)
				.build();
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> processPlaintextsAlgorithm.processPlaintexts(processPlaintextsContext, input));
		assertEquals(
				String.format("The number of selectable voting options must be smaller or equal to 120. [psi: %s]", tooBigNumberOfSelectableVotes),
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("processPlaintexts with number of write-ins + 1 bigger than number of elements in the decrypted votes throws an IllegalArgumentException")
	void processPlaintextsWithTooSmallBigNumberOfAllowedWriteInsPlusOneThrows() {
		final int tooBigNumberOfWriteInsPlusOne = plaintextVotes.getElementSize() + 1;
		final GroupVector<PrimeGqElement, GqGroup> anotherWriteInVotingOption = PrimeGqElementFactory.getSmallPrimeGroupMembers(GQ_GROUP,
				tooBigNumberOfWriteInsPlusOne - 1);
		final ProcessPlaintextsInput input = new ProcessPlaintextsInput.Builder()
				.setPlaintextVotes(plaintextVotes)
				.setWriteInVotingOptions(anotherWriteInVotingOption)
				.setNumberOfSelectableVotingOptions(numberOfSelectableVotes)
				.setNumberOfAllowedWriteInsPlusOne(tooBigNumberOfWriteInsPlusOne)
				.build();

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> processPlaintextsAlgorithm.processPlaintexts(processPlaintextsContext, input));
		assertEquals(String.format(
				"The number of allowed write-ins + 1 must be smaller or equal to the number of elements in the decrypted votes. [delta_hat: %s, l: %s]",
				tooBigNumberOfWriteInsPlusOne, plaintextVotes.getElementSize()), Throwables.getRootCause(exception).getMessage());
	}
}
