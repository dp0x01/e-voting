/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SignatureException;
import java.util.Objects;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.securedatamanager.configuration.EncryptionParametersConfigService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@DisplayName("EncryptionParametersConfigService")
class EncryptionParametersConfigServiceTest {

	private static final String ELECTION_EVENT_ID = "be658216a38b421b943457846c6a68f9";
	private static final String WRONG_ELECTION_EVENT_ID = "414bd34dcf6e4de4b771a92fa3849d3d";

	private static EncryptionParametersConfigService encryptionParametersConfigService;

	@BeforeAll
	static void setUpAll() throws URISyntaxException, SignatureException {
		final Path path = Paths.get(Objects.requireNonNull(EncryptionParametersConfigServiceTest.class.getResource("/encryptionParametersTest/")).toURI());
		final PathResolver pathResolver = new PathResolver(path.toString());
		final EncryptionParametersPayloadFileRepository encryptionParametersPayloadFileRepository =
				new EncryptionParametersPayloadFileRepository(pathResolver, DomainObjectMapper.getNewInstance());
		final SignatureKeystore<Alias> signatureKeystore = mock(SignatureKeystore.class);
		when(signatureKeystore.verifySignature(any(), any(), any(), any())).thenReturn(true);
		encryptionParametersConfigService = new EncryptionParametersConfigService(encryptionParametersPayloadFileRepository, signatureKeystore);
	}

	@Test
	@DisplayName("loading existing parameters returns them")
	void loadExisting() {
		final GqGroup gqGroup = assertDoesNotThrow(() -> encryptionParametersConfigService.loadEncryptionGroup(ELECTION_EVENT_ID));
		assertNotNull(gqGroup);
	}

	@Test
	@DisplayName("loading invalid election event id throws FailedValidationException")
	void invalidElectionEventId() {
		assertThrows(FailedValidationException.class, () -> encryptionParametersConfigService.loadEncryptionGroup("invalidId"));
	}

	@Test
	@DisplayName("loading non existing parameters throws IllegalStateException")
	void loadNonExisting() {
		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> encryptionParametersConfigService.loadEncryptionGroup(WRONG_ELECTION_EVENT_ID));

		final String errorMessage = String.format("Requested encryption parameters payload is not present. [electionEventId: %s]",
				WRONG_ELECTION_EVENT_ID);
		assertEquals(errorMessage, exception.getMessage());
	}
}