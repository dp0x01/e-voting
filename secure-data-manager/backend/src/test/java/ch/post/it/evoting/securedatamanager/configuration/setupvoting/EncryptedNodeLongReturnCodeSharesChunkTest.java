/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;

@DisplayName("EncryptedNodeLongReturnCodeSharesChunk")
class EncryptedNodeLongReturnCodeSharesChunkTest {

	private static final String VALID_ID = "1de81acb944d4161a7148a2240edea47";
	private static final int VALID_CHUNK_ID = 0;
	private static final String ANOTHER_VALID_ID = "2de81acb944d4161a7148a2240edea47";
	private static final String INVALID_ID = "invalid id";
	private static ElGamalMultiRecipientCiphertext ciphertext;
	private static EncryptedSingleNodeLongReturnCodeSharesChunk encryptedSingleNodeLongReturnCodeSharesChunk;

	@BeforeAll
	static void setup() {
		final GqGroup gqGroup = GroupTestData.getGqGroup();
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
		ciphertext = elGamalGenerator.genRandomCiphertext(1);
		encryptedSingleNodeLongReturnCodeSharesChunk = new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
				.setNodeId(1)
				.setChunkId(VALID_CHUNK_ID)
				.setVerificationCardIds(Collections.singletonList(VALID_ID))
				.setExponentiatedEncryptedPartialChoiceReturnCodes(Collections.singletonList(ciphertext))
				.setExponentiatedEncryptedConfirmationKeys(Collections.singletonList(ciphertext))
				.build();
	}

	private static Stream<Arguments> provideNullInputsForEncryptedSingleNodeLongReturnCodeSharesChunk() {
		return Stream.of(
				Arguments.of(null, VALID_ID, VALID_CHUNK_ID, Collections.singletonList(VALID_ID), Collections.singletonList(
						encryptedSingleNodeLongReturnCodeSharesChunk)),
				Arguments.of(VALID_ID, null, VALID_CHUNK_ID, Collections.singletonList(VALID_ID), Collections.singletonList(
						encryptedSingleNodeLongReturnCodeSharesChunk)),
				Arguments.of(VALID_ID, VALID_ID, VALID_CHUNK_ID, null, Collections.singletonList(encryptedSingleNodeLongReturnCodeSharesChunk)),
				Arguments.of(VALID_ID, VALID_ID, VALID_CHUNK_ID, Collections.singletonList(VALID_ID), null)
		);
	}

	@ParameterizedTest
	@MethodSource("provideNullInputsForEncryptedSingleNodeLongReturnCodeSharesChunk")
	@DisplayName("calling builder with null values throws NullPointerException")
	void buildWithInvalidValuesThrows(final String electionEventId, final String verificationCardSetId, final int chunkId,
			final List<String> verificationCardIds,
			final List<EncryptedSingleNodeLongReturnCodeSharesChunk> nodeReturnCodesValues) {
		final EncryptedNodeLongReturnCodeSharesChunk.Builder builder = new EncryptedNodeLongReturnCodeSharesChunk.Builder()
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setChunkId(chunkId)
				.setVerificationCardIds(verificationCardIds)
				.setNodeReturnCodesValues(nodeReturnCodesValues);
		assertThrows(NullPointerException.class, builder::build);
	}

	private static Stream<Arguments> provideInvalidUUIDInputsForEncryptedSingleNodeLongReturnCodeSharesChunk() {
		return Stream.of(
				Arguments.of(INVALID_ID, VALID_ID, VALID_CHUNK_ID, Collections.singletonList(VALID_ID)),
				Arguments.of(VALID_ID, INVALID_ID, VALID_CHUNK_ID, Collections.singletonList(VALID_ID)),
				Arguments.of(VALID_ID, VALID_ID, VALID_CHUNK_ID, Collections.singletonList(INVALID_ID))
		);
	}

	@ParameterizedTest
	@MethodSource("provideInvalidUUIDInputsForEncryptedSingleNodeLongReturnCodeSharesChunk")
	@DisplayName("calling builder with null values throws FailedValidationException")
	void buildWithInvalidUUIDValuesThrows(final String electionEventId, final String verificationCardSetId, final int chunkId,
			final List<String> verificationCardIds) {
		final EncryptedNodeLongReturnCodeSharesChunk.Builder builder = new EncryptedNodeLongReturnCodeSharesChunk.Builder()
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setChunkId(chunkId)
				.setVerificationCardIds(verificationCardIds);
		assertThrows(FailedValidationException.class, builder::build);
	}

	@Test
	@DisplayName("calling builder with invalid node id values throws IllegalArgumentException")
	void buildWithInvalidNodeIdValues() {
		final EncryptedSingleNodeLongReturnCodeSharesChunk.Builder encryptedSingleNodeLongReturnCodeSharesBuilder = new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
				.setChunkId(VALID_CHUNK_ID)
				.setVerificationCardIds(Arrays.asList(VALID_ID, ANOTHER_VALID_ID))
				.setExponentiatedEncryptedPartialChoiceReturnCodes(Arrays.asList(ciphertext, ciphertext))
				.setExponentiatedEncryptedConfirmationKeys(Arrays.asList(ciphertext, ciphertext));

		// Wrong size node return codes values
		final List<EncryptedSingleNodeLongReturnCodeSharesChunk> wrongSizeNodeReturnCodesValues = Arrays.asList(
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(1).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(2).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(3).build()
		);
		final EncryptedNodeLongReturnCodeSharesChunk.Builder wrongSizeBuilder = new EncryptedNodeLongReturnCodeSharesChunk.Builder()
				.setElectionEventId(VALID_ID)
				.setVerificationCardSetId(VALID_ID)
				.setVerificationCardIds(Arrays.asList(VALID_ID, VALID_ID))
				.setNodeReturnCodesValues(wrongSizeNodeReturnCodesValues);

		// Missing node id return codes values
		final List<EncryptedSingleNodeLongReturnCodeSharesChunk> missingNodeIdReturnCodesValues = Arrays.asList(
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(1).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(2).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(3).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(3).build()
		);
		final EncryptedNodeLongReturnCodeSharesChunk.Builder missingNodeIdBuilder = new EncryptedNodeLongReturnCodeSharesChunk.Builder()
				.setElectionEventId(VALID_ID)
				.setVerificationCardSetId(VALID_ID)
				.setVerificationCardIds(Arrays.asList(VALID_ID, VALID_ID))
				.setNodeReturnCodesValues(missingNodeIdReturnCodesValues);

		// Wrong node id return codes values
		final List<EncryptedSingleNodeLongReturnCodeSharesChunk> wrongNodeIdReturnCodesValues = Arrays.asList(
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(1).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(2).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(3).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(3).build()
		);
		final EncryptedNodeLongReturnCodeSharesChunk.Builder wrongNodeIdBuilder = new EncryptedNodeLongReturnCodeSharesChunk.Builder()
				.setElectionEventId(VALID_ID)
				.setVerificationCardSetId(VALID_ID)
				.setVerificationCardIds(Arrays.asList(VALID_ID, VALID_ID))
				.setNodeReturnCodesValues(wrongNodeIdReturnCodesValues);

		assertAll(
				() -> assertThrows(IllegalArgumentException.class, wrongSizeBuilder::build),
				() -> assertThrows(IllegalArgumentException.class, missingNodeIdBuilder::build),
				() -> assertThrows(IllegalArgumentException.class, wrongNodeIdBuilder::build)
		);

	}

	@Test
	@DisplayName("calling builder with negative chunk id values throws IllegalArgumentException")
	void buildWithNegativeChunkIdValues() {
		final EncryptedSingleNodeLongReturnCodeSharesChunk.Builder encryptedSingleNodeLongReturnCodeSharesBuilder = new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
				.setChunkId(VALID_CHUNK_ID)
				.setVerificationCardIds(Arrays.asList(VALID_ID, ANOTHER_VALID_ID))
				.setExponentiatedEncryptedPartialChoiceReturnCodes(Arrays.asList(ciphertext, ciphertext))
				.setExponentiatedEncryptedConfirmationKeys(Arrays.asList(ciphertext, ciphertext));

		// Missing node id return codes values
		final List<EncryptedSingleNodeLongReturnCodeSharesChunk> nodeIdReturnCodesValues = Arrays.asList(
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(1).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(2).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(3).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(4).build()
		);
		final EncryptedNodeLongReturnCodeSharesChunk.Builder negativeChunkIdBuilder = new EncryptedNodeLongReturnCodeSharesChunk.Builder()
				.setChunkId(-1)
				.setElectionEventId(VALID_ID)
				.setVerificationCardSetId(VALID_ID)
				.setVerificationCardIds(Arrays.asList(VALID_ID, VALID_ID))
				.setNodeReturnCodesValues(nodeIdReturnCodesValues);

		final IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, negativeChunkIdBuilder::build);
		assertTrue(illegalArgumentException.getMessage().startsWith("Control component chunk id must be positive."));

	}

	@Test
	@DisplayName("calling builder with different verification card set ids count throw IllegalArgumentException")
	void buildWithDifferentCount() {
		final EncryptedSingleNodeLongReturnCodeSharesChunk.Builder encryptedSingleNodeLongReturnCodeSharesBuilder = new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
				.setChunkId(VALID_CHUNK_ID)
				.setVerificationCardIds(Arrays.asList(VALID_ID, ANOTHER_VALID_ID))
				.setExponentiatedEncryptedPartialChoiceReturnCodes(Arrays.asList(ciphertext, ciphertext))
				.setExponentiatedEncryptedConfirmationKeys(Arrays.asList(ciphertext, ciphertext));
		final List<EncryptedSingleNodeLongReturnCodeSharesChunk> nodeReturnCodesValues = Arrays.asList(
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(1).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(2).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(3).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder
						.setVerificationCardIds(Arrays.asList(VALID_ID))
						.setExponentiatedEncryptedPartialChoiceReturnCodes(Arrays.asList(ciphertext))
						.setExponentiatedEncryptedConfirmationKeys(Arrays.asList(ciphertext))
						.setNodeId(4).build()
		);
		final EncryptedNodeLongReturnCodeSharesChunk.Builder builder = new EncryptedNodeLongReturnCodeSharesChunk.Builder()
				.setChunkId(VALID_CHUNK_ID)
				.setElectionEventId(VALID_ID)
				.setVerificationCardSetId(VALID_ID)
				.setVerificationCardIds(Arrays.asList(VALID_ID, ANOTHER_VALID_ID))
				.setNodeReturnCodesValues(nodeReturnCodesValues);
		final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);
		assertTrue(ex.getMessage().startsWith("All nodes must return the same number of verificationCardIds."));
	}

	@Test
	@DisplayName("calling builder with different verification card set ids within nodes throw IllegalArgumentException")
	void buildWithDifferentValuesWithinNodes() {
		final EncryptedSingleNodeLongReturnCodeSharesChunk.Builder encryptedSingleNodeLongReturnCodeSharesBuilder = new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
				.setChunkId(VALID_CHUNK_ID)
				.setVerificationCardIds(Arrays.asList(VALID_ID, ANOTHER_VALID_ID))
				.setExponentiatedEncryptedPartialChoiceReturnCodes(Arrays.asList(ciphertext, ciphertext))
				.setExponentiatedEncryptedConfirmationKeys(Arrays.asList(ciphertext, ciphertext));
		final List<EncryptedSingleNodeLongReturnCodeSharesChunk> nodeReturnCodesValues = Arrays.asList(
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(1).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(2).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(3).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder
						.setVerificationCardIds(Arrays.asList(VALID_ID, "3de81acb944d4161a7148a2240edea47"))
						.setNodeId(4).build()
		);
		final EncryptedNodeLongReturnCodeSharesChunk.Builder builder = new EncryptedNodeLongReturnCodeSharesChunk.Builder()
				.setChunkId(VALID_CHUNK_ID)
				.setElectionEventId(VALID_ID)
				.setVerificationCardSetId(VALID_ID)
				.setVerificationCardIds(Arrays.asList(VALID_ID, ANOTHER_VALID_ID))
				.setNodeReturnCodesValues(nodeReturnCodesValues);
		final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);
		assertTrue(ex.getMessage().startsWith("All nodes must return the same verificationCardIds."));
	}

	@Test
	@DisplayName("calling builder with different verification card set ids throw IllegalArgumentException")
	void buildWithDifferentValues() {
		final EncryptedSingleNodeLongReturnCodeSharesChunk.Builder encryptedSingleNodeLongReturnCodeSharesBuilder = new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
				.setChunkId(VALID_CHUNK_ID)
				.setVerificationCardIds(Arrays.asList(VALID_ID, ANOTHER_VALID_ID))
				.setExponentiatedEncryptedPartialChoiceReturnCodes(Arrays.asList(ciphertext, ciphertext))
				.setExponentiatedEncryptedConfirmationKeys(Arrays.asList(ciphertext, ciphertext));
		final List<EncryptedSingleNodeLongReturnCodeSharesChunk> nodeReturnCodesValues = Arrays.asList(
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(1).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(2).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(3).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(4).build()
		);
		final EncryptedNodeLongReturnCodeSharesChunk.Builder builder = new EncryptedNodeLongReturnCodeSharesChunk.Builder()
				.setChunkId(VALID_CHUNK_ID)
				.setElectionEventId(VALID_ID)
				.setVerificationCardSetId(VALID_ID)
				.setVerificationCardIds(Arrays.asList(VALID_ID))
				.setNodeReturnCodesValues(nodeReturnCodesValues);
		final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);
		assertTrue(ex.getMessage().startsWith("The verificationCardIds must match the verificationCardIds in the nodeReturnCodesValues."));
	}

	@Test
	@DisplayName("calling builder with valid values does not throw")
	void buildWithValidValues() {
		final EncryptedSingleNodeLongReturnCodeSharesChunk.Builder encryptedSingleNodeLongReturnCodeSharesBuilder = new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
				.setChunkId(VALID_CHUNK_ID)
				.setVerificationCardIds(Arrays.asList(VALID_ID, ANOTHER_VALID_ID))
				.setExponentiatedEncryptedPartialChoiceReturnCodes(Arrays.asList(ciphertext, ciphertext))
				.setExponentiatedEncryptedConfirmationKeys(Arrays.asList(ciphertext, ciphertext));
		final List<EncryptedSingleNodeLongReturnCodeSharesChunk> nodeReturnCodesValues = Arrays.asList(
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(1).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(2).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(3).build(),
				encryptedSingleNodeLongReturnCodeSharesBuilder.setNodeId(4).build()
		);
		final EncryptedNodeLongReturnCodeSharesChunk.Builder builder = new EncryptedNodeLongReturnCodeSharesChunk.Builder()
				.setChunkId(VALID_CHUNK_ID)
				.setElectionEventId(VALID_ID)
				.setVerificationCardSetId(VALID_ID)
				.setVerificationCardIds(Arrays.asList(VALID_ID, ANOTHER_VALID_ID))
				.setNodeReturnCodesValues(nodeReturnCodesValues);
		assertDoesNotThrow(builder::build);
	}

}
