/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.security.SignatureException;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientMessage;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.mixnet.VerifiableShuffle;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.DecryptionProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.VerifiableDecryptions;
import ch.post.it.evoting.domain.tally.VerifyMixDecHelper;
import ch.post.it.evoting.securedatamanager.services.application.service.BallotBoxService;
import ch.post.it.evoting.securedatamanager.services.application.service.ElectionEventService;
import ch.post.it.evoting.securedatamanager.services.application.service.IdentifierValidationService;

class MixDecOfflineServiceTest extends TestGroupSetup {

	private static final int UUID_LENGTH = 32;
	private static final Random random = RandomFactory.createRandom();
	private static String electionEventId;
	private static String ballotId;
	private static String ballotBoxId;
	final List<char[]> ELECTORAL_BOARD_PASSWORDS = List.of("Password_ElectoralBoard1".toCharArray(), "Password_ElectoralBoard2".toCharArray());
	private MixDecOfflineAlgorithm mixDecOfflineAlgorithm;
	private EncryptionParametersTallyService encryptionParametersTallyService;
	private SignatureKeystore<Alias> signatureKeystoreSdmTallyService;
	private MixDecOfflineService mixDecOfflineService;
	private ElectionEventService electionEventService;
	private BallotBoxService ballotBoxService;
	private IdentifierValidationService identifierValidationService;

	@BeforeAll
	static void setUpSuite() {
		electionEventId = genValidUUID();
		ballotId = genValidUUID();
		ballotBoxId = genValidUUID();
	}

	@BeforeEach
	void setUp() {
		final TallyComponentShufflePayloadFileRepository finalPayloadFileRepository = mock(TallyComponentShufflePayloadFileRepository.class);
		mixDecOfflineAlgorithm = mock(MixDecOfflineAlgorithm.class);
		encryptionParametersTallyService = mock(EncryptionParametersTallyService.class);
		signatureKeystoreSdmTallyService = mock(SignatureKeystore.class);
		electionEventService = mock(ElectionEventService.class);
		ballotBoxService = mock(BallotBoxService.class);
		identifierValidationService = new IdentifierValidationService(electionEventService, ballotBoxService);

		mixDecOfflineService = new MixDecOfflineService(finalPayloadFileRepository, encryptionParametersTallyService, mixDecOfflineAlgorithm,
				signatureKeystoreSdmTallyService, identifierValidationService);
	}

	@Test
	void mixFacadeThrowsIllegalStateExceptionWhenCannotSignePayload() throws SignatureException {
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
		final int N = 2;
		final int l = 5;
		final int nodeId = NODE_IDS.last();

		final VerifyMixDecHelper verifyMixDecOnlineHelper = new VerifyMixDecHelper(gqGroup);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> ciphertexts = elGamalGenerator.genRandomCiphertextVector(N, l);
		final DecryptionProof decryptionProof = new DecryptionProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(l));
		final VerifiableDecryptions verifiableDecryptions = new VerifiableDecryptions(ciphertexts, GroupVector.of(decryptionProof, decryptionProof));
		final VerifiableShuffle verifiableShuffle = new VerifiableShuffle(ciphertexts, verifyMixDecOnlineHelper.createShuffleArgument(N, l));

		final GroupVector<ElGamalMultiRecipientMessage, GqGroup> decryptedVotes = elGamalGenerator.genRandomMessageVector(N, l);
		final GroupVector<DecryptionProof, ZqGroup> decryptionProofs = GroupVector.of(decryptionProof, decryptionProof);

		final ControlComponentShufflePayload lastPayload = new ControlComponentShufflePayload(gqGroup, electionEventId, ballotBoxId, nodeId,
				verifiableDecryptions, verifiableShuffle);

		final MixDecOfflineOutput mixDecOfflineOutput = new MixDecOfflineOutput(verifiableShuffle, decryptedVotes, decryptionProofs);
		when(mixDecOfflineAlgorithm.mixDecOffline(any(), any())).thenReturn(mixDecOfflineOutput);
		when(encryptionParametersTallyService.loadEncryptionGroup(electionEventId)).thenReturn(gqGroup);
		when(signatureKeystoreSdmTallyService.generateSignature(any(), any())).thenThrow(SignatureException.class);
		when(electionEventService.exists(electionEventId)).thenReturn(true);
		when(ballotBoxService.getBallotBoxesId(electionEventId)).thenReturn(List.of(ballotBoxId));
		when(ballotBoxService.getBallotId(ballotBoxId)).thenReturn(ballotId);

		IllegalStateException ex = assertThrows(IllegalStateException.class,
				() -> mixDecOfflineService.mixDecrypt(electionEventId, ballotId, ballotBoxId, 1, lastPayload, ELECTORAL_BOARD_PASSWORDS));

		final String expectedMesage = "Failed to generate payload signature [TallyComponentShufflePayload, ch.post.it.evoting.cryptoprimitives.hashing.HashableList";
		assertTrue(ex.getMessage().startsWith(expectedMesage));
	}

	@Test
	void mixDecryptWhenNonExistingElectionEventIdThrowsIllegalArgumentException() {
		final String nonExistingElectionEventId = "0123456789abcdef0123456789abcdef";
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
		final int N = 2;
		final int l = 5;
		final int nodeId = NODE_IDS.last();

		final VerifyMixDecHelper verifyMixDecOnlineHelper = new VerifyMixDecHelper(gqGroup);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> ciphertexts = elGamalGenerator.genRandomCiphertextVector(N, l);
		final DecryptionProof decryptionProof = new DecryptionProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(l));
		final VerifiableDecryptions verifiableDecryptions = new VerifiableDecryptions(ciphertexts, GroupVector.of(decryptionProof, decryptionProof));
		final VerifiableShuffle verifiableShuffle = new VerifiableShuffle(ciphertexts, verifyMixDecOnlineHelper.createShuffleArgument(N, l));

		final ControlComponentShufflePayload lastPayload = new ControlComponentShufflePayload(gqGroup, electionEventId, ballotBoxId, nodeId,
				verifiableDecryptions, verifiableShuffle);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> mixDecOfflineService.mixDecrypt(nonExistingElectionEventId, ballotId, ballotBoxId, 1, lastPayload,
						ELECTORAL_BOARD_PASSWORDS));

		final String expectedMesage = String.format("The given election event ID does not exist. [electionEventId: %s]", nonExistingElectionEventId);
		assertEquals(expectedMesage, exception.getMessage());
	}

	@Test
	void mixDecryptWhenNonExistingBallotBoxIdThrowsIllegalArgumentException() {
		final String nonExistingBallotBoxId = "0123456789abcdef0123456789abcdef";
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
		final int N = 2;
		final int l = 5;
		final int nodeId = NODE_IDS.last();

		final VerifyMixDecHelper verifyMixDecOnlineHelper = new VerifyMixDecHelper(gqGroup);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> ciphertexts = elGamalGenerator.genRandomCiphertextVector(N, l);
		final DecryptionProof decryptionProof = new DecryptionProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(l));
		final VerifiableDecryptions verifiableDecryptions = new VerifiableDecryptions(ciphertexts, GroupVector.of(decryptionProof, decryptionProof));
		final VerifiableShuffle verifiableShuffle = new VerifiableShuffle(ciphertexts, verifyMixDecOnlineHelper.createShuffleArgument(N, l));

		final ControlComponentShufflePayload lastPayload = new ControlComponentShufflePayload(gqGroup, electionEventId, ballotBoxId, nodeId,
				verifiableDecryptions, verifiableShuffle);
		when(electionEventService.exists(electionEventId)).thenReturn(true);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> mixDecOfflineService.mixDecrypt(electionEventId, ballotId, nonExistingBallotBoxId, 1, lastPayload,
						ELECTORAL_BOARD_PASSWORDS));

		final String expectedMesage = String.format(
				"The given ballot box ID does not belong to the given election event ID. [ballotBoxId: %s, electionEventId: %s]",
				nonExistingBallotBoxId, electionEventId);
		assertEquals(expectedMesage, exception.getMessage());
	}

	@Test
	void mixDecryptWhenNonExistingBallotIdThrowsIllegalArgumentException() {
		final String nonExistingBallotId = "0123456789abcdef0123456789abcdef";
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
		final int N = 2;
		final int l = 5;
		final int nodeId = NODE_IDS.last();

		final VerifyMixDecHelper verifyMixDecOnlineHelper = new VerifyMixDecHelper(gqGroup);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> ciphertexts = elGamalGenerator.genRandomCiphertextVector(N, l);
		final DecryptionProof decryptionProof = new DecryptionProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(l));
		final VerifiableDecryptions verifiableDecryptions = new VerifiableDecryptions(ciphertexts, GroupVector.of(decryptionProof, decryptionProof));
		final VerifiableShuffle verifiableShuffle = new VerifiableShuffle(ciphertexts, verifyMixDecOnlineHelper.createShuffleArgument(N, l));

		final ControlComponentShufflePayload lastPayload = new ControlComponentShufflePayload(gqGroup, electionEventId, ballotBoxId, nodeId,
				verifiableDecryptions, verifiableShuffle);
		when(electionEventService.exists(electionEventId)).thenReturn(true);
		when(ballotBoxService.getBallotBoxesId(electionEventId)).thenReturn(List.of(ballotBoxId));
		when(ballotBoxService.getBallotId(ballotBoxId)).thenReturn(ballotId);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> mixDecOfflineService.mixDecrypt(electionEventId, nonExistingBallotId, ballotBoxId, 1, lastPayload,
						ELECTORAL_BOARD_PASSWORDS));

		final String expectedMessage = String.format(
				"The given ballot ID does not belong to the given ballot box ID. [ballotId: %s, ballotBoxId: %s]", nonExistingBallotId, ballotBoxId);
		assertEquals(expectedMessage, exception.getMessage());
	}

	private static String genValidUUID() {
		return random.genRandomBase16String(UUID_LENGTH).toLowerCase(Locale.ROOT);
	}
}
