/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.tools.xmlsignature.keystore;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
public class KeystoreRepository {

	private final String keystoreLocation;
	private final String keystorePasswordLocation;

	public KeystoreRepository(
			@Value("${direct.trust.keystore.location}")
			final String keystoreLocation,
			@Value("${direct.trust.keystore.password.location}")
			final String keystorePasswordLocation) {
		this.keystoreLocation = keystoreLocation;
		this.keystorePasswordLocation = keystorePasswordLocation;
	}

	public InputStream getKeyStore() throws IOException {
		return Files.newInputStream(Paths.get(keystoreLocation));
	}

	public char[] getKeystorePassword() throws IOException {
		return Files.readString(Paths.get(keystorePasswordLocation)).toCharArray();
	}
}