/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.config.commands.encryptionparametersandprimes;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.EncryptionParametersPayload;

/**
 * Serializes pre-configuration data to file.
 */
@Service
public final class EncryptionParametersAndPrimesOutputSerializer {
	private static final String ENCRYPTION_PARAMETERS_PAYLOAD_FILENAME = "encryptionParametersPayload";
	private static final String JSON_EXTENSION = ".json";
	private static final String OUTPUT_FOLDER = "output";
	private final ObjectMapper mapper = DomainObjectMapper.getNewInstance();

	/**
	 * Serialize the received encryption parameters as a JWT.
	 *
	 * @param encryptionParametersPayload the encryption parameters to be serialized.
	 * @param optionalOutputPath          an optional output path. Defaults to "output".
	 * @return the path to the file containing the serialized encryption parameters.
	 * @throws IOException if an error occurrs during serialization.
	 */
	public Path serialize(final EncryptionParametersPayload encryptionParametersPayload, final Path optionalOutputPath) throws IOException {

		checkNotNull(encryptionParametersPayload);

		final Path outputPath = Objects.requireNonNullElseGet(optionalOutputPath, () -> Paths.get(OUTPUT_FOLDER));

		serializeEncryptionParameters(encryptionParametersPayload, outputPath);

		return outputPath;
	}

	private void serializeEncryptionParameters(final EncryptionParametersPayload params, final Path outputPath)
			throws IOException {

		final Path encryptionParamsPath = outputPath.resolve(ENCRYPTION_PARAMETERS_PAYLOAD_FILENAME + JSON_EXTENSION);
		final String paramsJson = mapper.writeValueAsString(params);

		Files.createDirectories(outputPath);
		Files.writeString(encryptionParamsPath, paramsJson);
	}
}
