/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.config.commands.encryptionparametersandprimes;

import java.nio.file.Path;

/**
 * Encapsulates the data.
 */
public record EncryptionParametersAndPrimesParameters(Path seedPath, Path outputPath) {
}
