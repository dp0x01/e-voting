/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.config.commands.encryptionparametersandprimes;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.SignatureException;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Service;

import com.google.common.annotations.VisibleForTesting;

import ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.EncryptionParametersPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.SignedPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystoreFactory;

/**
 * Allows the generation of the {@link EncryptionParametersPayload}.
 */
@Service
public final class EncryptionParametersAndPrimesGenerator {
	private final GetElectionEventEncryptionParametersAlgorithm getElectionEventEncryptionParametersAlgorithm;
	private int omega = VotingOptionsConstants.MAXIMUM_NUMBER_OF_VOTING_OPTIONS;

	public EncryptionParametersAndPrimesGenerator(final GetElectionEventEncryptionParametersAlgorithm getElectionEventEncryptionParametersAlgorithm) {
		this.getElectionEventEncryptionParametersAlgorithm = getElectionEventEncryptionParametersAlgorithm;
	}

	@VisibleForTesting
	void setOmega(final int value) {
		this.omega = value;
	}

	/**
	 * Generates the {@link EncryptionParametersPayload}.
	 *
	 * @param seedPath                           the path to the file containing the seed needed by the generation process.
	 * @param keystoreLocationConfigPath         the path to the file containing the keystore needed by the generation process.
	 * @param keystorePasswordLocationConfigPath the path to the file containing the keystore password needed by the generation process.
	 * @return the generated verifiable encryption parameters.
	 */
	public EncryptionParametersPayload generate(final Path seedPath, final Path keystoreLocationConfigPath,
			final Path keystorePasswordLocationConfigPath) {
		checkNotNull(seedPath);
		checkNotNull(keystoreLocationConfigPath);
		checkNotNull(keystorePasswordLocationConfigPath);

		final String seed = readSeedFromFile(seedPath);
		final GetElectionEventEncryptionParametersOutput electionEventEncryptionParameters = getElectionEventEncryptionParametersAlgorithm.getElectionEventEncryptionParameters(
				omega, seed);

		final EncryptionParametersPayload payload = new EncryptionParametersPayload(electionEventEncryptionParameters.encryptionGroup(), seed,
				electionEventEncryptionParameters.smallPrimes());

		// Signing of the payload (temporary signature, the final signature will be set by the SDM when imported)
		final CryptoPrimitivesSignature signature =
				signature(payload,
						HashableString.from("encryption parameters"),
						keystoreLocationConfigPath,
						keystorePasswordLocationConfigPath);

		payload.setSignature(signature);

		return payload;
	}

	private String readSeedFromFile(final Path seedPath) {
		final List<String> lines;
		try {
			lines = Files.readAllLines(seedPath);
		} catch (IOException e) {
			throw new UncheckedIOException(String.format("Failed to read seed located at: %s", seedPath), e);
		}

		final String seed = lines.stream().reduce("", (s1, s2) -> s1 + s2.trim());
		if (seed.isEmpty()) {
			throw new IllegalArgumentException("The seed must not be an empty string.");
		}

		return seed;
	}

	private CryptoPrimitivesSignature signature(final SignedPayload payload, final Hashable additionalContextData,
			final Path keystoreLocationConfigPath, final Path keystorePasswordLocationConfigPath) {
		try {

			final byte[] signature = getSignatureKeystore(keystoreLocationConfigPath, keystorePasswordLocationConfigPath)
					.generateSignature(payload, additionalContextData);

			return new CryptoPrimitivesSignature(signature);

		} catch (final SignatureException se) {
			throw new IllegalStateException(
					String.format("Failed to sign payload [%s, %s]", payload.getClass().getSimpleName(), additionalContextData), se);
		}
	}

	private SignatureKeystore<Alias> getSignatureKeystore(final Path keystoreLocationConfigPath, final Path keystorePasswordLocationConfigPath) {

		return SignatureKeystoreFactory.createSignatureKeystore(
				getConfigKeyStore(keystoreLocationConfigPath),
				"JKS",
				getConfigKeystorePassword(keystorePasswordLocationConfigPath),
				Objects::nonNull,
				Alias.SDM_CONFIG);
	}

	public InputStream getConfigKeyStore(final Path keystoreLocationConfigPath) {
		try {
			return Files.newInputStream(keystoreLocationConfigPath);
		} catch (final Exception ex) {
			throw new IllegalArgumentException(
					String.format("Keystore location config is invalid. [direct.trust.keystore.location.config: %s]",
							keystoreLocationConfigPath), ex);
		}
	}

	public char[] getConfigKeystorePassword(final Path keystorePasswordLocationConfigPath) {
		try {
			return Files.readString(keystorePasswordLocationConfigPath).toCharArray();
		} catch (final Exception ex) {
			throw new IllegalArgumentException(
					String.format("Keystore password location config is invalid. [direct.trust.keystore.password.location.config: %s]",
							keystorePasswordLocationConfigPath), ex);
		}
	}

}
