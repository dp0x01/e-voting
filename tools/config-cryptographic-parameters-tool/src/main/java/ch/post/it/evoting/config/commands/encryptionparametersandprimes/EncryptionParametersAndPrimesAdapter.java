/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.config.commands.encryptionparametersandprimes;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.String.format;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.config.CommandParameter;
import ch.post.it.evoting.config.Parameters;

/**
 * Provides functionality for extracting and transforming parameters for the pre-configuration process.
 */
@Service
public final class EncryptionParametersAndPrimesAdapter {

	/**
	 * Processes {@code receivedParameters} returns an adapted version of the parameters, encapsulated within a
	 * {@link EncryptionParametersAndPrimesParameters}.
	 *
	 * @param receivedParameters the parameters to adapt.
	 * @return the adapted parameters.
	 */
	public EncryptionParametersAndPrimesParameters adapt(final Parameters receivedParameters) {
		final Path seedPath = Paths.get(receivedParameters.getParam(CommandParameter.SEED_PATH.getParameterName()));

		checkArgument(checkValidFile(seedPath), format("Seed path is invalid. [path: %s]", seedPath));

		Path outputPath = null;
		if (receivedParameters.contains(CommandParameter.OUT.getParameterName())) {
			outputPath = Paths.get(receivedParameters.getParam(CommandParameter.OUT.getParameterName()));
		}

		validateReceivedParameters(CommandParameter.SEED_PATH.getParameterName(), seedPath);

		return new EncryptionParametersAndPrimesParameters(seedPath, outputPath);
	}

	private boolean checkValidFile(final Path path) {
		return Files.exists(path) && Files.isReadable(path) && Files.isRegularFile(path);
	}

	private void validateReceivedParameters(final String nameParameterToValidate, final Path parameterValue) {
		if ((parameterValue == null) || parameterValue.toString().isEmpty()) {
			throw new IllegalArgumentException(String.format("The %s must be informed.", nameParameterToValidate));
		}
	}
}
