/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.config.commands.encryptionparametersandprimes;

import static ch.post.it.evoting.config.commands.encryptionparametersandprimes.ParametersMapGenerator.getMapWithAllParameters;
import static ch.post.it.evoting.config.commands.encryptionparametersandprimes.ParametersMapGenerator.mapToParameters;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import ch.post.it.evoting.config.Application;
import ch.post.it.evoting.config.CommandParameter;
import ch.post.it.evoting.config.Config;
import ch.post.it.evoting.config.Parameters;
import ch.post.it.evoting.config.integrationtests.keystore.TestKeyStoreInitializer;
import ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants;

@SpringJUnitConfig(classes = Application.class)
@ContextConfiguration(classes = Config.class, loader = AnnotationConfigContextLoader.class, initializers = TestKeyStoreInitializer.class)
class EncryptionParametersAndPrimesCommandProcessorTest {

	@Autowired
	EncryptionParametersAndPrimesCommandProcessor processor;

	@TempDir
	Path tempDir;

	@ParameterizedTest
	@CsvSource({ "seed.txt" })
	void providedValidSeedParameters_encryptionParametersPayloadJsonFileCreated(final ArgumentsAccessor argumentsAccessor) throws IOException {
		// given
		final Path outputPath = tempDir.resolve("output");
		final Map<String, String> parametersMap = getParametersMap(argumentsAccessor.getString(0), outputPath);

		Files.createDirectory(outputPath);

		final Parameters parameters = mapToParameters(parametersMap);

		processor.getConfigEncryptionParametersGenerator().setOmega(VotingOptionsConstants.MAXIMUM_NUMBER_OF_VOTING_OPTIONS);

		// when
		processor.accept(parameters);

		// then
		final String paramsJson = Files.readString(Path.of(outputPath + "/encryptionParametersPayload.json"));

		Assertions.assertAll(
				() -> assertThat(Files.list(outputPath)).hasSize(1),
				() -> assertThat(paramsJson).isNotEmpty()
						.contains("\"encryptionGroup\":")
						.contains("\"seed\":")
						.contains("\"smallPrimes\":")
						.contains("\"signature\":")
		);
	}

	@ParameterizedTest
	@CsvSource({ "non_existing_file.txt", "non_existing_path/seed.txt", "just_a_path" })
	void providedInvalidSeedPaths_exceptionIsThrown(final ArgumentsAccessor argumentsAccessor) throws IOException {
		// given
		final Path outputPath = tempDir.resolve("output");
		final Map<String, String> parametersMap = getParametersMap(argumentsAccessor.getString(0), outputPath);

		Files.createDirectory(outputPath);

		final Parameters parameters = mapToParameters(parametersMap);

		// when / then
		assertThatThrownBy(() -> processor.accept(parameters)).isInstanceOf(IllegalArgumentException.class);
	}

	@ParameterizedTest
	@CsvSource({ "seed_empty.txt" })
	void providedInvalidSeeds_exceptionIsThrown(final ArgumentsAccessor argumentsAccessor) throws IOException {
		// given
		final Path outputPath = tempDir.resolve("output");
		final Map<String, String> parametersMap = getParametersMap(argumentsAccessor.getString(0), outputPath);

		Files.createDirectory(outputPath);

		final Parameters parameters = mapToParameters(parametersMap);

		// when / then
		assertThatThrownBy(() -> processor.accept(parameters)).isInstanceOf(IllegalArgumentException.class);

	}

	@ParameterizedTest
	@CsvSource({ "seed.txt", "seed_with_leading_whitespace.txt", "seed_with_trailing_whitespace.txt",
			"seed_with_leading_crlf.txt", "seed_with_trailing_crlf.txt" })
	void providedValidSeeds_noExceptionIsThrown(final ArgumentsAccessor argumentsAccessor) throws IOException {
		// given
		final Path outputPath = tempDir.resolve("output");
		final Map<String, String> parametersMap = getParametersMap(argumentsAccessor.getString(0), outputPath);

		Files.createDirectory(outputPath);

		final Parameters parameters = mapToParameters(parametersMap);

		// when / then
		Assertions.assertDoesNotThrow(() -> processor.accept(parameters));

	}

	@ParameterizedTest
	@ValueSource(strings = { "encryptionParametersPayload.json" })
	void provideOutputDirWhichAlreadyExist_noExceptionIsThrown(final String fileName) throws IOException {
		// given
		final Path outputPath = tempDir.resolve("output");
		final Map<String, String> parametersMap = getParametersMap("seed.txt", outputPath);

		Files.createDirectory(outputPath);
		Files.createFile(outputPath.resolve(fileName));

		final Parameters parameters = mapToParameters(parametersMap);

		// when / then
		Assertions.assertDoesNotThrow(() -> processor.accept(parameters));
	}

	private Map<String, String> getParametersMap(final String seed, final Path outputPath) {
		final Path seedPath = ParametersMapGenerator.getPath(seed);

		final Map<String, String> parametersMap = getMapWithAllParameters();
		parametersMap.put(CommandParameter.SEED_PATH.getParameterName(), seedPath.toString());
		parametersMap.put(CommandParameter.OUT.getParameterName(), outputPath.toString());
		return parametersMap;
	}

}