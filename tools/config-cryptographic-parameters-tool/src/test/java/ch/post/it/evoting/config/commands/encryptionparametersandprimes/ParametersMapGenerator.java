/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.config.commands.encryptionparametersandprimes;

import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import ch.post.it.evoting.config.CommandParameter;
import ch.post.it.evoting.config.Parameters;

public final class ParametersMapGenerator {

	private ParametersMapGenerator() {
		// utility class
	}

	public static final String SEED_PATH_VALUE = getPath("seed.txt").toString();
	public static final String OUT_VALUE = "out_value";

	public static Map<String, String> getMapWithAllParameters() {
		final Map<String, String> parameters = getMapWithMandatoryParameters();
		parameters.put(CommandParameter.OUT.getParameterName(), OUT_VALUE);
		return parameters;
	}

	public static Map<String, String> getMapWithMandatoryParameters() {
		final Map<String, String> parameters = new HashMap<>();
		parameters.put(CommandParameter.SEED_PATH.getParameterName(), SEED_PATH_VALUE);
		return parameters;
	}

	public static Parameters mapToParameters(Map<String, String> map) {
		final Parameters parameters = new Parameters();
		map.forEach(parameters::addParam);
		return parameters;
	}

	static Path getPath(final String name) {
		final ClassLoader classLoader = EncryptionParametersAndPrimesCommandProcessorTest.class.getClassLoader();

		final URL baseURL = classLoader.getResource(".");
		final String filePath = Objects.requireNonNull(baseURL).getPath() + name;

		return new File(filePath).toPath();
	}

}
