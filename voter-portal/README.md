# Voter Portal

The voter-portal provides a front-end for the voter and covers the entire voting process:

- confirming the legal conditions,
- entering the start voting key,
- selecting voting options,
- confirming the selection of voting options,
- displaying the Choice Return Codes for every selected voting option,
- entering the Ballot Casting Key,
- displaying the Vote Cast Return Code,
- providing a help menu to the voter.

Moreover, the voter-portal supports four languages (German, French, Italian, Rumantsch) and
authentication factors in addition to the Start Voting Key (date of birth, year of birth).

The voter-portal supports multiple electoral models:

- Referendums / Initiatives (allowing the voter to answer yes, no, or empty for specific questions)
- Elections with lists and candidates, with the possibility of accumulating candidates, list combinations,
  and candidates appearing on multiple lists
- Elections without lists

The voter-portal builds upon the voting-client-js, which implements the voting client's
cryptographic algorithms.

## Usage

The voter portal is packaged as an Angular application.

In our productive infrastructure, we deploy the voter portal behind a reverse proxy, which makes sure that the
following [HTTP Response Headers](https://owasp.org/www-project-secure-headers) are set:

| HTTP Response Header         | Value                                                                                                                                                                                                                                                              |
|------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Cache-Control                | no-cache; no-store; must-revalidate;max-age=0                                                                                                                                                                                                                      |
| Content-Disposition          | Inline                                                                                                                                                                                                                                                             |
| Content-Security-Policy      | default-src 'none'; script-src 'self' 'unsafe-inline' 'unsafe-eval'; style-src 'self' 'unsafe-inline'; img-src 'self' data:; connect-src 'self'; worker-src 'self'; frame-src 'none'; font-src 'self'; base-uri 'self'; frame-ancestors 'none'; form-action 'none' |
| Content-Type                 | Always add charset=utf-8 for HTML, JSON, Javascript and CSS resources                                                                                                                                                                                              |
| Cross-Origin-Embedder-Policy | require-corp                                                                                                                                                                                                                                                       |
| Cross-Origin-Opener-Policy   | same-origin                                                                                                                                                                                                                                                        |
| Cross-Origin-Resource-Policy | same-origin                                                                                                                                                                                                                                                        |
| Permission-policy            | vibrate=(), microphone=(), geolocation=(), camera=(), display-capture=()                                                                                                                                                                                           |
| Pragma                       | no-cache                                                                                                                                                                                                                                                           |
| Referrer-Policy              | no-referrer                                                                                                                                                                                                                                                        |
| Strict-Transport-Security    | max-age=63072000; includeSubDomains; preload                                                                                                                                                                                                                       |
| X-Content-Type-Options       | nosniff                                                                                                                                                                                                                                                            |
| X-Frame-Options              | DENY                                                                                                                                                                                                                                                               |

Our content security policy (CSP) differs from a more strict policy in three parameters: we allow unsafe-eval and unsafe-inline for script-src, as well as unsafe-inline for style-src. 
While CSP is designed to disable certain features, we need to integrate a customizable voter portal and load WebAssembly code, which depend on these features.
It's important to note that for the Swiss Post Voting System, the CSP headers serve as a defense-in-depth mechanism. However, verifiability and privacy are not reliant on them.

Moreover, our productive infrastructure enforces best practices in server configuration such as [DNSSEC](https://www.nic.ch/security/dnssec/)
, [OCSP](https://www.ietf.org/rfc/rfc2560.txt), and [CAA records](https://support.dnsimple.com/articles/caa-record/).

## Configuration

The Voter Portal needs some configuration keys to bootstrap properly. These configuration values are set
in `webapps/ROOT/assets/configuration/portalConfig.json`.

### Header customization

The Voter Portal header can be customized to display for example a specific logo. Here are the customizable keys in the `header` node.

| Header node key       | Data type                           | Description                                                                                                                 |
|-----------------------|-------------------------------------|-----------------------------------------------------------------------------------------------------------------------------|
| logoPath              | `{desktop:string, mobile: string}`  | Path to the logo displayed in the header. Each path is relative from `assets/configuration/img/`.                           |
| logoHeight            | `{desktop:number, mobile: number}`  | Height of the logo in pixels.                                                                                               |
| reverse (optional)    | `boolean`                           | If `true` the header is reversed, the logo appears on the right. Default is `false`.                                        |
| background (optional) | `string`                            | CSS background definition (i.e. `linear-gradient(180deg,#f7f7f7 0,#ebebeb)`). Default is `none`.                            |
| bars (optional)       | `bar[]`                             | A `bar` represents a line displayed below the navigation bar. It has a `height` and a `color` properties. Default is empty. |
| bar.height            | `{desktop:number, mobile: number}`  | Height of the bar in pixels.                                                                                                |
| bar.color             | `string`                            | Hexadecimal color value (i.e. `#000000`)                                                                                    |

```json5
{
  "header": {
    "logoPath": {
      "desktop": "post-logo.svg",
      "mobile": "post-logo.svg"
    },
    "logoHeight": {
      "desktop": 72,
      "mobile": 56
    },
    "reverse": false,
    "background": "#fff",
    "bars": [
      {
        "height": {
          "desktop": 2,
          "mobile": 2
        },
        "color": "#000000"
      }
    ]
  }
}
```

## Development

Check the build instructions in the readme of the repository 'evoting' for compiling the voter-portal.

The file `package.json` contains the script section for building and test the Voter Portal.
