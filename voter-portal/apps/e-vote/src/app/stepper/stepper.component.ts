/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Route, Router } from '@angular/router';
import { concatLatestFrom } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import { Nullable } from '@swiss-post/types';
import { Subject } from 'rxjs';
import { filter, map, switchMap, takeUntil } from 'rxjs/operators';
import { routes } from '../app-routing.module';

@Component({
  selector: 'swp-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: [ './stepper.component.scss' ],
})
export class StepperComponent implements OnInit, OnDestroy {
  steps: string[] = [];
  currentStep: string | undefined;
  currentStepIndex: number | undefined;
  destroy$ = new Subject<void>();

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly translate: TranslateService,
    private readonly titleService: Title
  ) {
    this.steps = routes.reduce((steps, route) => {
      const step = this.getStepFrom(route);
      return step ? [...steps, step] : steps;
    }, [] as string[]);
  }

  ngOnInit() {
    const currentStepChanges$ = this.router.events.pipe(
      filter(routerEvent => routerEvent instanceof NavigationEnd),
      map(() => this.activatedRoute.snapshot.firstChild?.routeConfig),
      map(currentStep => this.getStepFrom(currentStep)),
    );

    currentStepChanges$.pipe(
      takeUntil(this.destroy$)
    ).subscribe(currentStep => {
      this.currentStep = currentStep;
      this.currentStepIndex = this.steps.findIndex(step => step === currentStep);
      this.setFirstFocus();
    });

    currentStepChanges$.pipe(
      filter((currentStep): currentStep is string => !!currentStep),
      switchMap(currentStep => this.translate.stream(currentStep)),
      concatLatestFrom(() => this.translate.get('common.pageTitle')),
      takeUntil(this.destroy$),
    ).subscribe(([stepName, pageTitle]) => {
      this.titleService.setTitle(pageTitle + (stepName ? ` - ${stepName}` : ''));
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getStepFrom(route: Nullable<Route>): string | undefined {
    return route?.data?.stepKey;
  }

  private setFirstFocus(): void {
    window.requestAnimationFrame(() => {
      const stepper = document.querySelector<HTMLElement>('#voting-progress-stepper [aria-current="step"]');

      if (stepper) {
        stepper.focus();
      }
    });
  }
}
