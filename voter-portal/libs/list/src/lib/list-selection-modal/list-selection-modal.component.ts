/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ElectionContest, CandidateList} from '@swiss-post/types';
import {Subscription} from 'rxjs';

@Component({
  selector: 'swp-list-selection-modal',
  templateUrl: './list-selection-modal.component.html',
  styleUrls: ['./list-selection-modal.component.scss'],
})
export class ListSelectionModalComponent implements OnInit, OnDestroy {
  @Input() electionContest!: ElectionContest;
  searchTerm: FormControl;
  searchTermSubscription!: Subscription;
  lists!: CandidateList[];
  filteredLists!: CandidateList[];

  get searchResultsArgs(): object {
    return {
      resultCount: this.filteredLists.length,
      listCount: this.lists.length,
      searchTerm: this.searchTerm.value
    };
  }

  constructor(
    private readonly fb: FormBuilder,
    public readonly activeModal: NgbActiveModal
  ) {
    this.searchTerm = this.fb.control('');
  }

  ngOnInit() {
    this.filteredLists = this.lists = this.getLists();
    this.searchTermSubscription = this.searchTerm.valueChanges
      .subscribe(searchTerm => {
        this.filterLists(searchTerm);
      });
  }

  ngOnDestroy() {
    this.searchTermSubscription.unsubscribe();
  }

  filterLists(searchTerm: string) {
    this.filteredLists = this.lists.filter(list => {
      return list.details?.listType_attribute1.toLowerCase().includes(searchTerm.toLowerCase());
    })
  }

  private getLists() {
    if (!this.electionContest.lists) {
      return [];
    }
    return this.electionContest.lists?.filter(list => {
      return !list.isBlank && !list.isWriteIn;
    });
  }
}
