/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {Candidate} from '@swiss-post/types';

@Component({
  selector: 'swp-candidate-details',
  templateUrl: './candidate-details.component.html',
  styleUrls: ['./candidate-details.component.scss'],
})
export class CandidateDetailsComponent {
  @Input() candidate: Candidate | null | undefined;
  @Input() candidateWriteIn: string | null | undefined;
  @Input() candidatePlaceholder: string | null | undefined;
  @Input() headingLevel: number | null | undefined;
  @Input() headingDisplayLevel: number | null | undefined;
  @Input() screenReaderLabel: string | null | undefined;
  @Input() showAllDetails = false;

  get headingClass(): string {
    const headingLevel = this.headingDisplayLevel ?? this.headingLevel;
    return headingLevel ? `h${headingLevel}` : '';
  }
}
