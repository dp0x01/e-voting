/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormControl, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {getDefinedWriteInAlphabet} from '@swiss-post/shared/state';
import {FAQService} from '@swiss-post/shared/ui';
import {FAQSection} from '@swiss-post/types';
import {Subscription} from 'rxjs';

@Component({
  selector: 'swp-candidate-write-in',
  templateUrl: './candidate-write-in.component.html',
  styleUrls: ['./candidate-write-in.component.scss'],
})
export class CandidateWriteInComponent implements OnInit, OnDestroy {
  @ViewChild('writeIn', {read: ElementRef}) writeInInput: ElementRef<HTMLInputElement> | undefined;
  @Input() writeInControl!: FormControl;
  @Input() position!: number;
  @Input() showErrors = false;
  initialWriteIn = '';
  writeInMaxLength = 95;
  private writeInAlphabet: RegExp | undefined;
  private alphabetSubscription!: Subscription;

  constructor(
    private readonly faqService: FAQService,
    private readonly store: Store,
  ) {
  }

  private get writeInValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      // Control value length is checked to prevent regex DoS attack
      const writeInFormat = /^.+\s.+$/;
      if (!control.value || control.value.length > this.writeInMaxLength || !writeInFormat.test(control.value)) {
        return {incorrectFormat: true};
      }

      if (this.writeInAlphabet && !this.writeInAlphabet.test(control.value)) {
        return {incorrectCharacters: true};
      }

      return null;
    }
  }

  ngOnInit() {
    this.alphabetSubscription = this.store.pipe(
      getDefinedWriteInAlphabet
    ).subscribe(alphabet => {
      this.writeInAlphabet = new RegExp(`^[${alphabet.substring(1)}]+$`);
    });

    this.initialWriteIn = this.writeInControl.value;

    this.writeInControl.setValidators([this.writeInValidator, Validators.maxLength(this.writeInMaxLength)]);
    setTimeout(() => {
      this.writeInControl.updateValueAndValidity();
      this.writeInInput?.nativeElement.focus();
    });
  }

  ngOnDestroy() {
    this.alphabetSubscription.unsubscribe();

    this.writeInControl.clearValidators();
    setTimeout(() => this.writeInControl.updateValueAndValidity());
  }

  setWriteIn(writeIn: string) {
    this.writeInControl.setValue(writeIn.trim());
  }

  showWriteInsFAQ() {
    this.faqService.showFAQ(FAQSection.HowToUseWriteIns)
  }

  preventFormSubmission(): false {
    return false;
  }
}
