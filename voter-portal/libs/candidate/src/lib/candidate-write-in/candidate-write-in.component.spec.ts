/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormsModule } from '@angular/forms';
import { provideMockStore } from '@ngrx/store/testing';
import { FAQService } from '@swiss-post/shared/ui';
import { MockProvider } from 'ng-mocks';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { CandidateWriteInComponent } from './candidate-write-in.component';

describe('CandidateWriteInComponent', () => {
  let component: CandidateWriteInComponent;
  let fixture: ComponentFixture<CandidateWriteInComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CandidateWriteInComponent],
      providers: [
        provideMockStore({}),
        MockProvider(FAQService)
      ],
      imports: [
        TranslateTestingModule.withTranslations({}),
        FormsModule
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateWriteInComponent);
    component = fixture.componentInstance;
    component.writeInControl = new FormControl();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});