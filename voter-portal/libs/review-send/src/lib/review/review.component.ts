/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Store } from '@ngrx/store';
import { getContestsAndContestsUserData, getLoading, ReviewActions } from '@swiss-post/shared/state';
import { ConfirmationService, ProcessCancellationService } from '@swiss-post/shared/ui';
import { ConfirmationModalConfig, ContestAndContestUserData } from '@swiss-post/types';
import { Observable, partition } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { SendVoteModalComponent } from '../send-vote-modal/send-vote-modal.component';

@Component({
  selector: 'swp-review',
  templateUrl: './review.component.html',
  styleUrls: [ './review.component.scss' ],
})
export class ReviewComponent {
  contestsAndValues$: Observable<ContestAndContestUserData[] | undefined>;
  isLoading$: Observable<boolean> = this.store.select(getLoading);

  constructor(
    private readonly store: Store,
    private readonly modalService: NgbModal,
    private readonly cancelProcessService: ProcessCancellationService,
    private readonly confirmationService: ConfirmationService,
  ) {
    this.contestsAndValues$ = this.store
      .select(getContestsAndContestsUserData)
      .pipe(
        map(({contests, contestsUserData}) =>
          contests?.map((contest, i) => {
            const contestUserData = contestsUserData
              ? contestsUserData[i]
              : {questions: []};
            return {contest, contestUserData};
          })
        )
      );
  }

  confirmCancel() {
    this.cancelProcessService.cancelVote();
  }

  confirmSeal(): void {
    const sealingModalConfig: ConfirmationModalConfig = {
      title: 'review.confirm.title',
      content: [ 'review.confirm.questionsealvote', 'review.confirm.hintconfirm' ],
      confirmLabel: 'review.confirm.yes',
      cancelLabel: 'review.confirm.no',
      modalOptions: { size: 'lg' },
    };

    const [sealingConfirmed$, sealingRejected$] = partition(
      this.confirmationService.confirm(sealingModalConfig),
      (wasSealingConfirmed) => wasSealingConfirmed
    );

    sealingConfirmed$.subscribe(() => this.seal());
    sealingRejected$.subscribe(() => this.store.dispatch(ReviewActions.sealVoteCanceled()));
  }

  private seal() {
    const modalRef = this.modalService.open(SendVoteModalComponent, {
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
    });

    modalRef.shown
      .pipe(
        take(1),
      )
      .subscribe(() => {
        this.store.dispatch(ReviewActions.sealVoteClicked());
      });
  }
}
