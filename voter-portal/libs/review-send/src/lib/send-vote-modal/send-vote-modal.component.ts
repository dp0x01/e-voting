/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Store } from '@ngrx/store';
import { getDefinedBackendError, getDefinedChoiceReturnCodes } from '@swiss-post/shared/state';
import { merge } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'swp-progress',
  templateUrl: './send-vote-modal.component.html',
  styleUrls: [ './send-vote-modal.component.scss' ],
})
export class SendVoteModalComponent implements OnInit {
  constructor(
    public readonly store: Store,
    public readonly activeModal: NgbActiveModal,
  ) {
  }

  ngOnInit() {
    const choiceReturnCodes$ = this.store.pipe(getDefinedChoiceReturnCodes);
    const backendError$ = this.store.pipe(getDefinedBackendError);

    merge(choiceReturnCodes$, backendError$)
      .pipe(take(1))
      .subscribe(() => {
        this.activeModal.close();
      });
  }
}
