/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { CandidateModule } from '@swiss-post/candidate';
import { ListModule } from '@swiss-post/list';
import { QuestionsModule } from '@swiss-post/questions';
import { SharedIconsModule } from '@swiss-post/shared/icons';
import { SharedUiModule } from '@swiss-post/shared/ui';
import { ReviewCandidateListComponent } from './review-candidate-list/review-candidate-list.component';
import { ReviewContestContainerComponent } from './review-contest-container/review-contest-container.component';
import { ReviewQuestionsComponent } from './review-questions/review-questions.component';
import { ReviewSendRoutingModule } from './review-send-routing.module';
import { ReviewComponent } from './review/review.component';
import { SendVoteModalComponent } from './send-vote-modal/send-vote-modal.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    ReviewSendRoutingModule,
    QuestionsModule,
    CandidateModule,
    ListModule,
    SharedUiModule,
    SharedIconsModule,
  ],
  declarations: [
    ReviewComponent,
    ReviewContestContainerComponent,
    ReviewQuestionsComponent,
    ReviewCandidateListComponent,
    SendVoteModalComponent,
  ],
})
export class ReviewSendModule {
}
