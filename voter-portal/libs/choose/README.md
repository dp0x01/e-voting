# choose

This library was generated with [Nx](https://nx.dev).

`Choose` `FeatureModule`: state is managed with `ngrx` by `SharedStateModule`;

`ChooseComponent` selects `Contests` from `Ballot` in `ngrx` `store` and displays the list

## Running unit tests

Run `nx test choose` to execute the unit tests.
