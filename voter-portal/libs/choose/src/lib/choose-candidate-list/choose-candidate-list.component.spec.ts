/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormArray, FormGroup} from '@angular/forms';
import {provideMockStore} from '@ngrx/store/testing';
import {CandidatesComponent} from '@swiss-post/candidate';
import {ElectionContest} from '@swiss-post/types';
import {MockComponent} from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {ChooseListComponent} from '../choose-list/choose-list.component';

import {ChooseCandidateListComponent} from './choose-candidate-list.component';

describe('ChooseCandidateListComponent', () => {
  let component: ChooseCandidateListComponent;
  let fixture: ComponentFixture<ChooseCandidateListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ChooseCandidateListComponent,
        MockComponent(ChooseListComponent),
        MockComponent(CandidatesComponent),
      ],
      providers: [
        provideMockStore({}),
      ],
      imports: [
        TranslateTestingModule.withTranslations({}),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseCandidateListComponent);
    component = fixture.componentInstance;

    component.electionContest = {
      id: 'contestId',
      template: 'contestTemplate',
    } as ElectionContest;

    component.contestFormGroup = new FormGroup({
      candidates: new FormArray([]),
    });

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
