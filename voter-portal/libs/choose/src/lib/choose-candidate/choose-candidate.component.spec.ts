/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormArray, FormGroup} from '@angular/forms';
import {provideMockStore} from '@ngrx/store/testing';
import {CandidatesComponent} from '@swiss-post/candidate';
import { SharedUiModule } from '@swiss-post/shared/ui';
import {ElectionContest} from '@swiss-post/types';
import { MockComponent, MockDirective, MockModule } from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {ChooseListComponent} from '../choose-list/choose-list.component';

import {ChooseCandidateComponent} from './choose-candidate.component';

describe('ChooseCandidateListComponent', () => {
  let component: ChooseCandidateComponent;
  let fixture: ComponentFixture<ChooseCandidateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ChooseCandidateComponent,
        MockComponent(ChooseListComponent),
        MockComponent(CandidatesComponent),
      ],
      providers: [
        provideMockStore({}),
      ],
      imports: [
        MockModule(SharedUiModule),
        TranslateTestingModule.withTranslations({}),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseCandidateComponent);
    component = fixture.componentInstance;

    component.electionContest = {
      id: 'contestId',
      template: 'contestTemplate',
    } as ElectionContest;

    component.contestFormGroup = new FormGroup({
      candidates: new FormArray([]),
    });

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
