/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { ConfigurationService } from '@swiss-post/shared/configuration';
import { ExtendedFactor, VoterPortalConfig } from '@swiss-post/types';
import { MockProvider } from 'ng-mocks';
import { TranslateTestingModule } from 'ngx-translate-testing';

import { LegalTermsComponent } from './legal-terms.component';

describe('LegalTermsComponent', () => {
	let component: LegalTermsComponent;
	let fixture: ComponentFixture<LegalTermsComponent>;
	let voterPortalConfig: VoterPortalConfig;

	beforeEach(async () => {
		voterPortalConfig = {
			identification: ExtendedFactor.YearOfBirth,
			contestsCapabilities: {
				writeIns: true,
			},
			header: {
				logoPath: { desktop: '', mobile: '' },
				logoHeight: { desktop: 0, mobile: 0 },
			},
		};

		await TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				HttpClientTestingModule,
				TranslateTestingModule.withTranslations({}),
			],
			declarations: [LegalTermsComponent],
			providers: [
				provideMockStore({}),
				MockProvider(ConfigurationService, voterPortalConfig),
			],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(LegalTermsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
