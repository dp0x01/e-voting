/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';

import {LegalTermsRoutingModule} from './legal-terms-routing.module';
import {LegalTermsComponent} from './legal-terms/legal-terms.component';

@NgModule({
  imports: [CommonModule, LegalTermsRoutingModule, TranslateModule],
  declarations: [LegalTermsComponent],
  exports: [LegalTermsComponent],
})
export class LegalTermsModule {
}
