/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Component, HostBinding, Input } from '@angular/core';
import bootstrapIcons from 'bootstrap-icons/font/bootstrap-icons.json';
import votingCardIcons from './voting-card-icons';

type BootstrapIconName = keyof typeof bootstrapIcons;
type VotingCardIconName = keyof typeof votingCardIcons;

@Component({
  selector: 'swp-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
})
export class IconComponent {
  @Input() name!: BootstrapIconName | VotingCardIconName;

  @HostBinding('attr.aria-label') @Input() label: string | undefined;

  @HostBinding('attr.role') role = 'img';

  @HostBinding('attr.aria-hidden') get hidden() {
    return String(!this.label);
  }

  get isBootstrapIcon(): boolean {
    return Object.keys(bootstrapIcons).includes(this.name);
  }

  get isVotingCardIcon(): boolean {
    return Object.keys(votingCardIcons).includes(this.name);
  }

  get votingCardIconPath(): string {
    return votingCardIcons[this.name as VotingCardIconName];
  }
}
