# shared-ui

This library was generated with [Nx](https://nx.dev).

It is going to store reusable components.

## Running unit tests

Run `nx test shared-ui` to execute the unit tests.
