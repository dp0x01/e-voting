/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Pipe, PipeTransform } from '@angular/core';
import { environment } from '@swiss-post/backend';
import createDOMPurify from 'dompurify';
import { marked, Renderer } from 'marked';

@Pipe({
  name: 'markdown',
})
export class MarkdownPipe implements PipeTransform {
  constructor() {
    marked.setOptions({ renderer: new RestrictedRenderer() });
  }

  transform(value: string | undefined): string {
    if (!value || !window) {
      return '';
    }

    const DOMPurify = createDOMPurify(window);
    return DOMPurify.sanitize(marked.parseInline(value));
  }
}

class RestrictedRenderer extends Renderer {
  constructor() {
    super();

    environment.markdownDenylist.forEach((deniedToken) => {
      Object.assign(this, {[deniedToken]: (text: string) => super.text(text)})
    })
  }
}
