/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { TranslateParser, TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from '@swiss-post/shared/configuration';
import { TranslatePlaceholders } from '@swiss-post/types';

@Pipe({
  name: 'replacePlaceholders',
  pure: false,
})
export class ReplacePlaceholdersPipe implements PipeTransform {
  query: string | undefined;
  value: string | undefined;
  placeholders: TranslatePlaceholders | undefined;
  lang: string | undefined;

  constructor(
    private parser: TranslateParser,
    private translate: TranslateService,
    private configuration: ConfigurationService,
  ) {
    this.placeholders = configuration.translatePlaceholders;
  }

  public transform(query: string): string {
    if (!this.placeholders) {
      return query;
    }

    if (this.lang !== this.translate.currentLang) {
      this.lang = this.translate.currentLang;
    } else if (query === this.query) {
      return this.value ?? query;
    }

    const currentPlaceholders = this.placeholders[this.lang as keyof TranslatePlaceholders];
    this.query = query;
    this.value = this.parser.interpolate(query, currentPlaceholders) ?? query;
    return this.value;
  }
}
