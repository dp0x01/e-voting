/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { merge } from 'rxjs';
import { filter, take, tap } from 'rxjs/operators';

function translationExists(translationObject: object): boolean {
  return Object.entries(translationObject).every(([ translationKey, translation ]) => {
    return !!translation && translation !== translationKey;
  });
}

@Directive({
  selector: '[swpParagraphs]',
})
export class ParagraphsDirective {
  translationKey: string | undefined;

  constructor(
    private readonly templateRef: TemplateRef<{ paragraphKey: string }>,
    private readonly viewContainer: ViewContainerRef,
    private readonly translate: TranslateService,
  ) {
  }

  @Input() set swpParagraphs(translationKey: string | undefined) {
    if (!translationKey) {
      return;
    }

    this.translationKey = translationKey;

    const singleParagraph$ = this.translate.get([ translationKey ]).pipe(
      filter(translationExists),
      tap(() => this.showSingleParagraph()),
    );

    const multipleParagraphs$ = this.translate.get([ `${translationKey}.p1` ]).pipe(
      filter(translationExists),
      tap(() => this.showMultipleParagraphs()),
    );

    merge(singleParagraph$, multipleParagraphs$).pipe(take(1)).subscribe();
  }

  private showSingleParagraph(): void {
    this.viewContainer.createEmbeddedView(this.templateRef, { paragraphKey: this.translationKey });
  }

  private showMultipleParagraphs(): void {
    let position = 1;
    let paragraphKey = this.getParagraphKey(position);

    while (paragraphKey && this.translationExistsForKey(paragraphKey)) {
      this.viewContainer.createEmbeddedView(this.templateRef, { paragraphKey });

      position++;
      paragraphKey = this.getParagraphKey(position);
    }
  }

  private translationExistsForKey(key: string): boolean {
    const translation = this.translate.instant([ key ]);
    return translationExists(translation);
  }

  private getParagraphKey(position: number): string {
    return `${this.translationKey}.p${position}`;
  }
}
