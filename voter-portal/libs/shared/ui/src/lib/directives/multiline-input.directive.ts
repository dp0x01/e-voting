/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { AfterViewInit, Directive, ElementRef, OnDestroy, OnInit } from '@angular/core';

@Directive({
  selector: 'textarea[multilineInput]',
})
export class MultilineInputDirective implements OnInit, AfterViewInit, OnDestroy {
  observer = new ResizeObserver(() => this.updateInputHeight());

  constructor(
    private readonly el: ElementRef<HTMLTextAreaElement>
  ) {
    this.el.nativeElement.style.height = '0';
    this.el.nativeElement.style.resize = 'none';
    this.el.nativeElement.style.overflow = 'hidden';
  }

  public ngOnInit(): void {
    this.observer.observe(this.el.nativeElement);
  }

  public ngAfterViewInit(): void {
    this.updateInputHeight();
  }

  public ngOnDestroy(): void {
    this.observer.disconnect();
  }

  updateInputHeight(): void {
    const { value, placeholder } = this.el.nativeElement;

    // if there is no value, use the placeholder
    if (value === '' && placeholder) {
      this.el.nativeElement.value = placeholder;
    }

    this.el.nativeElement.style.height = `${this.el.nativeElement.scrollHeight}px`;

    // reset initial value
    if (value === '' && placeholder) {
      this.el.nativeElement.value = value;
    }
  }
}
