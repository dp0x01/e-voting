/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Component, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Store } from '@ngrx/store';
import { getElectionEventId } from '@swiss-post/shared/state';
import { ProcessCancellationService } from '@swiss-post/shared/ui';
import { BackAction, RouteData } from '@swiss-post/types';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DeactivationGuard implements CanDeactivate<Component> {
  constructor(
    private readonly cancelProcessService: ProcessCancellationService,
    private readonly router: Router,
    private readonly store: Store,
    private readonly modalService: NgbModal,
  ) {
  }

  canDeactivate(
    component: Component,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot | undefined
  ): boolean | UrlTree | Observable<UrlTree> {
    if (!this.cancelProcessService.backButtonPressed) {
      return true;
    }

    this.cancelProcessService.backButtonPressed = false;

    if (this.modalService.hasOpenModals()) {
      history.pushState(null, '', location.href);
      return false;
    }

    const routeData = currentRoute.data as RouteData;
    if (routeData?.allowedBackPaths) {
      const isBackPathAllowed = routeData.allowedBackPaths.some(path => {
        return nextState?.url && nextState.url.indexOf(path) >= 0;
      });

      if (isBackPathAllowed) {
        return true;
      }
    }

    switch (routeData?.backAction) {
      case BackAction.ShowCancelVoteDialog:
        this.cancelProcessService.cancelVote();
        return false;
      case BackAction.ShowLeaveProcessDialog:
        this.cancelProcessService.leaveProcess();
        return false;
      case BackAction.GoToStartVotingPage:
        return this.router.createUrlTree(['start-voting']);
      case BackAction.GoToLegalTermsPage:
        return this.store.select(getElectionEventId).pipe(
          map((electionEventId) => {
            return this.router.createUrlTree(
              electionEventId ? [ 'legal-terms', electionEventId ] : [ '' ],
            );
          })
        );
      default:
        history.pushState(null, '', location.href);
        return false;
    }
  }
}
