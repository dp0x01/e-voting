/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { AfterViewInit, Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfigurationService } from '@swiss-post/shared/configuration';
import { ExtendedFactor, FAQSection } from '@swiss-post/types';

@Component({
  selector: 'swp-faq',
  templateUrl: './faq-modal.component.html',
  styleUrls: [ './faq-modal.component.scss' ],
})
export class FAQModalComponent implements AfterViewInit {
  @Input() activeFAQSection: FAQSection | undefined;

  readonly FAQSection = FAQSection;
  readonly ExtendedFactor = ExtendedFactor;
  readonly allowedCharacters = 'ÀÁÂÃÄÅ àáâãäå ÈÉÊË èéêë ÌÍÎÏ ìíîï ÒÓÔÕÖ òóôõö ÙÚÛÜ ùúûü Ææ Çç Œœ Þþ Ññ Øø Šš Ýý Ÿÿ Žž ¢ () ð Ð ß \' , - . /';

  public constructor(
    public readonly configuration: ConfigurationService,
    public readonly activeModal: NgbActiveModal,
  ) {}

  ngAfterViewInit() {
    if (this.activeFAQSection) {
      const activeSectionButton = document.querySelector<HTMLButtonElement>(`button[aria-controls=${this.activeFAQSection}]`);
      activeSectionButton?.focus();
    }
  }
}
