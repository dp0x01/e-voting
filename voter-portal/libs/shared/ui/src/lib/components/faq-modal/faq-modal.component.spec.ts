/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NgbAccordionModule, NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfigurationService } from '@swiss-post/shared/configuration';
import { RandomKey } from '@swiss-post/shared/testing';
import { ExtendedFactor, FAQSection, VoterPortalConfig } from '@swiss-post/types';
import { MockDirective, MockPipe, MockProvider } from 'ng-mocks';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { ParagraphsDirective } from '../../directives/paragraphs.directive';
import { ReplacePlaceholdersPipe } from '../../pipes/replace-placeholders.pipe';

import { FAQModalComponent } from './faq-modal.component';

describe('FAQModalComponent', () => {
  let component: FAQModalComponent;
  let fixture: ComponentFixture<FAQModalComponent>;
  let dismiss: jest.Mock<() => void>;
  let voterPortalConfig: VoterPortalConfig;

  beforeEach(async () => {
    voterPortalConfig = {
      identification: ExtendedFactor.YearOfBirth,
      contestsCapabilities: {
        writeIns: true,
      },
      header: {
        logoPath: { desktop: '', mobile: '' },
        logoHeight: { desktop: 0, mobile: 0 },
      },
    };

    await TestBed.configureTestingModule({
      declarations: [
        FAQModalComponent,
        MockDirective(ParagraphsDirective),
        MockPipe(ReplacePlaceholdersPipe, value => value),
      ],
      imports: [
        TranslateTestingModule.withTranslations({}),
        NgbAccordionModule,
      ],
      providers: [
        MockProvider(NgbModal),
        MockProvider(NgbActiveModal, {dismiss}),
        MockProvider(ConfigurationService, voterPortalConfig),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FAQModalComponent);
    component = fixture.componentInstance;

    component.activeFAQSection = FAQSection.HowToStart;

    fixture.detectChanges();
  });

  it('should show all the FAQ', () => {
    const faqTitles = fixture.debugElement.queryAll(By.css('.accordion-item'));
    expect(faqTitles.length).toBe(12);
  });

  it('should properly adapt to the portal configuration', () => {
    const howtoStartSection: HTMLElement = fixture.debugElement.query(
      By.css(`#${FAQSection.HowToStart}-content`)
    ).nativeElement;

    expect(howtoStartSection.textContent).toContain('faq.howtostart.content.' + voterPortalConfig.identification);
  });

  describe('without an active section', () => {
    beforeEach(() => {
      component.activeFAQSection = undefined;
      fixture.detectChanges();
    });

    it('should not show any accordion content', () => {
      const accordionItems = fixture.debugElement.queryAll(By.css('.accordion-item'));
      accordionItems.forEach(accordionItem => {
        const accordionBody = accordionItem.query(By.css('.accordion-body'));

        expect(accordionBody).toBeFalsy();
      });
    });
  });

  describe('with an active section', () => {
    let activeSection: FAQSection;

    beforeEach(() => {
      activeSection = component.activeFAQSection = FAQSection[RandomKey(FAQSection)];
      fixture.detectChanges();
    });

    it('should show only accordion content matching the active section provided', () => {
      const accordionContents = fixture.debugElement.queryAll(By.css('.accordion-item > .collapse'));
      expect(accordionContents.length).toBe(1);
      expect(accordionContents[0].nativeElement.id).toBe(activeSection);
    });

    it('should focus on the active section\'s accordion header button', () => {
      const activeSectionButton: HTMLElement = fixture.debugElement.query(
        By.css(`#${activeSection}-header > button`)
      ).nativeElement;
      jest.spyOn(activeSectionButton, 'focus');

      component.ngAfterViewInit();

      expect(activeSectionButton.focus).toHaveBeenCalled();
    });
  });

});
