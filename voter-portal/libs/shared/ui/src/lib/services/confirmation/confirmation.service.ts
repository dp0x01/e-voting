/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationModalConfig } from '@swiss-post/types';
import { from, Observable, of } from 'rxjs';
import { catchError, filter } from 'rxjs/operators';
import { ConfirmationModalComponent } from '../../components/confirmation-modal/confirmation-modal.component';

@Injectable({
  providedIn: 'root',
})
export class ConfirmationService {
  constructor(
    private readonly modalService: NgbModal,
  ) {
  }

  confirm(config: ConfirmationModalConfig): Observable<boolean> {
    const modalRef = this.modalService.open(ConfirmationModalComponent, config.modalOptions);

    modalRef.componentInstance.content = config.content;
    modalRef.componentInstance.title = config.title ?? 'common.confirmaction';
    modalRef.componentInstance.confirmLabel = config.confirmLabel ?? 'common.confirm';
    modalRef.componentInstance.cancelLabel = config.cancelLabel ?? 'common.cancel';

    return from(modalRef.result)
      .pipe(
        catchError(() => of(null)),
        filter((modalResult: boolean | null): modalResult is boolean => modalResult !== null),
      );
  }
}
