/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {
  ElectionContest,
  Candidate,
  CandidateList,
  Contest,
  ContestUserData,
  TemplateType,
} from '@swiss-post/types';
import {RandomItemOrUndefined} from './random';

let contestIndex = 0;
let candidateIndex = 0;
let listIndex = 0;

export const MockCandidatesContestUserData = (
  contest: Contest
): ContestUserData => {
  const contestUserData: ContestUserData = {};
  const electionContest = new ElectionContest(contest);

  if (!electionContest.lists) {
    throw new Error(
      'Unable to create mock contest-userdata because there are no lists in the contest!'
    );
  }

  if (contest.listQuestion) {
    contestUserData.listId = RandomItemOrUndefined(electionContest.realLists)?.id;
  }

  let availableCandidates = electionContest.realCandidates;
  const candidateCount = contest.candidatesQuestion?.maxChoices ?? 0;
  const candidatesCumulMap = new Map<string, number>();

  contestUserData.candidates = Array.from({length: candidateCount}, () => {
    const randomCandidate = RandomItemOrUndefined(availableCandidates);

    if (randomCandidate) {
      const previousCumul = candidatesCumulMap.get(randomCandidate.id) ?? 0;
      const newCumul = previousCumul + 1;
      candidatesCumulMap.set(randomCandidate.id, newCumul);

      if (newCumul >= (contest.candidatesQuestion?.cumul ?? 0)) {
        availableCandidates = availableCandidates?.filter(
          (candidate) => candidate?.id !== randomCandidate.id
        );
      }
    }

    return {candidateId: randomCandidate?.id ?? null, writeIn: null};
  });

  return contestUserData;
};

export const MockCandidatesContest = (options: {
  cumulAllowed: boolean;
  numLists: number;
  numCandidatesPerList: number;
  numSeats: number;
  hasListQuestion: boolean;
}): Contest => {
  contestIndex++;

  const contest: Contest = {
    id: `contest-${contestIndex}`,
    template: TemplateType.ListsAndCandidates,
    allowFullBlank: true,
    title: `title-${contestIndex}`,
    howToVote: `howToVote-${contestIndex}`,
    description: `description-${contestIndex}`,
  };

  if (options.hasListQuestion) {
    contest.listQuestion = {
      id: `listQuestion-${contestIndex}`,
      minChoices: 0,
      maxChoices: 1,
      cumul: 1,
    };
  }

  contest.candidatesQuestion = {
    cumul: options.cumulAllowed ? 2 : 1,
    fusions: [],
    hasWriteIns: false,
    id: `candidate-questions-${contestIndex}`,
    minChoices: 1,
    maxChoices: options.numSeats,
  };

  const blankList = MockCandidateList({
    isBlank: true,
    isWriteIN: false,
    numCandidates: options.numSeats,
    cumulAllowed: false,
  });

  const writeInList = MockCandidateList({
    isBlank: false,
    isWriteIN: true,
    numCandidates: options.numSeats,
    cumulAllowed: false,
  });


  const realLists = Array.from({length: options.numLists ?? 1}, () => {
    return MockCandidateList({
      cumulAllowed: options.cumulAllowed,
      numCandidates: options.numCandidatesPerList,
      isBlank: false,
      isWriteIN: false,
    });
  });

  return {...contest, lists: [blankList, writeInList, ...realLists]};
};

const MockCandidateList = (options: {
  isBlank: boolean;
  isWriteIN: boolean;
  numCandidates: number;
  cumulAllowed: boolean;
}): CandidateList => {
  listIndex++;

  const candidates = Array.from({length: options.numCandidates}, () => {
    return MockCandidate({
      isBlank: options.isBlank,
      isWriteIn: options.isWriteIN,
      cumulAllowed: options.cumulAllowed,
    });
  });

  return {
    id: `candidateList-${listIndex}`,
    isBlank: options.isBlank ?? false,
    isWriteIn: false,
    candidates: candidates,
    prime: `prime-list-${listIndex}`,
  };
};

const MockCandidate = (options: {
  isBlank?: boolean;
  isWriteIn?: boolean;
  cumulAllowed?: boolean;
}): Candidate => {
  candidateIndex++;

  const candidate: Candidate = {
    id: `candidate-${candidateIndex}`,
    isBlank: options.isBlank ?? false,
    isWriteIn: options.isWriteIn ?? false,
    allIds: [],
    allRepresentations: [],
    details: {text: 'no name selected'}
  };

  if (!options.isBlank) {
    candidate.details = {
      candidateType_attribute1: `surname-${candidateIndex} firstname-${candidateIndex}`,
      candidateType_attribute2: `(partyName)`,
      candidateType_attribute3: `till now`,
    };
  }

  if (!options.cumulAllowed) {
    candidate.prime = `prime-candidate-${candidateIndex}`
  } else {
    candidate.allIds = Array.from({length: 2}, (_, i) => {
      return `allIds-${candidateIndex}-${i}`;
    });

    candidate.allRepresentations = Array.from({length: 2}, (_, i) => {
      return `allRepresentations-candidate-${candidateIndex}-${i}`;
    });
  }

  return candidate;
};
