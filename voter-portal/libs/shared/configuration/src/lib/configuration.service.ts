/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@swiss-post/backend';
import {
  ContestsCapabilities,
  ExtendedFactor,
  HeaderConfig,
  TranslatePlaceholders,
  VoterPortalConfig,
} from '@swiss-post/types';
import { firstValueFrom } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ConfigurationService implements VoterPortalConfig {
  contestsCapabilities!: ContestsCapabilities;
  header!: HeaderConfig;
  identification!: ExtendedFactor;
  translatePlaceholders?: TranslatePlaceholders;

  constructor(
    private readonly http: HttpClient,
  ) {
  }

  fetchPortalConfiguration(): Promise<VoterPortalConfig> {
    const voterPortalConfig$ = this.http
      .get<VoterPortalConfig>(environment.voterPortalConfig)
      .pipe(tap(configuration => Object.assign(this, configuration)));

    return firstValueFrom(voterPortalConfig$);
  }
}
