/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

@NgModule({
  imports: [CommonModule],
})
export class SharedConfigurationModule {
}
