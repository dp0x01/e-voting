/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { BackendModule } from '@swiss-post/backend';
import { NavigationEffects } from './effects/navigation.effects';
import { SharedStateEffects } from './effects/shared-state.effects';
import * as fromSharedState from './reducer/shared-state.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(
      fromSharedState.SHARED_FEATURE_KEY,
      fromSharedState.reducer,
    ),
    EffectsModule.forFeature([ SharedStateEffects, NavigationEffects ]),
    BackendModule,
  ],
})
export class SharedStateModule {
}
