/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
export interface CandidateFilters {
  searchTerm: string;
  listId?: string;
  selectedCandidatesOnly: boolean;
}
