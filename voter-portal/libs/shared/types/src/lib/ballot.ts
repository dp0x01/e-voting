/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
export interface Ballot {
  id: string;
  contests: Contest[];
  correctnessIds: { [key: string]: string[] };
  description?: string;
  title?: string;
  writeInAlphabet?: string;
}

export interface Contest {
  id: string;
  defaultTitle?: string;
  description?: string;
  template: string;
  title?: string;
  howToVote?: string;
  questions?: Question[];
  candidatesQuestion?: CandidatesQuestion;
  listQuestion?: ListQuestion;
  allowFullBlank?: boolean;
  lists?: CandidateList[];
}

export interface ListQuestion {
  id: string;
  minChoices: number;
  maxChoices: number;
  cumul: number;
}

export interface CandidateList {
  id: string;
  blankId?: string;
  isBlank: boolean;
  isWriteIn: boolean;
  description?: string[];
  candidates: Candidate[];
  details?: CandidateListDetails;
  text?: string;
  ordinal?: number;
  prime?: string;
}

export interface CandidateListDetails {
  text?: string;
  listType_apparentment: string;
  listType_sousApparentment: string;
  listType_attribute1: string;
  listType_attribute2: string;
  listType_attribute3: string;
}

export interface Candidate {
  id: string;
  alias?: string;
  allIds: string[] | null;
  allRepresentations: string[] | null;
  details: CandidateDetail;
  isBlank: boolean;
  isWriteIn: boolean;
  ordinal?: number;
  prime?: string;
}

export interface CandidateDetail {
  candidateType_initialAccumulation?: string;
  candidateType_attribute1?: string;
  candidateType_attribute2?: string;
  candidateType_attribute3?: string;
  candidateType_positionOnList?: string;
  text?: string;
}

export interface CandidatesQuestion {
  id: string;
  minChoices: number;
  maxChoices: number;
  hasWriteIns: boolean;
  cumul: number;
  fusions: string[][];
}

export interface Question {
  id: string;
  attrIndex: number;
  attribute: string;
  details: QuestionDetail;
  blankOption: Option;
  options: Option[];
  optionsMaxChoices: number;
  optionsMinChoices: number;
  ordinal: number;
  text?: string;
  title?: string;
  chosen?: string;
}

export interface QuestionDetail {
  questionType_text?: string;
}

export interface Option {
  attrIndex: number;
  attribute: string;
  id: string;
  chosen: false;
  description?: string;
  details: OptionDetail;
  isBlank: boolean;
  ordinal: number;
  prime?: string;
  representation?: string;
  text?: string;
  title?: string;
}

export interface OptionDetail {
  answerType_text?: string;
  text?: string;
}
