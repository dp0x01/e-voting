/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';

export interface ConfirmationModalConfig {
  content: string | string[];
  title?: string;
  confirmLabel?: string;
  cancelLabel?: string;
  modalOptions?: NgbModalOptions;
}
