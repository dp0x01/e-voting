/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

import { ExtendedFactor } from './extended-factor';

export interface VoterPortalConfig {
  identification: ExtendedFactor;
  contestsCapabilities: ContestsCapabilities;
  translatePlaceholders?: TranslatePlaceholders;
  header: HeaderConfig;
}

export interface ContestsCapabilities {
  writeIns: boolean;
}

export type TranslatePlaceholders = {
  [lang in 'de' | 'fr' | 'it' | 'rm' | 'en']: Record<string, string>
}

export interface HeaderConfig {
  logoPath: ResponsiveConfig<string>;
  logoHeight: ResponsiveConfig<number>;
  reverse?: boolean;
  background?: string;
  bars?: HeaderBarConfig[];
}

interface HeaderBarConfig {
  height: ResponsiveConfig<number>;
  color: string;
}

interface ResponsiveConfig<T=unknown> {
  desktop: T;
  mobile: T;
}
