/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
export enum TemplateType {
  ListsAndCandidates = 'listsAndCandidates',
  Options = 'options',
}
