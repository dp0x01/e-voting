/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Candidate, CandidateList, CandidatesQuestion, Contest, ListQuestion, Question } from './ballot';
import { TemplateType } from './template-type';

export class ElectionContest implements Contest {
  id!: string;
  defaultTitle?: string;
  description?: string;
  template!: string;
  title?: string;
  howToVote?: string;
  questions?: Question[];
  candidatesQuestion?: CandidatesQuestion;
  listQuestion?: ListQuestion;
  allowFullBlank?: boolean;
  lists?: CandidateList[];

  constructor(contest: Contest) {
    if (contest.template !== TemplateType.ListsAndCandidates) {
      throw new Error(
        'Unable to create a ListAndCandidateContest with a contest whose template is of type "OPTIONS".'
      );
    }

    Object.assign(this, contest);
  }

  get hasListQuestion(): boolean {
    return !!this.listQuestion && this.listQuestion.maxChoices > 0;
  }

  get allowsListWithoutCandidate(): boolean {
    return !!this.candidatesQuestion && this.candidatesQuestion.minChoices === 0;
  }

  private _allCandidates?: Candidate[];

  get allCandidates(): Candidate[] {
    if (!this._allCandidates) {
      const lists = this.lists ?? [];
      this._allCandidates = lists.reduce((candidates, list) => {
        return list.candidates
          ? [...candidates, ...list.candidates]
          : candidates;
      }, [] as Candidate[]);
    }
    return this._allCandidates;
  }

  private _allCandidatesById?: ReadonlyMap<string, Candidate>;

  get allCandidatesById(): ReadonlyMap<string, Candidate> {
    if (!this._allCandidatesById) {
      this._allCandidatesById = this.allCandidates.reduce((candidateMap, candidate) => {
        return candidateMap.set(candidate.id, candidate);
      }, new Map<string, Candidate>());
    }
    return this._allCandidatesById;
  }

  private _realCandidates?: Candidate[];

  get realCandidates(): Candidate[] {
    if (!this._realCandidates) {
      this._realCandidates = this.allCandidates.filter(
        (candidate) => !candidate.isBlank && !candidate.isWriteIn
      );
    }
    return this._realCandidates;
  }

  private _blankCandidates?: Candidate[];

  get blankCandidates(): Candidate[] {
    if (!this._blankCandidates) {
      this._blankCandidates = this.allCandidates.filter(
        (candidate) => candidate.isBlank
      );
    }
    return this._blankCandidates;
  }

  get blankCandidate(): Candidate | undefined {
    return this.blankCandidates[0];
  }

  private _writeInCandidates?: Candidate[];

  get writeInCandidates(): Candidate[] {
    if (!this._writeInCandidates) {
      this._writeInCandidates = this.allCandidates.filter(
        (candidate) => candidate.isWriteIn
      );
    }
    return this._writeInCandidates;
  }

  get writeInCandidate(): Candidate | undefined {
    return this.writeInCandidates[0];
  }

  private _realLists?: CandidateList[];

  get realLists(): CandidateList[] {
    if (!this._realLists) {
      this._realLists = this.lists?.filter(
        (list) => !list.isBlank && !list.isWriteIn
      ) ?? [];
    }
    return this._realLists;
  }

  private _blankLists?: CandidateList[];

  get blankLists(): CandidateList[] {
    if (!this._blankLists) {
      this._blankLists = this.lists?.filter(
        (list) => list.isBlank
      ) ?? [];
    }

    return this._blankLists;
  }

  get blankList(): CandidateList | undefined {
    return this.blankLists[0];
  }

  getCandidate(candidateId: string | null): Candidate | null {
    return candidateId && this.allCandidatesById.has(candidateId)
      ? (this.allCandidatesById.get(candidateId) as Candidate)
      : null;
  }

  getList(listId: string | null | undefined): CandidateList | null {
    if (listId) {
      return this.lists?.find((list) => list.id === listId) ?? null;
    }

    return null;
  }
}
