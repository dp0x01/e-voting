/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';

import {VerifyContestContainerComponent} from './verify-contest-container.component';

describe('ContestVerifyContainerComponent', () => {
  let component: VerifyContestContainerComponent;
  let fixture: ComponentFixture<VerifyContestContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VerifyContestContainerComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyContestContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
