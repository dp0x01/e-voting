/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CandidateModule } from '@swiss-post/candidate';
import { ListModule } from '@swiss-post/list';
import { QuestionsModule } from '@swiss-post/questions';
import { SharedIconsModule } from '@swiss-post/shared/icons';
import { SharedUiModule } from '@swiss-post/shared/ui';
import { NgxMaskModule } from 'ngx-mask';
import { VerifyCandidateListComponent } from './verify-candidate-list/verify-candidate-list.component';
import { VerifyContestContainerComponent } from './verify-contest-container/verify-contest-container.component';
import { VerifyQuestionsComponent } from './verify-questions/verify-questions.component';
import { VerifyRoutingModule } from './verify-routing-module';
import { VerifyComponent } from './verify/verify.component';

@NgModule({
  imports: [
    CommonModule,
    VerifyRoutingModule,
    TranslateModule,
    ReactiveFormsModule,
    SharedIconsModule,
    SharedUiModule,
    QuestionsModule,
    CandidateModule,
    ListModule,
    NgxMaskModule.forRoot({ validation: false }),
  ],
  declarations: [
    VerifyComponent,
    VerifyContestContainerComponent,
    VerifyQuestionsComponent,
    VerifyCandidateListComponent,
  ],
})
export class VerifyModule {
}
