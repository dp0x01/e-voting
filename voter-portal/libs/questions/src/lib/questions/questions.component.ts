/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, Input, OnChanges, SimpleChanges, TemplateRef} from '@angular/core';
import {Contest, ContestUserData, Question} from '@swiss-post/types';

interface IQuestionTemplateContext {
  question: Question;
  questionIndex: number;
  answer?: string;
}

@Component({
  selector: 'swp-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss'],
})
export class QuestionsComponent implements OnChanges {
  @Input() contest: Contest | undefined;
  @Input() contestUserData: ContestUserData | undefined;
  @Input() headingLevel: number = 3;
  @Input() leftColumnTemplate: TemplateRef<IQuestionTemplateContext> | undefined;
  @Input() rightColumnTemplate: TemplateRef<IQuestionTemplateContext> | undefined;

  questions: Question[] | undefined;
  answers: Map<string, string> | undefined;

  ngOnChanges({contest, contestUserData}: SimpleChanges) {
    const newContest: Contest = contest?.currentValue;
    if (newContest) {
      this.questions = newContest.questions;
    }

    const newContestUserData: ContestUserData = contestUserData?.currentValue;
    if (newContest || newContestUserData) {
      this.answers = this.questions?.reduce((answerMap, question) => {
        const questionUserData = this.contestUserData?.questions?.find(q => q.id === question.id);
        const chosenOptionId = questionUserData?.chosenOption;
        const answer = !chosenOptionId && questionUserData ? question.blankOption.text : question.options.find(option => option.id === chosenOptionId)?.text;
        return answerMap.set(question.id, answer ?? '');
      }, new Map<string, string>());
    }
  }

  getTemplateContext(question: Question, questionIndex: number): IQuestionTemplateContext {
    return {question, questionIndex, answer: this.answers?.get(question.id)};
  }
}
