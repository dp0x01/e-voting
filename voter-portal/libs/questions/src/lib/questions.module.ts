/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedUiModule} from '@swiss-post/shared/ui';
import {SharedIconsModule} from '@swiss-post/shared/icons';
import {QuestionsComponent} from './questions/questions.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    SharedIconsModule,
    SharedUiModule,
  ],
  declarations: [
    QuestionsComponent,
  ],
  exports: [
    QuestionsComponent,
  ],
})
export class QuestionsModule {
}
