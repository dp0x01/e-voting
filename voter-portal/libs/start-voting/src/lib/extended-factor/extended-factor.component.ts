/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { AfterViewInit, Component, ElementRef, Input, OnDestroy, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from '@swiss-post/shared/configuration';
import { SwpDateAdapter } from '@swiss-post/shared/ui';
import { ErrorStatus, ExtendedFactor } from '@swiss-post/types';
import { MaskPipe } from 'ngx-mask';
import { combineLatest, distinctUntilChanged, pairwise, startWith, Subscription } from 'rxjs';

@Component({
  selector: 'swp-extended-factor',
  templateUrl: './extended-factor.component.html',
  styleUrls: [ './extended-factor.component.scss' ],
  providers: [ SwpDateAdapter, MaskPipe ],
})
export class ExtendedFactorComponent implements AfterViewInit, OnDestroy {
  @ViewChild('dateOfBirthInput') dateOfBirthInput: ElementRef<HTMLInputElement> | undefined;
  @Input() voterForm!: FormGroup;
  @Input() formSubmitted = false;
  datepickerMinDate: NgbDateStruct;
  datepickerMaxDate: NgbDateStruct;
  datepickerDefaultDate: NgbDateStruct;
  dateOfBirthMaskExpression = '';
  subscription: Subscription | undefined;

  readonly ExtendedFactor = ExtendedFactor;
  readonly ErrorMessage = ErrorStatus;

  get extendedFactor(): FormControl {
    return this.voterForm.get('extendedFactor') as FormControl;
  }

  get datePickerSelectedDate(): NgbDateStruct | null {
    return this.dateAdapter.fromModel(this.extendedFactor.value);
  }

  set datePickerSelectedDate(date: NgbDateStruct | null) {
    this.extendedFactor.setValue(this.dateAdapter.toModel(date));
  }

  constructor(
    public readonly configuration: ConfigurationService,
    private readonly dateAdapter: SwpDateAdapter,
    private readonly calendar: NgbCalendar,
    private readonly translate: TranslateService,
    private readonly mask: MaskPipe,
  ) {
    const today = this.calendar.getToday();
    this.datepickerMinDate = this.calendar.getPrev(today, 'y', 120);
    this.datepickerMaxDate = this.calendar.getPrev(today, 'y', 16);
    this.datepickerDefaultDate = this.calendar.getPrev(today, 'y', 40);
  }

  public ngAfterViewInit(): void {
    switch (this.configuration.identification) {
      case ExtendedFactor.YearOfBirth:
        this.extendedFactor.addValidators(Validators.pattern(/^\d{4}$/));
        break;
      case ExtendedFactor.DateOfBirth:
        this.extendedFactor.addValidators(Validators.pattern(/^\d{8}$/));
        this.setupDateOfBirthMask();
        break;
    }
  }

  public ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  private setupDateOfBirthMask(): void {
    const datePlaceholderChanges$ = this.translate.stream('datepicker.placeholder');
    const extendedFactorChanges$ = this.extendedFactor.valueChanges
      .pipe(
        startWith(this.extendedFactor.value),
        distinctUntilChanged(),
        pairwise(),
      );

    this.subscription = combineLatest([ datePlaceholderChanges$, extendedFactorChanges$ ])
      .subscribe(([ datePlaceholder, userInput ]) => {
        const [ previousUserInput, currentUserInput ] = userInput;
        const wasCharacterDeleted = previousUserInput.length > currentUserInput.length;
        this.updateDateOfBirthMaskExpression(currentUserInput, datePlaceholder, wasCharacterDeleted);
      });
  }

  private updateDateOfBirthMaskExpression(dateOfBirth: string, datePlaceholder: string, wasCharacterDeleted: boolean) {
    // get a mask expression by filling the placeholder with the current user input
    const maskedDateOfBirth = this.mask.transform(dateOfBirth, '99.99.9999');
    this.dateOfBirthMaskExpression = maskedDateOfBirth + datePlaceholder.substring(maskedDateOfBirth.length);

    // adjust the position of the cursor in the input since it is moved to the end of the mask expression by default
    const selectionStart = this.dateOfBirthInput?.nativeElement?.selectionStart;
    setTimeout(() => this.setDateOfBirthCursorPosition(
      // move the cursor before the last character deleted or after the last character added
      wasCharacterDeleted ? selectionStart : maskedDateOfBirth.length,
    ));
  }

  private setDateOfBirthCursorPosition(position: number | null | undefined) {
    if (!this.dateOfBirthInput || typeof position !== 'number') {
      return;
    }

    const input = this.dateOfBirthInput.nativeElement;
    input.setSelectionRange(position, position);
    input.focus();
  }
}
