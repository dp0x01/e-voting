/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Injectable} from '@angular/core';
import {
  ElectionContest,
  Ballot,
  BallotUserData,
  Candidate,
  Contest,
  ContestUserData,
  TemplateType,
} from '@swiss-post/types';

type Representation = {
  id: string;
  prime: string;
};

type RepresentationsAndWriteIns = {
  representations: Representation[];
  writeIns: string[];
};

type PrimesAndWriteIns = {
  primes: string[];
  writeIns: string[];
};

@Injectable({
  providedIn: 'root',
})
export class RepresentationBuilderService {
  private writeInsSeparator = '#';

  public getPrimesAndWriteIns(
    ballot: Ballot,
    ballotUserData: BallotUserData
  ): PrimesAndWriteIns {
    let allRepresentations: Representation[] = [];
    let allWriteIns: string[] = [];

    if (ballot.writeInAlphabet?.length) {
      this.writeInsSeparator = ballot.writeInAlphabet.substring(0, 1);
    }

    ballot.contests.forEach((contest, i) => {
      const contestUserData = ballotUserData.contests[i];
      const {representations, writeIns} = this.getRepresentationsAndWriteIns(contest, contestUserData);

      allRepresentations = [...allRepresentations, ...representations];
      allWriteIns = [...allWriteIns, ...writeIns];
    });

    const allPrimes = allRepresentations.reduce((primes, {prime}) => {
      return prime ? [...primes, prime] : primes;
    }, [] as string[]);

    return {
      primes: allPrimes,
      writeIns: allWriteIns,
    };
  }

  private getRepresentationsAndWriteIns(
    contest: Contest,
    contestUserData: ContestUserData
  ): RepresentationsAndWriteIns {
    switch (contest.template) {
      case TemplateType.Options: {
        const representations = this.getOptionRepresentations(contest, contestUserData);
        return {representations, writeIns: []};
      }
      case TemplateType.ListsAndCandidates: {
        const electionContest = new ElectionContest(contest);
        return this.getListAndCandidateRepresentations(electionContest, contestUserData);
      }
      default:
        throw Error(`Unknown template for contest with id ${contest.id}`);
    }
  }

  private getListAndCandidateRepresentations(
    contest: ElectionContest,
    contestUserData: ContestUserData
  ): RepresentationsAndWriteIns {
    const {representations, writeIns} = this.getCandidateRepresentationsAndWriteIns(contest, contestUserData);

    if (contest.hasListQuestion) {
      const list = contestUserData.listId ? contest.getList(contestUserData.listId) : contest.blankList;
      const listRepresentation = list ? {id: list.id, prime: list.prime ?? ''} : null;

      if (listRepresentation) {
        representations.unshift(listRepresentation);
      }
    }

    return {
      representations,
      writeIns,
    };
  }

  private getCandidateRepresentationsAndWriteIns(
    contest: ElectionContest,
    contestUserData: ContestUserData
  ): RepresentationsAndWriteIns {
    if (!contestUserData.candidates?.length) {
      return {
        representations: [],
        writeIns: [],
      };
    }

    const representations: Representation[] = [];
    const writeIns: string[] = [];

    contestUserData.candidates.forEach(({candidateId, writeIn}, i) => {
      let candidate = contest.getCandidate(candidateId);
      if (!candidate) {
        candidate = contest.blankCandidates ? contest.blankCandidates[i] : null;
      } else if (candidate.isWriteIn) {
        candidate = contest.writeInCandidates ? contest.writeInCandidates[i] : null;
        writeIns.push(
          `${candidate?.prime}${this.writeInsSeparator}${writeIn}`
        );
      }

      if (candidate) {
        const unusedRepresentation = this.getCandidateUnusedRepresentation(representations, candidate);
        representations.push(unusedRepresentation);
      }
    });

    return {
      representations,
      writeIns,
    };
  }

  private getCandidateUnusedRepresentation(
    uniqueRepresentations: Representation[],
    candidate: Candidate
  ): Representation {
    if (!candidate.allIds?.length || !candidate.allRepresentations?.length) {
      return {
        id: candidate.id,
        prime: candidate.prime ?? '',
      };
    }

    const unusedRepresentationIndex = candidate.allIds.findIndex((id) =>
      uniqueRepresentations.every(
        (uniqueRepr) => uniqueRepr.id !== id
      )
    );

    if (unusedRepresentationIndex === -1) {
      throw new Error(
        `No unused representation has been found for candidate ${candidate.id}.`
      );
    }

    return {
      id: candidate.allIds[unusedRepresentationIndex],
      prime: candidate.allRepresentations[unusedRepresentationIndex] ?? '',
    };
  }

  private getOptionRepresentations(
    contest: Contest,
    contestUserData: ContestUserData
  ): Representation[] {
    const questions = contest.questions ?? [];
    return questions
      .reduce((representations, question) => {
        const selectedOption = contestUserData.questions?.find(
          (questionUserData) => questionUserData.id === question.id
        );

        const option = question.options.find(
          ({id}) => id === selectedOption?.chosenOption
        ) ?? question.blankOption;

        if (option) {
          representations.push({
            id: question.id,
            prime: option?.prime || option?.representation || '',
          });
        }

        return representations;
      }, [] as Representation[]);
  }
}
