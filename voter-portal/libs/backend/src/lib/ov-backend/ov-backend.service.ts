/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Injectable } from '@angular/core';
import {
  AuthenticateVoterResponse,
  BackendConfig,
  BackendError,
  Ballot,
  BallotUserData,
  OvApi,
  Voter,
} from '@swiss-post/types';
import { BackendService } from '../backend.service';
import { RepresentationBuilderService } from '../representation-builder/representation-builder.service';

declare global {
  const OvApi: () => OvApi;

  interface Window {
    ovApi: OvApi;
  }
}

function deepClone<T>(obj: T): T {
  return JSON.parse(JSON.stringify(obj));
}

@Injectable({
  providedIn: 'root',
})
export class OvBackendService implements BackendService {
  constructor(
    private readonly representationBuilder: RepresentationBuilderService,
  ) {
  }

  private get ovApi(): OvApi {
    return window.ovApi;
  }

  async authenticateVoter(voter: Voter, config: BackendConfig): Promise<AuthenticateVoterResponse> {
    let authenticateVoterResponse: AuthenticateVoterResponse;
    try {
      this.initializeOvApi();
      authenticateVoterResponse = await this.ovApi.authenticateVoter(
        voter.startVotingKey,
        voter.extendedFactor,
        config.electionEventId,
        `${config.lang}-CH`,
      );
    } catch (error) {
      throw new BackendError(error);
    }

    return authenticateVoterResponse;
  }

  async translateBallot(ballot: Ballot, lang: string): Promise<Ballot> {
    const mutableBallot = deepClone(ballot);

    try {
      this.ovApi.translateBallot(mutableBallot, `${lang}-CH`);
    } catch (error) {
      throw new BackendError(error);
    }

    return mutableBallot;
  }

  async sendVote(
    ballot: Ballot,
    ballotUserData: BallotUserData,
  ): Promise<string[]> {
    const { primes, writeIns } = this.representationBuilder.getPrimesAndWriteIns(ballot, ballotUserData);

    try {
      const { choiceReturnCodes } = await this.ovApi.sendVote(primes, writeIns);
      return choiceReturnCodes;
    } catch (error) {
      throw new BackendError(error);
    }
  }

  async confirmVote(confirmationKey: string): Promise<string> {
    try {
      const { voteCastReturnCode } = await this.ovApi.confirmVote(confirmationKey);
      return voteCastReturnCode;
    } catch (error) {
      throw new BackendError(error);
    }
  }

  private initializeOvApi(): void {
    window.ovApi = OvApi();
  }
}
