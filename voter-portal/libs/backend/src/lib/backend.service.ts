/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { AuthenticateVoterResponse, BackendConfig, Ballot, BallotUserData, Voter } from '@swiss-post/types';

export abstract class BackendService {
  /**
   * @throws BackendError
   */
  abstract authenticateVoter(voter: Voter, config: BackendConfig): Promise<AuthenticateVoterResponse>;

  abstract translateBallot(ballot: Ballot, lang: string): Promise<Ballot>;

  abstract sendVote(ballotResponse: Ballot, userData: BallotUserData): Promise<string[]>;

  abstract confirmVote(confirmationKey: string): Promise<string>;
}
