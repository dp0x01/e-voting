/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {Environment} from '@swiss-post/types';

export const environment: Environment = {
  production: false,
  voterPortalConfig: './assets/configuration/portalConfig-template.json',
  availableLanguages: [
    {id: 'de', label: 'Deutsch'},
    {id: 'fr', label: 'Français'},
    {id: 'it', label: 'Italiano'},
    {id: 'rm', label: 'Rumantsch'},
    {id: 'en', label: 'English'},
  ],
  markdownDenylist: ['link', 'image'], // Inline-level tokens not processed by https://marked.js.org/using_pro#renderer
  progressOverlayCloseDelay: 1000,
  progressOverlayNavigateDelay: 1100, //a bit longer than progressOverlayCloseDelay
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 * import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
 */
