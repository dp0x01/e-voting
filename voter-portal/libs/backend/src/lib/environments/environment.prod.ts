/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Environment} from '@swiss-post/types';

export const environment: Environment = {
  production: true,
  voterPortalConfig: './assets/configuration/portalConfig.json',
  availableLanguages: [
    {id: 'de', label: 'Deutsch'},
    {id: 'fr', label: 'Français'},
    {id: 'it', label: 'Italiano'},
    {id: 'rm', label: 'Rumantsch'},
  ],
  markdownDenylist: ['link', 'image'],
  progressOverlayCloseDelay: 1000,
  progressOverlayNavigateDelay: 1100,
};
