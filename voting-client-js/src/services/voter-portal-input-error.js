/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

"use strict";
Object.defineProperty(exports, "__esModule", {value: true});
exports.VoterPortalInputError = void 0;

class VoterPortalInputError extends Error {
  constructor(status) {
    super("Voter Portal input error.");
    this.errorResponse = {errorStatus: status};
    // Set the prototype explicitly.
    Object.setPrototypeOf(this, VoterPortalInputError.prototype);
  }
}

exports.VoterPortalInputError = VoterPortalInputError;
//# sourceMappingURL=illegal_argument_error.js.map