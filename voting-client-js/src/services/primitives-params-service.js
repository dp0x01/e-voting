/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

const {validateActualVotingOptions} = require("./validation-service");
const {deserializeElGamalMultiRecipientPublicKey, deserializeGqGroup} = require("./primitives-deserializer");
const {GqGroup} = require("crypto-primitives-ts/lib/cjs/math/gq_group");
const {GqElement} = require("crypto-primitives-ts/lib/cjs/math/gq_element");
const {GroupVector} = require("crypto-primitives-ts/lib/cjs/group_vector");
const {PrimeGqElement} = require("crypto-primitives-ts/lib/cjs/math/prime_gq_element");
const {ElGamalMultiRecipientPublicKey} = require("crypto-primitives-ts/lib/cjs/elgamal/elgamal_multi_recipient_public_key");

module.exports = (function () {
  'use strict';

  /**
   * Computes and deserializes the primitives parameters from an {@link AuthenticateVoterResponsePayload}.
   *
   * @param {AuthenticateVoterResponsePayload} authenticateVoterResponsePayload, the JSON containing the serialized parameters.
   * @returns {PrimitivesParams}, the primitives params object.
   */
  function parsePrimitivesParams(authenticateVoterResponsePayload) {
    const votingClientPublicKeys = authenticateVoterResponsePayload.votingClientPublicKeys;

    // Encryption group, we assume the signature has been checked and verified
    const encryptionParameters = deserializeGqGroup(votingClientPublicKeys.encryptionParameters);

    // Election public key
    const electionPublicKey = deserializeElGamalMultiRecipientPublicKey(
      votingClientPublicKeys.electionPublicKey,
      encryptionParameters
    );

    // Choice return codes encryption public key
    const choiceReturnCodesEncryptionPublicKey = deserializeElGamalMultiRecipientPublicKey(
      votingClientPublicKeys.choiceReturnCodesEncryptionPublicKey,
      encryptionParameters
    );

    // Encoded voting options
    const encodedVotingOptionsList = authenticateVoterResponsePayload.verificationCard.encodedVotingOptions
      .map(encodedVotingOptionString => {
        const encodedVotingOption = parseInt(encodedVotingOptionString, 10);
        return PrimeGqElement.fromValue(encodedVotingOption, encryptionParameters);
      });
    const encodedVotingOptions = GroupVector.from(encodedVotingOptionsList);

    // Actual voting options
    validateActualVotingOptions(authenticateVoterResponsePayload.verificationCard.actualVotingOptions);

    /**
     * @typedef {object} PrimitivesParams
     * @property {ElGamalMultiRecipientPublicKey} electionPublicKey electionPublicKey, the election public key.
     * @property {ElGamalMultiRecipientPublicKey} choiceReturnCodesEncryptionPublicKey choiceReturnCodesEncryptionPublicKey, the CCR encryption public key.
     * @property {GqGroup} encryptionGroup encryptionGroup, the encryption group.
     * @property {GroupVector<PrimeGqElement>} encodedVotingOptions encodedVotingOptions, the ballot encoded voting options.
     * @property {string[]} actualVotingOptions actualVotingOptions, the ballot actual voting options.
     * @property {string[]} semanticInformation semanticInformation, the ballot semantic information.
     * @property {string[]} correctnessInformationSelections ciSelections, the correctness information for selections.
     * @property {string[]} correctnessInformationVotingOptions ciVotingOptions, the correctness information for voting options.
     * @property {number} totalNumberOfWriteIns totalNumberOfWriteIns, the total number write-ins.
     */
    return {
      electionPublicKey: electionPublicKey,
      choiceReturnCodesEncryptionPublicKey: choiceReturnCodesEncryptionPublicKey,
      encryptionGroup: encryptionParameters,
      encodedVotingOptions: encodedVotingOptions,
      actualVotingOptions: authenticateVoterResponsePayload.verificationCard.actualVotingOptions,
      semanticInformation: authenticateVoterResponsePayload.verificationCard.semanticInformation,
      correctnessInformationSelections: authenticateVoterResponsePayload.verificationCard.ciSelections,
      correctnessInformationVotingOptions: authenticateVoterResponsePayload.verificationCard.ciVotingOptions,
      totalNumberOfWriteIns: authenticateVoterResponsePayload.verificationCard.totalNumberOfWriteIns,
    };
  }

  return {
    parsePrimitivesParams: parsePrimitivesParams
  };

})();
