/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

module.exports = (function () {
  'use strict';

  let sessionState = {}

  function session(name, value) {
    if (value) {
      sessionState[name] = value;
    }
    return sessionState[name];
  }

  /**
   * Get / Set the StartVotingKey
   * @param {string} [value], the start voting key
   * @returns {string} the start voting key
   */
  function startVotingKey(value) {
    return session('startVotingKey', value);
  }

  /**
   * Get / Set the extended authentication factor.
   * @param {string} [value], the extended authentication factor
   * @returns {string} the extended authentication factor
   */
  function extendedAuthenticationFactor(value) {
    return session('extendedAuthenticationFactor', value);
  }

  /**
   * Get / Set the primitives params.
   * @param {PrimitivesParams} [value], the primitives params object.
   * @returns {PrimitivesParams} the primitives params object.
   */
  function primitivesParams(value ){
    return session('primitivesParams', value);
  }

  /**
   * Get / Set the voter authentication data.
   * @param {VoterAuthenticationData} [value], the voter authentication data object.
   * @returns {VoterAuthenticationData} the voter authentication data object.
   */
  function voterAuthenticationData(value ){
    return session('voterAuthenticationData', value);
  }

  /**
   * Get / Set the verification card secret key.
   * @param {ZqElement} [value], the verification card secret key.
   * @returns {ZqElement} the verification card secret key.
   */
  function verificationCardSecretKey(value ){
    return session('verificationCardSecretKey', value);
  }

  return {
    startVotingKey: startVotingKey,
    extendedAuthenticationFactor: extendedAuthenticationFactor,
    primitivesParams: primitivesParams,
    voterAuthenticationData: voterAuthenticationData,
    verificationCardSecretKey: verificationCardSecretKey,
  }
})();
