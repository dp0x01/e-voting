/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

const {WRITE_IN_ALPHABET} = require("./write-in-alphabet");
const writeInToIntegerAlgorithm = require("./write-in-to-integer-algorithm");
const {GqElement} = require("crypto-primitives-ts/lib/cjs/math/gq_element");
const {ImmutableBigInteger} = require("crypto-primitives-ts/lib/cjs/immutable_big_integer");
const {checkNotNull, checkArgument} = require("crypto-primitives-ts/lib/cjs/validation/preconditions");

module.exports = (function () {
  'use strict';

  /**
   * @typedef {object} WriteInToQuadraticResidueContext
   * @property {GqGroup} encryptionGroup, the encryption parameters context.
   */

  /**
   * Implements the WriteInToQuadraticResidue algorithm described in the cryptographic protocol.
   * Maps a character string to a value ∈ Gq.
   *
   * @param {WriteInToQuadraticResidueContext} context, the WriteInToQuadraticResidue context.
   * @param {string} characterString, s the character string to map.
   * @returns {GqElement}, y the mapped value.
   */
  function writeInToQuadraticResidue(
    context,
    characterString
  ) {
    checkNotNull(context);
    const encryptionGroup = checkNotNull(context.encryptionGroup);
    const s = checkNotNull(characterString);

    const a = ImmutableBigInteger.fromNumber(WRITE_IN_ALPHABET.length);
    const s_length = ImmutableBigInteger.fromNumber(s.length);

    // Require.
    checkArgument(writeInToIntegerAlgorithm.checkExpLength(a, s_length, encryptionGroup),
      'The exponential form of a to s_length must be smaller than q.');
    checkArgument(s_length > 0, 'The character string length must be greater than 0.');
    checkArgument(s.charAt(0) !== WRITE_IN_ALPHABET[0], 'The character string must not start with rank 0 character.');

    // Operation.
    const x = writeInToIntegerAlgorithm.writeInToInteger({encryptionGroup: encryptionGroup}, s);

    return GqElement.fromSquareRoot(x.value, encryptionGroup);
  }

  return {
    writeInToQuadraticResidue: writeInToQuadraticResidue
  }
})();
