/*
 * (c) Copyright 2023 Swiss Post Ltd
 */

const {CHARACTER_LENGTH_OF_START_VOTING_KEY} = require("./constants");
const {validateUUID, validateBase32StringWithoutPadding} = require("../services/validation-service");
const {HashService} = require("crypto-primitives-ts/lib/cjs/hashing/hash_service");
const {Base16Service} = require("crypto-primitives-ts/lib/cjs/math/base16_service");
const {Argon2Profile} = require("crypto-primitives-ts/lib/cjs/hashing/argon2_profile");
const {Argon2Service} = require("crypto-primitives-ts/lib/cjs/hashing/argon2_service");
const {cutToBitLength} = require("crypto-primitives-ts/lib/cjs/arrays");
const {stringToByteArray} = require("crypto-primitives-ts/lib/cjs/conversions");

module.exports = (function () {
	'use strict';

	/**
	 * Derives a voter's identifier credentialId from the Start Voting Key SVK_id.
	 *
	 * @param {string} electionEventId, ee the election event id.
	 * @param {string} startVotingKey, SVK_id the start voting key.
	 * @return {Promise<string>} credentialID_id, the derived credentialID_id.
	 */
	async function deriveCredentialId(
		electionEventId,
		startVotingKey
	) {
		const l_SVK = CHARACTER_LENGTH_OF_START_VOTING_KEY;
		const SVK_id = validateBase32StringWithoutPadding(startVotingKey, l_SVK);
		const ee = validateUUID(electionEventId);

		const hashService = new HashService();
		const base16Service = new Base16Service();
		const argon2Service = new Argon2Service(Argon2Profile.LESS_MEMORY);

		const securityLevel = 128;
		const salt = cutToBitLength(hashService.recursiveHash(ee, 'credentialId'), securityLevel);

		const bCredentialID_id = await argon2Service.getArgon2id(stringToByteArray(SVK_id), salt);

		return base16Service.base16Encode(cutToBitLength(bCredentialID_id, securityLevel).value());
	}

	return {
		deriveCredentialId: deriveCredentialId
	}
})();
