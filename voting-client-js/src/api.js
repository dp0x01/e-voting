/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

const {sendVotePhase} = require('./voting-phase/send-vote/send-vote');
const {translateBallot} = require('./model/ballot-parser');
const {confirmVotePhase} = require('./voting-phase/confirm-vote/confirm-vote');
const {authenticateVoterPhase} = require('./voting-phase/authenticate-voter/authenticate-voter');

/**
 * Provides an api to the Voter-Portal to communicate with the Voting-Server for all the voting phases described in the protocol.
 */
window.OvApi = function () {

  return {
    /**
     * Authenticates the voter to the Voting-Server.
     *
     * @param {string} startVotingKey, the start voting key of a voter.
     * @param {string} extendedAuthenticationFactor, the extended authentication factor, such as a year of birth OR date of birth.
     * @param {string} electionEventId, the related election event identifier.
     * @param {string} lang, the supported language code.
     * @returns {Promise<AuthenticateVoterResponse>} the authenticateVoter response.
     */
    authenticateVoter: function (startVotingKey, extendedAuthenticationFactor, electionEventId, lang) {
      return authenticateVoterPhase(startVotingKey, extendedAuthenticationFactor, electionEventId, lang)
        .then(authenticateVoterResponse => Promise.resolve(authenticateVoterResponse))
        .catch(authenticateVoterResponseError => Promise.reject(authenticateVoterResponseError.errorResponse));
    },

    /**
     * Sends the vote to the Voting-Server.
     *
     * @param {string[]} selectedVotingOptions, the voter selected voting options.
     * @param {string[]} voterWriteIns, the voter write ins.
     * @returns {Promise<SendVoteResponse>} the sendVote response.
     */
    sendVote: function (selectedVotingOptions, voterWriteIns) {
      return sendVotePhase(selectedVotingOptions, voterWriteIns)
        .then(sendVoteResponse => Promise.resolve(sendVoteResponse))
        .catch(sendVoteResponseError => Promise.reject(sendVoteResponseError.errorResponse));
    },

    /**
     * Confirms the vote to the Voting-Server.
     *
     * @param {string} ballotCastingKey The 'ballot casting key'.
     * @returns {Promise<ConfirmVoteResponse>} The confirmVote response.
     */
    confirmVote: function (ballotCastingKey) {
      return confirmVotePhase(ballotCastingKey)
        .then(confirmVoteResponse => Promise.resolve(confirmVoteResponse))
        .catch(confirmVoteResponseError => Promise.reject(confirmVoteResponseError.errorResponse));
    },

    /**
     * Selects the i18n texts associated to the locale on the supplied ballot.
     * @param {Ballot} ballot, the ballot object
     * @param {string} lang, the supported language code.
     * @returns {void}
     */
    translateBallot: function (ballot, lang) {
      translateBallot(ballot, lang);
    },
  };
};
