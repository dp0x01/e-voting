/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

const {validateUUID, validateActualVotingOptions} = require("../../services/validation-service");
const {ZqGroup} = require("crypto-primitives-ts/lib/cjs/math/zq_group");
const {GqElement} = require("crypto-primitives-ts/lib/cjs/math/gq_element");
const {ZqElement} = require("crypto-primitives-ts/lib/cjs/math/zq_element");
const {GroupVector} = require("crypto-primitives-ts/lib/cjs/group_vector");
const {RandomService} = require("crypto-primitives-ts/lib/cjs/math/random_service");
const {integerToString} = require("crypto-primitives-ts/lib/cjs/conversions");
const {ZeroKnowledgeProofService} = require("crypto-primitives-ts/lib/cjs/zeroknowledgeproofs/zero_knowledge_proof_service");
const {checkArgument, checkNotNull} = require("crypto-primitives-ts/lib/cjs/validation/preconditions");
const {ElGamalMultiRecipientMessage} = require("crypto-primitives-ts/lib/cjs/elgamal/elgamal_multi_recipient_message");
const {ElGamalMultiRecipientPublicKey} = require("crypto-primitives-ts/lib/cjs/elgamal/elgamal_multi_recipient_public_key");
const {ElGamalMultiRecipientCiphertext} = require("crypto-primitives-ts/lib/cjs/elgamal/elgamal_multi_recipient_ciphertext");

module.exports = (function () {
  'use strict';

  // Maximum value of selectable voting options
  const phi = 120;

  /**
   * @typedef {object} CreateVoteContext
   * @property {GqGroup} encryptionGroup, the encryption parameters p, q and g.
   * @property {string} electionEventId ee, the election event id.
   * @property {number} numberOfSelectableVotingOptions psi, the number of selectable voting options.
   * @property {GroupVector<PrimeGqElement>} encodedVotingOptions p_tilde, the encoded voting options.
   * @property {string[]} actualVotingOptions v_tilde, the actual voting options.
   * @property {string[]} semanticInformation sigma, the semanticInformation.
   */

  /**
   * Implements the CreateVote algorithm described in the cryptographic protocol.
   *
   * @param {CreateVoteContext} context, the createVote context.
   * @param {string} verificationCardId vc_id, the verification card id.
   * @param {GroupVector<PrimeGqElement>} selectedEncodedVotingOptions p_id_hat, the voter's selected encoded voting options.
   * @param {GroupVector<GqElement>} encodedWriteIns w_id, the vector of encoded write-ins.
   * @param {ElGamalMultiRecipientPublicKey} electionPublicKey EL_pk, the election public key.
   * @param {ElGamalMultiRecipientPublicKey} choiceReturnCodesEncryptionPublicKey pk_CCR, the choice return codes encryption public key.
   * @param {ZqElement} verificationCardSecretKey k_id, the verification card secret key.
   * @returns {Vote}, the vote object.
   */
  function createVote(
    context,
    verificationCardId,
    selectedEncodedVotingOptions,
    encodedWriteIns,
    electionPublicKey,
    choiceReturnCodesEncryptionPublicKey,
    verificationCardSecretKey,
  ) {
    checkNotNull(context);
    const encryptionGroup = checkNotNull(context.encryptionGroup);
    const ee = validateUUID(context.electionEventId);
    const psi = context.numberOfSelectableVotingOptions;
    const p_tilde = checkNotNull(context.encodedVotingOptions);
    const v_tilde = checkNotNull(context.actualVotingOptions);
    validateActualVotingOptions(v_tilde);
    const sigma = checkNotNull(context.semanticInformation);

    const vc_id = validateUUID(verificationCardId);
    const p_id_hat = checkNotNull(selectedEncodedVotingOptions);
    p_id_hat.elements
      .map(p_i_hat => p_i_hat.value.intValue())
      .forEach(p_i_hat_intValue => checkArgument(
        p_tilde.elements.map(p_i_tilde => p_i_tilde.value.intValue()).includes(p_i_hat_intValue),
        'The selected encoded voting options must be a subset of the encoded voting options.'
      ));
    const w_id = checkNotNull(encodedWriteIns);
    const EL_pk = checkNotNull(electionPublicKey);
    const pk_CCR = checkNotNull(choiceReturnCodesEncryptionPublicKey);
    const k_id = checkNotNull(verificationCardSecretKey);

    const randomService = new RandomService();
    const zeroKnowledgeProofService = new ZeroKnowledgeProofService();
    const q = encryptionGroup.q;
    const delta_hat = encodedWriteIns.size + 1;
    const delta = EL_pk.size;

    // Cross-checks.
    checkArgument(p_tilde.size === v_tilde.length, "The encoded voting options and the actual voting options must have the same size.");
    checkArgument(p_id_hat.size === psi,
      "The size of the vector of selected encoded voting options must be equal to the number of selectable voting options");
    checkArgument(encryptionGroup.equals(p_id_hat.group),
      "The encryption group of the context must equal the encryption group of the selected encoded voting options.");
    checkArgument(w_id.size > 0 ? encryptionGroup.equals(w_id.group) : true,
      "The encryption group of the context must equal the encryption group of the encoded write-ins.");
    checkArgument(encryptionGroup.equals(EL_pk.group),
      "The encryption group of the context must equal the encryption group of the election public key.");
    checkArgument(encryptionGroup.equals(pk_CCR.group),
      "The encryption group of the context must equal the encryption group of the choice return codes encryption public key.");
    checkArgument(encryptionGroup.hasSameOrderAs(k_id.group),
      "The encryption group of the context must equal the order of the verification card secret key.");

    // Requires.
    checkArgument(new Set(p_id_hat.elements.map(p_i_hat => p_i_hat.value.intValue())).size === p_id_hat.size,
      "The voter cannot select the same option twice.");
    checkArgument(0 < psi && psi <= phi,
      "The number of selectable voting options must be greater than zero and less than or equal to the maximum value of voting options.");
    checkArgument(0 < delta_hat && delta_hat <= delta,
      "The number of encoded write-ins must be smaller than or equal to the number of election public key elements.");

    // Operation.
    const identity = encryptionGroup.identity;
    const reducer = (product, currentValue) => product.multiply(currentValue);
    const rho = p_id_hat.elements.reduce(reducer, identity);

    const r_value = randomService.genRandomInteger(q);
    const r = ZqElement.create(r_value, ZqGroup.sameOrderAs(encryptionGroup));

    const E1 = ElGamalMultiRecipientCiphertext.getCiphertext(new ElGamalMultiRecipientMessage([rho, ...w_id.elements]), r, EL_pk);

    const pCC_id_vector = [];
    for (let i = 0; i < psi; i++) {
      pCC_id_vector[i] = p_id_hat.elements[i].exponentiate(k_id);
    }

    const pCC_id = new ElGamalMultiRecipientMessage(pCC_id_vector);

    const r_prime_value = randomService.genRandomInteger(q);
    const r_prime = ZqElement.create(r_prime_value, ZqGroup.sameOrderAs(encryptionGroup));

    const E2 = ElGamalMultiRecipientCiphertext.getCiphertext(pCC_id, r_prime, pk_CCR);

    const E1_tilde = ElGamalMultiRecipientCiphertext.create(E1.gamma, [E1.get(0)]).getCiphertextExponentiation(k_id);

    const E2_tilde_phis_product = E2.phis.elements.reduce(reducer, identity);
    const E2_tilde = ElGamalMultiRecipientCiphertext.create(E2.gamma, [E2_tilde_phis_product]);

    const K_id = encryptionGroup.generator.exponentiate(k_id);

    const EL_pk_toString = EL_pk.stream().map(EL_pk_i => integerToString(EL_pk_i.value));
    const phi_1_toString = E1.phis.elements.map(phi_1_i => integerToString(phi_1_i.value));
    const p_tilde_toString = p_tilde.elements.map(p_tilde_i => integerToString(p_tilde_i.value));
    const i_aux = ["CreateVote", ee, vc_id, ...EL_pk_toString,
      ...phi_1_toString,
      "EncodedVotingOptions", ...p_tilde_toString,
      "ActualVotingOptions", ...v_tilde,
      "SemanticInformation", ...sigma];

    const bases = GroupVector.of(encryptionGroup.generator, E1.gamma, E1.get(0));
    const gamma_1_k_id = E1_tilde.gamma;
    const phi_1_0_k_id = E1_tilde.get(0);
    const exponentiations = GroupVector.of(K_id, gamma_1_k_id, phi_1_0_k_id);
    const pi_Exp = zeroKnowledgeProofService.genExponentiationProof(bases, k_id, exponentiations, i_aux);

    const pk_CCR_tilde = pk_CCR.stream().slice(0, psi).reduce(reducer, identity);

    const randomness = GroupVector.of(r.multiply(k_id), r_prime);
    const pi_EqEnc = zeroKnowledgeProofService.genPlaintextEqualityProof(E1_tilde, E2_tilde, EL_pk.get(0), pk_CCR_tilde, randomness, i_aux);

    /**
     * @typedef {object} Vote.
     * @property {ElGamalMultiRecipientCiphertext} encryptedVote E1, the encrypted vote.
     * @property {ElGamalMultiRecipientCiphertext} encryptedPartialChoiceReturnCodes E2, the encrypted partial Choice Return Codes.
     * @property {ElGamalMultiRecipientCiphertext} exponentiatedEncryptedVote E1_tilde, the exponentiated encrypted vote.
     * @property {ExponentiationProof} exponentiationProof pi_Exp, the exponentiation proof.
     * @property {PlaintextEqualityProof} plaintextEqualityProof pi_EqEnc, the plaintext equality proof.
     */
    return {
      encryptedVote: E1,
      encryptedPartialChoiceReturnCodes: E2,
      exponentiatedEncryptedVote: E1_tilde,
      exponentiationProof: pi_Exp,
      plaintextEqualityProof: pi_EqEnc
    };
  }

  return {
    createVote: createVote
  }
})();
