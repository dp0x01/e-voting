/*
 * (c) Copyright 2023 Swiss Post Ltd
 */

const {deriveCredentialId} = require("../../voter-authentication/derive-credential-id-algorithm");
const {CHARACTER_LENGTH_OF_START_VOTING_KEY} = require("../../voter-authentication/constants");
const {deriveBaseAuthenticationChallenge} = require("../../voter-authentication/derive-base-authentication-challenge-algorithm");
const {validateUUID, validateBase32StringWithoutPadding, validateExtendedAuthenticationFactor} = require("../../services/validation-service");
const {HashService} = require("crypto-primitives-ts/lib/cjs/hashing/hash_service");
const {Argon2Profile} = require("crypto-primitives-ts/lib/cjs/hashing/argon2_profile");
const {Argon2Service} = require("crypto-primitives-ts/lib/cjs/hashing/argon2_service");
const {Base64Service} = require("crypto-primitives-ts/lib/cjs/math/base64_service");
const {cutToBitLength} = require("crypto-primitives-ts/lib/cjs/arrays");
const {ImmutableBigInteger} = require("crypto-primitives-ts/lib/cjs/immutable_big_integer");
const {ImmutableUint8Array} = require("crypto-primitives-ts/lib/cjs/immutable_uint8Array");
const {checkArgument, checkNotNull} = require("crypto-primitives-ts/lib/cjs/validation/preconditions");
const {stringToByteArray, integerToByteArray} = require("crypto-primitives-ts/lib/cjs/conversions");
const {RandomService} = require("crypto-primitives-ts/lib/cjs/math/random_service");

module.exports = (function () {
	'use strict';

	const AUTHENTICATION_STEPS = ['authenticateVoter', 'sendVote', 'confirmVote'];
	const TWO_POW_256 = ImmutableBigInteger.fromString("115792089237316195423570985008687907853269984665640564039457584007913129639936");

	/**
	 * @typedef {object} AuthenticationChallengeContext
	 * @property {string} electionEventId, ee the election event id.
	 * @property {string} authenticationStep, authStep the corresponding authentication step.
	 */

	/**
	 * Implements the GetAuthenticationChallenge algorithm described in the cryptographic protocol.
	 *
	 * @param {AuthenticationChallengeContext} context, the AuthenticationChallenge context.
	 * @param {string} startVotingKey, svk_id the start voting key.
	 * @param {string} extendedAuthenticationFactor, EA_id the extended authentication factor.
	 * @returns {AuthenticationChallenge}, the vote object.
	 */
	async function getAuthenticationChallenge(
		context,
		startVotingKey,
		extendedAuthenticationFactor
	) {
		checkNotNull(context);
		const ee = validateUUID(context.electionEventId);
		const authStep = checkNotNull(context.authenticationStep);
		checkArgument(AUTHENTICATION_STEPS.includes(authStep), "The authentication step must be one of the valid values.")
		const l_SVK = CHARACTER_LENGTH_OF_START_VOTING_KEY;
		const SVK_id = validateBase32StringWithoutPadding(startVotingKey, l_SVK);
		const EA_id = validateExtendedAuthenticationFactor(extendedAuthenticationFactor);

		const hashService = new HashService();
		const argon2Service = new Argon2Service(Argon2Profile.LESS_MEMORY);
		const base64Service = new Base64Service();

		const credentialID_id = await deriveCredentialId(ee, SVK_id);
		const hAuth_id = await deriveBaseAuthenticationChallenge(ee, SVK_id, EA_id);
		const nonce = new RandomService().genRandomInteger(TWO_POW_256);
		const TS = getTimeStamp();
		const T = Math.floor(TS / 30);
		const salt_id = cutToBitLength(hashService.recursiveHash(ee, credentialID_id, "dAuth", authStep, nonce), 128);
		const bhhAuth_id = await argon2Service.getArgon2id(ImmutableUint8Array.from([...stringToByteArray(hAuth_id).value(), ...integerToByteArray(ImmutableBigInteger.fromNumber(T)).value()]), salt_id);
		const hhAuth_id = base64Service.base64Encode(bhhAuth_id.value());

		/**
		 * @typedef {object} AuthenticationChallenge.
		 * @property {string} derivedVoterIdentifier, credentialID_id The derived voter identifier.
		 * @property {string} derivedAuthenticationChallenge, hhAuth_id The derived authentication challenge.
		 */
		return {
			derivedVoterIdentifier: credentialID_id,
			derivedAuthenticationChallenge: hhAuth_id,
			authenticationNonce: nonce.toString(10)
		}
	}

	/**
	 * Gets a time stamp of the current UNIX time in seconds.
	 *
	 * @return {number} the number of seconds passed since 1 January 1970
	 */
	const getTimeStamp = function () {
		return Math.floor(Date.now() / 1000);
	}

	return {
		getAuthenticationChallenge: getAuthenticationChallenge
	}
})();
