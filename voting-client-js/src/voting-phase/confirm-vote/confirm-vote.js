/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
/* global require */

const {validateUUID, validateBallotCastingKey, validateBase32StringWithoutPadding} = require("../../services/validation-service");
const sessionService = require("../../services/session-service");
const endpointService = require("../../services/endpoint-service");
const {VoterPortalInputError} = require("../../services/voter-portal-input-error");
const {VotingServerResponseError} = require("../../services/voting-server-response-error");
const createConfirmMessageAlgorithm = require("./create-confirm-message-algorithm");
const getAuthenticationChallengeAlgorithm = require("../authenticate-voter/get-authentication-challenge-algorithm");
const {serializeGroupElement, serializeGqGroup} = require("../../services/primitives-serializer");
const {CHARACTER_LENGTH_OF_BALLOT_CASTING_KEY, CONFIRMATION_KEY_INCORRECT_ERROR_CODE} = require("../../voter-authentication/constants");

module.exports = (function () {
  'use strict';

  /**
   * Implements the confirmVote phase of the protocol.
   *
   * @param {string} ballotCastingKey, the voter ballot casting key.
   * @returns {Promise<ConfirmVoteResponse>}, the confirmVote response expected by the Voter-Portal.
   */
  async function confirmVotePhase(ballotCastingKey) {

    validateBallotCastingKeyInput(ballotCastingKey);

    const primitivesParams = sessionService.primitivesParams();
    const voterAuthenticationData = sessionService.voterAuthenticationData();

    // Call CreateConfirmMessage algorithm
    const confirmationKey = createConfirmMessageAlgorithm.createConfirmMessage(
      {
        encryptionGroup: primitivesParams.encryptionGroup,
        electionEventId: validateUUID(voterAuthenticationData.electionEventId),
        characterLengthOfTheBallotCastingKey: CHARACTER_LENGTH_OF_BALLOT_CASTING_KEY
      },
      ballotCastingKey,
      sessionService.verificationCardSecretKey()
    );

    // Call GetAuthenticationChallenge algorithm
    const authenticationChallenge = await getAuthenticationChallengeAlgorithm.getAuthenticationChallenge(
      {
        electionEventId: voterAuthenticationData.electionEventId,
        authenticationStep: 'confirmVote'
      },
      sessionService.startVotingKey(),
      sessionService.extendedAuthenticationFactor(),
    );

    // Prepare and post payload to the Voting Server
    const confirmVoteResponsePayload = await confirmVoteToVotingServer(confirmationKey, authenticationChallenge);

    /**
     * @typedef {object} ConfirmVoteResponse.
     * @property {string} voteCastReturnCode, the vote cast return code.
     */
    return {
      voteCastReturnCode: confirmVoteResponsePayload.shortVoteCastReturnCode
    }
  }

  /**
   * Throws a Voter Portal input error if the ballot casting key is not valid.
   * @param {string} ballotCastingKey, the voter ballot casting key.
   */
  function validateBallotCastingKeyInput(ballotCastingKey) {
    try {
      validateBallotCastingKey(ballotCastingKey);
    } catch (_error) {
      throw new VoterPortalInputError(CONFIRMATION_KEY_INCORRECT_ERROR_CODE);
    }
  }

  /**
   * Confirms the vote to the Voting-Server.
   *
   * @param {GqElement} confirmationKey, the confirmVote payload.
   * @param {AuthenticationChallenge} authenticationChallenge, the authentication challenge.
   * @returns {ConfirmVoteResponsePayload}, the confirmVote response payload produced by the Voting-Server.
   */
  async function confirmVoteToVotingServer(confirmationKey, authenticationChallenge) {
    const primitivesParams = sessionService.primitivesParams();
    const voterAuthenticationData = sessionService.voterAuthenticationData();

    /**
     * @typedef {object} ContextIds.
     * @property {string} electionEventId, the election event id.
     * @property {string} verificationCardSetId, the verification card set id.
     * @property {string} verificationCardId, the verification card id.
     */
    /**
     * @typedef {object} ConfirmVotePayload.
     * @property {ContextIds} contextIds, the context ids.
     * @property {AuthenticationChallenge} authenticationChallenge, the authentication challenge.
     * @property {string} confirmationKey, the serialized confirmation key.
     * @property {string} encryptionGroup, the serialized encryption group.
     */
    const confirmVotePayload = {
      contextIds: {
        electionEventId: voterAuthenticationData.electionEventId,
        verificationCardSetId: voterAuthenticationData.verificationCardSetId,
        verificationCardId: voterAuthenticationData.verificationCardId
      },
      authenticationChallenge: authenticationChallenge,
      confirmationKey: serializeGroupElement(confirmationKey),
      encryptionGroup: JSON.parse(serializeGqGroup(primitivesParams.encryptionGroup))
    };

    // Prepare endpoint
    const confirmVoteEndpoint = endpointService.confirmVote(
      voterAuthenticationData.electionEventId,
      voterAuthenticationData.verificationCardSetId,
      voterAuthenticationData.credentialId,
      voterAuthenticationData.verificationCardId
    );

    // Post the payload
    const response = await fetch(confirmVoteEndpoint,
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json;charset=UTF-8'
        },
        body: JSON.stringify(confirmVotePayload)
      }
    );

    // Check if errors
    if (!response.ok) {
      const errorJson = await response.json();
      throw new VotingServerResponseError(errorJson);
    }

    /**
     * @typedef {object} ConfirmVoteResponsePayload.
     * @property {string} shortVoteCastReturnCode, the short vote cast return code.
     */
    return response.json();
  }

  return {
    confirmVotePhase: confirmVotePhase
  };
})();
