/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

const {HashService} = require("crypto-primitives-ts/lib/cjs/hashing/hash_service");
const {stringToInteger} = require("crypto-primitives-ts/lib/cjs/conversions");
const {checkNotNull, checkArgument} = require("crypto-primitives-ts/lib/cjs/validation/preconditions");

module.exports = (function () {
  'use strict';

  /**
   * @typedef {object} CreateConfirmMessageContext
   * @property {GqGroup} encryptionGroup, the encryption parameters p, q and g.
   * @property {string} electionEventId ee, the election event id.
   * @property {number} characterLengthOfTheBallotCastingKey l_BCK, the length of the Ballot Casting Key.
   */

  /**
   * Implements the CreateConfirmMessage algorithm described in the cryptographic protocol.
   * Generates a confirmation key.
   *
   * @param {CreateConfirmMessageContext} context, the CreateConfirmMessage context.
   * @param {string} ballotCastingKey BCK_id, the ballot casting key.
   * @param {ZqElement} verificationCardSecretKey k_id, the verification card secret key.
   * @returns {GqElement} CK_id, the confirmation key.
   */
  function createConfirmMessage(
    context,
    ballotCastingKey,
    verificationCardSecretKey
  ) {
    checkNotNull(context);
    const encryptionGroup = checkNotNull(context.encryptionGroup);
    const l_BCK = checkNotNull(context.characterLengthOfTheBallotCastingKey);

    const BCK_id = checkNotNull(ballotCastingKey);
    checkArgument(BCK_id.length === l_BCK, `The ballot casting key length must be ${l_BCK}`);
    checkArgument(BCK_id.match("^\\d+$") != null, "The ballot casting key must be a numeric value");
    checkArgument(BCK_id.match("^0+$") == null, "The ballot casting key must contain one non-zero element");
    const k_id = checkNotNull(verificationCardSecretKey);

    const hashService = new HashService();

    // Cross-checks.
    checkArgument(encryptionGroup.hasSameOrderAs(k_id.group),
      "The encryption group of the context must equal the order of the verification card secret key.");


    // Operation.
    const hBCK_id = hashService.hashAndSquare(stringToInteger(BCK_id), encryptionGroup);
    // CK_id
    return hBCK_id.exponentiate(k_id);
  }

  return {
    createConfirmMessage: createConfirmMessage
  }
})();
