/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

/* jshint maxlen: 6666 */

const primitivesParamsParser = require("../../../src/services/primitives-params-service");
const getKeyAlgorithm = require("../../../src/voting-phase/authenticate-voter/get-key-algorithm");
const authResponse = require("../../tools/data/authResponse-get-key.json");

describe('Get key algorithm', function () {

	it('should return a private key', async function () {
		const primitivesParams = primitivesParamsParser.parsePrimitivesParams(authResponse);

		// NOSONAR
		const key = await getKeyAlgorithm.getKey(
			{
				encryptionGroup: primitivesParams.encryptionGroup,
				electionEventId: authResponse.authenticationToken.voterAuthenticationData.electionEventId,
				verificationCardSetId: authResponse.authenticationToken.voterAuthenticationData.verificationCardSetId,
				electionPublicKey: primitivesParams.electionPublicKey,
				choiceReturnCodesEncryptionPublicKey: primitivesParams.choiceReturnCodesEncryptionPublicKey,
				encodedVotingOptions: primitivesParams.encodedVotingOptions,
				actualVotingOptions: primitivesParams.actualVotingOptions,
				semanticInformation: primitivesParams.semanticInformation,
				ciSelections: primitivesParams.correctnessInformationSelections,
				ciVotingOptions: primitivesParams.correctnessInformationVotingOptions,
				characterLengthOfTheStartVotingKey: 24
			},
			'qy7qinbqzu2mwntvqkyugeuj',
			authResponse.verificationCard.verificationCardKeystore,
			authResponse.verificationCard.id
		);

		expect(null).not.toBe(key);
	}, 30000);

});


