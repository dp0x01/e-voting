/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

/* jshint node:true */
'use strict';

const {ImmutableBigInteger} = require("crypto-primitives-ts/lib/cjs/immutable_big_integer");
const {checkArgument} = require("crypto-primitives-ts/lib/cjs/validation/preconditions");
const {RandomService} = require("crypto-primitives-ts/lib/cjs/math/random_service");
const {GqElement} = require("crypto-primitives-ts/lib/cjs/math/gq_element");
const {PrimeGqElement} = require("crypto-primitives-ts/lib/cjs/math/prime_gq_element");
const {Primes} = require("crypto-primitives-ts/lib/cjs/math/primes");

module.exports = function (gqGroup) {
    const MAX_GROUP_SIZE = ImmutableBigInteger.fromNumber(1000);
    const MAXIMUM_NUMBER_OF_VOTING_OPTIONS = 3000;
    const random = new RandomService();
    const groupMembers = getMembers();

    function genRandomMembers(count) {
        checkArgument(count <= groupMembers.length, "It would take too much time to generate as many members.");

        const randomIndexes = new Set();

        while (randomIndexes.size < count) {
            randomIndexes.add(random.nextInt(groupMembers.length));
        }

        return Array.from(randomIndexes).map(index => groupMembers[index]);
    }

    function genRandomGqElements(count) {
        return genRandomMembers(count).map(groupMember => GqElement.fromValue(groupMember, gqGroup));
    }

    function getSmallPrimeMembers(count) {
        checkArgument(count <= MAXIMUM_NUMBER_OF_VOTING_OPTIONS);

        let current = ImmutableBigInteger.fromNumber(5);
        let p_vector = [];
        let i = 0;
        while (i < count && current.compareTo(gqGroup.p) < 0 && current.compareTo(ImmutableBigInteger.fromNumber(2147483647)) < 0) {
            if (gqGroup.isGroupMember(current) && Primes.isSmallPrime(current.intValue()) && !current.equals(gqGroup.generator.value)) {
                p_vector = [...p_vector, PrimeGqElement.fromValue(current.intValue(), gqGroup)];
                i = i + 1;
            }
            current =
                current.add(ImmutableBigInteger.fromNumber(2));
        }
        return p_vector;
    }

    function getMembers() {
        return integersModP()
            .map(bigInteger => bigInteger.modPow(ImmutableBigInteger.fromNumber(2), gqGroup.p))
            .filter(bigInteger => !bigInteger.equals(ImmutableBigInteger.ZERO));
    }

    function integersModP() {
        const length = Math.min(gqGroup.p.intValue(), MAX_GROUP_SIZE.intValue());
        return Array.from({length}, (_, i) => ImmutableBigInteger.fromNumber(i + 1));
    }

    return {
        genRandomGqElements: genRandomGqElements,
        getSmallPrimeMembers: getSmallPrimeMembers
    };

};
