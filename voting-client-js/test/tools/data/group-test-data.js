/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

/* jshint node:true */
'use strict';

const {ImmutableBigInteger} = require("crypto-primitives-ts/lib/cjs/immutable_big_integer");
const {GqGroup} = require("crypto-primitives-ts/lib/cjs/math/gq_group");
const {ImmutableArray} = require("crypto-primitives-ts/lib/cjs/immutable_array");
const {RandomService} = require("crypto-primitives-ts/lib/cjs/math/random_service");

const smallTestGroups = getSmallTestGroups();
const random = new RandomService();

module.exports = {
	/**
	 * Creates a random GqGroup for testing purposes.
	 *
	 * @function getGqGroup
	 * @returns {GqGroup} random test GqGroup.
	 */
	getGqGroup: function () {
		return getRandomGqGroupFrom(smallTestGroups);
	}
};

function getRandomGqGroupFrom(groups) {
	return groups.get(random.nextInt(groups.length));
}

function getSmallTestGroups() {
	const p1 = ImmutableBigInteger.fromNumber(11);
	const q1 = ImmutableBigInteger.fromNumber(5);
	const g1 = ImmutableBigInteger.fromNumber(3);
	const group1 = new GqGroup(p1, q1, g1);
	const p2 = ImmutableBigInteger.fromNumber(23);
	const q2 = ImmutableBigInteger.fromNumber(11);
	const g2 = ImmutableBigInteger.fromNumber(2);
	const group2 = new GqGroup(p2, q2, g2);
	const p3 = ImmutableBigInteger.fromNumber(47);
	const q3 = ImmutableBigInteger.fromNumber(23);
	const g3 = ImmutableBigInteger.fromNumber(2);
	const group3 = new GqGroup(p3, q3, g3);
	const p4 = ImmutableBigInteger.fromNumber(59);
	const q4 = ImmutableBigInteger.fromNumber(29);
	const g4 = ImmutableBigInteger.fromNumber(3);
	const group4 = new GqGroup(p4, q4, g4);
	return ImmutableArray.of(group1, group2, group3, group4);
}
