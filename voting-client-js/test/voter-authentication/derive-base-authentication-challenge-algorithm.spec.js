/*
 * (c) Copyright 2023 Swiss Post Ltd
 */

const deriveBaseAuthenticationChallengeAlgorithm = require("../../src/voter-authentication/derive-base-authentication-challenge-algorithm");
const {NullPointerError} = require("crypto-primitives-ts/lib/cjs/error/null_pointer_error");
const {FailedValidationError} = require("../../src/services/failed_validation_error");

describe('Derive base authentication challenge', function () {

	const electionEventId = '34caee78ed3d4cf981ca06b659f558eb';
	const startVotingKey = '4d65ej2adb4ia6ghhzb52kg6';
	const extendedAuthenticationFactor = '01061944'
	const challenge = 'dSkssGP8FYAhcBsPwGTgQ6y4AIiJwI8mvZ5yAqS8lW0='

	it('should return expected challenge', async function () {
		deriveBaseAuthenticationChallengeAlgorithm.deriveBaseAuthenticationChallenge(
			electionEventId,
			startVotingKey,
			extendedAuthenticationFactor).then(actualChallenge => expect(challenge).toBe(actualChallenge))
	}, 30000);

	describe('should throw an Error when given null arguments', function () {
		it('election event id', async function () {
			await expectAsync(deriveBaseAuthenticationChallengeAlgorithm.deriveBaseAuthenticationChallenge(
				null,
				startVotingKey,
				extendedAuthenticationFactor)).toBeRejectedWith(new NullPointerError())
		}, 30000);

		it('start voting key', async function () {
			await expectAsync(deriveBaseAuthenticationChallengeAlgorithm.deriveBaseAuthenticationChallenge(
				electionEventId,
				null,
				extendedAuthenticationFactor)).toBeRejectedWith(new NullPointerError())
		}, 30000);

		it('extended authentication factor', async function () {
			await expectAsync(deriveBaseAuthenticationChallengeAlgorithm.deriveBaseAuthenticationChallenge(
				electionEventId,
				startVotingKey,
				null)).toBeRejectedWith(new NullPointerError())
		}, 30000);
	});

	it('should throw an Error when given non UUID election event id', async function () {
		const nonUuidElectionEventId = '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!';
		const expectedErrorMessage = `The given string does not comply with the required format. [string: ${nonUuidElectionEventId}, format: ^[a-fA-F0-9]{32}$].`;

		await expectAsync(deriveBaseAuthenticationChallengeAlgorithm.deriveBaseAuthenticationChallenge(
			nonUuidElectionEventId,
			startVotingKey,
			extendedAuthenticationFactor)).toBeRejectedWith(new FailedValidationError(expectedErrorMessage))
	}, 30000);

	it('should throw an Error when given non Base32 start voting key', async function () {
		const nonBase32StartVotingKey = '111111111111111111111111';
		const expectedErrorMessage = `The given string does not comply with the required format. [string: ${nonBase32StartVotingKey}, format: ^[a-z2-7]{24}$].`;

		await expectAsync(deriveBaseAuthenticationChallengeAlgorithm.deriveBaseAuthenticationChallenge(
			electionEventId,
			nonBase32StartVotingKey,
			extendedAuthenticationFactor)).toBeRejectedWith(new FailedValidationError(expectedErrorMessage))
	}, 30000);

	describe('should throw an Error when given non compliant extended authentication factor', function () {
		it('non digit', async function () {
			const nonCompliantExtendedAuthenticationFactor = 'a106';
			const expectedErrorMessage = 'The given string does not comply with the required format. [string: a106, format: ^(\\d{4})(\\d{4})?$].';

			await expectAsync(deriveBaseAuthenticationChallengeAlgorithm.deriveBaseAuthenticationChallenge(
				electionEventId,
				startVotingKey,
				nonCompliantExtendedAuthenticationFactor)).toBeRejectedWith(new FailedValidationError(expectedErrorMessage))
		}, 30000);

		it('incorrect size', async function () {
			const nonCompliantExtendedAuthenticationFactor = '010644';
			const expectedErrorMessage = 'The given string does not comply with the required format. [string: 010644, format: ^(\\d{4})(\\d{4})?$].';

			await expectAsync(deriveBaseAuthenticationChallengeAlgorithm.deriveBaseAuthenticationChallenge(
				electionEventId,
				startVotingKey,
				nonCompliantExtendedAuthenticationFactor)).toBeRejectedWith(new FailedValidationError(expectedErrorMessage))
		}, 30000);
	});
})
