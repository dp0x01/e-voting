# Building guide

To easily build the e-voting system, we provide a Docker image available
on [GitLab](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev/container_registry/3223064). This is the recommended way to build the
e-voting system
since it requires no installation nor configuration on the user machine, except having to install Docker.

## Prerequesites

- **Install Docker**

  Please refer to the official [Get Docker](https://docs.docker.com/get-docker/) guide.

- **GitLab connectivity**

  The provided Docker image and e-voting system are published on GitLab.
  
- **Availability of a published compatible end-to-end**

  Please check if the corresponding version of "evoting-e2e-dev" exists.

## Pull the Docker image

Run the command

```sh
docker pull registry.gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev/ev/evoting-build:<VERSION>
```

For example, if you want to build e-voting 1.3.1.x, you need to run

```sh
docker pull registry.gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev/ev/evoting-build:1.3.1
```

## Run the Docker image in a container

Run the command

```sh
docker run -v <SHARED_VOLUME>:/home/baseuser/data -v //var/run/docker.sock:/var/run/docker.sock -it -t registry.gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev/ev/evoting-build:<VERSION>
```

For example, if you want to share `c:/tmp` and use the evoting-build Docker image 1.3.1 on a Windows machine, you need to run

```sh
winpty docker run -v c:\\tmp:/home/baseuser/data -v //var/run/docker.sock:/var/run/docker.sock -it -t registry.gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev/ev/evoting-build:1.3.1
```

If you select the “Use MinTTY” option when you have installed your bash, your Bash prompt will be hosted in the MinTTY terminal emulator, rather than the CMD console that ships with Windows. The MinTTY terminal emulator is not compatible with Windows console programs unless you prefix your commands with winpty.

As we build on linux and our artifacts are created with Windows we need 'Wine'.
It is automatically downloaded in the 'evoting-build' docker image.


## Run the e-voting build in the Docker container

After having successfully ran the Docker image, you are connected to the bash of the Docker container. Now, in the container's bash, you need to run:

```sh
./build.sh --version <VERSION>
```

For example, if you want to build the e-voting system 1.3.1.0, you need to run:

```sh
./build.sh --version 1.3.1.0
```

Once the process of building is achieved, you can exit the container with the `exit` command.

In the shared volume of your machine (for example `c:/tmp`), there will be

- a `build.tar.gz` archive containing the built e-voting system
- an `e2e.sh` script that you can use to configure and deploy locally all services needed to run an e-voting end-to-end test.

## Remarks

- For detailed instructions on how to use the e2e.sh script, please refer to
  the [README.md](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev/-/blob/master/README.md) of the end-to-end repository.
- Our build is fully [reproducible](https://reproducible-builds.org/), allowing researchers to verify the path from source code to binaries.
