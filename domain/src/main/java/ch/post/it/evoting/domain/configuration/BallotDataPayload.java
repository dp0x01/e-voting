/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
package ch.post.it.evoting.domain.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;

public record BallotDataPayload(String electionEventId,
								String ballotId,
								Ballot ballot,
								String ballotTextsJson) {

	public BallotDataPayload {
		validateUUID(electionEventId);
		validateUUID(ballotId);
		checkNotNull(ballot);
		checkNotNull(ballotTextsJson);
	}

}
