/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.voting.sendvote;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ch.post.it.evoting.cryptoprimitives.domain.signature.SignedPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

@JsonDeserialize(using = ControlComponentLCCSharePayloadDeserializer.class)
@JsonPropertyOrder({ "encryptionGroup", "longChoiceReturnCodesShare", "signature" })
public class ControlComponentLCCSharePayload implements SignedPayload {

	private final GqGroup encryptionGroup;

	private final LongChoiceReturnCodesShare longChoiceReturnCodesShare;

	private CryptoPrimitivesSignature signature;

	public ControlComponentLCCSharePayload(
			final GqGroup encryptionGroup,
			final LongChoiceReturnCodesShare longChoiceReturnCodesShare,
			final CryptoPrimitivesSignature signature) {
		this(encryptionGroup, longChoiceReturnCodesShare);
		this.signature = checkNotNull(signature);
	}

	public ControlComponentLCCSharePayload(final GqGroup encryptionGroup, final LongChoiceReturnCodesShare longChoiceReturnCodesShare) {
		this.encryptionGroup = checkNotNull(encryptionGroup);
		this.longChoiceReturnCodesShare = checkNotNull(longChoiceReturnCodesShare);

		checkArgument(encryptionGroup.equals(longChoiceReturnCodesShare.longChoiceReturnCodeShare().getGroup()),
				"The groups of the longChoiceReturnCodesShare and the control component LCC share must be equal.");
	}

	public GqGroup getEncryptionGroup() {
		return encryptionGroup;
	}

	public LongChoiceReturnCodesShare getLongChoiceReturnCodesShare() {
		return longChoiceReturnCodesShare;
	}

	@Override
	public CryptoPrimitivesSignature getSignature() {
		return signature;
	}

	@Override
	public void setSignature(final CryptoPrimitivesSignature signature) {
		this.signature = checkNotNull(signature);
	}

	@Override
	public List<Hashable> toHashableForm() {
		return List.of(encryptionGroup, longChoiceReturnCodesShare);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final ControlComponentLCCSharePayload that = (ControlComponentLCCSharePayload) o;
		return encryptionGroup.equals(that.encryptionGroup) &&
				longChoiceReturnCodesShare.equals(that.longChoiceReturnCodesShare) &&
				Objects.equals(signature, that.signature);
	}

	@Override
	public int hashCode() {
		return Objects.hash(encryptionGroup, longChoiceReturnCodesShare, signature);
	}
}
