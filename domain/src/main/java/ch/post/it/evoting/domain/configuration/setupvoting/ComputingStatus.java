/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration.setupvoting;

public enum ComputingStatus {
	COMPUTING,
	COMPUTED,
	COMPUTING_ERROR
}
