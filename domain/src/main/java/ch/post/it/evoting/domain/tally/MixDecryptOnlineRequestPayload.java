/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.tally;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.stream.IntStream;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.cryptoprimitives.utils.Validations;

public record MixDecryptOnlineRequestPayload(String electionEventId,
											 String ballotBoxId,
											 int nodeId,
											 List<ControlComponentShufflePayload> controlComponentShufflePayloads) {

	public MixDecryptOnlineRequestPayload {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);
		checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);
		checkNotNull(List.copyOf(controlComponentShufflePayloads));

		checkArgument(controlComponentShufflePayloads.size() == nodeId - 1, "The must be exactly (nodeId-1) control component shuffle payloads.");
		checkArgument(
				controlComponentShufflePayloads.isEmpty() || controlComponentShufflePayloads.get(0).getElectionEventId().equals(electionEventId),
				"The election event id of the control component shuffle payloads and the mix decrypt online request payload must be the same.");
		checkArgument(
				controlComponentShufflePayloads.isEmpty() || controlComponentShufflePayloads.get(0).getBallotBoxId().equals(ballotBoxId),
				"The ballot box id of the control component shuffle payloads and the mix decrypt online request payload must be the same.");

		checkArgument(Validations.allEqual(controlComponentShufflePayloads.stream(), ControlComponentShufflePayload::getElectionEventId),
				"All control component shuffle payloads must have the same election event id.");
		checkArgument(Validations.allEqual(controlComponentShufflePayloads.stream(), ControlComponentShufflePayload::getBallotBoxId),
				"All control component shuffle payloads must have the same ballot box id.");
		checkArgument(Validations.allEqual(controlComponentShufflePayloads.stream(), ControlComponentShufflePayload::getEncryptionGroup),
				"All control component shuffle payloads must have the same encryption group.");

		checkArgument(IntStream.range(1, nodeId)
						.allMatch(orderedNodeId -> controlComponentShufflePayloads.get(orderedNodeId - 1).getNodeId() == orderedNodeId),
				"The control component payloads must be sorted ascending by node id.");
	}
}
