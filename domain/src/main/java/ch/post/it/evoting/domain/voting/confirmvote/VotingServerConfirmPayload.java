/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.voting.confirmvote;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ch.post.it.evoting.cryptoprimitives.domain.signature.SignedPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

@JsonPropertyOrder({ "encryptionGroup", "confirmationKey", "signature" })
@JsonDeserialize(using = VotingServerConfirmPayloadDeserializer.class)
public class VotingServerConfirmPayload implements SignedPayload {

	@JsonProperty
	private final GqGroup encryptionGroup;

	@JsonProperty
	private final ConfirmationKey confirmationKey;

	@JsonProperty
	private CryptoPrimitivesSignature signature;

	@JsonCreator
	public VotingServerConfirmPayload(

			@JsonProperty("encryptionGroup")
			final GqGroup encryptionGroup,

			@JsonProperty("confirmationKey")
			final ConfirmationKey confirmationKey,

			@JsonProperty("signature")
			final CryptoPrimitivesSignature signature) {

		this(encryptionGroup, confirmationKey);
		this.signature = checkNotNull(signature);
	}

	public VotingServerConfirmPayload(final GqGroup encryptionGroup, final ConfirmationKey confirmationKey) {
		this.encryptionGroup = checkNotNull(encryptionGroup);
		this.confirmationKey = checkNotNull(confirmationKey);

		checkArgument(encryptionGroup.equals(confirmationKey.element().getGroup()), "The confirmation key must be in the encryption group");
	}

	public GqGroup getEncryptionGroup() {
		return encryptionGroup;
	}

	public ConfirmationKey getConfirmationKey() {
		return confirmationKey;
	}

	public CryptoPrimitivesSignature getSignature() {
		return signature;
	}

	public void setSignature(final CryptoPrimitivesSignature signature) {
		this.signature = checkNotNull(signature);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final VotingServerConfirmPayload that = (VotingServerConfirmPayload) o;
		return encryptionGroup.equals(that.encryptionGroup) && confirmationKey.equals(that.confirmationKey)
				&& Objects.equals(signature, that.signature);
	}

	@Override
	public int hashCode() {
		return Objects.hash(encryptionGroup, confirmationKey, signature);
	}

	@Override
	public List<Hashable> toHashableForm() {
		return List.of(encryptionGroup, confirmationKey);
	}
}
