/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ExponentiationProofGroupVectorDeserializer;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.GqGroupVectorDeserializer;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;

public record PartiallyDecryptedEncryptedPCC(ContextIds contextIds,
											 int nodeId,
											 @JsonDeserialize(using = GqGroupVectorDeserializer.class)
											 GroupVector<GqElement, GqGroup> exponentiatedGammas,
											 @JsonDeserialize(using = ExponentiationProofGroupVectorDeserializer.class)
											 GroupVector<ExponentiationProof, ZqGroup> exponentiationProofs) implements HashableList {

	public PartiallyDecryptedEncryptedPCC {
		checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);
		checkNotNull(contextIds);
		checkNotNull(exponentiatedGammas);
		checkNotNull(exponentiationProofs);
		checkArgument(exponentiatedGammas.getGroup().hasSameOrderAs(exponentiationProofs.getGroup()),
				"The groups of the exponentiation gammas and the exponentation proofs must be of same order.");
	}

	@Override
	public List<Hashable> toHashableForm() {
		return List.of(
				contextIds,
				HashableBigInteger.from(BigInteger.valueOf(nodeId)),
				exponentiatedGammas,
				exponentiationProofs);
	}

}
