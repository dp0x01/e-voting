/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.signers;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.xml.XsdConstants;
import ch.post.it.evoting.evotinglibraries.xml.hashable.HashableCantonConfigFactory;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;

public class CantonConfigSigner extends XmlSigner<Configuration> {

	public CantonConfigSigner(final SignatureKeystore<Alias> signatureKeystore) {
		super(signatureKeystore,
				Configuration::setSignature,
				Configuration::getSignature,
				HashableCantonConfigFactory::fromConfiguration,
				configuration -> ChannelSecurityContextData.cantonConfig(),
				Alias.CANTON,
				Configuration.class,
				CantonConfigSigner.class.getResource(XsdConstants.CANTON_CONFIG_XSD));
	}
}
