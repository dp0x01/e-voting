/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.tally;

public enum BallotBoxStatus {
	UNKNOWN,
	LOCKED,
	READY,
	SIGNED,
	UPDATED,
	MIXING_NOT_STARTED,
	MIXING,
	MIXED,
	MIXING_ERROR,
	DOWNLOADED,
	DECRYPTING,
	DECRYPTED
}
