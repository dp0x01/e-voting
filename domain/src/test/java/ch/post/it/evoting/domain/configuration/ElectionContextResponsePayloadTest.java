/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Locale;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;

class ElectionContextResponsePayloadTest {

	private static final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
	private static final Random random = RandomFactory.createRandom();
	private static ElectionContextResponsePayload electionContextResponsePayload;
	private static ObjectNode rootNode;

	@BeforeAll
	static void setupAll() {

		// Create payload.
		final String electionEventId = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
		final int nodeId = 1;
		electionContextResponsePayload = new ElectionContextResponsePayload(nodeId, electionEventId);

		// Create expected Json.
		rootNode = mapper.createObjectNode();
		rootNode.put("nodeId", nodeId);
		rootNode.put("electionEventId", electionEventId);
	}

	@Test
	@DisplayName("serialized gives expected json")
	void serializePayload() throws JsonProcessingException {
		final String serializedPayload = mapper.writeValueAsString(electionContextResponsePayload);
		assertEquals(rootNode.toString(), serializedPayload);
	}

	@Test
	@DisplayName("deserialized gives expected payload")
	void deserializePayload() throws IOException {
		final ElectionContextResponsePayload deserializedPayload = mapper.readValue(rootNode.toString(), ElectionContextResponsePayload.class);
		assertEquals(electionContextResponsePayload, deserializedPayload);
	}

	@Test
	@DisplayName("serialized then deserialized gives original payload")
	void cycle() throws IOException {
		final ElectionContextResponsePayload deserializedPayload = mapper
				.readValue(mapper.writeValueAsString(electionContextResponsePayload), ElectionContextResponsePayload.class);

		assertEquals(electionContextResponsePayload, deserializedPayload);
	}
}
