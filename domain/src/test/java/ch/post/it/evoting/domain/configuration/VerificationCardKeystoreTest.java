/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Locale;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;

@DisplayName("A VerificationCardKeystore")
class VerificationCardKeystoreTest extends TestGroupSetup {
	private static final Random random = RandomFactory.createRandom();
	private static final String verificationCardId = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String verificationCardKeystore = "verificationCardKeystore";

	@Test
	@DisplayName("created with a null verificationCardId, throws a NullPointerException.")
	void nullVerificationCardId() {
		assertThrows(NullPointerException.class, () -> new VerificationCardKeystore(null, verificationCardKeystore));
	}

	@Test
	@DisplayName("created with an invalid verificationCardId, throws a FailedValidationException.")
	void invalidVerificationCardId() {
		assertThrows(FailedValidationException.class, () -> new VerificationCardKeystore("verificationCardId", verificationCardKeystore));
	}

	@Test
	@DisplayName("created with a null verificationCardKeystore, throws a NullPointerException.")
	void nullVerificationCardKeystore() {
		assertThrows(NullPointerException.class, () -> new VerificationCardKeystore(verificationCardId, null));
	}

	@Test
	@DisplayName("created with a non-null values does not throw.")
	void withNonNullValuesDoesNotThrow() {
		assertDoesNotThrow(() -> new VerificationCardKeystore(verificationCardId, verificationCardKeystore));
	}
}
