/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;

@DisplayName("A setupComponentLVCCAllowListPayload")
class SetupComponentLVCCAllowListPayloadTest {

	private static final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
	private static final ch.post.it.evoting.cryptoprimitives.hashing.Hash hash = HashFactory.createHash();
	private static final String electionEventId = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String verificationCardSetId = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final List<String> longVoteCastReturnCodesAllowList = Collections.singletonList(
			Base64.getEncoder().encodeToString(new byte[] { 1, 2 }));
	private static SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload;
	private static CryptoPrimitivesSignature signature;
	private static ObjectNode rootNode;

	@BeforeAll
	static void setupAll() throws JsonProcessingException {

		// Create payload.
		setupComponentLVCCAllowListPayload = new SetupComponentLVCCAllowListPayload(electionEventId, verificationCardSetId,
				longVoteCastReturnCodesAllowList);

		byte[] payloadHash = hash.recursiveHash(setupComponentLVCCAllowListPayload);

		signature = new CryptoPrimitivesSignature(payloadHash);
		setupComponentLVCCAllowListPayload.setSignature(signature);

		// Create expected Json.
		rootNode = mapper.createObjectNode();
		rootNode.set("electionEventId", mapper.readTree(mapper.writeValueAsString(electionEventId)));
		rootNode.set("verificationCardSetId", mapper.readTree(mapper.writeValueAsString(verificationCardSetId)));
		rootNode.set("longVoteCastReturnCodesAllowList", mapper.readTree(mapper.writeValueAsString(longVoteCastReturnCodesAllowList)));
		rootNode.set("signature", mapper.readTree(mapper.writeValueAsString(signature)));
	}

	@Test
	@DisplayName("serialized gives expected json")
	void serializePayload() throws JsonProcessingException {
		final String serializedPayload = mapper.writeValueAsString(setupComponentLVCCAllowListPayload);
		assertEquals(rootNode.toString(), serializedPayload);
	}

	@Test
	@DisplayName("deserialized gives expected result")
	void deserializePayload() throws IOException {
		final SetupComponentLVCCAllowListPayload deserializedPayload = mapper.readValue(rootNode.toString(),
				SetupComponentLVCCAllowListPayload.class);
		assertEquals(setupComponentLVCCAllowListPayload, deserializedPayload);
		assertEquals(electionEventId, setupComponentLVCCAllowListPayload.getElectionEventId());
		assertEquals(longVoteCastReturnCodesAllowList, setupComponentLVCCAllowListPayload.getLongVoteCastReturnCodesAllowList());
		assertEquals(signature, setupComponentLVCCAllowListPayload.getSignature());
	}

	@Test
	@DisplayName("serialized then deserialized gives original object")
	void cycle() throws IOException {
		final SetupComponentLVCCAllowListPayload deserializedPayload = mapper.readValue(
				mapper.writeValueAsString(setupComponentLVCCAllowListPayload), SetupComponentLVCCAllowListPayload.class);

		assertEquals(setupComponentLVCCAllowListPayload, deserializedPayload);
	}

	@Test
	@DisplayName("constructed with invalid fields throws an exception")
	void testInvalidSetupComponentLVCCAllowList() {
		final List<String> emptyAllowList = List.of();
		final List<String> emptyStringInAllowList = List.of("");
		final List<String> whitespaceOnlyStringInAllowList = List.of("    ");
		final List<String> notBase64InAllowList = List.of("not base64");

		assertAll(
				() -> assertThrows(NullPointerException.class,
						() -> new SetupComponentLVCCAllowListPayload(null, verificationCardSetId, longVoteCastReturnCodesAllowList, signature)),
				() -> assertThrows(FailedValidationException.class,
						() -> new SetupComponentLVCCAllowListPayload("invalidElectionEventId", verificationCardSetId,
								longVoteCastReturnCodesAllowList, signature)),
				() -> assertThrows(NullPointerException.class,
						() -> new SetupComponentLVCCAllowListPayload(electionEventId, null, longVoteCastReturnCodesAllowList, signature)),
				() -> assertThrows(FailedValidationException.class,
						() -> new SetupComponentLVCCAllowListPayload(electionEventId, "invalidVerificationCardSetId",
								longVoteCastReturnCodesAllowList, signature)),
				() -> assertThrows(NullPointerException.class,
						() -> new SetupComponentLVCCAllowListPayload(electionEventId, verificationCardSetId, null, signature)),
				() -> assertThrows(NullPointerException.class,
						() -> new SetupComponentLVCCAllowListPayload(electionEventId, verificationCardSetId, longVoteCastReturnCodesAllowList,
								null)),

				() -> assertThrows(NullPointerException.class,
						() -> new SetupComponentLVCCAllowListPayload(null, verificationCardSetId, longVoteCastReturnCodesAllowList)),
				() -> assertThrows(FailedValidationException.class,
						() -> new SetupComponentLVCCAllowListPayload("invalidElectionEventId", verificationCardSetId,
								longVoteCastReturnCodesAllowList)),
				() -> assertThrows(NullPointerException.class,
						() -> new SetupComponentLVCCAllowListPayload(electionEventId, null, longVoteCastReturnCodesAllowList)),
				() -> assertThrows(FailedValidationException.class,
						() -> new SetupComponentLVCCAllowListPayload(electionEventId, "invalidVerificationCardSetId",
								longVoteCastReturnCodesAllowList)),
				() -> assertThrows(NullPointerException.class,
						() -> new SetupComponentLVCCAllowListPayload(electionEventId, verificationCardSetId, null)),

				() -> assertThrows(IllegalArgumentException.class,
						() -> new SetupComponentLVCCAllowListPayload(electionEventId, verificationCardSetId, emptyAllowList)),
				() -> assertThrows(IllegalArgumentException.class,
						() -> new SetupComponentLVCCAllowListPayload(electionEventId, verificationCardSetId, emptyStringInAllowList)),
				() -> assertThrows(IllegalArgumentException.class,
						() -> new SetupComponentLVCCAllowListPayload(electionEventId, verificationCardSetId, whitespaceOnlyStringInAllowList)),
				() -> assertThrows(IllegalArgumentException.class,
						() -> new SetupComponentLVCCAllowListPayload(electionEventId, verificationCardSetId, notBase64InAllowList)));
	}

	@Test
	@DisplayName("equals give expected result")
	void testEquality() {
		final SetupComponentLVCCAllowListPayload same = new SetupComponentLVCCAllowListPayload(electionEventId, verificationCardSetId,
				longVoteCastReturnCodesAllowList, signature);
		final SetupComponentLVCCAllowListPayload different = new SetupComponentLVCCAllowListPayload(
				RandomFactory.createRandom().genRandomBase16String(32).toLowerCase(Locale.ENGLISH), verificationCardSetId,
				longVoteCastReturnCodesAllowList, signature);

		assertAll(
				() -> assertEquals(setupComponentLVCCAllowListPayload, setupComponentLVCCAllowListPayload),
				() -> assertEquals(setupComponentLVCCAllowListPayload, same),
				() -> assertNotEquals(setupComponentLVCCAllowListPayload, different),
				() -> assertNotEquals(null, setupComponentLVCCAllowListPayload),
				() -> assertNotEquals(1L, setupComponentLVCCAllowListPayload),
				() -> assertEquals(setupComponentLVCCAllowListPayload.hashCode(), same.hashCode())
		);
	}
}
