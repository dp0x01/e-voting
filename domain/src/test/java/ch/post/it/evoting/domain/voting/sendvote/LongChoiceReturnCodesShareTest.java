/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

package ch.post.it.evoting.domain.voting.sendvote;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Locale;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;

class LongChoiceReturnCodesShareTest {
	private static final Random random = RandomFactory.createRandom();
	private final GqGroup gqGroup = GroupTestData.getGqGroup();
	private final GqGroupGenerator gqGroupGenerator = new GqGroupGenerator(gqGroup);

	private final String electionEventId = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private final String verificationCardSetId = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private final String verificationCardId = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private final int nodeId = 1;
	private final GroupVector<GqElement, GqGroup> longChoiceReturnCodeShare = gqGroupGenerator.genRandomGqElementVector(2);

	private final ZqElement randomZqElement = new ZqGroupGenerator(ZqGroup.sameOrderAs(gqGroup)).genRandomZqElementMember();
	private final ExponentiationProof exponentiationProof = new ExponentiationProof(randomZqElement, randomZqElement);

	@Nested
	@DisplayName("Check constructor validation")
	class CheckConstructor {
		@Test
		@DisplayName("Check null arguments")
		void nullArgs() {

			assertAll(
					() -> assertThrows(NullPointerException.class,
							() -> new LongChoiceReturnCodesShare(null, verificationCardSetId, verificationCardId, nodeId,
									longChoiceReturnCodeShare, exponentiationProof)),
					() -> assertThrows(NullPointerException.class,
							() -> new LongChoiceReturnCodesShare(electionEventId, null, verificationCardId, nodeId,
									longChoiceReturnCodeShare, exponentiationProof)),
					() -> assertThrows(NullPointerException.class,
							() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, null, nodeId,
									longChoiceReturnCodeShare, exponentiationProof)),
					() -> assertThrows(NullPointerException.class,
							() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, nodeId,
									null, exponentiationProof)),
					() -> assertThrows(NullPointerException.class,
							() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, nodeId,
									longChoiceReturnCodeShare, null)),

					() -> assertDoesNotThrow(
							() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, nodeId,
									longChoiceReturnCodeShare, exponentiationProof))
			);
		}

		@Test
		@DisplayName("Check UUID format")
		void formatArgs() {

			final String invalidElectionEventId = "electionEventId";
			final String invalidVerificationCardSetId = "verificationCardSetId";
			final String invalidVerificationCardId = "verificationCardId";

			assertAll(
					() -> assertThrows(FailedValidationException.class,
							() -> new LongChoiceReturnCodesShare(invalidElectionEventId, verificationCardSetId, verificationCardId, nodeId,
									longChoiceReturnCodeShare, exponentiationProof)),
					() -> assertThrows(FailedValidationException.class,
							() -> new LongChoiceReturnCodesShare(electionEventId, invalidVerificationCardSetId, verificationCardId, nodeId,
									longChoiceReturnCodeShare, exponentiationProof)),
					() -> assertThrows(FailedValidationException.class,
							() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, invalidVerificationCardId, nodeId,
									longChoiceReturnCodeShare, exponentiationProof))
			);
		}

		@Test
		@DisplayName("Check nodeIds in range 1 to 4")
		void nodeIsArgs() {

			assertAll(
					() -> assertThrows(IllegalArgumentException.class,
							() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 0,
									longChoiceReturnCodeShare, exponentiationProof)),
					() -> assertDoesNotThrow(() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 1,
							longChoiceReturnCodeShare, exponentiationProof)),
					() -> assertDoesNotThrow(() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 2,
							longChoiceReturnCodeShare, exponentiationProof)),
					() -> assertDoesNotThrow(() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 3,
							longChoiceReturnCodeShare, exponentiationProof)),
					() -> assertDoesNotThrow(() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 4,
							longChoiceReturnCodeShare, exponentiationProof)),
					() -> assertThrows(IllegalArgumentException.class,
							() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 5,
									longChoiceReturnCodeShare, exponentiationProof))
			);
		}
	}

	@Test
	@DisplayName("Check all groups are of the same order")
	void checkGroupAreSameOrder() {
		final GqGroup differentGqGroup = GroupTestData.getDifferentGqGroup(gqGroup);
		final GqGroupGenerator gqDifferentGroupGenerator = new GqGroupGenerator(differentGqGroup);

		final GroupVector<GqElement, GqGroup> longChoiceReturnCodeShareDifferentGroup = gqDifferentGroupGenerator.genRandomGqElementVector(2);

		final ZqElement randomZqElementDifferentGqGroup = new ZqGroupGenerator(ZqGroup.sameOrderAs(differentGqGroup)).genRandomZqElementMember();
		final ExponentiationProof exponentiationProofDifferentGroup = new ExponentiationProof(randomZqElementDifferentGqGroup,
				randomZqElementDifferentGqGroup);

		assertAll(
				() -> assertThrows(IllegalArgumentException.class,
						() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 1,
								longChoiceReturnCodeShareDifferentGroup, exponentiationProof)),
				() -> assertThrows(IllegalArgumentException.class,
						() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 1,
								longChoiceReturnCodeShare, exponentiationProofDifferentGroup))
		);
	}

}
