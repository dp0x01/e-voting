/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.votingserver.common.Constants;

class AuthenticationChallengeTest {

	@Test
	void test() throws JsonProcessingException {
		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
		final Random random = RandomFactory.createRandom();
		final String derivedVoterIdentifier = random.genRandomBase16String(32);
		final String derivedAuthenticationChallenge = random.genRandomBase64String(44);
		final BigInteger nonce = random.genRandomInteger(Constants.TWO_POW_256);
		final AuthenticationChallenge authenticationChallenge = new AuthenticationChallenge(derivedVoterIdentifier, derivedAuthenticationChallenge,
				nonce);
		final String serialized = objectMapper.writeValueAsString(authenticationChallenge);
		assertNotNull(serialized);
	}
}
