/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.votingserver.orchestrator.IntegrationTestSupport.RABBITMQ_EXCHANGE;
import static com.google.common.base.Preconditions.checkState;
import static org.awaitility.Awaitility.await;

import java.io.UncheckedIOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Command;
import ch.post.it.evoting.commandmessaging.CommandId;
import ch.post.it.evoting.commandmessaging.CommandRepository;
import ch.post.it.evoting.commandmessaging.CommandService;
import ch.post.it.evoting.commandmessaging.Context;

/**
 * Helper class to test broadcasting to CCs.
 */
@Service
public class BroadcastIntegrationTestService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BroadcastIntegrationTestService.class);

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private CommandService commandService;

	@Autowired
	private CommandRepository commandRepository;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	/**
	 * Simulate CCs message consumer.
	 *
	 * @param requestQueuePattern  queue on which the simulated CCs should listen.
	 * @param responseQueuePattern queue on which the simulated CCs should respond.
	 * @param responsePayload      function of nodeId to response payload to create the response to be sent on the response queue.
	 */
	public void respondWith(final String requestQueuePattern, final String responseQueuePattern, final Function<Integer, Object> responsePayload) {
		NODE_IDS.forEach((nodeId) -> {
			respondWithForGivenNodeId(requestQueuePattern, responseQueuePattern, responsePayload, nodeId);
		});
	}

	public void respondWithForGivenNodeId(String requestQueuePattern, String responseQueuePattern, Function<Integer, Object> responsePayload,
			Integer nodeId) {
		final String requestQueue = requestQueuePattern + nodeId;
		final Message requestMessage = rabbitTemplate.receive(requestQueue, 5000);
		assert requestMessage != null;
		LOGGER.info("Received request [queue: {}, nodeId: {}]", requestQueue, nodeId);

		final MessageProperties messageProperties = new MessageProperties();
		messageProperties.setCorrelationId(requestMessage.getMessageProperties().getCorrelationId());
		try {
			final byte[] payloadBytes = objectMapper.writeValueAsBytes(responsePayload.apply(nodeId));
			final Message message = new Message(payloadBytes, messageProperties);
			final String responseQueue = responseQueuePattern + nodeId;
			rabbitTemplate.send(RABBITMQ_EXCHANGE, responseQueue, message);
			LOGGER.info("Sent response [queue: {}, nodeId: {}, message : {}, messageProperties : {}]", responseQueue, nodeId, message,
					messageProperties);
		} catch (JsonProcessingException e) {
			throw new UncheckedIOException(e);
		}
	}

	/**
	 * Wait with timeout for broadcast request to have been saved
	 */
	public void awaitBroadcastRequestsSaved(Context context, String contextId, long timeout, TimeUnit unit) {
		await().atMost(timeout, unit)
				.until(() -> NODE_IDS.stream().allMatch(nodeId -> requestSaved(context, contextId, nodeId)));
	}

	private boolean requestSaved(Context context, String contextId, int nodeId) {
		final CommandId command = CommandId.builder().contextId(contextId).context(context.toString()).nodeId(nodeId).build();
		final List<Command> requests = commandService.findSemanticallyIdenticalCommand(command);
		checkState(requests.size() <= 1, "Another request was sent simultaneously, cannot differentiate the request specific "
				+ "to this test. We should make sure that IT tests on real queues don't run in parallel. ");
		final boolean requestFound = requests.size() == 1;
		LOGGER.debug("Request found?: {} [Context: {}, contextId:{}, nodeId:{}]", requestFound, context, contextId, nodeId);
		return requestFound;
	}

	public void cleanUpOrchestrator() {
		commandRepository.deleteAll();
	}
}
