/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URL;
import java.security.SignatureException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;

@DisplayName("ElectionEventContextController")
@ExtendWith(MockitoExtension.class)
class ElectionEventContextControllerTest {

	private static ElectionEventContextPayload electionEventContextPayload;
	private static String electionEventId;

	@InjectMocks
	ElectionEventContextController electionEventContextController;

	@Mock
	private ElectionEventContextService electionEventContextService;

	@Mock
	private ElectionEventService electionEventService;

	@Mock
	private SignatureKeystore<Alias> signatureKeystoreService;

	@BeforeAll
	static void setUpAll() throws IOException {
		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
		final URL electionContextPayloadUrl = ElectionEventContextControllerTest.class.getResource(
				"/processor/electionEventContextServiceTest/election-event-context-payload.json");
		electionEventContextPayload = mapper.readValue(electionContextPayloadUrl, ElectionEventContextPayload.class);

		electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();
	}

	@Test
	@DisplayName("save election event context with valid parameters")
	void saveElectionEventContextHappyPath() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), eq(electionEventContextPayload), any(), any())).thenReturn(true);

		electionEventContextController.saveElectionEventContext(electionEventId, electionEventContextPayload);

		verify(electionEventContextService, times(1)).saveElectionEventContext(any());
	}

	@Test
	@DisplayName("Error verifying payload signature while saving throws IllegalStateException")
	void savingErrorVerifyingSignatureThrows() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), eq(electionEventContextPayload), any(), any())).thenThrow(SignatureException.class);

		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> electionEventContextController.saveElectionEventContext(electionEventId, electionEventContextPayload));
		assertEquals(String.format("Could not verify the signature of the election event context. [electionEventId: %s]", electionEventId),
				exception.getMessage());
	}

	@Test
	@DisplayName("Saving payload with invalid signature throws InvalidPayloadSignatureException")
	void savingInvalidSignatureThrows() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), eq(electionEventContextPayload), any(), any())).thenReturn(false);
		final InvalidPayloadSignatureException exception = assertThrows(InvalidPayloadSignatureException.class,
				() -> electionEventContextController.saveElectionEventContext(electionEventId, electionEventContextPayload));
		assertEquals(String.format("Signature of payload %s is invalid. [electionEventId: %s]", ElectionEventContextPayload.class.getSimpleName(),
				electionEventId), exception.getMessage());
	}
}
