/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting.authenticatevoter;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigInteger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.votingserver.processor.voting.VerifyAuthenticationChallengeInput;

class VerifyAuthenticationChallengeInputTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final String TWO_POW_256 = "115792089237316195423570985008687907853269984665640564039457584007913129639936";

	private String credentialId;
	private String derivedAuthenticationChallenge;
	private String baseAuthenticationChallenge;
	private BigInteger authenticationNonce;

	@BeforeEach
	void setup() {
		credentialId = RANDOM.genRandomBase16String(32);
		derivedAuthenticationChallenge = RANDOM.genRandomBase64String(44);
		baseAuthenticationChallenge = RANDOM.genRandomBase64String(44);
		authenticationNonce = RANDOM.genRandomInteger(new BigInteger(TWO_POW_256));
	}

	@Test
	void constructWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class,
				() -> new VerifyAuthenticationChallengeInput(null, derivedAuthenticationChallenge, baseAuthenticationChallenge, authenticationNonce));
		assertThrows(NullPointerException.class,
				() -> new VerifyAuthenticationChallengeInput(credentialId, null, baseAuthenticationChallenge, authenticationNonce));
		assertThrows(NullPointerException.class,
				() -> new VerifyAuthenticationChallengeInput(credentialId, derivedAuthenticationChallenge, null, authenticationNonce));
		assertThrows(NullPointerException.class,
				() -> new VerifyAuthenticationChallengeInput(credentialId, derivedAuthenticationChallenge, baseAuthenticationChallenge, null));
	}

	@Test
	void constructWithCredentialIdNotValidUuidThrows() {
		final String tooShortCredentialId = credentialId.substring(1);
		assertThrows(FailedValidationException.class,
				() -> new VerifyAuthenticationChallengeInput(tooShortCredentialId, derivedAuthenticationChallenge, baseAuthenticationChallenge,
						authenticationNonce));
		final String tooLongCredentialId = credentialId + "1";
		assertThrows(FailedValidationException.class,
				() -> new VerifyAuthenticationChallengeInput(tooLongCredentialId, derivedAuthenticationChallenge, baseAuthenticationChallenge,
						authenticationNonce));
		final String credentialIdBadCharacter = "?" + credentialId;
		assertThrows(FailedValidationException.class,
				() -> new VerifyAuthenticationChallengeInput(credentialIdBadCharacter, derivedAuthenticationChallenge, baseAuthenticationChallenge,
						authenticationNonce));
	}

	@Test
	void constructWithChallengesNotBase64Throws() {
		final String badDerivedAuthenticationChallenge = "invalid!";
		assertThrows(FailedValidationException.class,
				() -> new VerifyAuthenticationChallengeInput(credentialId, badDerivedAuthenticationChallenge, baseAuthenticationChallenge,
						authenticationNonce));
		final String badBaseAuthenticationChallenge = "invalid!";
		assertThrows(FailedValidationException.class,
				() -> new VerifyAuthenticationChallengeInput(credentialId, derivedAuthenticationChallenge, badBaseAuthenticationChallenge,
						authenticationNonce));
	}

	@Test
	void constructWithNegativeAuthenticationNonceThrows() {
		final BigInteger badAuthenticationNonce = BigInteger.valueOf(-1);
		assertThrows(IllegalArgumentException.class,
				() -> new VerifyAuthenticationChallengeInput(credentialId, derivedAuthenticationChallenge, baseAuthenticationChallenge,
						badAuthenticationNonce));
	}

	@Test
	void constructWithValidInputDoesNotThrow() {
		assertDoesNotThrow(() -> new VerifyAuthenticationChallengeInput(credentialId, derivedAuthenticationChallenge, baseAuthenticationChallenge,
				authenticationNonce));
	}
}
