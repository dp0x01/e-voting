/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;

@WebMvcTest(controllers = VotingCardManagerController.class)
class VotingCardManagerControllerTest {

	private static ObjectMapper objectMapper;
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private VerificationCardService verificationCardService;

	@BeforeAll
	static void setUp() {
		objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
	}

	@Test
	void searchHappyPathRest() throws Exception {
		// given
		final String votingCardId = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase();
		final String votingCardId2 = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase();
		final String votingCardId3 = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase();
		final String votingCardId4 = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase();
		final String votingCardId5 = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase();
		final String votingCardId6 = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase();
		final String votingCardId7 = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase();
		final String votingCardId8 = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase();
		final String votingCardId9 = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase();
		final String votingCardId10 = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase();

		final String targetUrl = String.format("/api/v1/voting-card-manager/voting-cards/%s", votingCardId);

		final List<VotingCardDto> expectedVotingCards = new ArrayList<>();
		expectedVotingCards.add(new VotingCardDto(votingCardId, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId2, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId3, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId4, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId5, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId6, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId7, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId8, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId9, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId10, VerificationCardState.CONFIRMED, LocalDateTime.now()));

		when(verificationCardService.searchVotingCards(votingCardId)).thenReturn(expectedVotingCards);

		// when
		final String jsonResult = mockMvc.perform(MockMvcRequestBuilders.get(targetUrl))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();

		final List<VotingCardDto> actualVotingCards = objectMapper.readValue(jsonResult, new TypeReference<>() {
		});

		// then
		Assertions.assertThat(actualVotingCards).isEqualTo(expectedVotingCards);
	}
}