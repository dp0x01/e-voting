/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;

import java.io.IOException;
import java.net.URL;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayload;

@DisplayName("ReturnCodesMappingTableService")
class ReturnCodesMappingTableServiceTest {

	private static final ObjectMapper objectMapper = spy(DomainObjectMapper.getNewInstance());
	private static final VerificationCardSetService verificationCardSetService = mock(VerificationCardSetService.class);
	private static final ReturnCodesMappingTableRepository returnCodesMappingTableEntryRepository = mock(
			ReturnCodesMappingTableRepository.class);

	private static ReturnCodesMappingTableService returnCodesMappingTableService;
	private static SetupComponentCMTablePayload setupComponentCMTablePayload;
	private static String verificationCardSetId;

	@BeforeAll
	static void setUpAll() throws IOException {
		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
		final URL returnCodesMappingTableUrl = ReturnCodesMappingTableServiceTest.class.getResource(
				"/processor/returnCodesMappingTableServiceTest/setupComponentCMTablePayload.0.json");
		setupComponentCMTablePayload = mapper.readValue(returnCodesMappingTableUrl, SetupComponentCMTablePayload.class);

		verificationCardSetId = setupComponentCMTablePayload.getVerificationCardSetId();
		returnCodesMappingTableService = new ReturnCodesMappingTableService(verificationCardSetService, returnCodesMappingTableEntryRepository, 2);
	}

	@AfterEach
	void tearDown() {
		reset(objectMapper, verificationCardSetService, returnCodesMappingTableEntryRepository);
	}

	@Test
	@DisplayName("Saving with null parameters throws NullPointerException")
	void savingNullThrows() {
		assertThrows(NullPointerException.class, () -> returnCodesMappingTableService.saveReturnCodesMappingTable(null));
	}

}
