/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.configuration.setupvoting;

import static ch.post.it.evoting.domain.SharedQueue.LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST_RESPONSE_PATTERN;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.test.web.reactive.server.WebTestClient;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.domain.configuration.setupvoting.LongVoteCastReturnCodesAllowListResponsePayload;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.votingserver.orchestrator.BroadcastIntegrationTestService;
import ch.post.it.evoting.votingserver.orchestrator.IntegrationTestSupport;

@DisplayName("A LongVoteCastReturnCodesAllowListController")
class LongVoteCastReturnCodesAllowListControllerIT extends IntegrationTestSupport {

	@Autowired
	private BroadcastIntegrationTestService broadcastIntegrationTestService;

	@Autowired
	private WebTestClient webTestClient;

	@AfterEach
	void cleanUp() {
		broadcastIntegrationTestService.cleanUpOrchestrator();
	}

	@Test
	@DisplayName("processing a request behaves as expected.")
	void process() throws InterruptedException {
		final Context context = Context.CONFIGURATION_SETUP_VOTING_LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST;
		final CountDownLatch webClientCountDownLatch = new CountDownLatch(1);
		final SetupComponentLVCCAllowListPayload payload = getLongVoteCastReturnCodesAllowListPayload();
		final ExecutorService executorService = Executors.newFixedThreadPool(1, new CustomizableThreadFactory("http-pool-"));

		//send request to controller
		executorService.execute(() -> {
			webTestClient.post()
					.uri(uriBuilder -> uriBuilder
							.path("/api/v1/configuration/setupvoting/longvotecastreturncodesallowlist/electionevent/")
							.pathSegment(electionEventId, "verificationcardset", verificationCardSetId)
							.build(1L))
					.accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
					.bodyValue(payload)
					.exchange()
					.expectStatus().isOk();
			webClientCountDownLatch.countDown();
		});

		final String contextId = String.join("-", Arrays.asList(electionEventId, verificationCardSetId));
		broadcastIntegrationTestService.awaitBroadcastRequestsSaved(context, contextId, 30, SECONDS);

		broadcastIntegrationTestService.respondWith(
				LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST_REQUEST_PATTERN,
				LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST_RESPONSE_PATTERN,
				nodeId -> new LongVoteCastReturnCodesAllowListResponsePayload(nodeId, electionEventId, verificationCardSetId));

		assertTrue(webClientCountDownLatch.await(30, SECONDS));
	}

	private SetupComponentLVCCAllowListPayload getLongVoteCastReturnCodesAllowListPayload() {
		final String lVCC1 = Base64.getEncoder().encodeToString(new byte[] { 35 });
		final String lVCC2 = Base64.getEncoder().encodeToString(new byte[] { 45 });
		return new SetupComponentLVCCAllowListPayload(
				electionEventId,
				verificationCardSetId,
				Arrays.asList(lVCC1, lVCC2),
				new CryptoPrimitivesSignature("".getBytes(StandardCharsets.UTF_8)));
	}
}
