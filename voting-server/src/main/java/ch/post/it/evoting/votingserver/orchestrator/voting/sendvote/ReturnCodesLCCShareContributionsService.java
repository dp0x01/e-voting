/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.domain.SharedQueue.CREATE_LCC_SHARE_REQUEST_PATTERN;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.domain.voting.sendvote.CombinedControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentLCCSharePayload;
import ch.post.it.evoting.votingserver.orchestrator.voting.BroadcastCommand;
import ch.post.it.evoting.votingserver.orchestrator.voting.BroadcastCommandProducer;

@Service
public class ReturnCodesLCCShareContributionsService {

	private final ObjectMapper objectMapper;
	private final BroadcastCommandProducer broadcastCommandProducer;

	public ReturnCodesLCCShareContributionsService(
			final ObjectMapper objectMapper,
			final BroadcastCommandProducer broadcastCommandProducer) {
		this.objectMapper = objectMapper;
		this.broadcastCommandProducer = broadcastCommandProducer;
	}

	public List<ControlComponentLCCSharePayload> getLongChoiceReturnCodesContributions(final String electionEventId,
			final String verificationCardSetId, final String verificationCardId,
			final CombinedControlComponentPartialDecryptPayload combinedControlComponentPartialDecryptPayload) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);
		checkNotNull(combinedControlComponentPartialDecryptPayload);

		final String contextId = String.join("-", electionEventId, verificationCardSetId, verificationCardId);

		final BroadcastCommand<ControlComponentLCCSharePayload> broadcastCommand = new BroadcastCommand.Builder<ControlComponentLCCSharePayload>()
				.contextId(contextId)
				.context(Context.VOTING_RETURN_CODES_CREATE_LCC_SHARE)
				.payload(combinedControlComponentPartialDecryptPayload)
				.pattern(CREATE_LCC_SHARE_REQUEST_PATTERN)
				.deserialization(this::deserializePayload)
				.build();

		return broadcastCommandProducer.sendMessagesAwaitingNotification(broadcastCommand);
	}

	private ControlComponentLCCSharePayload deserializePayload(final byte[] payload) {
		try {
			return objectMapper.readValue(payload, ControlComponentLCCSharePayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

}
