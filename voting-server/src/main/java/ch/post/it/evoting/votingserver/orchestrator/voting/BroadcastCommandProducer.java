/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.voting;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.UncheckedIOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.benmanes.caffeine.cache.Cache;

import ch.post.it.evoting.commandmessaging.Command;
import ch.post.it.evoting.commandmessaging.CommandService;
import ch.post.it.evoting.commandmessaging.Context;

/**
 * Provides functionality for sending requests and collecting the contributions.
 */
@Service
public class BroadcastCommandProducer {

	private static final Logger LOGGER = LoggerFactory.getLogger(BroadcastCommandProducer.class);

	private static final String RABBITMQ_EXCHANGE = "evoting-exchange";

	private final RabbitTemplate rabbitTemplate;
	private final ObjectMapper objectMapper;
	private final CommandFacade commandFacade;
	private final CommandService commandService;
	private final Cache<String, CompletableFuture<String>> inFlightRequests;

	@Value("${controller.completable.future.timeout.seconds}")
	private int controllerCompletableFutureTimeout;

	public BroadcastCommandProducer(
			final RabbitTemplate rabbitTemplate,
			final ObjectMapper objectMapper,
			final CommandFacade commandFacade,
			final CommandService commandService,
			final Cache<String, CompletableFuture<String>> inFlightRequests) {
		this.rabbitTemplate = rabbitTemplate;
		this.objectMapper = objectMapper;
		this.commandFacade = commandFacade;
		this.commandService = commandService;
		this.inFlightRequests = inFlightRequests;
	}

	/**
	 * Sends a request for the calculation of the contributions to the queues with the given name pattern and collects the responses.
	 *
	 * @param command contains the data for request for the calculation of the contributions. Must be non-null.
	 * @return a list of the collected contributions
	 * @throws NullPointerException if any of the inputs is null.
	 * @throws CompletionException  if the timeout is reached when waiting for the contributions.
	 */
	public <R> List<R> sendMessagesAwaitingNotification(BroadcastCommand<R> command) {
		checkNotNull(command);

		final String correlationId = UUID.randomUUID().toString();

		LOGGER.info("Starting calculations. [contextId: {}, correlationId: {}]", command.getContextId(), correlationId);

		final CompletableFuture<String> completableFuture = new CompletableFuture<>();
		inFlightRequests.put(correlationId, completableFuture);

		sendAllMessages(command.getContextId(), command.getContext(), correlationId, command.getPayload(), command.getPattern(), true);

		// Wait for all contributions to have returned or timeout.
		try {
			completableFuture.get(controllerCompletableFutureTimeout, TimeUnit.SECONDS);
		} catch (final ExecutionException e) {
			throw new IllegalStateException("This should not happen as the CompletableFuture does not execute any computation.");
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new CompletionException("The thread waiting for the CompletableFuture was interrupted.", e);
		} catch (final TimeoutException e) {
			throw new CompletionException(String.format("The CompletableFuture did not complete during the timeout period. [timeout: %s]",
					controllerCompletableFutureTimeout), e);
		}

		inFlightRequests.invalidate(correlationId);

		LOGGER.info("All nodes have completed their calculations. [contextId: {}, correlationId: {}]", command.getContextId(), correlationId);

		final List<Command> allMessagesWithCorrelationId = commandFacade.getAllNodesResponses(correlationId);

		return allMessagesWithCorrelationId.stream()
				.map(Command::getResponsePayload)
				.map(command.getDeserialization())
				.toList();
	}

	//Note: The response must be saved in a specific repository and not use the command table
	public void sendMessagesAndForget(BroadcastCommand<?> command) {
		checkNotNull(command);

		final String correlationId = UUID.randomUUID().toString();

		LOGGER.info("Starting calculations. [contextId: {}, correlationId: {}]", command.getContextId(), correlationId);

		sendAllMessages(command.getContextId(), command.getContext(), correlationId, command.getPayload(), command.getPattern(), false);

		LOGGER.info("Send all calculations to nodes. [contextId: {}, correlationId: {}]", command.getContextId(), correlationId);
	}

	/**
	 * Sends a request to each node through the queues with the specified name pattern.
	 *
	 * @param contextId      the context id. Must be non-null.
	 * @param context        the context in which the request is sent. Must be non-null.
	 * @param correlationId  the correlation id that must be used by the responses. Must be non-null.
	 * @param requestPayload the request payload to be sent. Must be non-null.
	 * @param queuePattern   the name pattern of the queue to which to send the message. Must be non-null.
	 * @param saveRequest	whether to save the content of the request or not.
	 * @throws NullPointerException if any of the inputs is null.
	 */
	private void sendAllMessages(final String contextId, final Context context, final String correlationId, final Object requestPayload,
			final String queuePattern, boolean saveRequest) {
		checkNotNull(contextId);
		checkNotNull(context);
		checkNotNull(correlationId);
		checkNotNull(requestPayload);
		checkNotNull(queuePattern);

		if (commandService.isRequestForContextIdAndContextAlreadyPresent(contextId, context.toString())) {
			LOGGER.warn("Request already sent [contextId: {}, context: {}]", contextId, context);
		}

		byte[] payload;
		try {
			payload = objectMapper.writeValueAsBytes(requestPayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(e);
		}

		NODE_IDS.forEach(nodeId -> {
			if (saveRequest) {
				commandFacade.saveRequest(payload, correlationId, contextId, context, nodeId);
			}
			MessageProperties messageProperties = new MessageProperties();
			messageProperties.setCorrelationId(correlationId);
			Message message = new Message(payload, messageProperties);
			rabbitTemplate.send(RABBITMQ_EXCHANGE, queuePattern + nodeId, message);
			LOGGER.debug("Message sent. [contextId: {}, context: {}, nodeId: {}]", contextId, context, nodeId);
		});
	}

}
