/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validatePartialUUID;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.votingserver.common.Constants;

@RestController
@RequestMapping("api/v1/voting-card-manager/")
public class VotingCardManagerController {

	private static final int MIN_PARTIAL_UUID_LENGTH = 3;

	private final VerificationCardService verificationCardService;

	public VotingCardManagerController(final VerificationCardService verificationCardService) {
		this.verificationCardService = verificationCardService;
	}

	/**
	 * @param partialVotingCardId partial voting card id. Must be non-null and a valid partial UUID with minimal length of
	 *                            {@value MIN_PARTIAL_UUID_LENGTH}.
	 * @return a list of VotingCardDto corresponding to the partial voting id searched.
	 */
	@GetMapping("voting-cards/{partialVotingCardId}")
	@ResponseStatus(HttpStatus.OK)
	public List<VotingCardDto> searchVotingCards(
			@PathVariable(Constants.PARTIAL_VOTING_CARD_ID)
			final String partialVotingCardId) throws TooManyVerificationCardsException {

		validatePartialUUID(partialVotingCardId, MIN_PARTIAL_UUID_LENGTH);

		return verificationCardService.searchVotingCards(partialVotingCardId);
	}

	/**
	 * Block the specified voting card.
	 *
	 * @param votingCardId the voting card id. Must be non-null and a valid UUID.
	 * @return the blocked VotingCardDto.
	 */
	@PutMapping("voting-card/block/{votingCardId}")
	@ResponseStatus(HttpStatus.OK)
	public VotingCardDto blockVotingCard(
			@PathVariable(Constants.VOTING_CARD_ID)
			final String votingCardId) throws VerificationCardNotFoundException {

		validateUUID(votingCardId);

		return verificationCardService.blockVotingCard(votingCardId);
	}

	/**
	 * Lists all used voting cards for a given election event id.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @return The list of used voting cards for a given election event id.
	 */
	@GetMapping(value = "used-voting-cards/election-event/{electionEventId}", produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	public List<UsedVotingCardDTO> getUsedVotingCardsByElectionEventId(
			@PathVariable(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId) {

		validateUUID(electionEventId);

		return verificationCardService.getUsedVotingCardsByElectionEventId(electionEventId);
	}

}
