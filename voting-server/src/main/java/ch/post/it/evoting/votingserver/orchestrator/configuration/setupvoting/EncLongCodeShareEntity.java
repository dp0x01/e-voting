/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "ENC_LONG_CODE_SHARE")
@SuppressWarnings("java:S1068")
class EncLongCodeShareEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ENC_LONG_CODE_SHARE_GENERATOR")
	@SequenceGenerator(name = "ENC_LONG_CODE_SHARE_GENERATOR", sequenceName = "ENC_LONG_CODE_SHARE_SEQ", allocationSize = 1)
	private Long id;

	private String electionEventId;

	private String verificationCardSetId;

	private int chunkId;

	private int nodeId;

	private byte[] encLongCodeShare;

	@Version
	private Integer changeControlId;

	public EncLongCodeShareEntity() {

	}

	public EncLongCodeShareEntity(final String electionEventId, final String verificationCardSetId, final int chunkId,
			int nodeId, final byte[] encLongCodeShare) {
		this.electionEventId = validateUUID(electionEventId);
		this.verificationCardSetId = validateUUID(verificationCardSetId);
		checkState(chunkId >= 0);
		this.chunkId = chunkId;
		checkState(NODE_IDS.contains(nodeId));
		this.nodeId = nodeId;
		this.encLongCodeShare = checkNotNull(encLongCodeShare);
	}

	public byte[] getEncLongCodeShare() {
		return encLongCodeShare;
	}
}
