/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.tally;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineResponsePayload;
import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;
import ch.post.it.evoting.votingserver.orchestrator.voting.CommandFacade;

@Service
public class MixDecryptOnlineConsumer {

	private static final Logger LOGGER = LoggerFactory.getLogger(MixDecryptOnlineConsumer.class);

	private final ObjectMapper objectMapper;
	private final MixnetPayloadService mixnetPayloadService;
	private final MixDecryptOnlineProducer mixDecryptOnlineProducer;
	private final TransactionalCommandRepositorySave transactionalCommandRepositorySave;

	public MixDecryptOnlineConsumer(
			final ObjectMapper objectMapper,
			final MixnetPayloadService mixnetPayloadService,
			final MixDecryptOnlineProducer mixDecryptOnlineProducer,
			final TransactionalCommandRepositorySave transactionalCommandRepositorySave) {
		this.objectMapper = objectMapper;
		this.mixnetPayloadService = mixnetPayloadService;
		this.mixDecryptOnlineProducer = mixDecryptOnlineProducer;
		this.transactionalCommandRepositorySave = transactionalCommandRepositorySave;
	}

	@RabbitListener(queues = "#{queueNameResolver.get(\"MIX_DEC_ONLINE_RESPONSE_PATTERN\")}")
	public void consumer(final Message message) {
		try {
			final String correlationId = message.getMessageProperties().getCorrelationId();
			final byte[] byteContent = message.getBody();

			final MixDecryptOnlineResponsePayload payload = objectMapper.readValue(byteContent, MixDecryptOnlineResponsePayload.class);

			final String electionEventId = payload.controlComponentShufflePayload().getElectionEventId();
			final String ballotBoxId = payload.controlComponentShufflePayload().getBallotBoxId();
			final int nodeId = payload.controlComponentShufflePayload().getNodeId();

			final String contextId = String.join("-", Arrays.asList(electionEventId, ballotBoxId));
			final Context context = Context.MIXING_TALLY_MIX_DEC_ONLINE;

			LOGGER.info("Received mix online response [nodeId:{}, electionEventId:{}, ballotBoxId:{}, correlationId:{}]", nodeId, electionEventId,
					ballotBoxId, correlationId);

			transactionalCommandRepositorySave.save(correlationId, contextId, context, nodeId, payload, byteContent);

			if (nodeId < NODE_IDS.size()) {
				sendNextRequest(correlationId, electionEventId, ballotBoxId, nodeId);
			}

		} catch (final IOException ex) {
			throw new UncheckedIOException("Failed to process the mixing DTO", ex);
		}
	}

	private void sendNextRequest(final String correlationId, final String electionEventId, final String ballotBoxId, final int nodeId) {
		final int nextNodeId = nodeId + 1;
		final List<ControlComponentShufflePayload> shufflePayloads = mixnetPayloadService.getControlComponentShufflePayloadsOrderByNodeId(
				electionEventId, ballotBoxId);
		mixDecryptOnlineProducer.send(electionEventId, ballotBoxId, correlationId, nextNodeId, shufflePayloads);
		LOGGER.info("Sent next request to node {} [electionEventId:{}, ballotBoxId:{}, correlationId:{}]", nextNodeId, electionEventId, ballotBoxId,
				correlationId);
	}

	@Service
	static class TransactionalCommandRepositorySave {

		private final CommandFacade commandFacade;
		private final MixnetPayloadService mixnetPayloadService;

		public TransactionalCommandRepositorySave(
				final CommandFacade commandFacade,
				final MixnetPayloadService mixnetPayloadService) {
			this.commandFacade = commandFacade;
			this.mixnetPayloadService = mixnetPayloadService;
		}

		@Transactional(propagation = Propagation.REQUIRES_NEW)
		public void save(final String correlationId, final String contextId, final Context context, final int nodeId,
				final MixDecryptOnlineResponsePayload payload, final byte[] byteContent) {

			commandFacade.saveResponse(byteContent, correlationId, contextId, context, nodeId);

			final ControlComponentBallotBoxPayload controlComponentBallotBoxPayload = payload.controlComponentBallotBoxPayload();
			mixnetPayloadService.saveControlComponentBallotBoxPayload(controlComponentBallotBoxPayload);

			final ControlComponentShufflePayload mixnetShufflePayload = payload.controlComponentShufflePayload();
			mixnetPayloadService.saveShufflePayload(mixnetShufflePayload);
		}

	}

}
