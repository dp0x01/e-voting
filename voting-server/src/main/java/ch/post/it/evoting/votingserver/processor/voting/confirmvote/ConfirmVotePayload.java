/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting.confirmvote;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.votingserver.processor.voting.AuthenticationChallenge;

/**
 * Confirm vote payload sent by the voting-client.
 *
 * @param contextIds              the context ids. Must be non-null.
 * @param encryptionGroup         the encryption group use to deserialize {@code confirmationKey}. Must be non-null.
 * @param confirmationKey         the confirmation key as a {@link GqElement}. Must be non-null.
 * @param authenticationChallenge the authentication challenge. Must be non-null.
 */
@JsonDeserialize(using = ConfirmVotePayloadDeserializer.class)
record ConfirmVotePayload(ContextIds contextIds,
						  GqGroup encryptionGroup,
						  GqElement confirmationKey,
						  AuthenticationChallenge authenticationChallenge) {

	/**
	 * @throws NullPointerException     if any parameter is null.
	 * @throws IllegalArgumentException if {@code encryptionGroup} does not match the group of {@code confirmationKey}.
	 */
	ConfirmVotePayload {
		checkNotNull(contextIds);
		checkNotNull(encryptionGroup);
		checkNotNull(confirmationKey);
		checkNotNull(authenticationChallenge);

		checkArgument(encryptionGroup.equals(confirmationKey.getGroup()), "The encryption group must match the confirmation key's group.");
	}

}
