/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting.authenticatevoter;

import static com.google.common.base.Preconditions.checkNotNull;

import com.fasterxml.jackson.annotation.JsonInclude;

import ch.post.it.evoting.votingserver.processor.VerificationCardState;
import ch.post.it.evoting.votingserver.processor.VotingClientPublicKeys;
import ch.post.it.evoting.votingserver.processor.voting.VoterAuthenticationData;

/**
 * Response to the voting-client after a successful authentication.
 * <p>
 * During a re-login when the verification card is {@link VerificationCardState#CONFIRMED}, only {@code verificationCardState} and
 * {@code voterMaterial} are present.
 *
 * @param verificationCardState   the current state of the verification card.
 * @param voterMaterial           the material associated to this verification card.
 * @param voterAuthenticationData the voter authentication data associated to this verification card.
 * @param verificationCard        the verification card as a json string.
 * @param votingClientPublicKeys  the voting client public keys corresponding to this verification card.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
record AuthenticateVoterResponsePayload(VerificationCardState verificationCardState,
										VoterMaterial voterMaterial,
										VoterAuthenticationData voterAuthenticationData,
										String verificationCard,
										VotingClientPublicKeys votingClientPublicKeys) {

	/**
	 * @throws NullPointerException if {@code verificationCardState} or {@code voterMaterial} is null, or if any other is null when the state is
	 *                              {@link VerificationCardState#INITIAL} or {@link VerificationCardState#SENT}.
	 */
	AuthenticateVoterResponsePayload {
		checkNotNull(verificationCardState);

		if (VerificationCardState.INITIAL.equals(verificationCardState) || VerificationCardState.SENT.equals(verificationCardState)) {
			checkNotNull(voterAuthenticationData);
			checkNotNull(verificationCard);
			checkNotNull(votingClientPublicKeys);
		}

		checkNotNull(voterMaterial);
	}

	/**
	 * @throws NullPointerException if any field is null.
	 */
	AuthenticateVoterResponsePayload(final VerificationCardState verificationCardState, final VoterMaterial voterMaterial) {
		this(checkNotNull(verificationCardState), checkNotNull(voterMaterial), null, null, null);
	}

}
