/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting;

import static ch.post.it.evoting.cryptoprimitives.internal.utils.ByteArrays.cutToBitLength;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.integerToByteArray;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.stringToByteArray;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.primitives.Bytes.concat;

import java.math.BigInteger;
import java.time.Instant;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.cryptoprimitives.hashing.Argon2;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.votingserver.processor.VerificationCardService;

@Service
public class VerifyAuthenticationChallengeAlgorithm {

	private final Hash hash;
	private final Argon2 argon2;
	private final Base64 base64;
	private final VerificationCardService verificationCardService;

	public VerifyAuthenticationChallengeAlgorithm(
			final Hash hash,
			final Argon2 argon2,
			final Base64 base64,
			final VerificationCardService verificationCardService) {
		this.hash = hash;
		this.argon2 = argon2;
		this.base64 = base64;
		this.verificationCardService = verificationCardService;
	}

	/**
	 * Verifies the authentication challenge of a given verification card.
	 * <p>
	 * The verification is successful if all of the following are all fulfilled:
	 *     <ul>
	 *         <li>The number of failed authentication attempts for the given verification card must be strictly smaller than 5.</li>
	 *         <li>The time stamp of the last successful authentication attempt must be strictly smaller than the current timestamp.</li>
	 *         <li>The authentication challenge of the last successful authentication must be different from the current challenge.</li>
	 *         <li>The current authentication challenge must correspond to the expected challenge for the current timestamp.</li>
	 *     </ul>
	 * </p>
	 *
	 * @param context the {@link VerifyAuthenticationChallengeContext} containing the election event id and the authentication step. Must be
	 *                non-null.
	 * @param input   the {@link VerifyAuthenticationChallengeInput} containing the credential id and the challenges to be verified. Must be
	 *                non-null.
	 * @return {@link VerifyAuthenticationChallengeOutput} containing the result of the verification.
	 * @throws NullPointerException if the context or the input is null.
	 */
	@SuppressWarnings("java:S117")
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public VerifyAuthenticationChallengeOutput verifyAuthenticationChallenge(final VerifyAuthenticationChallengeContext context,
			final VerifyAuthenticationChallengeInput input) {
		checkNotNull(context);
		checkNotNull(input);

		final String ee = context.electionEventId();
		final String authStep = context.authenticationStep().getName();

		final String credentialID_id = input.credentialId();
		final String hhAuth_id = input.derivedAuthenticationChallenge();
		final String hAuth_id = input.baseAuthenticationChallenge();
		final BigInteger nonce = input.authenticationNonce();

		final long TS = getTimeStamp();
		final long T_1 = TS / 30;
		final long T_0 = T_1 - 1;
		final int attempts_id = verificationCardService.getAuthenticationAttempts(credentialID_id);

		if (attempts_id >= 5) {
			final String errorMessage = String.format(
					"The credentialId %s already used the maximum number of authentication attempts and is no longer allowed to authenticate.",
					credentialID_id);
			return VerifyAuthenticationChallengeOutput.authenticationChallengeError(errorMessage);
		}
		if (verificationCardService.getSuccessfulAuthenticationChallenges(credentialID_id).contains(hhAuth_id)) {
			final String errorMessage = String.format(
					"The derivedAuthenticationChallenge %s for the credentialId %s was already used and is no longer allowed to authenticate.",
					hhAuth_id, credentialID_id);
			return VerifyAuthenticationChallengeOutput.authenticationChallengeError(errorMessage);
		}

		final byte[] salt_id = cutToBitLength(hash.recursiveHash(HashableString.from(ee),
						HashableString.from(credentialID_id),
						HashableString.from("dAuth"),
						HashableString.from(authStep),
						HashableBigInteger.from(nonce)),
				128);
		final byte[] bhhAuth_id_1_prime = argon2.getArgon2id(concat(stringToByteArray(hAuth_id), integerToByteArray(BigInteger.valueOf(T_1))),
				salt_id);
		final String hhAuth_id_1_prime = base64.base64Encode(bhhAuth_id_1_prime);
		final byte[] bhhAuth_id_0_prime = argon2.getArgon2id(concat(stringToByteArray(hAuth_id), integerToByteArray(BigInteger.valueOf(T_0))),
				salt_id);
		final String hhAuth_id_0_prime = base64.base64Encode(bhhAuth_id_0_prime);

		if (hhAuth_id_1_prime.equals(hhAuth_id) || hhAuth_id_0_prime.equals(hhAuth_id)) {
			verificationCardService.setLastTimeStepAndSuccessfulAuthenticationChallenge(credentialID_id, T_1, hhAuth_id);
			return VerifyAuthenticationChallengeOutput.success();
		} else {
			verificationCardService.incrementAuthenticationAttempts(credentialID_id);
			final String errorMessage = String.format(
					"The Authentication attempt (%d) for credentialID %s failed. Presumably, the voter entered a wrong extended authentication factor. The voter has %d authentication attempts left.",
					attempts_id + 1, credentialID_id, 5 - (attempts_id + 1));
			return VerifyAuthenticationChallengeOutput.invalidExtendedFactor(errorMessage, 5 - (attempts_id + 1));
		}
	}

	private long getTimeStamp() {
		return Instant.now().getEpochSecond();
	}

}
