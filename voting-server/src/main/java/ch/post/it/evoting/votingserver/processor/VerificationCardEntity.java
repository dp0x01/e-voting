/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Version;

import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationData;

@Entity
@Table(name = "VERIFICATION_CARD")
public class VerificationCardEntity {

	@Id
	private String verificationCardId;

	@ManyToOne
	@JoinColumn(name = "VERIFICATION_CARD_SET_FK_ID", referencedColumnName = "VERIFICATION_CARD_SET_ID")
	private VerificationCardSetEntity verificationCardSetEntity;

	private String credentialId;

	private String votingCardId;

	@Convert(converter = VoterAuthenticationDataConverter.class)
	private SetupComponentVoterAuthenticationData voterAuthenticationData;

	@PrimaryKeyJoinColumn
	@OneToOne(fetch = FetchType.LAZY, optional = false)
	private VerificationCardStateEntity verificationCardStateEntity;

	@Version
	private Integer changeControlId;

	public VerificationCardEntity() {
	}

	public VerificationCardEntity(final String verificationCardId, final VerificationCardSetEntity verificationCardSetEntity,
			final String credentialId, final String votingCardId, final SetupComponentVoterAuthenticationData voterAuthenticationData,
			final VerificationCardStateEntity verificationCardStateEntity) {
		this.verificationCardId = validateUUID(verificationCardId);
		this.verificationCardSetEntity = checkNotNull(verificationCardSetEntity);
		this.credentialId = validateUUID(credentialId);
		this.votingCardId = validateUUID(votingCardId);
		this.voterAuthenticationData = checkNotNull(voterAuthenticationData);
		this.verificationCardStateEntity = checkNotNull(verificationCardStateEntity);
	}

	public String getVerificationCardId() {
		return verificationCardId;
	}

	public VerificationCardSetEntity getVerificationCardSetEntity() {
		return verificationCardSetEntity;
	}

	public String getCredentialId() {
		return credentialId;
	}

	public String getVotingCardId() {
		return votingCardId;
	}

	public SetupComponentVoterAuthenticationData getVoterAuthenticationData() {
		return voterAuthenticationData;
	}

	public VerificationCardStateEntity getVerificationCardStateEntity() {
		return verificationCardStateEntity;
	}

}
