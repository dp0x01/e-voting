/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.votingserver.common.Constants;
import ch.post.it.evoting.votingserver.processor.idempotence.IdempotenceContext;
import ch.post.it.evoting.votingserver.processor.idempotence.IdempotenceService;

/**
 * Web service for retrieving the voting client public keys.
 */
@RestController
@RequestMapping("api/v1/processor/configuration")
public class SetupComponentPublicKeysController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentPublicKeysController.class);

	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final SetupComponentPublicKeysService setupComponentPublicKeysService;
	private final IdempotenceService<IdempotenceContext> idempotenceService;

	public SetupComponentPublicKeysController(
			final SignatureKeystore<Alias> signatureKeystoreService,
			final SetupComponentPublicKeysService setupComponentPublicKeysService,
            final IdempotenceService<IdempotenceContext> idempotenceService) {
		this.signatureKeystoreService = signatureKeystoreService;
		this.setupComponentPublicKeysService = setupComponentPublicKeysService;
		this.idempotenceService = idempotenceService;
	}

	@PostMapping("setupkeys/electionevent/{electionEventId}")
	public void saveSetupComponentPublicKeys(
			@PathVariable(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@RequestBody
			final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload) {

		validateUUID(electionEventId);
		verifyPayloadSignature(setupComponentPublicKeysPayload);
		checkArgument(electionEventId.equals(setupComponentPublicKeysPayload.getElectionEventId()));

		idempotenceService.execute(IdempotenceContext.SAVE_SETUP_COMPONENT_PUBLIC_KEYS, electionEventId,
				() -> setupComponentPublicKeysService.saveSetupComponentPublicKeys(setupComponentPublicKeysPayload));

		LOGGER.info("Successfully uploaded and saved the setup component public keys. [electionEventId: {}]", electionEventId);
	}

	@GetMapping("setupkeys/tenant/{tenantId}/electionevent/{electionEventId}")
	public VotingClientPublicKeys getVotingClientPublicKeys(
			@PathVariable(Constants.PARAMETER_VALUE_TENANT_ID)
			final String tenantId,
			@PathVariable(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId) {

		checkNotNull(tenantId);
		validateUUID(electionEventId);

		return setupComponentPublicKeysService.getVotingClientPublicKeys(electionEventId);
	}

	private void verifyPayloadSignature(final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload) {
		final String electionEventId = setupComponentPublicKeysPayload.getElectionEventId();

		final CryptoPrimitivesSignature signature = setupComponentPublicKeysPayload.getSignature();

		checkState(signature != null, "The signature of the setup component public keys payload is null. [electionEventId: %s]", electionEventId);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentPublicKeys(electionEventId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.SDM_CONFIG, setupComponentPublicKeysPayload,
					additionalContextData, signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of the setup component public keys. [electionEventId: %s]", electionEventId));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(SetupComponentPublicKeysPayload.class,
					String.format("[electionEventId: %s]", electionEventId));
		}
	}
}
