/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateBase64Encoded;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.votingserver.common.Constants.TWO_POW_256;
import static com.google.common.base.Preconditions.checkArgument;

import java.math.BigInteger;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;

/**
 * Regroups the data of the authentication challenge.
 *
 * @param derivedVoterIdentifier         which corresponds to the credentialId. Must be a valid uuid.
 * @param derivedAuthenticationChallenge the derived authentication challenge. Must be a valid base64 encoded string.
 */
public record AuthenticationChallenge(String derivedVoterIdentifier, String derivedAuthenticationChallenge, BigInteger authenticationNonce) {

	private static final int l_HB64 = 44;

	/**
	 * @throws NullPointerException      if any parameter is null.
	 * @throws FailedValidationException if {@code derivedVoterIdentifier} is not a valid uuid or {@code derivedAuthenticationChallenge} is not a
	 *                                   valid base64 encoded string.
	 * @throws IllegalArgumentException  if {@code derivedAuthenticationChallenge} is not of length {@value l_HB64}.
	 * @throws IllegalArgumentException  if the {@code authenticationNonce} is smaller than 0 or greater than 2<sup>256</sup>.
	 */
	public AuthenticationChallenge {
		validateUUID(derivedVoterIdentifier);
		validateBase64Encoded(derivedAuthenticationChallenge);

		final int challengeLength = derivedAuthenticationChallenge.length();
		checkArgument(challengeLength == l_HB64, "The derived authentication challenge must be of length of l_HB64. [actual: {}, l_BH64: {}]",
				challengeLength, l_HB64);

		checkArgument(authenticationNonce.compareTo(BigInteger.ZERO) >= 0);
		checkArgument(authenticationNonce.compareTo(TWO_POW_256) < 0);
	}

}
