/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationData;
import ch.post.it.evoting.votingserver.processor.VerificationCardService;
import ch.post.it.evoting.votingserver.processor.VerificationCardSetEntity;
import ch.post.it.evoting.votingserver.processor.VerificationCardState;
import ch.post.it.evoting.votingserver.processor.configuration.setupvoting.VoterAuthenticationDataService;
import ch.post.it.evoting.votingserver.processor.voting.VerifyAuthenticationChallengeOutput.VerifyAuthenticationChallengeStatus;

@Service
public class VerifyAuthenticationChallengeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerifyAuthenticationChallengeService.class);
	private static final List<VerificationCardState> ALLOWED_STATES = List.of(VerificationCardState.INITIAL, VerificationCardState.SENT,
			VerificationCardState.CONFIRMED);

	private final VerificationCardService verificationCardService;
	private final VoterAuthenticationDataService voterAuthenticationDataService;
	private final VerifyAuthenticationChallengeAlgorithm verifyAuthenticationChallengeAlgorithm;

	public VerifyAuthenticationChallengeService(
			final VerificationCardService verificationCardService,
			final VoterAuthenticationDataService voterAuthenticationDataService,
			final VerifyAuthenticationChallengeAlgorithm verifyAuthenticationChallengeAlgorithm) {
		this.verificationCardService = verificationCardService;
		this.voterAuthenticationDataService = voterAuthenticationDataService;
		this.verifyAuthenticationChallengeAlgorithm = verifyAuthenticationChallengeAlgorithm;
	}

	/**
	 * Verifies the authentication challenge for the given {@code credentialId}.
	 *
	 * @param electionEventId         the election event id.
	 * @param authenticationStep      the {@link AuthenticationStep} for which to verify the challenge.
	 * @param authenticationChallenge the {@link AuthenticationChallenge} containing the credential id and derived authentication challenge.
	 * @throws NullPointerException                   if any parameter is null.
	 * @throws FailedValidationException              if {@code electionEventId} is invalid.
	 * @throws VerifyAuthenticationChallengeException if the verification of the authentication challenge is unsuccessful for the given
	 *                                                {@code authenticationChallenge}.
	 */
	public void verifyAuthenticationChallenge(final String electionEventId, final AuthenticationStep authenticationStep,
			final AuthenticationChallenge authenticationChallenge) {

		validateUUID(electionEventId);
		checkNotNull(authenticationStep);
		checkNotNull(authenticationChallenge);

		final String credentialId = authenticationChallenge.derivedVoterIdentifier();

		// Check that verification card is not blocked or in an incoherent state with respect to the current authenticationStep.
		validateVerificationCardState(authenticationStep, credentialId);

		// Check that the ballot box is still opened.
		validateBallotBoxTime(authenticationStep, credentialId);

		// Retrieve base authentication challenge.
		final SetupComponentVoterAuthenticationData voterAuthenticationData = voterAuthenticationDataService.getVoterAuthenticationData(
				electionEventId, credentialId);
		final String baseAuthenticationChallenge = voterAuthenticationData.baseAuthenticationChallenge();

		// Verify authentication challenge.
		final String derivedAuthenticationChallenge = authenticationChallenge.derivedAuthenticationChallenge();
		final BigInteger authenticationNonce = authenticationChallenge.authenticationNonce();

		final VerifyAuthenticationChallengeContext context = new VerifyAuthenticationChallengeContext(electionEventId, authenticationStep);
		final VerifyAuthenticationChallengeInput input = new VerifyAuthenticationChallengeInput(credentialId, derivedAuthenticationChallenge,
				baseAuthenticationChallenge, authenticationNonce);
		final VerifyAuthenticationChallengeOutput output = verifyAuthenticationChallengeAlgorithm.verifyAuthenticationChallenge(context, input);

		final VerifyAuthenticationChallengeStatus status = output.getStatus();

		if (!VerifyAuthenticationChallengeStatus.SUCCESS.equals(status)) {
			LOGGER.error("Verification of authentication challenge failed. [electionEventId: {}, credentialId: {}, reason: {}]", electionEventId,
					credentialId, output.getErrorMessage());
			throw new VerifyAuthenticationChallengeException(status, output.getAttemptsLeft());
		}

		LOGGER.info("Successfully verified authentication challenge. [electionEventId: {}, credentialId: {}]", electionEventId, credentialId);
	}

	private void validateVerificationCardState(final AuthenticationStep authenticationStep, final String credentialId) {
		final VerificationCardState verificationCardState = verificationCardService.getVerificationCardState(credentialId);

		// If the verification card is in an inactive state, return blocked.
		if (verificationCardState.isInactive()) {
			throw new VerifyAuthenticationChallengeException(VerifyAuthenticationChallengeStatus.VOTING_CARD_BLOCKED);
		}

		// Otherwise check state is coherent with current step.
		switch (authenticationStep) {
		case AUTHENTICATE_VOTER -> {
			if (!ALLOWED_STATES.contains(verificationCardState)) {
				throw new VerifyAuthenticationChallengeException(VerifyAuthenticationChallengeStatus.VOTING_CARD_BLOCKED);
			}
		}
		case SEND_VOTE -> {
			if (!VerificationCardState.INITIAL.equals(verificationCardState)) {
				throw new VerifyAuthenticationChallengeException(VerifyAuthenticationChallengeStatus.AUTHENTICATION_CHALLENGE_ERROR);
			}
		}
		case CONFIRM_VOTE -> {
			if (!VerificationCardState.SENT.equals(verificationCardState)) {
				throw new VerifyAuthenticationChallengeException(VerifyAuthenticationChallengeStatus.AUTHENTICATION_CHALLENGE_ERROR);
			}
		}
		}
	}

	private void validateBallotBoxTime(final AuthenticationStep authenticationStep, final String credentialId) {
		final VerificationCardSetEntity verificationCardSetEntity = verificationCardService.getVerificationCardSetEntity(credentialId);

		final LocalDateTime now = LocalDateTime.now();
		final LocalDateTime ballotBoxStartTime = verificationCardSetEntity.getBallotBoxStartTime();
		// The voter can still vote and confirm during the grace period window after the ballot box is closed.
		LocalDateTime ballotBoxFinishTime = verificationCardSetEntity.getBallotBoxFinishTime();
		if (AuthenticationStep.SEND_VOTE.equals(authenticationStep) || AuthenticationStep.CONFIRM_VOTE.equals(authenticationStep)) {
			ballotBoxFinishTime = ballotBoxFinishTime.plusSeconds(verificationCardSetEntity.getGracePeriod());
		}

		if (now.isBefore(ballotBoxStartTime)) {
			throw new VerifyAuthenticationChallengeException(VerifyAuthenticationChallengeStatus.BALLOT_BOX_NOT_STARTED);
		} else if (now.isAfter(ballotBoxFinishTime)) {
			throw new VerifyAuthenticationChallengeException(VerifyAuthenticationChallengeStatus.BALLOT_BOX_ENDED);
		}
	}

}
