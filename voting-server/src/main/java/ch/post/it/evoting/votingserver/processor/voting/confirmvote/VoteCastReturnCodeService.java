/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting.confirmvote;

import static ch.post.it.evoting.votingserver.common.Constants.MAX_CONFIRMATION_ATTEMPTS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.voting.confirmvote.ConfirmationKey;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponentlVCCSharePayload;
import ch.post.it.evoting.domain.voting.confirmvote.LongVoteCastReturnCodesShare;
import ch.post.it.evoting.domain.voting.confirmvote.VotingServerConfirmPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.votingserver.common.ConfirmationKeyInvalidException;
import ch.post.it.evoting.votingserver.orchestrator.OrchestratorFacade;
import ch.post.it.evoting.votingserver.processor.ElectionEventService;
import ch.post.it.evoting.votingserver.processor.VerificationCardService;
import ch.post.it.evoting.votingserver.processor.VerificationCardState;
import ch.post.it.evoting.votingserver.processor.voting.ReturnCodesMappingTable;
import ch.post.it.evoting.votingserver.processor.voting.ReturnCodesMappingTableSupplier;

/**
 * Generate the short Vote Cast Return Code based on the confirmation message - in interaction with the control components.
 */
@Service
public class VoteCastReturnCodeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VoteCastReturnCodeService.class);

	private final OrchestratorFacade orchestratorFacade;
	private final ExtractVCCAlgorithm extractVCCAlgorithm;
	private final ElectionEventService electionEventService;
	private final VerificationCardService verificationCardService;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final ReturnCodesMappingTableSupplier returnCodesMappingTableSupplier;

	public VoteCastReturnCodeService(
			final OrchestratorFacade orchestratorFacade,
			final ExtractVCCAlgorithm extractVCCAlgorithm,
			final ElectionEventService electionEventService,
			final VerificationCardService verificationCardService,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final ReturnCodesMappingTableSupplier returnCodesMappingTableSupplier) {
		this.orchestratorFacade = orchestratorFacade;
		this.extractVCCAlgorithm = extractVCCAlgorithm;
		this.electionEventService = electionEventService;
		this.verificationCardService = verificationCardService;
		this.signatureKeystoreService = signatureKeystoreService;
		this.returnCodesMappingTableSupplier = returnCodesMappingTableSupplier;
	}

	/**
	 * Calculates in interaction with the control components the short Vote Cast Return Code based on the confirmation message received by the voting
	 * client.
	 *
	 * @param contextIds      the context ids.
	 * @param confirmationKey the confirmation key.
	 * @return The short Vote Cast Return Code.
	 * @throws NullPointerException if any parameter is null.
	 */
	@SuppressWarnings("java:S117")
	public String retrieveShortVoteCastCode(final ContextIds contextIds, final GqElement confirmationKey) {
		checkNotNull(contextIds);
		checkNotNull(confirmationKey);

		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);
		checkArgument(encryptionGroup.equals(confirmationKey.getGroup()),
				"The encryption group does not match the confirmation key's group. [contextIds: %s]", contextIds);

		LOGGER.debug("Generating the short Vote Cast Return Code... [contextIds: {}]", contextIds);

		// Create VotingServerConfirmPayload to send.
		final VotingServerConfirmPayload votingServerConfirmPayload = createVotingServerConfirmPayload(contextIds, confirmationKey);

		// Ask the control components to compute the long vote cast return code shares lCC_j_id.
		final List<ControlComponentlVCCSharePayload> controlComponentlVCCSharePayloads = collectLongVoteCastReturnCodeShares(contextIds,
				votingServerConfirmPayload);

		if (!controlComponentlVCCSharePayloads.stream().allMatch(ControlComponentlVCCSharePayload::isVerified)) {
			final int confirmationAttempt = verificationCardService.incrementConfirmationAttempts(verificationCardId);
			final int remainingAttempts = MAX_CONFIRMATION_ATTEMPTS - confirmationAttempt;

			throw new ConfirmationKeyInvalidException(
					String.format("The ControlComponentlVCCSharePayload are not all verified. [contextIds: %s]", contextIds), remainingAttempts);
		}

		final ExtractVCCContext extractVCCContext = new ExtractVCCContext(votingServerConfirmPayload.getEncryptionGroup(), electionEventId);

		// Retrieve short codes by combining CCR shares and looking up the CMTable.
		final GroupVector<GqElement, GqGroup> lVCCShares = controlComponentlVCCSharePayloads.stream()
				.map(ControlComponentlVCCSharePayload::getLongVoteCastReturnCodesShare)
				.map(o -> o.orElseThrow(() -> new IllegalStateException("We should not reach this state.")))
				.map(LongVoteCastReturnCodesShare::longVoteCastReturnCodeShare)
				.collect(GroupVector.toGroupVector());
		final ReturnCodesMappingTable returnCodesMappingTable = returnCodesMappingTableSupplier.get(verificationCardSetId);
		final ExtractVCCInput extractVCCInput = new ExtractVCCInput(lVCCShares, verificationCardId, returnCodesMappingTable);

		final ExtractVCCOutput voteCastReturnCodeOutput = extractVCCAlgorithm.extractVCC(extractVCCContext, extractVCCInput);
		LOGGER.info("Short Vote Cast Return Code successfully retrieved. [contextIds: {}]", contextIds);

		// Save the short Vote Cast Return Code for later use in case of a re-login and transition to CONFIRMED.
		final String shortVoteCastReturnCode = voteCastReturnCodeOutput.shortVoteCastReturnCode();
		verificationCardService.saveConfirmedState(verificationCardId, shortVoteCastReturnCode);
		LOGGER.info("Successfully saved state. [contextIds: {}, state: {}]", contextIds, VerificationCardState.CONFIRMED);

		return shortVoteCastReturnCode;
	}

	private VotingServerConfirmPayload createVotingServerConfirmPayload(final ContextIds contextIds, final GqElement confirmationKeyElement) {
		final GqGroup encryptionGroup = confirmationKeyElement.getGroup();
		final ConfirmationKey confirmationKey = new ConfirmationKey(contextIds, confirmationKeyElement);

		// Create and sign payload.
		final VotingServerConfirmPayload votingServerConfirmPayload = new VotingServerConfirmPayload(encryptionGroup, confirmationKey);
		final CryptoPrimitivesSignature signature = generateSignature(votingServerConfirmPayload, contextIds);
		votingServerConfirmPayload.setSignature(signature);
		LOGGER.info("Successfully signed the voting server confirm payload. [contextIds: {}]", contextIds);

		return votingServerConfirmPayload;
	}

	private CryptoPrimitivesSignature generateSignature(final VotingServerConfirmPayload votingServerConfirmPayload, final ContextIds contextIds) {
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final Hashable additionalContextData = ChannelSecurityContextData.votingServerConfirm(electionEventId, verificationCardSetId,
				verificationCardId);

		try {
			final byte[] signature = signatureKeystoreService.generateSignature(votingServerConfirmPayload, additionalContextData);

			return new CryptoPrimitivesSignature(signature);

		} catch (final SignatureException se) {
			final String message = String.format("Failed to sign the voting server confirm payload. [contextIds: %s]", contextIds);
			throw new IllegalStateException(message, se);
		}
	}

	private List<ControlComponentlVCCSharePayload> collectLongVoteCastReturnCodeShares(final ContextIds contextIds,
			final VotingServerConfirmPayload votingServerConfirmPayload) {

		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final List<ControlComponentlVCCSharePayload> longReturnCodesSharePayloads = orchestratorFacade.getLongVoteCastReturnCodesContributions(
				electionEventId, verificationCardSetId, verificationCardId, votingServerConfirmPayload);

		// Vote Cast Return Code computation response correctly received.
		LOGGER.info("Successfully retrieved the Long Vote Cast Return Code shares. [contextIds: {}]", contextIds);

		verifyPayloadSignatures(contextIds, longReturnCodesSharePayloads);
		LOGGER.info("Successfully verified the Long Vote Cast Return Code shares. [contextIds: {}]", contextIds);

		return longReturnCodesSharePayloads.stream()
				.sorted(Comparator.comparingInt(ControlComponentlVCCSharePayload::getNodeId))
				.toList();
	}

	private void verifyPayloadSignatures(final ContextIds contextIds,
			final List<ControlComponentlVCCSharePayload> controlComponentlVCCSharePayloads) {

		for (final ControlComponentlVCCSharePayload payload : controlComponentlVCCSharePayloads) {
			final int nodeId = payload.getNodeId();
			final CryptoPrimitivesSignature signature = payload.getSignature();

			checkState(signature != null, "The signature of the Control Component lVCC Share payload is null. [nodeId: %s, contextIds: %s]",
					nodeId, contextIds);

			final Hashable additionalContextData = ChannelSecurityContextData.controlComponentlVCCShare(nodeId, contextIds.electionEventId(),
					contextIds.verificationCardSetId(), contextIds.verificationCardId());

			final boolean isSignatureValid;
			try {
				isSignatureValid = signatureKeystoreService.verifySignature(Alias.getControlComponentByNodeId(nodeId), payload, additionalContextData,
						signature.signatureContents());
			} catch (final SignatureException e) {
				throw new IllegalStateException(
						String.format("Could not verify the signature of the Control Component lVCC Share payload. [nodeId: %s, contextIds: %s]",
								nodeId, contextIds));
			}
			if (!isSignatureValid) {
				throw new InvalidPayloadSignatureException(ControlComponentlVCCSharePayload.class,
						String.format("[nodeId: %s, contextIds: %s]", nodeId, contextIds));
			}
		}
	}

}
