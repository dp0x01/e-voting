/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.annotations.VisibleForTesting;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.votingserver.processor.idempotence.IdempotenceContext;
import ch.post.it.evoting.votingserver.processor.idempotence.IdempotenceService;

/**
 * Web service for saving and retrieving the return codes mapping table.
 */
@RestController
@RequestMapping("api/v1/processor/configuration/returncodesmappingtable")
public class ReturnCodesMappingTableController {

	@VisibleForTesting
	static final String PARAMETER_VALUE_ELECTION_EVENT_ID = "electionEventId";
	@VisibleForTesting
	static final String PARAMETER_VALUE_VERIFICATION_CARD_SET_ID = "verificationCardSetId";
	@VisibleForTesting
	static final String PARAMETER_VALUE_CHUNK_ID = "chunkId";

	private static final Logger LOGGER = LoggerFactory.getLogger(ReturnCodesMappingTableController.class);

	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final ReturnCodesMappingTableService returnCodesMappingTableService;

	private final IdempotenceService<IdempotenceContext> idempotenceService;

	public ReturnCodesMappingTableController(
			final SignatureKeystore<Alias> signatureKeystoreService,
			final ReturnCodesMappingTableService returnCodesMappingTableService,
            final IdempotenceService<IdempotenceContext> idempotenceService) {
		this.signatureKeystoreService = signatureKeystoreService;
		this.returnCodesMappingTableService = returnCodesMappingTableService;
		this.idempotenceService = idempotenceService;
	}

	@PostMapping("electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunk/{chunkId}")
	public void saveReturnCodesMappingTable(
			@PathVariable(PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@PathVariable(PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@PathVariable(PARAMETER_VALUE_CHUNK_ID)
			final int chunkId,
			@RequestBody
			final SetupComponentCMTablePayload setupComponentCMTablePayload) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(setupComponentCMTablePayload);
		checkArgument(electionEventId.equals(setupComponentCMTablePayload.getElectionEventId()));
		checkArgument(verificationCardSetId.equals(setupComponentCMTablePayload.getVerificationCardSetId()));
		checkArgument(Objects.equals(chunkId, setupComponentCMTablePayload.getChunkId()));

		verifyPayloadSignature(setupComponentCMTablePayload);

		idempotenceService.execute(IdempotenceContext.SAVE_RETURN_CODES_MAPPING_TABLE,
				String.format("%s-%s-%s", electionEventId, verificationCardSetId, chunkId),
				() -> returnCodesMappingTableService.saveReturnCodesMappingTable(setupComponentCMTablePayload));
		LOGGER.info("Successfully saved the return codes mapping table. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);
	}

	private void verifyPayloadSignature(final SetupComponentCMTablePayload setupComponentCMTablePayload) {

		final String electionEventId = setupComponentCMTablePayload.getElectionEventId();
		final String verificationCardSetId = setupComponentCMTablePayload.getVerificationCardSetId();

		final CryptoPrimitivesSignature signature = setupComponentCMTablePayload.getSignature();
		checkState(signature != null,
				"The signature of the setup component CMTable payload is null. [electionEventId: %s, verificationCardSetId: %s]", electionEventId,
				verificationCardSetId);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentCMTable(electionEventId, verificationCardSetId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.SDM_CONFIG, setupComponentCMTablePayload, additionalContextData,
					signature.signatureContents());

		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format(
							"Couldn't verify the signature of the setup component CMTable payload. [electionEventId: %s, verificationCardSetId: %s]",
							electionEventId, verificationCardSetId), e);
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(SetupComponentCMTablePayload.class,
					String.format("[electionEventId: %s, verificationCardSetId: %s]", electionEventId, verificationCardSetId));
		}
	}
}
