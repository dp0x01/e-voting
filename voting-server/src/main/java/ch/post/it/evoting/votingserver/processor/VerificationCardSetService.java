/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.UncheckedIOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.VerificationCardSetContext;

@Service
public class VerificationCardSetService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationCardSetService.class);

	private final ObjectMapper objectMapper;
	private final ElectionEventService electionEventService;
	private final VerificationCardSetRepository verificationCardSetRepository;

	public VerificationCardSetService(
			final ObjectMapper objectMapper,
			final ElectionEventService electionEventService,
			final VerificationCardSetRepository verificationCardSetRepository) {
		this.objectMapper = objectMapper;
		this.electionEventService = electionEventService;
		this.verificationCardSetRepository = verificationCardSetRepository;
	}

	@Transactional
	public void saveAllFromContext(final String electionEventId, final List<VerificationCardSetContext> verificationCardSetContexts) {
		validateUUID(electionEventId);
		checkNotNull(verificationCardSetContexts);

		final ElectionEventEntity electionEventEntity = electionEventService.retrieveElectionEventEntity(electionEventId);

		final List<VerificationCardSetEntity> verificationCardSetEntities = verificationCardSetContexts.stream()
				.map(verificationCardSetContext -> {
					final String verificationCardSetId = verificationCardSetContext.verificationCardSetId();

					final PrimesMappingTable primesMappingTable = verificationCardSetContext.primesMappingTable();
					final byte[] primesMappingTableBytes;
					try {
						primesMappingTableBytes = objectMapper.writeValueAsBytes(primesMappingTable);
					} catch (final JsonProcessingException e) {
						throw new UncheckedIOException(
								String.format("Failed to serialize primes mapping table. [electionEventId: %s, verificationCardSetId: %s]",
										electionEventId, verificationCardSetId), e);
					}

					return new VerificationCardSetEntity(verificationCardSetId,
							electionEventEntity, verificationCardSetContext.ballotBoxId(), verificationCardSetContext.gracePeriod(),
							verificationCardSetContext.numberOfVotingCards(), verificationCardSetContext.numberOfWriteInFields(),
							primesMappingTableBytes, verificationCardSetContext.ballotBoxStartTime(),
							verificationCardSetContext.ballotBoxFinishTime(), verificationCardSetContext.testBallotBox());

				})
				.toList();

		verificationCardSetRepository.saveAll(verificationCardSetEntities);
		LOGGER.info("Successfully saved verification card sets. [electionEventId: {}]", electionEventId);
	}

	@Transactional
	public VerificationCardSetEntity getVerificationCardSetEntity(final String verificationCardSetId) {
		validateUUID(verificationCardSetId);

		return verificationCardSetRepository.findById(verificationCardSetId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Verification card set not found. [verificationCardSetId: %s]", verificationCardSetId)));
	}

}
