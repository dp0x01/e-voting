/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "SETUP_COMPONENT_VERIFICATION_CARD_KEYSTORE")
public class SetupComponentVerificationCardKeystoreEntity {

	@Id
	private String verificationCardId;

	@MapsId
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VERIFICATION_CARD_ID", referencedColumnName = "VERIFICATION_CARD_ID")
	private VerificationCardEntity verificationCardEntity;

	private byte[] verificationCardKeystore;

	@Version
	private Integer changeControlId;

	public SetupComponentVerificationCardKeystoreEntity() {
	}

	public SetupComponentVerificationCardKeystoreEntity(final VerificationCardEntity verificationCardEntity, final byte[] verificationCardKeystore) {
		this.verificationCardEntity = checkNotNull(verificationCardEntity);
		this.verificationCardKeystore = checkNotNull(verificationCardKeystore);
	}

	public String getVerificationCardId() {
		return verificationCardId;
	}

	public VerificationCardEntity getVerificationCardEntity() {
		return verificationCardEntity;
	}

	public byte[] getVerificationCardKeystore() {
		return verificationCardKeystore;
	}

}
