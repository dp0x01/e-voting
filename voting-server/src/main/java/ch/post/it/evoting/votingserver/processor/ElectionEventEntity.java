/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

@Entity
@Table(name = "ELECTION_EVENT")
public class ElectionEventEntity {

	@Id
	@Column(name = "ELECTION_EVENT_ID")
	private String electionEventId;

	@Convert(converter = EncryptionGroupConverter.class)
	private GqGroup encryptionGroup;

	@Version
	private Integer changeControlId;

	public ElectionEventEntity() {
	}

	public ElectionEventEntity(final String electionEventId, final GqGroup encryptionGroup) {
		this.electionEventId = validateUUID(electionEventId);
		this.encryptionGroup = checkNotNull(encryptionGroup);
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public GqGroup getEncryptionGroup() {
		return encryptionGroup;
	}

	public Integer getChangeControlId() {
		return changeControlId;
	}

}
