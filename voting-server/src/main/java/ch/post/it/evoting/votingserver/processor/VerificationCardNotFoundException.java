/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

public class VerificationCardNotFoundException extends Exception {

	private static final long serialVersionUID = 1;

	public VerificationCardNotFoundException(final String message) {
		super(message);
	}

}