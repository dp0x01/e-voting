/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.votingserver.common.Constants.MAX_AUTHENTICATION_ATTEMPTS;
import static ch.post.it.evoting.votingserver.common.Constants.MAX_CONFIRMATION_ATTEMPTS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;

@Service
public class VerificationCardService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationCardService.class);
	private final VerificationCardRepository verificationCardRepository;
	private final VerificationCardStateService verificationCardStateService;
	private final int votingCardLimitNumber;

	public VerificationCardService(
			final VerificationCardRepository verificationCardRepository,
			final VerificationCardStateService verificationCardStateService,
			@Value("${voting-card.limit.number}")
			final int votingCardLimitNumber) {
		this.verificationCardRepository = verificationCardRepository;
		this.verificationCardStateService = verificationCardStateService;
		this.votingCardLimitNumber = votingCardLimitNumber;
	}

	@Transactional
	public void saveVerificationCards(final List<VerificationCardEntity> verificationCardEntities) {
		checkNotNull(verificationCardEntities);

		// Save cards.
		verificationCardRepository.saveAll(verificationCardEntities);

		// Save associated states.
		final List<VerificationCardStateEntity> verificationCardStateEntities = verificationCardEntities.stream()
				.map(VerificationCardEntity::getVerificationCardStateEntity)
				.toList();
		verificationCardStateService.saveVerificationCardStates(verificationCardStateEntities);

		LOGGER.info("Saved verification cards and states. [amount: {}]", verificationCardEntities.size());
	}

	@Transactional
	public VerificationCardEntity getVerificationCardEntity(final String verificationCardId) {
		validateUUID(verificationCardId);

		return verificationCardRepository.findById(verificationCardId)
				.orElseThrow(
						() -> new IllegalStateException(String.format("Verification card not found. [verificationCardId: %s]", verificationCardId)));
	}

	@Transactional
	public VerificationCardEntity getVerificationCardEntityByCredentialId(final String credentialId) {
		validateUUID(credentialId);

		return verificationCardRepository.findByCredentialId(credentialId)
				.orElseThrow(
						() -> new IllegalStateException(String.format("Verification card not found. [credentialId: %s]", credentialId)));
	}

	@Transactional
	public List<String> getShortChoiceReturnCodes(final String credentialId) {
		validateUUID(credentialId);

		return getVerificationCardEntityByCredentialId(credentialId).getVerificationCardStateEntity().getShortChoiceReturnCodes();
	}

	@Transactional
	public String getShortVoteCastReturnCode(final String credentialId) {
		validateUUID(credentialId);

		return getVerificationCardEntityByCredentialId(credentialId).getVerificationCardStateEntity().getShortVoteCastReturnCode();
	}

	@Transactional
	public VerificationCardState getVerificationCardState(final String credentialId) {
		validateUUID(credentialId);

		return getVerificationCardEntityByCredentialId(credentialId).getVerificationCardStateEntity().getState();
	}

	@Transactional
	public int getAuthenticationAttempts(final String credentialId) {
		validateUUID(credentialId);

		return getVerificationCardEntityByCredentialId(credentialId).getVerificationCardStateEntity().getAuthenticationAttempts();
	}

	@Transactional
	public void incrementAuthenticationAttempts(final String credentialId) {
		validateUUID(credentialId);

		final String verificationCardId = getVerificationCardIdByCredentialId(credentialId);
		final int authenticationAttempts = verificationCardStateService.incrementAuthenticationAttempts(verificationCardId);

		// Check if the attempts have been exceeded.
		if (authenticationAttempts >= MAX_AUTHENTICATION_ATTEMPTS) {
			final VerificationCardEntity verificationCardEntity = getVerificationCardEntityByCredentialId(credentialId);
			final VerificationCardStateEntity verificationCardStateEntity = verificationCardEntity.getVerificationCardStateEntity();
			final VerificationCardState verificationCardState = verificationCardStateEntity.getState();

			// In case the card is already confirmed, don't update its state further.
			if (!VerificationCardState.CONFIRMED.equals(verificationCardState)) {
				verificationCardStateEntity.setState(VerificationCardState.AUTHENTICATION_ATTEMPTS_EXCEEDED);
				verificationCardStateService.saveVerificationCardState(verificationCardStateEntity);
			}
		}
	}

	@Transactional
	public long getLastTimeStep(final String credentialId) {
		validateUUID(credentialId);

		return getVerificationCardEntityByCredentialId(credentialId).getVerificationCardStateEntity().getLastSuccessfulAuthenticationTimeStep();
	}

	@Transactional
	public List<String> getSuccessfulAuthenticationChallenges(final String credentialId) {
		validateUUID(credentialId);

		return getVerificationCardEntityByCredentialId(credentialId).getVerificationCardStateEntity().getSuccessfulAuthenticationAttempts()
				.successfulChallenges();
	}

	@Transactional
	public void setLastTimeStepAndSuccessfulAuthenticationChallenge(final String credentialId, final long timeStepT1,
			final String authenticationChallenge) {
		validateUUID(credentialId);
		checkNotNull(authenticationChallenge);

		final String verificationCard = getVerificationCardIdByCredentialId(credentialId);
		verificationCardStateService.addToListOfSuccessfulAuthenticationChallenges(verificationCard, timeStepT1, authenticationChallenge);
	}

	@Transactional
	public int incrementConfirmationAttempts(final String verificationCardId) {
		validateUUID(verificationCardId);

		final int confirmationAttempts = verificationCardStateService.incrementConfirmationAttempts(verificationCardId);

		// Check if the attempts have been exceeded.
		if (confirmationAttempts >= MAX_CONFIRMATION_ATTEMPTS) {
			final VerificationCardEntity verificationCardEntity = getVerificationCardEntity(verificationCardId);
			final VerificationCardStateEntity verificationCardStateEntity = verificationCardEntity.getVerificationCardStateEntity();

			verificationCardStateEntity.setState(VerificationCardState.CONFIRMATION_ATTEMPTS_EXCEEDED);
			verificationCardStateService.saveVerificationCardState(verificationCardStateEntity);
		}

		return confirmationAttempts;
	}

	/**
	 * Saves the short Choice Return Codes and updates the verification card state to {@link VerificationCardState#SENT}.
	 *
	 * @param verificationCardId     the id of the verification card to update. Must be non-null and a valid uuid.
	 * @param shortChoiceReturnCodes the list of short Choice Return Codes to save. Must be non-null and not empty.
	 * @throws NullPointerException      if any parameter is null.
	 * @throws FailedValidationException if {@code verificationCardId} is not a valid uuid.
	 * @throws IllegalArgumentException  if {@code shortChoiceReturnCodes} is empty.
	 */
	@Transactional
	public void saveSentState(final String verificationCardId, final List<String> shortChoiceReturnCodes) {
		validateUUID(verificationCardId);
		checkNotNull(shortChoiceReturnCodes);

		final List<String> shortChoiceReturnCodesCopy = List.copyOf(shortChoiceReturnCodes);
		checkArgument(!shortChoiceReturnCodesCopy.isEmpty(), "The list of short Choice Return Codes must not be empty. [verificationCardId: %s]",
				verificationCardId);

		final VerificationCardEntity verificationCardEntity = getVerificationCardEntity(verificationCardId);

		final VerificationCardStateEntity verificationCardStateEntity = verificationCardEntity.getVerificationCardStateEntity();
		verificationCardStateEntity.setShortChoiceReturnCodes(shortChoiceReturnCodesCopy);
		verificationCardStateEntity.setState(VerificationCardState.SENT);

		verificationCardStateService.saveVerificationCardState(verificationCardStateEntity);
		LOGGER.info("Saved short Choice Return Codes and updated state. [verificationCardId: {}]", verificationCardId);
	}

	/**
	 * Saves the short Vote Cast Return Code and updates the verification card state to {@link VerificationCardState#CONFIRMED}.
	 *
	 * @param verificationCardId      the id of the verification card to update. Must be non-null and a valid uuid.
	 * @param shortVoteCastReturnCode the short Vote Cast Return code to save. Must be non-null.
	 * @throws NullPointerException      if any parameter is null.
	 * @throws FailedValidationException if {@code verificationCardId} is not a valid uuid.
	 */
	@Transactional
	public void saveConfirmedState(final String verificationCardId, final String shortVoteCastReturnCode) {
		validateUUID(verificationCardId);
		checkNotNull(shortVoteCastReturnCode);

		final VerificationCardEntity verificationCardEntity = getVerificationCardEntity(verificationCardId);

		final VerificationCardStateEntity verificationCardStateEntity = verificationCardEntity.getVerificationCardStateEntity();
		verificationCardStateEntity.setShortVoteCastReturnCode(shortVoteCastReturnCode);
		verificationCardStateEntity.setState(VerificationCardState.CONFIRMED);

		verificationCardStateService.saveVerificationCardState(verificationCardStateEntity);
		LOGGER.info("Saved short Vote Cast Return Code and updated state. [verificationCardId: {}]", verificationCardId);
	}

	@Transactional
	public List<VotingCardDto> searchVotingCards(final String partialVotingCardId) throws TooManyVerificationCardsException {

		final long matchingVerificationCards = verificationCardRepository.countAllByPartialVotingCardId(
				partialVotingCardId);

		if (matchingVerificationCards > votingCardLimitNumber) {
			throw new TooManyVerificationCardsException(
					String.format("Too many voting cards match the partialId. [limit: %s, found: %s, partialId: %s]", votingCardLimitNumber,
							matchingVerificationCards, partialVotingCardId));
		}

		final List<VerificationCardEntity> searchedVerificationCardsEntity = verificationCardRepository.findAllByContainingPartialId(
				partialVotingCardId);

		return searchedVerificationCardsEntity.stream()
				.map(verificationCardEntity -> {
					final VerificationCardStateEntity verificationCardStateEntity = verificationCardEntity.getVerificationCardStateEntity();
					return new VotingCardDto(verificationCardEntity.getVotingCardId(), verificationCardStateEntity.getState(),
							verificationCardStateEntity.getStateDate());
				})
				.toList();
	}

	@Transactional
	public VotingCardDto blockVotingCard(final String votingCardId) throws VerificationCardNotFoundException {
		validateUUID(votingCardId);

		final VerificationCardEntity verificationCardEntity = verificationCardRepository.findByVotingCardId(votingCardId).orElseThrow(
				() -> new VerificationCardNotFoundException(String.format("Verification card not found. [votingCardIdSearched: %s]", votingCardId)));

		final VerificationCardStateEntity verificationCardStateEntity = verificationCardEntity.getVerificationCardStateEntity();
		final VerificationCardState state = verificationCardStateEntity.getState();

		checkState(!VerificationCardState.BLOCKED.equals(state) && !VerificationCardState.CONFIRMED.equals(state),
				"Verification card not blocked. The current state does not allow to block it. [verificationCardId: %s, verificationCardState: %s]",
				votingCardId, state);

		verificationCardStateEntity.setState(VerificationCardState.BLOCKED);
		verificationCardStateEntity.setStateDate(LocalDateTime.now());

		verificationCardStateService.saveVerificationCardState(verificationCardStateEntity);

		return new VotingCardDto(verificationCardEntity.getVotingCardId(),
				verificationCardStateEntity.getState(), verificationCardStateEntity.getStateDate());
	}

	@Transactional
	public List<UsedVotingCardDTO> getUsedVotingCardsByElectionEventId(final String electionEventId) {
		validateUUID(electionEventId);

		return verificationCardRepository.findAllUsedByElectionEventId(electionEventId);
	}

	@Transactional
	public VerificationCardSetEntity getVerificationCardSetEntity(final String credentialId) {
		validateUUID(credentialId);

		return getVerificationCardEntityByCredentialId(credentialId).getVerificationCardSetEntity();
	}

	private String getVerificationCardIdByCredentialId(final String credentialId) {
		validateUUID(credentialId);

		return verificationCardRepository.findByCredentialId(credentialId)
				.orElseThrow(() -> new IllegalStateException(String.format("Verification card not found. [credentialId: %s]", credentialId)))
				.getVerificationCardId();
	}

}
