/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.domain.configuration.setupvoting.ComputingStatus;

@RestController
@RequestMapping("/api/v1/configuration/")
public class EncryptedLongReturnCodeSharesController {

	private static final Logger LOGGER = LoggerFactory.getLogger(EncryptedLongReturnCodeSharesController.class);

	private final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService;

	public EncryptedLongReturnCodeSharesController(final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService) {
		this.encryptedLongReturnCodeSharesService = encryptedLongReturnCodeSharesService;
	}

	@PutMapping("/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunk/{chunkId}/computegenenclongcodeshares")
	public ResponseEntity<Void> computeGenEncLongCodeShares(
			@PathVariable
			final String electionEventId,
			@PathVariable
			final String verificationCardSetId,
			@PathVariable
			final int chunkId,
			@RequestBody
			final SetupComponentVerificationDataPayload setupComponentVerificationDataPayload) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		LOGGER.debug("Received request to generate EncLongCodeShares. [electionEventId: {}, verificationCardSetId: {},  chunkId: {}]",
				electionEventId, verificationCardSetId, chunkId);

		encryptedLongReturnCodeSharesService.computeGenEncLongCodeShares(electionEventId, verificationCardSetId, chunkId,
				setupComponentVerificationDataPayload);

		LOGGER.info("Broadcasted requests to generate EncLongCodeShares. [electionEventId: {}, verificationCardSetId: {},  chunkId: {}]",
				electionEventId, verificationCardSetId, chunkId);

		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@GetMapping(value = "/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunkcount/{chunkCount}/status", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ComputingStatus> checkGenEncLongCodeSharesStatus(
			@PathVariable
			final String electionEventId,
			@PathVariable
			final String verificationCardSetId,
			@PathVariable
			final int chunkCount) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		LOGGER.debug(
				"Received request to check EncLongCodeShares generation status. [electionEventId: {}, verificationCardSetId: {},  chunkCount: {}]",
				electionEventId, verificationCardSetId, chunkCount);

		final ComputingStatus computingStatus = encryptedLongReturnCodeSharesService.getEncLongCodeSharesComputingStatus(electionEventId,
				verificationCardSetId, chunkCount);

		LOGGER.info("Checked computing status. [electionEventId: {}, verificationCardSetId: {},  chunkCount: {}, status: {}]", electionEventId,
				verificationCardSetId, chunkCount, computingStatus);

		return new ResponseEntity<>(computingStatus, HttpStatus.OK);
	}

	@GetMapping("/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunk/{chunkId}/download")
	public ResponseEntity<List<ControlComponentCodeSharesPayload>> downloadGenEncLongCodeShares(
			@PathVariable
			final String electionEventId,
			@PathVariable
			final String verificationCardSetId,
			@PathVariable
			final int chunkId) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		LOGGER.debug(
				"Received request to download control component code shares payload. [electionEventId: {}, verificationCardSetId: {},  chunkId: {}]",
				electionEventId, verificationCardSetId, chunkId);

		final List<ControlComponentCodeSharesPayload> controlComponentCodeSharesPayloads = encryptedLongReturnCodeSharesService.getEncLongCodeShares(
				electionEventId, verificationCardSetId, chunkId);

		LOGGER.info("Retrieved control component code shares payload for download. [electionEventId: {}, verificationCardSetId: {},  chunkId: {}]",
				electionEventId, verificationCardSetId, chunkId);

		return new ResponseEntity<>(controlComponentCodeSharesPayloads, HttpStatus.OK);
	}

}
