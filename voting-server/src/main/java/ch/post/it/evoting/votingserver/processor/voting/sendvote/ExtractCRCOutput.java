/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting.sendvote;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

/**
 * Regroups the output of the ExtractCRC algorithm.
 */
public class ExtractCRCOutput {
	private static final int L_CC = ExtractCRCAlgorithm.CHOICE_RETURN_CODES_LENGTH;
	private final List<String> shortChoiceReturnCodes;

	/**
	 * @param shortChoiceReturnCodes CC<sub>id</sub> ∈ ((A<sub>10</sub>)<sup>L<sub>CC</sup></sup>)<sup>&#x1D713;</sup>, short Choice Return Codes.
	 * @throws NullPointerException     if {@code shortChoiceReturnCodes} is null.
	 * @throws IllegalArgumentException if the {@code shortChoiceReturnCodes} is not decimal and values length are not L_CC.
	 */
	public ExtractCRCOutput(final List<String> shortChoiceReturnCodes) {
		checkNotNull(shortChoiceReturnCodes);
		final List<String> shortChoiceReturnCodesCopy = List.copyOf(shortChoiceReturnCodes);
		checkArgument(shortChoiceReturnCodesCopy.stream().parallel().allMatch(cc -> cc.matches("^[0-9]{" + L_CC + "}$")),
				"Short Choice Return Codes values must be only digits and have a length of " + L_CC);

		this.shortChoiceReturnCodes = shortChoiceReturnCodesCopy;
	}

	public List<String> getShortChoiceReturnCodes() {
		return shortChoiceReturnCodes;
	}
}
