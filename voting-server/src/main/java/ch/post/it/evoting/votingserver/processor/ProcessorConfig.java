/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FullyQualifiedAnnotationBeanNameGenerator;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Factory;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Profile;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystoreFactory;
import ch.post.it.evoting.cryptoprimitives.symmetric.Symmetric;
import ch.post.it.evoting.cryptoprimitives.symmetric.SymmetricFactory;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivation;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivationFactory;
import ch.post.it.evoting.cryptoprimitives.utils.VerificationResult;
import ch.post.it.evoting.evotinglibraries.direct.trust.KeystoreValidator;

@Configuration
@ComponentScan(value = "ch.post.it.evoting.votingserver.processor", nameGenerator = FullyQualifiedAnnotationBeanNameGenerator.class)
@EntityScan(basePackages = "ch.post.it.evoting.votingserver.processor")
@EnableJpaRepositories(basePackages = "ch.post.it.evoting.votingserver.processor")
public class ProcessorConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessorConfig.class);

	@Bean
	public KeyDerivation keyDerivation() {
		return KeyDerivationFactory.createKeyDerivation();
	}

	@Bean
	public Base64 base64() {
		return BaseEncodingFactory.createBase64();
	}

	@Bean
	public SignatureKeystore<Alias> keystoreService(final KeystoreRepository repository) throws IOException {
		return SignatureKeystoreFactory.createSignatureKeystore(repository.getKeyStore(), "PKCS12", repository.getKeystorePassword(),
				keystore -> {
					final VerificationResult result = KeystoreValidator.validateKeystore(keystore, repository.getKeystoreAlias());
					if (!result.isVerified()) {
						LOGGER.error("Keystore validation failed. [alias: {}]", repository.getKeystoreAlias().get());
						result.getErrorMessages().descendingIterator().forEachRemaining(LOGGER::error);
					}
					return result.isVerified();
				}, repository.getKeystoreAlias());
	}

	@Bean
	public Symmetric symmetric() {
		return SymmetricFactory.createSymmetric();
	}

	@Bean
	public Argon2 argon2() {
		return Argon2Factory.createArgon2(Argon2Profile.LESS_MEMORY.getContext());
	}
}
