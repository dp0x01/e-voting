/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.configuration.setuptally;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Regroups the context values needed for the SetupTallyCCM<sub>j</sub> algorithm.
 *
 * <ul>
 * <li>encryptionGroup, the {@code GqGroup} with modulus p, cardinality q and generator g. Not null.</li>
 * <li>j, the CCM's index.</li>
 * <li>ee, the election event id. Not null and a valid UUID.</li>
 * <li>mu, the maximum number of supported write-ins plus one. Strictly positive.</li>
 * </ul>
 */
public class SetupTallyCCMContext {

	private final GqGroup encryptionGroup;
	private final int nodeId;
	private final String electionEventId;
	private final int maximumNumberOfSupportedWriteInsPlusOne;

	private SetupTallyCCMContext(final GqGroup encryptionGroup, final int nodeId, final String electionEventId,
			final int maximumNumberOfSupportedWriteInsPlusOne) {
		this.encryptionGroup = encryptionGroup;
		this.nodeId = nodeId;
		this.electionEventId = electionEventId;
		this.maximumNumberOfSupportedWriteInsPlusOne = maximumNumberOfSupportedWriteInsPlusOne;
	}

	public GqGroup getEncryptionGroup() {
		return encryptionGroup;
	}

	public int getNodeId() {
		return nodeId;
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public int getMaximumNumberOfSupportedWriteInsPlusOne() {
		return maximumNumberOfSupportedWriteInsPlusOne;
	}

	/**
	 * Builder performing input validations before constructing a {@link SetupTallyCCMContext}.
	 */
	public static class Builder {

		private String electionEventId;
		private GqGroup encryptionGroup;
		private int nodeId;
		private int maximumNumberOfSupportedWriteInsPlusOne;

		public Builder setEncryptionGroup(final GqGroup gqGroup) {
			this.encryptionGroup = gqGroup;
			return this;
		}

		public Builder setNodeId(final int nodeId) {
			this.nodeId = nodeId;
			return this;
		}

		public Builder setElectionEventId(final String electionEventId) {
			this.electionEventId = electionEventId;
			return this;
		}

		public Builder setMaximumNumberOfSupportedWriteInsPlusOne(final int maximumNumberOfSupportedWriteInsPlusOne) {
			this.maximumNumberOfSupportedWriteInsPlusOne = maximumNumberOfSupportedWriteInsPlusOne;
			return this;
		}

		/**
		 * Creates the SetupTallyCCMContext object.
		 *
		 * @throws NullPointerException      if any of the fields are null.
		 * @throws FailedValidationException if the election event id is not a valid UUID.
		 * @throws IllegalArgumentException  if
		 *                                   <ul>
		 *                                       <li>the node id is not part of the known node ids.</li>
		 *                                       <li>the maximum number of supported write-ins plus one is strictly smaller than 1.</li>
		 *                                   </ul>
		 */
		public SetupTallyCCMContext build() {
			checkNotNull(encryptionGroup);
			checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %]", nodeId);
			validateUUID(electionEventId);
			checkArgument(maximumNumberOfSupportedWriteInsPlusOne > 0,
					"The maximum number of supported write-ins plus one must be strictly positive.");
			return new SetupTallyCCMContext(encryptionGroup, nodeId, electionEventId, maximumNumberOfSupportedWriteInsPlusOne);
		}
	}
}
