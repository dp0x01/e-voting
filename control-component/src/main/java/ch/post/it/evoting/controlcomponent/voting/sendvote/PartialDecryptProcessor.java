/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.sendvote;

import static ch.post.it.evoting.controlcomponent.ControlComponentsApplicationBootstrap.RABBITMQ_EXCHANGE;
import static ch.post.it.evoting.domain.SharedQueue.PARTIAL_DECRYPT_PCC_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.PARTIAL_DECRYPT_PCC_RESPONSE_PATTERN;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.SignatureException;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.controlcomponent.ElectionEventService;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommand;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommandExecutor;
import ch.post.it.evoting.controlcomponent.Messages;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.PartiallyDecryptedEncryptedPCC;
import ch.post.it.evoting.domain.voting.sendvote.VotingServerEncryptedVotePayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.common.EncryptedVerifiableVote;

/**
 * Consumes the messages asking for the partial decryption of the encrypted Partial Choice Return Codes.
 */
@Service
public class PartialDecryptProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(PartialDecryptProcessor.class);

	private final ObjectMapper objectMapper;
	private final RabbitTemplate rabbitTemplate;
	private final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor;
	private final PartialDecryptService partialDecryptService;
	private final ElectionEventService electionEventService;
	private final SignatureKeystore<Alias> signatureKeystoreService;

	private String responseQueue;

	@Value("${nodeID}")
	private int nodeId;

	public PartialDecryptProcessor(
			final ObjectMapper objectMapper,
			final RabbitTemplate rabbitTemplate,
			final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor,
			final PartialDecryptService partialDecryptService,
			final ElectionEventService electionEventService,
			final SignatureKeystore<Alias> signatureKeystoreService) {
		this.objectMapper = objectMapper;
		this.rabbitTemplate = rabbitTemplate;
		this.exactlyOnceCommandExecutor = exactlyOnceCommandExecutor;
		this.partialDecryptService = partialDecryptService;
		this.electionEventService = electionEventService;
		this.signatureKeystoreService = signatureKeystoreService;
	}

	@PostConstruct
	public void initQueue() {
		responseQueue = String.format("%s%s", PARTIAL_DECRYPT_PCC_RESPONSE_PATTERN, nodeId);
	}

	@RabbitListener(queues = PARTIAL_DECRYPT_PCC_REQUEST_PATTERN + "${nodeID}", autoStartup = "false", concurrency = "4")
	public void onMessage(final Message message) throws IOException {

		final String correlationId = message.getMessageProperties().getCorrelationId();
		checkNotNull(correlationId, "Correlation Id must not be null.");

		// Deserialize message.
		final byte[] messageBytes = message.getBody();
		final VotingServerEncryptedVotePayload votingServerEncryptedVotePayload = objectMapper.readValue(messageBytes,
				VotingServerEncryptedVotePayload.class);

		// Verify payload signature and consistency.
		verifyPayload(votingServerEncryptedVotePayload);

		final ContextIds contextIds = votingServerEncryptedVotePayload.getEncryptedVerifiableVote().contextIds();
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final String contextId = String.join("-", electionEventId, verificationCardSetId, verificationCardId);
		LOGGER.info("Received partial decrypt request. [contextId: {}, correlationId: {}]", contextId, correlationId);

		// Perform the partial decryption and create response payload.
		final ExactlyOnceCommand partiallyDecryptInput = new ExactlyOnceCommand.Builder()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(Context.VOTING_RETURN_CODES_PARTIAL_DECRYPT_PCC.toString())
				.setTask(() -> generatePartiallyDecryptedEncryptedPayload(votingServerEncryptedVotePayload))
				.setRequestContent(messageBytes)
				.build();
		final byte[] payloadBytes = exactlyOnceCommandExecutor.process(partiallyDecryptInput);

		final Message responseMessage = Messages.createMessage(correlationId, payloadBytes);

		rabbitTemplate.send(RABBITMQ_EXCHANGE, responseQueue, responseMessage);
		LOGGER.info("Partial decryption response sent. [contextId: {}]", contextId);
	}

	/**
	 * Validates the authenticity of the given {@link VotingServerEncryptedVotePayload} and verifies that the payload's encryption group is consistent
	 * with the control component's one.
	 * <p>
	 * The following checks are done in {@link PartialDecryptService#performPartialDecrypt(EncryptedVerifiableVote)}:
	 *     <ul>
	 *         <li>context ids consistency</li>
	 *         <li>start and end time validity</li>
	 *         <li>mixing status</li>
	 *     </ul>
	 * </p>
	 *
	 * @param votingServerEncryptedVotePayload the payload to be verified.
	 */
	private void verifyPayload(final VotingServerEncryptedVotePayload votingServerEncryptedVotePayload) {

		final ContextIds contextIds = votingServerEncryptedVotePayload.getEncryptedVerifiableVote().contextIds();
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final CryptoPrimitivesSignature signature = votingServerEncryptedVotePayload.getSignature();

		checkState(signature != null, "The signature of the voting server encrypted vote payload is null. [contextIds: %s]", contextIds);

		final Hashable additionalContextData = ChannelSecurityContextData.votingServerEncryptedVote(electionEventId, verificationCardSetId,
				verificationCardId);

		// Verify signature.
		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.VOTING_SERVER, votingServerEncryptedVotePayload,
					additionalContextData, signature.signatureContents());

		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of the voting server encrypted vote payload. [contextIds: %s]", contextIds));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(VotingServerEncryptedVotePayload.class, String.format("[contextIds: %s]", contextIds));
		}

		// Verify consistency.
		final GqGroup ccGqGroup = electionEventService.getEncryptionGroup(electionEventId);

		if (!votingServerEncryptedVotePayload.getEncryptionGroup().equals(ccGqGroup)) {
			throw new IllegalArgumentException(
					String.format("The payload's group is different from the control-component's group. [contextIds: %s]", contextIds));
		}
	}

	private byte[] generatePartiallyDecryptedEncryptedPayload(final VotingServerEncryptedVotePayload votingServerEncryptedVotePayload) {
		final EncryptedVerifiableVote encryptedVerifiableVote = votingServerEncryptedVotePayload.getEncryptedVerifiableVote();
		final ContextIds contextIds = encryptedVerifiableVote.contextIds();
		final GqGroup encryptionGroup = encryptedVerifiableVote.encryptedVote().getGroup();

		// Perform partial decryption.
		final PartiallyDecryptedEncryptedPCC partiallyDecryptedEncryptedPCC = partialDecryptService.performPartialDecrypt(encryptedVerifiableVote);

		// Create and sign response payload.
		final ControlComponentPartialDecryptPayload controlComponentPartialDecryptPayload =
				new ControlComponentPartialDecryptPayload(encryptionGroup, partiallyDecryptedEncryptedPCC);

		controlComponentPartialDecryptPayload.setSignature(getPayloadSignature(controlComponentPartialDecryptPayload));

		LOGGER.info("Successfully signed control component partial decrypt payload. [contextIds: {}]", contextIds);

		try {
			return objectMapper.writeValueAsBytes(controlComponentPartialDecryptPayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(
					String.format("Could not serialize control component partial decrypt payload. [contextIds: %s]", contextIds), e);
		}
	}

	private CryptoPrimitivesSignature getPayloadSignature(final ControlComponentPartialDecryptPayload payload) {
		final ContextIds contextIds = payload.getPartiallyDecryptedEncryptedPCC().contextIds();
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final Hashable additionalContextData = ChannelSecurityContextData.controlComponentPartialDecrypt(nodeId, electionEventId,
				verificationCardSetId, verificationCardId);

		try {
			final byte[] signature = signatureKeystoreService.generateSignature(payload, additionalContextData);

			return new CryptoPrimitivesSignature(signature);
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not generate control component partial decrypt payload signature. [contextIds: %s]", contextIds));
		}
	}
}
