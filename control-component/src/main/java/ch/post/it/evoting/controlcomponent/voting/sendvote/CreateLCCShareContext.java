/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.controlcomponent.PartialChoiceReturnCodeAllowList;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Regroups the context values needed by the CreateLCCShare<sub>j</sub> algorithm.
 *
 * <ul>
 * <li>encryptionGroup, the {@code GqGroup} with modulus p, cardinality q and generator g. Not null.</li>
 * <li>j, the CCR's index.</li>
 * <li>ee, the election event id. Not null and a valid UUID.</li>
 * <li>vcs, the verification card set id. Not null and a valid UUID.</li>
 * <li>psi, the number of selectable voting options. In range [1, 120].</li>
 * <li>L<sub>pCC</sub>, the partial Choice Return Codes allow list. Not null.</li>
 * </ul>
 */
public record CreateLCCShareContext(GqGroup encryptionGroup, int nodeId, String electionEventId, String verificationCardSetId,
									int numberOfSelectableVotingOptions, PartialChoiceReturnCodeAllowList pCCAllowList) {

	public CreateLCCShareContext(final GqGroup encryptionGroup, final int nodeId, final String electionEventId, final String verificationCardSetId,
			final int numberOfSelectableVotingOptions, final PartialChoiceReturnCodeAllowList pCCAllowList) {
		this.encryptionGroup = checkNotNull(encryptionGroup);
		this.electionEventId = validateUUID(electionEventId);
		this.verificationCardSetId = validateUUID(verificationCardSetId);
		this.pCCAllowList = checkNotNull(pCCAllowList);

		checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);
		this.nodeId = nodeId;

		checkArgument(numberOfSelectableVotingOptions >= 1 && numberOfSelectableVotingOptions <= MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS,
				"The number of selectable voting options is out of range.");
		this.numberOfSelectableVotingOptions = numberOfSelectableVotingOptions;
	}

}
