/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.confirmvote;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface LongVoteCastReturnCodesShareRepository extends CrudRepository<LongVoteCastReturnCodesShareEntity, Long> {

	@Query("select e from LongVoteCastReturnCodesShareEntity e where e.verificationCardEntity.verificationCardId = ?1 and e.confirmationKey = ?2")
	Optional<LongVoteCastReturnCodesShareEntity> findByVerificationCardIdAndConfirmationKey(final String verificationCardId,
			final String confirmationKeyValue);

}
