/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "ELECTION_EVENT_CONTEXT")
public class ElectionContextEntity {

	@Id
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "ELECTION_EVENT_FK_ID")
	private ElectionEventEntity electionEventEntity;

	private LocalDateTime startTime;

	private LocalDateTime finishTime;

	@Version
	@Column(name = "CHANGE_CONTROL_ID")
	private Integer changeControlId;

	public ElectionContextEntity() {

	}

	private ElectionContextEntity(final ElectionEventEntity electionEventEntity, final LocalDateTime startTime, final LocalDateTime finishTime) {

		this.electionEventEntity = checkNotNull(electionEventEntity);

		checkNotNull(startTime);
		checkNotNull(finishTime);
		checkArgument(startTime.isBefore(finishTime), "Start time must be before finish time.");

		this.startTime = startTime;
		this.finishTime = finishTime;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public LocalDateTime getFinishTime() {
		return finishTime;
	}

	public ElectionEventEntity getElectionEventEntity() {
		return electionEventEntity;
	}

	public static class Builder {

		private ElectionEventEntity electionEventEntity;
		private LocalDateTime startTime;
		private LocalDateTime finishTime;

		public Builder() {
			// Do nothing
		}

		public Builder setElectionEventEntity(final ElectionEventEntity electionEventEntity) {
			this.electionEventEntity = checkNotNull(electionEventEntity);
			return this;
		}

		public Builder setStartTime(final LocalDateTime startTime) {
			checkNotNull(startTime);
			this.startTime = startTime;
			return this;
		}

		public Builder setFinishTime(final LocalDateTime finishTime) {
			checkNotNull(finishTime);
			this.finishTime = finishTime;
			return this;
		}

		public ElectionContextEntity build() {
			return new ElectionContextEntity(electionEventEntity, startTime, finishTime);
		}
	}
}
