/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;

public record CcrjReturnCodesKeys(String electionEventId, ZqElement ccrjReturnCodesGenerationSecretKey,
								  ElGamalMultiRecipientKeyPair ccrjChoiceReturnCodesEncryptionKeyPair) {

	public CcrjReturnCodesKeys {

		validateUUID(electionEventId);
		checkNotNull(ccrjReturnCodesGenerationSecretKey);
		checkNotNull(ccrjChoiceReturnCodesEncryptionKeyPair);

		checkArgument(ccrjReturnCodesGenerationSecretKey.getGroup().hasSameOrderAs(ccrjChoiceReturnCodesEncryptionKeyPair.getGroup()),
				"The generation secret key must have the same order as the encryption key pair.");

	}

}
