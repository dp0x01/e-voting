/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionEventContext;

@Service
public class ElectionContextService {

	private final BallotBoxService ballotBoxService;
	private final ElectionEventService electionEventService;
	private final ElectionContextRepository electionContextRepository;

	public ElectionContextService(
			final BallotBoxService ballotBoxService,
			final ElectionEventService electionEventService,
			final ElectionContextRepository electionContextRepository) {
		this.ballotBoxService = ballotBoxService;
		this.electionEventService = electionEventService;
		this.electionContextRepository = electionContextRepository;
	}

	@Transactional
	public void save(final ElectionEventContext electionEventContext) {
		checkNotNull(electionEventContext);

		// Save election event context entity.
		final ElectionEventEntity electionEventEntity = electionEventService.getElectionEventEntity(electionEventContext.electionEventId());

		final ElectionContextEntity electionContextEntity = new ElectionContextEntity.Builder()
				.setElectionEventEntity(electionEventEntity)
				.setStartTime(electionEventContext.startTime())
				.setFinishTime(electionEventContext.finishTime())
				.build();
		electionContextRepository.save(electionContextEntity);

		// Save ballot box entities.
		ballotBoxService.saveFromContexts(electionEventContext.verificationCardSetContexts());
	}

	@Transactional
	public ElectionContextEntity getElectionContextEntity(final String electionEventId) {
		validateUUID(electionEventId);

		final Optional<ElectionContextEntity> electionContextEntity = electionContextRepository.findByElectionEventId(electionEventId);

		return electionContextEntity.orElseThrow(
				() -> new IllegalStateException(String.format("Election context entity not found. [electionEventId: %s]", electionEventId)));
	}
}
