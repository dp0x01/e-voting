/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.confirmvote;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;

public class CreateLVCCShareOutput {

	private final GqElement hashedSquaredConfirmationKey;
	private final GqElement longVoteCastReturnCodeShare;
	private final String hashedLongVoteCastReturnCodeShare;
	private final GqElement voterVoteCastReturnCodeGenerationPublicKey;
	private final ExponentiationProof exponentiationProof;
	private final int confirmationAttempts;

	CreateLVCCShareOutput(final GqElement hashedSquaredConfirmationKey, final GqElement longVoteCastReturnCodeShare,
			final String hashedLongVoteCastReturnCodeShare, final GqElement voterVoteCastReturnCodeGenerationPublicKey,
			final ExponentiationProof exponentiationProof, final int confirmationAttempts) {

		checkArgument(confirmationAttempts >= 0);
		checkArgument(confirmationAttempts < CreateLVCCShareAlgorithm.MAX_CONFIRMATION_ATTEMPTS);
		checkNotNull(hashedSquaredConfirmationKey);
		checkNotNull(longVoteCastReturnCodeShare);
		checkNotNull(hashedLongVoteCastReturnCodeShare);
		checkNotNull(voterVoteCastReturnCodeGenerationPublicKey);
		checkNotNull(exponentiationProof);

		// Cross group checks.
		checkArgument(hashedSquaredConfirmationKey.getGroup().equals(longVoteCastReturnCodeShare.getGroup()),
				"Confirmation key and long vote cast return code share must have the same group.");
		checkArgument(hashedSquaredConfirmationKey.getGroup().equals(voterVoteCastReturnCodeGenerationPublicKey.getGroup()),
				"Confirmation key and Voter Vote Cast Return Code Generation public key must have the same group.");
		checkArgument(hashedSquaredConfirmationKey.getGroup().hasSameOrderAs(exponentiationProof.getGroup()),
				"Confirmation key and exponentiation proof must have the same group order.");

		this.confirmationAttempts = confirmationAttempts;
		this.hashedSquaredConfirmationKey = hashedSquaredConfirmationKey;
		this.longVoteCastReturnCodeShare = longVoteCastReturnCodeShare;
		this.hashedLongVoteCastReturnCodeShare = hashedLongVoteCastReturnCodeShare;
		this.voterVoteCastReturnCodeGenerationPublicKey = voterVoteCastReturnCodeGenerationPublicKey;
		this.exponentiationProof = exponentiationProof;
	}

	GqElement getHashedSquaredConfirmationKey() {
		return hashedSquaredConfirmationKey;
	}

	public GqElement getLongVoteCastReturnCodeShare() {
		return longVoteCastReturnCodeShare;
	}

	public String getHashedLongVoteCastReturnCodeShare() {
		return hashedLongVoteCastReturnCodeShare;
	}

	GqElement getVoterVoteCastReturnCodeGenerationPublicKey() {
		return voterVoteCastReturnCodeGenerationPublicKey;
	}

	ExponentiationProof getExponentiationProof() {
		return exponentiationProof;
	}

	int getConfirmationAttempts() {
		return confirmationAttempts;
	}
}
