/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LVCCAllowListEntryService {

	private static final Logger LOGGER = LoggerFactory.getLogger(LVCCAllowListEntryService.class);

	private final LVCCAllowListEntryRepository castReturnCodeRepository;
	private final int batchSize;

	@PersistenceContext
	private EntityManager entityManager;

	public LVCCAllowListEntryService(
			final LVCCAllowListEntryRepository castReturnCodeRepository,
			@Value("${spring.jpa.properties.hibernate.jdbc.batch_size}")
			final int batchSize) {
		this.castReturnCodeRepository = castReturnCodeRepository;
		this.batchSize = batchSize;
	}

	@Transactional
	public void saveAll(final List<LVCCAllowListEntryEntity> castReturnCodeEntities) {
		checkNotNull(castReturnCodeEntities);
		checkArgument(!castReturnCodeEntities.isEmpty());

		final Iterator<LVCCAllowListEntryEntity> iterator = castReturnCodeEntities.iterator();

		// Save the list of cast return codes in batches
		List<LVCCAllowListEntryEntity> entities = new ArrayList<>(batchSize);
		while (iterator.hasNext()) {
			final LVCCAllowListEntryEntity longVoteCastReturnCode = iterator.next();
			entities.add(longVoteCastReturnCode);
			if (entities.size() == batchSize || !iterator.hasNext()) {
				castReturnCodeRepository.saveAll(entities);
				entityManager.flush();
				entityManager.clear();
				entities = new ArrayList<>(batchSize);
			}
		}

		LOGGER.info("Long vote cast return codes successfully saved. [verificationCardSetId: {}]",
				castReturnCodeEntities.get(0).getVerificationCardSetEntity().getVerificationCardSetId());
	}

	@Transactional
	public List<LVCCAllowListEntryEntity> getLongVoteCastReturnCodeEntities(final String verificationCardSetId) {
		validateUUID(verificationCardSetId);

		return castReturnCodeRepository.findAllByVerificationCardSetId(verificationCardSetId);
	}

	@Transactional
	public boolean exists(final String verificationCardSetId, final String longVoteCastReturnCode) {
		return castReturnCodeRepository.existsByLongVoteCastReturnCode(verificationCardSetId, longVoteCastReturnCode);
	}
}
