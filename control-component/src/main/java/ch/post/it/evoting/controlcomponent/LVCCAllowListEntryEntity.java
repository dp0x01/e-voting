/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "LVCC_ALLOW_LIST_ENTRY")
public class LVCCAllowListEntryEntity {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VERIFICATION_CARD_SET_FK_ID", referencedColumnName = "ID")
	private VerificationCardSetEntity verificationCardSetEntity;

	@Id
	private String longVoteCastReturnCode;

	@Version
	private Integer changeControlId;

	public LVCCAllowListEntryEntity() {

	}

	public LVCCAllowListEntryEntity(final VerificationCardSetEntity verificationCardSetEntity, final String longVoteCastReturnCode) {
		this.verificationCardSetEntity = checkNotNull(verificationCardSetEntity);
		this.longVoteCastReturnCode = checkNotNull(longVoteCastReturnCode);
	}

	public VerificationCardSetEntity getVerificationCardSetEntity() {
		return this.verificationCardSetEntity;
	}

	public String getLongVoteCastReturnCode() {
		return this.longVoteCastReturnCode;
	}
}
