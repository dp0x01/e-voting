/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Set;

import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;

/**
 * Regroups the inputs needed by the CreateLCCShare<sub>j</sub> algorithm.
 *
 * <ul>
 * <li>pCC<sub>id</sub>, the vector of partial Choice Return Codes. Not null.</li>
 * <li>k'<sub>j</sub>, the CCRj Return Codes Generation secret key. Not null.</li>
 * <li>vc<sub>id</sub>, the verification card id. Not null and a valid UUID.</li>
 * </ul>
 */
public record CreateLCCShareInput(GroupVector<GqElement, GqGroup> partialChoiceReturnCodes, ZqElement ccrjReturnCodesGenerationSecretKey,
								  String verificationCardId) {

	public CreateLCCShareInput {
		checkNotNull(partialChoiceReturnCodes);
		checkNotNull(ccrjReturnCodesGenerationSecretKey);
		validateUUID(verificationCardId);

		checkArgument(partialChoiceReturnCodes.getGroup().hasSameOrderAs(ccrjReturnCodesGenerationSecretKey.getGroup()),
				"The partial choice return codes and return codes generation secret key must have the same group order.");

		checkArgument(partialChoiceReturnCodes.size() == Set.copyOf(partialChoiceReturnCodes).size(), "All pCC must be distinct.");
	}
}
