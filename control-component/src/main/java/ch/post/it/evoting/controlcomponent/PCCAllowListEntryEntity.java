/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "PCC_ALLOW_LIST_ENTRY")
public class PCCAllowListEntryEntity {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VERIFICATION_CARD_SET_FK_ID", referencedColumnName = "ID")
	private VerificationCardSetEntity verificationCardSetEntity;

	@Id
	private String partialChoiceReturnCode;

	private int chunkId;

	@Version
	private Integer changeControlId;

	public PCCAllowListEntryEntity() {

	}

	public PCCAllowListEntryEntity(final VerificationCardSetEntity verificationCardSetEntity, final String partialChoiceReturnCode,
			final int chunkId) {
		this.verificationCardSetEntity = verificationCardSetEntity;
		this.partialChoiceReturnCode = partialChoiceReturnCode;
		this.chunkId = chunkId;
	}

	public VerificationCardSetEntity getVerificationCardSetEntity() {
		return verificationCardSetEntity;
	}

	public String getPartialChoiceReturnCode() {
		return partialChoiceReturnCode;
	}

	public int getChunkId() {
		return chunkId;
	}
}
