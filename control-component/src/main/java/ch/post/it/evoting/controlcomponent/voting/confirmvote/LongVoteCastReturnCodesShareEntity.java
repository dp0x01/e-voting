/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.confirmvote;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import ch.post.it.evoting.controlcomponent.VerificationCardEntity;

@Entity
@Table(name = "LONG_VOTE_CAST_RETURN_CODES_SHARE")
public class LongVoteCastReturnCodesShareEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LONG_VOTE_CAST_RETURN_CODES_SHARE_SEQ_GENERATOR")
	@SequenceGenerator(sequenceName = "LONG_VOTE_CAST_RETURN_CODES_SHARE_SEQ", allocationSize = 1, name = "LONG_VOTE_CAST_RETURN_CODES_SHARE_SEQ_GENERATOR")
	private Long id;

	private String confirmationKey;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VERIFICATION_CARD_FK_ID", referencedColumnName = "ID")
	private VerificationCardEntity verificationCardEntity;

	private byte[] longVoteCastReturnCodeShare;

	private byte[] voterVoteCastReturnCodeGenerationPublicKey;

	private byte[] exponentiationProof;

	@Version
	private Integer changeControlId;

	public LongVoteCastReturnCodesShareEntity(final VerificationCardEntity verificationCardEntity, final byte[] longVoteCastReturnCodeShare,
			final byte[] voterVoteCastReturnCodeGenerationPublicKey, final byte[] exponentiationProof, final String confirmationKey) {
		this.verificationCardEntity = checkNotNull(verificationCardEntity);
		this.longVoteCastReturnCodeShare = checkNotNull(longVoteCastReturnCodeShare);
		this.voterVoteCastReturnCodeGenerationPublicKey = checkNotNull(voterVoteCastReturnCodeGenerationPublicKey);
		this.exponentiationProof = checkNotNull(exponentiationProof);
		this.confirmationKey = checkNotNull(confirmationKey);
	}

	public LongVoteCastReturnCodesShareEntity() {
	}

	public VerificationCardEntity getVerificationCardEntity() {
		return verificationCardEntity;
	}

	public byte[] getLongVoteCastReturnCodeShare() {
		return longVoteCastReturnCodeShare;
	}

	public byte[] getVoterVoteCastReturnCodeGenerationPublicKey() {
		return voterVoteCastReturnCodeGenerationPublicKey;
	}

	public byte[] getExponentiationProof() {
		return exponentiationProof;
	}

	public String getConfirmationKey() {
		return confirmationKey;
	}
}
