/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.confirmvote;

import static ch.post.it.evoting.controlcomponent.ControlComponentsApplicationBootstrap.RABBITMQ_EXCHANGE;
import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.utils.Validations.allEqual;
import static ch.post.it.evoting.domain.SharedQueue.VERIFY_LVCC_SHARE_HASH_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.VERIFY_LVCC_SHARE_HASH_RESPONSE_PATTERN;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.math.BigInteger;
import java.security.SignatureException;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.controlcomponent.ElectionEventService;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommand;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommandExecutor;
import ch.post.it.evoting.controlcomponent.IdentifierValidationService;
import ch.post.it.evoting.controlcomponent.LVCCAllowListEntryService;
import ch.post.it.evoting.controlcomponent.LongVoteCastReturnCodesAllowList;
import ch.post.it.evoting.controlcomponent.Messages;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.utils.Conversions;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ConfirmationKey;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponentlVCCSharePayload;
import ch.post.it.evoting.domain.voting.confirmvote.LongVoteCastReturnCodesShare;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;

@Service
public class LongVoteCastReturnCodesShareVerifyProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(LongVoteCastReturnCodesShareVerifyProcessor.class);

	private final ObjectMapper objectMapper;
	private final RabbitTemplate rabbitTemplate;
	private final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor;
	private final VerifyLVCCHashAlgorithm verifyLVCCHashAlgorithm;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final ElectionEventService electionEventService;
	private final LongVoteCastReturnCodesShareService longVoteCastReturnCodesShareService;
	private final LVCCAllowListEntryService lvccAllowListEntryService;
	private final Hash hash;
	private final IdentifierValidationService identifierValidationService;
	private String responseQueue;

	@Value("${nodeID}")
	private int nodeId;

	LongVoteCastReturnCodesShareVerifyProcessor(
			final ObjectMapper objectMapper,
			final RabbitTemplate rabbitTemplate,
			final VerifyLVCCHashAlgorithm verifyLVCCHashAlgorithm,
			final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final ElectionEventService electionEventService,
			final LongVoteCastReturnCodesShareService longVoteCastReturnCodesShareService,
			final LVCCAllowListEntryService lvccAllowListEntryService,
			final Hash hash,
			final IdentifierValidationService identifierValidationService) {
		this.objectMapper = objectMapper;
		this.rabbitTemplate = rabbitTemplate;
		this.exactlyOnceCommandExecutor = exactlyOnceCommandExecutor;
		this.verifyLVCCHashAlgorithm = verifyLVCCHashAlgorithm;
		this.signatureKeystoreService = signatureKeystoreService;
		this.electionEventService = electionEventService;
		this.longVoteCastReturnCodesShareService = longVoteCastReturnCodesShareService;
		this.lvccAllowListEntryService = lvccAllowListEntryService;
		this.hash = hash;
		this.identifierValidationService = identifierValidationService;
	}

	@PostConstruct
	public void initQueue() {
		responseQueue = String.format("%s%s", VERIFY_LVCC_SHARE_HASH_RESPONSE_PATTERN, nodeId);
	}

	@RabbitListener(queues = VERIFY_LVCC_SHARE_HASH_REQUEST_PATTERN + "${nodeID}", autoStartup = "false", concurrency = "4")
	public void onMessage(final Message message) throws IOException {
		checkNotNull(message);
		checkNotNull(message.getMessageProperties());

		final String correlationId = checkNotNull(message.getMessageProperties().getCorrelationId());
		final byte[] messageBytes = checkNotNull(message.getBody());

		final List<ControlComponenthlVCCPayload> controlComponenthlVCCPayloads =
				List.of(objectMapper.readValue(messageBytes, ControlComponenthlVCCPayload[].class));

		final List<ControlComponenthlVCCPayload> unsignedControlComponenthlVCCPayloads = controlComponenthlVCCPayloads.stream()
				.map(controlComponenthlVCCPayload -> new ControlComponenthlVCCPayload(controlComponenthlVCCPayload.getEncryptionGroup(),
						controlComponenthlVCCPayload.getNodeId(), controlComponenthlVCCPayload.getHashLongVoteCastCodeShare(),
						controlComponenthlVCCPayload.getConfirmationKey()))
				.toList();
		final byte[] messageContent = Conversions.stringToByteArray(objectMapper.writeValueAsString(unsignedControlComponenthlVCCPayloads));

		verifyPayloadsSignatures(controlComponenthlVCCPayloads);
		verifyPayloadsConsistency(controlComponenthlVCCPayloads);

		final ContextIds contextIds = controlComponenthlVCCPayloads.get(0).getConfirmationKey().contextIds();

		identifierValidationService.validateContextIds(contextIds);
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardId = contextIds.verificationCardId();
		final String verificationCardSetId = contextIds.verificationCardSetId();

		longVoteCastReturnCodesShareService.validateConfirmationIsAllowed(electionEventId, verificationCardId, LocalDateTime::now);

		final BigInteger confirmationKeyValue = controlComponenthlVCCPayloads.get(0).getConfirmationKey().element().getValue();
		final String confirmationKeyAsString = Base64.getEncoder().encodeToString(hash.recursiveHash(HashableBigInteger.from(confirmationKeyValue)));

		final String contextId = String.join("-", electionEventId, verificationCardSetId, verificationCardId, confirmationKeyAsString);

		LOGGER.info("Received Long Vote Cast Return Codes Share verify request. [contextId: {}, correlationId: {}]", contextId, correlationId);

		final ExactlyOnceCommand exactlyOnceCommand = new ExactlyOnceCommand.Builder()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(Context.VOTING_RETURN_CODES_VERIFY_LVCC_SHARE_HASH.toString())
				.setTask(() -> generateControlComponentlVCCSharePayload(unsignedControlComponenthlVCCPayloads))
				.setRequestContent(messageContent)
				.build();
		final byte[] payloadBytes = exactlyOnceCommandExecutor.process(exactlyOnceCommand);

		LOGGER.debug("Long Vote Cast Return Codes Share verify task successfully processed. [contextId: {}, correlationId: {}]", contextId,
				correlationId);

		final Message responseMessage = Messages.createMessage(correlationId, payloadBytes);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, responseQueue, responseMessage);

		LOGGER.info("Long Vote Cast Return Codes Share verify response sent. [contextId: {}, correlationId: {}]", contextId, correlationId);
	}

	private void verifyPayloadsSignatures(final List<ControlComponenthlVCCPayload> controlComponenthlVCCPayloads) {

		for (final ControlComponenthlVCCPayload controlComponenthlVCCPayload : controlComponenthlVCCPayloads) {

			final CryptoPrimitivesSignature signature = controlComponenthlVCCPayload.getSignature();
			final ContextIds contextIds = controlComponenthlVCCPayloads.get(0).getConfirmationKey().contextIds();
			final int payloadNodeId = controlComponenthlVCCPayload.getNodeId();

			checkState(signature != null, "The signature of Control Component hlVCC Payload is null. [contextIds: %s]",
					contextIds);

			final Hashable additionalContextData = ChannelSecurityContextData.controlComponenthlVCC(payloadNodeId, contextIds.electionEventId(),
					contextIds.verificationCardSetId(), contextIds.verificationCardId());
			final boolean isSignatureValid;
			try {
				isSignatureValid = signatureKeystoreService.verifySignature(Alias.getControlComponentByNodeId(payloadNodeId),
						controlComponenthlVCCPayload, additionalContextData, signature.signatureContents());
			} catch (final SignatureException e) {
				throw new IllegalStateException(String.format(
						"Cannot verify the signature of Control Component hlVCC Payload. [contextIds: %s, payload nodeId: %s]",
						contextIds, payloadNodeId), e);
			}

			if (!isSignatureValid) {
				throw new InvalidPayloadSignatureException(SetupComponentLVCCAllowListPayload.class, String.format(
						"The signature of Control Component hlVCC Payload is invalid. [contextIds: %s, payload nodeId: %s]",
						contextIds, payloadNodeId));
			}
		}
	}

	private static void verifyPayloadsConsistency(final List<ControlComponenthlVCCPayload> controlComponenthlVCCPayloads) {
		checkArgument(controlComponenthlVCCPayloads.size() == NODE_IDS.size(),
				"There are not exactly the expected number of Control Component hlVCC Payloads. [expected: %s, received: %s]",
				NODE_IDS.size(), controlComponenthlVCCPayloads.size());

		final ContextIds contextIds = controlComponenthlVCCPayloads.get(0).getConfirmationKey().contextIds();

		final List<Integer> payloadNodeIds = controlComponenthlVCCPayloads.stream()
				.map(ControlComponenthlVCCPayload::getNodeId)
				.toList();

		final Set<Integer> payloadsNodeIdsSet = new HashSet<>(payloadNodeIds);
		checkArgument(payloadsNodeIdsSet.equals(NODE_IDS),
				"The Control Component hlVCC Payloads node ids do not match the expected node ids. [received: %s, expected: %s, contextIds: %]",
				payloadsNodeIdsSet, NODE_IDS, contextIds);

		checkArgument(allEqual(controlComponenthlVCCPayloads.stream(), ControlComponenthlVCCPayload::getConfirmationKey),
				"The Long Vote Cast Return Codes Share hash payloads do not contain all the same confirmation key.[contextIds: %]", contextIds);

		checkArgument(allEqual(controlComponenthlVCCPayloads.stream(), payload -> payload.getConfirmationKey().contextIds()),
				"The Control Component hlVCC Payloads do not contain all the same context ids. [contextIds: %]", contextIds);
	}

	private byte[] generateControlComponentlVCCSharePayload(
			final List<ControlComponenthlVCCPayload> controlComponenthlVCCPayloads) {

		final ControlComponenthlVCCPayload controlComponenthlVCCPayload = controlComponenthlVCCPayloads.get(0);
		final ContextIds contextIds = controlComponenthlVCCPayload.getConfirmationKey().contextIds();
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardId = contextIds.verificationCardId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final ConfirmationKey confirmationKey = controlComponenthlVCCPayload.getConfirmationKey();

		final VerifyLVCCHashInput verifyLVCCHashInput = buildVerifyLVCCHashInput(controlComponenthlVCCPayloads, verificationCardSetId,
				verificationCardId);
		final String hashedLongVoteCastReturnCode = verifyLVCCHashInput.getCcrjHashedLongVoteCastReturnCode();
		final List<String> otherCCRsHashedLongVoteCastReturnCodes = verifyLVCCHashInput.getOtherCCRsHashedLongVoteCastReturnCodes();

		LOGGER.debug("This node's hashed LVCC [nodeId: {}, hashedLongVoteCastReturnCode: {}].", nodeId, hashedLongVoteCastReturnCode);
		LOGGER.debug("Other nodes's hashed LVCC [otherCCRsHashedLongVoteCastReturnCodes: {}]", otherCCRsHashedLongVoteCastReturnCodes);

		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);
		final boolean isVerified = verifyLVCCHashAlgorithm.verifyLVCCHash(
				new LVCCContext(encryptionGroup, nodeId, electionEventId, verificationCardSetId), verifyLVCCHashInput);

		final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload;

		if (isVerified) {
			final LongVoteCastReturnCodesShare longVoteCastReturnCodesShare = longVoteCastReturnCodesShareService.load(confirmationKey, nodeId);
			controlComponentlVCCSharePayload = new ControlComponentlVCCSharePayload(electionEventId, verificationCardSetId, verificationCardId,
					nodeId, encryptionGroup, longVoteCastReturnCodesShare, confirmationKey, true);
		} else {
			controlComponentlVCCSharePayload = new ControlComponentlVCCSharePayload(encryptionGroup, electionEventId, verificationCardSetId,
					verificationCardId, nodeId, confirmationKey, false);
		}

		controlComponentlVCCSharePayload.setSignature(getPayloadSignature(controlComponentlVCCSharePayload));
		LOGGER.info("Successfully signed Control Component lVCC Share payload. [contextIds: {}]", contextIds);

		return serializePayload(controlComponentlVCCSharePayload);
	}

	private LongVoteCastReturnCodesAllowList getlVCCAllowList(final String verificationCardSetId) {
		return longVoteCastReturnCode -> lvccAllowListEntryService.exists(verificationCardSetId, longVoteCastReturnCode);
	}

	private VerifyLVCCHashInput buildVerifyLVCCHashInput(final List<ControlComponenthlVCCPayload> controlComponenthlVCCPayloads,
			final String verificationCardSetId, final String verificationCardId) {

		final ContextIds contextIds = controlComponenthlVCCPayloads.get(0).getConfirmationKey().contextIds();

		final ControlComponenthlVCCPayload controlComponenthlVCCPayload = controlComponenthlVCCPayloads.stream()
				.filter(payload -> payload.getNodeId() == nodeId)
				.findAny()
				.orElseThrow(() -> new IllegalStateException(
						String.format(
								"Missing Control Component hlVCC Payload node contribution. [contextIds: %s, missing node id: %s]",
								contextIds, nodeId)));

		final List<ControlComponenthlVCCPayload> otherCCRsControlComponenthlVCCPayloads = controlComponenthlVCCPayloads.stream()
				.filter(payload -> payload.getNodeId() != nodeId)
				.sorted(Comparator.comparingInt(ControlComponenthlVCCPayload::getNodeId))
				.toList();

		return new VerifyLVCCHashInput.Builder()
				.setLongVoteCastReturnCodesAllowList(getlVCCAllowList(verificationCardSetId))
				.setCcrjHashedLongVoteCastReturnCode(controlComponenthlVCCPayload.getHashLongVoteCastCodeShare())
				.setOtherCCRsHashedLongVoteCastReturnCodes(otherCCRsControlComponenthlVCCPayloads.stream()
						.map(ControlComponenthlVCCPayload::getHashLongVoteCastCodeShare)
						.toList())
				.setVerificationCardId(verificationCardId)
				.build();
	}

	private CryptoPrimitivesSignature getPayloadSignature(final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload) {
		final String electionEventId = controlComponentlVCCSharePayload.getElectionEventId();
		final String verificationCardId = controlComponentlVCCSharePayload.getVerificationCardId();
		final String verificationCardSetId = controlComponentlVCCSharePayload.getVerificationCardSetId();
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);

		final Hashable additionalContextData = ChannelSecurityContextData.controlComponentlVCCShare(nodeId, electionEventId, verificationCardSetId,
				verificationCardId);
		try {
			final byte[] signature = signatureKeystoreService.generateSignature(controlComponentlVCCSharePayload, additionalContextData);

			return new CryptoPrimitivesSignature(signature);
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not generate Control Component lVCC Share payload signature. [contextIds: %s]", contextIds));
		}

	}

	private byte[] serializePayload(final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload) {
		final String electionEventId = controlComponentlVCCSharePayload.getElectionEventId();
		final String verificationCardId = controlComponentlVCCSharePayload.getVerificationCardId();
		final String verificationCardSetId = controlComponentlVCCSharePayload.getVerificationCardSetId();
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);

		try {
			final byte[] serializedPayload = objectMapper.writeValueAsBytes(controlComponentlVCCSharePayload);

			LOGGER.debug("Successfully serialized Control Component lVCC Share payload. [contextIds: {}]", contextIds);

			return serializedPayload;
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(String.format("Could not serialize Control Component lVCC Share payload. [contextIds: %s]", contextIds),
					e);
		}
	}

}
