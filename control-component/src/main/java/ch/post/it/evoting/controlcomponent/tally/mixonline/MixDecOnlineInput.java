/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.tally.mixonline;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;

/**
 * Regroups the input values needed by the MixDecOnline algorithm.
 *
 * <ul>
 * <li>partiallyDecryptedVotes c<sub>dec,j</sub>, a list of ciphertexts. Not null.</li>
 * <li>(EL<sub>pk,1</sub>, EL<sub>pk,2</sub>, EL<sub>pk,3</sub>, EL<sub>pk,4</sub>), a list of elgamal public keys. Not null.</li>
 * <li>EB<sub>pk</sub>, an elgamal public key. Not null.</li>
 * <li>EL<sub>sk,j</sub>, an elgamal secret key. Not null.</li>
 * </ul>
 */
public class MixDecOnlineInput {

	private final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> partiallyDecryptedVotes;
	private final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys;
	private final ElGamalMultiRecipientPublicKey electoralBoardPublicKey;
	private final ElGamalMultiRecipientPrivateKey ccmjElectionSecretKey;

	private MixDecOnlineInput(
			final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> partiallyDecryptedVotes,
			final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys,
			final ElGamalMultiRecipientPublicKey electoralBoardPublicKey,
			final ElGamalMultiRecipientPrivateKey ccmjElectionSecretKey) {
		this.partiallyDecryptedVotes = partiallyDecryptedVotes;
		this.ccmElectionPublicKeys = ccmElectionPublicKeys;
		this.electoralBoardPublicKey = electoralBoardPublicKey;
		this.ccmjElectionSecretKey = ccmjElectionSecretKey;
	}

	public GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> getPartiallyDecryptedVotes() {
		return partiallyDecryptedVotes;
	}

	public GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> getCcmElectionPublicKeys() {
		return ccmElectionPublicKeys;
	}

	public ElGamalMultiRecipientPublicKey getElectoralBoardPublicKey() {
		return electoralBoardPublicKey;
	}

	public ElGamalMultiRecipientPrivateKey getCcmjElectionSecretKey() {
		return ccmjElectionSecretKey;
	}

	/**
	 * Builder performing input validations before constructing a {@link MixDecOnlineInput}.
	 */
	public static class Builder {

		private GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> partiallyDecryptedVotes;
		private GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys;
		private ElGamalMultiRecipientPublicKey electoralBoardPublicKey;
		private ElGamalMultiRecipientPrivateKey ccmjElectionSecretKey;

		public Builder setPartiallyDecryptedVotes(
				final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> partiallyDecryptedVotes) {
			this.partiallyDecryptedVotes = partiallyDecryptedVotes;
			return this;
		}

		public Builder setCcmElectionPublicKeys(
				final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys) {
			this.ccmElectionPublicKeys = ccmElectionPublicKeys;
			return this;
		}

		public Builder setElectoralBoardPublicKey(final ElGamalMultiRecipientPublicKey electoralBoardPublicKey) {
			this.electoralBoardPublicKey = electoralBoardPublicKey;
			return this;
		}

		public Builder setCcmjElectionSecretKey(final ElGamalMultiRecipientPrivateKey ccmjElectionSecretKey) {
			this.ccmjElectionSecretKey = ccmjElectionSecretKey;
			return this;
		}

		/**
		 * Creates a MixDecryptInput object.
		 *
		 * @throws NullPointerException     if any of the fields are null.
		 * @throws IllegalArgumentException if
		 *                                  <ul>
		 *                                      <li>there are not exactly 4 CCM election public keys</li>
		 *                                      <li>the ciphertexts and the public keys do not have the same group</li>
		 *                                      <li>the secret key does not have the same group order than the public keys</li>
		 *                                      <li>the CCM election public keys do not have the same number of elements as the CCM<sub>j</sub> election secret key</li>
		 *                                  </ul>
		 */
		public MixDecOnlineInput build() {
			checkNotNull(partiallyDecryptedVotes);
			checkNotNull(ccmElectionPublicKeys);
			checkNotNull(electoralBoardPublicKey);
			checkNotNull(ccmjElectionSecretKey);

			checkArgument(ccmElectionPublicKeys.size() == 4, "There must be exactly 4 CCM election public keys.");

			// Cross group checks
			checkArgument(ccmElectionPublicKeys.getGroup().hasSameOrderAs(ccmjElectionSecretKey.getGroup()),
					"The CCM election public keys must have the same group order as the CCM_j election secret key.");
			checkArgument(ccmElectionPublicKeys.getGroup().equals(electoralBoardPublicKey.getGroup()),
					"The CCM election public keys must have the same group as the electoral board public key.");
			checkArgument(partiallyDecryptedVotes.getGroup().equals(electoralBoardPublicKey.getGroup()),
					"The partially decrypted votes must have the same group as the public keys.");

			// Cross dimension check
			checkArgument(ccmElectionPublicKeys.getElementSize() == ccmjElectionSecretKey.size(),
					"The CCM election public keys must have the same size as the CCM_j election secret key.");

			return new MixDecOnlineInput(partiallyDecryptedVotes, ccmElectionPublicKeys, electoralBoardPublicKey, ccmjElectionSecretKey);
		}
	}
}
