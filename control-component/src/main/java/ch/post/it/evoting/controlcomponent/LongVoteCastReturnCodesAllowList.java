/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

public interface LongVoteCastReturnCodesAllowList {

	boolean exists(final String longVoteCastReturnCode);
}
