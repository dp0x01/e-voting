/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.configuration.electioncontext;

import static ch.post.it.evoting.controlcomponent.ControlComponentsApplicationBootstrap.RABBITMQ_EXCHANGE;
import static ch.post.it.evoting.domain.SharedQueue.SETUP_COMPONENT_PUBLIC_KEYS_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.SETUP_COMPONENT_PUBLIC_KEYS_RESPONSE_PATTERN;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.SignatureException;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommand;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommandExecutor;
import ch.post.it.evoting.controlcomponent.Messages;
import ch.post.it.evoting.controlcomponent.SetupComponentPublicKeysService;
import ch.post.it.evoting.cryptoprimitives.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.SetupComponentPublicKeysResponsePayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;

/**
 * Consumes the messages asking for the Election Event Context.
 */
@Service
public class SetupComponentPublicKeysProcessor {

	public static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentPublicKeysProcessor.class);

	private final ObjectMapper objectMapper;
	private final RabbitTemplate rabbitTemplate;
	private final SetupComponentPublicKeysService setupComponentPublicKeysService;
	private final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor;
	private final SignatureKeystore<Alias> signatureKeystoreService;

	private String responseQueue;

	@Value("${nodeID}")
	private int nodeId;

	public SetupComponentPublicKeysProcessor(final ObjectMapper objectMapper,
			final RabbitTemplate rabbitTemplate,
			final SetupComponentPublicKeysService setupComponentPublicKeysService,
			final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor,
			final SignatureKeystore<Alias> signatureKeystoreService) {

		this.objectMapper = objectMapper;
		this.rabbitTemplate = rabbitTemplate;
		this.setupComponentPublicKeysService = setupComponentPublicKeysService;
		this.exactlyOnceCommandExecutor = exactlyOnceCommandExecutor;
		this.signatureKeystoreService = signatureKeystoreService;
	}

	@PostConstruct
	public void initQueue() {
		responseQueue = String.format("%s%s", SETUP_COMPONENT_PUBLIC_KEYS_RESPONSE_PATTERN, nodeId);
	}

	@RabbitListener(queues = SETUP_COMPONENT_PUBLIC_KEYS_REQUEST_PATTERN + "${nodeID}", autoStartup = "false")
	public void onMessage(final Message message) throws IOException {
		final String correlationId = message.getMessageProperties().getCorrelationId();
		checkNotNull(correlationId, "Correlation Id must not be null.");

		final byte[] messageBytes = message.getBody();
		final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload = objectMapper.readValue(messageBytes,
				SetupComponentPublicKeysPayload.class);
		verifyPayloadSignature(setupComponentPublicKeysPayload);

		final SetupComponentPublicKeys setupComponentPublicKeys = setupComponentPublicKeysPayload.getSetupComponentPublicKeys();

		final String contextId = setupComponentPublicKeysPayload.getElectionEventId();
		LOGGER.info("Received setup component public keys request. [contextId: {}, correlationId: {}]", contextId, correlationId);

		final ExactlyOnceCommand exactlyOnceCommand = new ExactlyOnceCommand.Builder()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(Context.CONFIGURATION_SETUP_COMPONENT_PUBLIC_KEYS.toString())
				.setTask(() -> createSetupComponentPublicKeysResponse(contextId, setupComponentPublicKeys)).setRequestContent(messageBytes)
				.build();
		final byte[] payloadBytes = exactlyOnceCommandExecutor.process(exactlyOnceCommand);

		final Message responseMessage = Messages.createMessage(correlationId, payloadBytes);

		rabbitTemplate.send(RABBITMQ_EXCHANGE, responseQueue, responseMessage);
		LOGGER.info("Setup component public keys response sent. [contextId: {}]", contextId);
	}

	private void verifyPayloadSignature(final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload) {
		final String electionEventId = setupComponentPublicKeysPayload.getElectionEventId();

		final CryptoPrimitivesSignature signature = setupComponentPublicKeysPayload.getSignature();

		checkState(signature != null, "The signature of the setup component public keys payload is null. [electionEventId: %s]", electionEventId);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentPublicKeys(electionEventId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.SDM_CONFIG, setupComponentPublicKeysPayload,
					additionalContextData, signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of the setup component public keys payload. [electionEventId: %s]",
							electionEventId));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(ElectionEventContextPayload.class,
					String.format("[electionEventId: %s]", electionEventId));
		}
	}

	private byte[] createSetupComponentPublicKeysResponse(final String electionEventId, final SetupComponentPublicKeys setupComponentPublicKeys) {
		setupComponentPublicKeysService.save(electionEventId, setupComponentPublicKeys);

		LOGGER.info("Saved setup component public keys. [electionEventId: {}]", electionEventId);

		final SetupComponentPublicKeysResponsePayload setupComponentPublicKeysResponsePayload = new SetupComponentPublicKeysResponsePayload(nodeId,
				electionEventId);

		try {
			return objectMapper.writeValueAsBytes(setupComponentPublicKeysResponsePayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(
					String.format("Could not serialize setup component public keys response payload. [electionEventId: %s]", electionEventId), e);
		}
	}

}
