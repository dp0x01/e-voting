/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.configuration;

import static ch.post.it.evoting.controlcomponent.configuration.setupvoting.GenKeysCCRAlgorithm.GenKeysCCROutput;
import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_WRITE_IN_OPTIONS;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.controlcomponent.CcmjElectionKeysService;
import ch.post.it.evoting.controlcomponent.CcrjReturnCodesKeys;
import ch.post.it.evoting.controlcomponent.CcrjReturnCodesKeysService;
import ch.post.it.evoting.controlcomponent.configuration.setuptally.SetupTallyCCMAlgorithm;
import ch.post.it.evoting.controlcomponent.configuration.setuptally.SetupTallyCCMContext;
import ch.post.it.evoting.controlcomponent.configuration.setuptally.SetupTallyCCMOutput;
import ch.post.it.evoting.controlcomponent.configuration.setupvoting.GenKeysCCRAlgorithm;
import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;

@Service
class KeyGenerationService {

	private final GenKeysCCRAlgorithm genKeysCCRAlgorithm;
	private final SetupTallyCCMAlgorithm setupTallyCCMAlgorithm;
	private final CcmjElectionKeysService ccmjElectionKeysService;
	private final CcrjReturnCodesKeysService ccrjReturnCodesKeysService;

	@Value("${nodeID}")
	private int nodeId;

	KeyGenerationService(
			final GenKeysCCRAlgorithm genKeysCCRAlgorithm,
			final SetupTallyCCMAlgorithm setupTallyCCMAlgorithm,
			final CcmjElectionKeysService ccmjElectionKeysService,
			final CcrjReturnCodesKeysService ccrjReturnCodesKeysService) {
		this.genKeysCCRAlgorithm = genKeysCCRAlgorithm;
		this.setupTallyCCMAlgorithm = setupTallyCCMAlgorithm;
		this.ccmjElectionKeysService = ccmjElectionKeysService;
		this.ccrjReturnCodesKeysService = ccrjReturnCodesKeysService;
	}

	@Transactional
	ControlComponentPublicKeys generateCCKeys(final String electionEventId, final GqGroup encryptionParameters) {
		// Generate ccrj keys and save them.
		final GenKeysCCROutput genKeysCCROutput = genKeysCCRAlgorithm.genKeysCCR(encryptionParameters, electionEventId);
		final ZqElement ccrjReturnCodesGenerationSecretKey = genKeysCCROutput.ccrjReturnCodesGenerationSecretKey();
		final ElGamalMultiRecipientKeyPair ccrjChoiceReturnCodesEncryptionKeyPair = genKeysCCROutput.ccrjChoiceReturnCodesEncryptionKeyPair();

		final CcrjReturnCodesKeys newCcrjReturnCodesKeys = new CcrjReturnCodesKeys(electionEventId, ccrjReturnCodesGenerationSecretKey,
				ccrjChoiceReturnCodesEncryptionKeyPair);
		ccrjReturnCodesKeysService.save(newCcrjReturnCodesKeys);

		// Generate ccm election key pair and save it.
		final SetupTallyCCMContext context = new SetupTallyCCMContext.Builder()
				.setEncryptionGroup(encryptionParameters)
				.setNodeId(nodeId)
				.setElectionEventId(electionEventId)
				.setMaximumNumberOfSupportedWriteInsPlusOne(MAXIMUM_NUMBER_OF_WRITE_IN_OPTIONS + 1)
				.build();

		final SetupTallyCCMOutput setupTallyCCMOutput = setupTallyCCMAlgorithm.setupTallyCCM(context);

		ccmjElectionKeysService.save(electionEventId, setupTallyCCMOutput.getCcmjElectionKeyPair());

		final GroupVector<SchnorrProof, ZqGroup> ccrjSchnorrProofs = genKeysCCROutput.ccrjSchnorrProofs();

		return new ControlComponentPublicKeys(nodeId, ccrjChoiceReturnCodesEncryptionKeyPair.getPublicKey(), ccrjSchnorrProofs,
				setupTallyCCMOutput.getCcmjElectionKeyPair().getPublicKey(), setupTallyCCMOutput.getSchnorrProofs());
	}

}
