/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.tally.mixonline;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.after;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.security.SignatureException;
import java.util.Arrays;
import java.util.UUID;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestClassOrder;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.DirtiesContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.CcmjElectionKeysService;
import ch.post.it.evoting.controlcomponent.ElectionEventService;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineRequestPayload;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineResponsePayload;

@TestClassOrder(ClassOrderer.OrderAnnotation.class)
@Order(3)
@SpringBootTest(properties = { "nodeID=3" })
@DisplayName("MixDecryptProcessor on node 3 consuming")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class MixDecryptProcessorCC3ITCase extends MixDecryptProcessorTestBase {

	private static final int NODE_ID_3 = 3;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private ElectionEventService electionEventService;

	@SpyBean
	private MixDecryptService mixDecryptService;

	@SpyBean
	private MixDecryptProcessor mixDecryptProcessor;

	@SpyBean
	private VerifyMixDecOnlineAlgorithm verifyMixDecOnlineAlgorithm;

	@SpyBean
	private SignatureKeystore<Alias> signatureKeystoreService;

	@BeforeAll
	static void setUpAll(
			@Autowired
			final CcmjElectionKeysService ccmjElectionKeysService) {

		// Each node needs to save its ccmj election key pair.
		ccmjElectionKeysService.save(electionEventId, getCcmjKeyPair(NODE_ID_3));
	}

	@Test
	@DisplayName("a mixDecryptOnlineRequestPayload correctly mixes")
	void request() throws IOException, SignatureException {
		doReturn(true).when(signatureKeystoreService).verifySignature(any(), any(), any(), any());

		final MixDecryptResponse mixDecryptResponseNode1 = getResponseMessage(1);
		final Message responseMessageNode1 = mixDecryptResponseNode1.getMessage();

		final MixDecryptResponse mixDecryptResponseNode2 = getResponseMessage(2);
		final String electionEventId = mixDecryptResponseNode2.getElectionEventId();
		final String ballotBoxIdToMix = mixDecryptResponseNode2.getBallotBoxId();
		final Message responseMessageNode2 = mixDecryptResponseNode2.getMessage();
		final GqGroup gqGroup = electionEventService.getEncryptionGroup(electionEventId);

		final MixDecryptOnlineResponsePayload mixDecryptOnlineResponsePayload1 = objectMapper.reader()
				.withAttribute("group", gqGroup)
				.readValue(responseMessageNode1.getBody(), MixDecryptOnlineResponsePayload.class);
		final MixDecryptOnlineResponsePayload mixDecryptOnlineResponsePayload2 = objectMapper.reader()
				.withAttribute("group", gqGroup)
				.readValue(responseMessageNode2.getBody(), MixDecryptOnlineResponsePayload.class);

		// Send to node 3.
		final MixDecryptOnlineRequestPayload request = new MixDecryptOnlineRequestPayload(electionEventId, ballotBoxIdToMix, NODE_ID_3,
				Arrays.asList(mixDecryptOnlineResponsePayload1.controlComponentShufflePayload(),
						mixDecryptOnlineResponsePayload2.controlComponentShufflePayload()));
		final byte[] requestBytes = objectMapper.writeValueAsBytes(request);

		final MessageProperties messageProperties = new MessageProperties();
		final String correlationId = UUID.randomUUID().toString();
		messageProperties.setCorrelationId(correlationId);

		// Sends a request to the processor.
		final Message message = new Message(requestBytes, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, MIX_DEC_ONLINE_REQUEST_QUEUE_3, message);

		// Collects the response of the processor.
		final Message responseMessage = rabbitTemplate.receive(MIX_DEC_ONLINE_RESPONSE_QUEUE_3, 5000);

		assertNotNull(responseMessage);
		assertEquals(correlationId, responseMessage.getMessageProperties().getCorrelationId());

		verify(mixDecryptProcessor, after(5000).times(1)).onMessage(any());
		verify(mixDecryptService).performMixDecrypt(any(), any(), any());
		verify(verifyMixDecOnlineAlgorithm).verifyMixDecOnline(any(), any());
		verify(signatureKeystoreService, times(2)).verifySignature(any(), any(), any(), any());
		verify(signatureKeystoreService, times(2)).generateSignature(any(), any());

		// Saves the response so the next nodes can use it.
		addResponseMessage(NODE_ID_3, electionEventId, ballotBoxIdToMix, responseMessage);
	}

}