/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.controlcomponent.voting.confirmvote.LongVoteCastReturnCodesShareRepository;

@Service
public class TestDatabaseCleanUpService {

	@Autowired
	private ElectionContextRepository electionContextRepository;

	@Autowired
	private SetupComponentPublicKeysRepository setupComponentPublicKeysRepository;

	@Autowired
	private CcrjReturnCodesKeysRepository ccrjReturnCodesKeysRepository;

	@Autowired
	private CcmjElectionKeysRepository ccmjElectionKeysRepository;

	@Autowired
	private VerificationCardRepository verificationCardRepository;

	@Autowired
	private BallotBoxRepository ballotBoxRepository;

	@Autowired
	private VerificationCardSetRepository verificationCardSetRepository;

	@Autowired
	private PCCAllowListEntryRepository pccAllowListEntryRepository;

	@Autowired
	private EncryptedVerifiableVoteRepository encryptedVerifiableVoteRepository;

	@Autowired
	private ElectionEventRepository electionEventRepository;

	@Autowired
	private LongVoteCastReturnCodesShareRepository longVoteCastReturnCodesShareRepository;

	@Autowired
	private LVCCAllowListEntryRepository lvccAllowListEntryRepository;

	public void cleanUp() {
		longVoteCastReturnCodesShareRepository.deleteAll();
		encryptedVerifiableVoteRepository.deleteAll();
		verificationCardRepository.deleteAll();
		ballotBoxRepository.deleteAll();
		pccAllowListEntryRepository.deleteAll();
		lvccAllowListEntryRepository.deleteAll();
		verificationCardSetRepository.deleteAll();
		ccrjReturnCodesKeysRepository.deleteAll();
		ccmjElectionKeysRepository.deleteAll();
		setupComponentPublicKeysRepository.deleteAll();
		electionContextRepository.deleteAll();
		electionEventRepository.deleteAll();
	}

}
