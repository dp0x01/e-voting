/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.after;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.security.SignatureException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.DirtiesContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.ElectionEventEntity;
import ch.post.it.evoting.controlcomponent.ElectionEventService;
import ch.post.it.evoting.controlcomponent.KeystoreRepository;
import ch.post.it.evoting.controlcomponent.MessageBrokerIntegrationTestBase;
import ch.post.it.evoting.controlcomponent.TestDatabaseCleanUpService;
import ch.post.it.evoting.controlcomponent.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponent.VerificationCardSetService;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystoreFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.domain.configuration.setupvoting.LongVoteCastReturnCodesAllowListResponsePayload;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;

@DisplayName("A LongVoteCastReturnCodesAllowListProcessor consuming")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class LongVoteCastReturnCodesAllowListProcessorIT extends MessageBrokerIntegrationTestBase {

	private static final String VERIFICATION_CARD_SET_ID = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String lVCC1 = Base64.getEncoder().encodeToString(new byte[] { 5 });
	private static final String lVCC2 = Base64.getEncoder().encodeToString(new byte[] { 17 });
	private static final List<String> LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST = Arrays.asList(lVCC1, lVCC2);

	private static SetupComponentLVCCAllowListPayload signedSetupComponentLVCCAllowListPayload;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	@SpyBean
	private LongVoteCastReturnCodesAllowListProcessor longVoteCastReturnCodesAllowListProcessor;

	@SpyBean
	private VerificationCardSetService verificationCardSetService;

	@BeforeAll
	static void setUpAll(
			@Autowired
			final ElectionEventService electionEventService,
			@Autowired
			final VerificationCardSetService verificationCardSetService,
			@Autowired
			final KeystoreRepository repository) throws SignatureException, IOException {

		// Save election event.
		final ElectionEventEntity electionEventEntity = electionEventService.save(electionEventId, GroupTestData.getGqGroup());

		// Save verification card sets.
		final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity(VERIFICATION_CARD_SET_ID, electionEventEntity,
				new CombinedCorrectnessInformation(Collections.emptyList()));
		verificationCardSetService.save(verificationCardSetEntity);

		// Request payload.
		signedSetupComponentLVCCAllowListPayload =
				new SetupComponentLVCCAllowListPayload(electionEventId, VERIFICATION_CARD_SET_ID, LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST);
		final SignatureKeystore<Alias> sdmSignatureKeystoreService = SignatureKeystoreFactory.createSignatureKeystore(repository.getKeyStore(),
				"PKCS12",
				repository.getKeystorePassword(), keystore -> true, Alias.SDM_CONFIG);
		final byte[] signature = sdmSignatureKeystoreService.generateSignature(signedSetupComponentLVCCAllowListPayload,
				ChannelSecurityContextData.setupComponentLVCCAllowList(electionEventId, VERIFICATION_CARD_SET_ID));
		signedSetupComponentLVCCAllowListPayload.setSignature(new CryptoPrimitivesSignature(signature));
	}

	@AfterAll
	static void cleanUp(
			@Autowired
			final TestDatabaseCleanUpService testDatabaseCleanUpService) {

		testDatabaseCleanUpService.cleanUp();
	}

	@Test
	@DisplayName("a longVoteCastReturnCodesAllowListPayload saves the lists in the database and returns a response.")
	void request() throws IOException {

		final byte[] longVoteCastReturnCodesAllowListPayloadBytes = objectMapper.writeValueAsBytes(signedSetupComponentLVCCAllowListPayload);

		final MessageProperties messageProperties = new MessageProperties();
		final String correlationId = UUID.randomUUID().toString();
		messageProperties.setCorrelationId(correlationId);

		// Sends a request to the processor.
		final Message message = new Message(longVoteCastReturnCodesAllowListPayloadBytes, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, LONG_VOTE_CAST_RETURN_CODES_LIST_REQUEST_QUEUE_1, message);

		// Collects the response of the processor.
		final Message responseMessage = rabbitTemplate.receive(LONG_VOTE_CAST_RETURN_CODES_LIST_RESPONSE_QUEUE_1, 5000);

		assertNotNull(responseMessage);
		assertEquals(correlationId, responseMessage.getMessageProperties().getCorrelationId());

		verify(longVoteCastReturnCodesAllowListProcessor, after(5000).times(1)).onMessage(any());

		assertEquals(LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST, verificationCardSetService.getLongVoteCastReturnCodesAllowList(VERIFICATION_CARD_SET_ID));

		final LongVoteCastReturnCodesAllowListResponsePayload longVoteCastReturnCodesAllowListResponsePayload =
				objectMapper.readValue(responseMessage.getBody(), LongVoteCastReturnCodesAllowListResponsePayload.class);

		assertEquals(1, longVoteCastReturnCodesAllowListResponsePayload.nodeId());
		assertEquals(electionEventId, longVoteCastReturnCodesAllowListResponsePayload.electionEventId());
	}
}
