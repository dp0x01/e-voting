/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import java.util.Locale;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;

/**
 * Tests of DecryptPCCAlgorithm.
 */
@DisplayName("DecryptPCCService")
class DecryptPCCAlgorithmTest extends TestGroupSetup {

	private static final int PHI = MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
	private static final int PSI = 5;
	private static final int NODE_ID = 1;
	private static final int ID_STRING_LENGTH = 32;
	private static final Random random = RandomFactory.createRandom();
	private static final ZeroKnowledgeProof zeroKnowledgeProofMock = spy(ZeroKnowledgeProof.class);

	private static DecryptPCCAlgorithm decryptPCCAlgorithm;
	private static ElGamalGenerator elGamalGenerator;

	private DecryptPCCContext decryptPCCContext;
	private DecryptPCCInput decryptPCCInput;

	@BeforeAll
	static void init() {
		decryptPCCAlgorithm = new DecryptPCCAlgorithm(zeroKnowledgeProofMock);
		elGamalGenerator = new ElGamalGenerator(gqGroup);
	}

	private GroupVector<ExponentiationProof, ZqGroup> genRandomExponentiationProofVector() {
		return Stream.generate(
						() -> new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(), zqGroupGenerator.genRandomZqElementMember()))
				.limit(DecryptPCCAlgorithmTest.PSI)
				.collect(GroupVector.toGroupVector());
	}

	@Nested
	@DisplayName("calling decryptPCC with")
	class DecryptPCCTest {

		@BeforeEach
		void setup() {
			final String electionEventId = random.genRandomBase16String(ID_STRING_LENGTH).toLowerCase(Locale.ENGLISH);
			final String verificationCardId = random.genRandomBase16String(ID_STRING_LENGTH).toLowerCase(Locale.ENGLISH);
			final String verificationCardSetId = random.genRandomBase16String(ID_STRING_LENGTH).toLowerCase(Locale.ENGLISH);
			final GroupVector<GqElement, GqGroup> exponentiatedGammaElements = gqGroupGenerator.genRandomGqElementVector(PSI);
			final GroupVector<GroupVector<GqElement, GqGroup>, GqGroup> otherExponentiatedGammaElements = GroupVector.of(
					gqGroupGenerator.genRandomGqElementVector(PSI),
					gqGroupGenerator.genRandomGqElementVector(PSI),
					gqGroupGenerator.genRandomGqElementVector(PSI)
			);
			final GroupVector<GroupVector<ExponentiationProof, ZqGroup>, ZqGroup> otherExponentiationProofs = GroupVector.of(
					genRandomExponentiationProofVector(),
					genRandomExponentiationProofVector(),
					genRandomExponentiationProofVector()
			);
			final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> otherChoiceReturnCodesEncryptionKeys = GroupVector.of(
					elGamalGenerator.genRandomPublicKey(PHI),
					elGamalGenerator.genRandomPublicKey(PHI),
					elGamalGenerator.genRandomPublicKey(PHI)
			);
			final ElGamalMultiRecipientCiphertext encryptedVote = elGamalGenerator.genRandomCiphertext(1);
			final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote = elGamalGenerator.genRandomCiphertext(1);
			final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes = elGamalGenerator.genRandomCiphertext(PSI);

			decryptPCCContext = new DecryptPCCContext.Builder()
					.setEncryptionGroup(gqGroup)
					.setNodeId(NODE_ID)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setNumberOfSelectableVotingOptions(PSI)
					.setNumberOfAllowedWriteInsPlusOne(1)
					.build();

			decryptPCCInput = new DecryptPCCInput.Builder().setVerificationCardId(verificationCardId)
					.setExponentiatedGammaElements(exponentiatedGammaElements)
					.setOtherCcrExponentiatedGammaElements(otherExponentiatedGammaElements)
					.setOtherCcrExponentiationProofs(otherExponentiationProofs)
					.setOtherCcrChoiceReturnCodesEncryptionKeys(otherChoiceReturnCodesEncryptionKeys)
					.setEncryptedVote(encryptedVote)
					.setExponentiatedEncryptedVote(exponentiatedEncryptedVote)
					.setEncryptedPartialChoiceReturnCodes(encryptedPartialChoiceReturnCodes)
					.build();
		}

		@Test
		@DisplayName("any null arguments throws a NullPointerException")
		void decryptPCCWithNullArgumentsThrows() {
			assertThrows(NullPointerException.class, () -> decryptPCCAlgorithm.decryptPCC(null, decryptPCCInput));
			assertThrows(NullPointerException.class, () -> decryptPCCAlgorithm.decryptPCC(decryptPCCContext, null));
		}

		@Test
		@DisplayName("valid arguments does not throw")
		void decryptPCCWithValidInputDoesNotThrow() {
			doReturn(true).when(zeroKnowledgeProofMock).verifyExponentiation(any(), any(), any(), anyList());

			final GroupVector<GqElement, GqGroup> gqElements = assertDoesNotThrow(
					() -> decryptPCCAlgorithm.decryptPCC(decryptPCCContext, decryptPCCInput));

			assertEquals(PSI, gqElements.size());
		}

		@Test
		@DisplayName("context and input having different groups throws an IllegalArgumentException")
		void decryptPCCWithContextAndInputFromDifferentGroupsThrows() {
			final DecryptPCCContext spyDecryptPCCContext = spy(DecryptPCCAlgorithmTest.this.decryptPCCContext);
			doReturn(otherGqGroup).when(spyDecryptPCCContext).getEncryptionGroup();
			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> decryptPCCAlgorithm.decryptPCC(spyDecryptPCCContext, decryptPCCInput));
			assertEquals("The context and input must have the same group.", Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("encrypted partial choice return codes with a size different from numberOfSelectableVotingOptions throws an IllegalArgumentException")
		void decryptPCCWithEncryptedPartialChoiceReturnCodesWrongSizeThrows() {
			// Encrypted partial choice return codes of size numberOfSelectableVotingOptions - 1
			final DecryptPCCInput decryptPCCInputSpy = spy(decryptPCCInput);
			final ElGamalMultiRecipientCiphertext tooShortEncryptedPartialChoiceReturnCodes = elGamalGenerator.genRandomCiphertext(PSI - 1);
			doReturn(tooShortEncryptedPartialChoiceReturnCodes).when(decryptPCCInputSpy).getEncryptedPartialChoiceReturnCodes();
			IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> decryptPCCAlgorithm.decryptPCC(decryptPCCContext, decryptPCCInputSpy));
			assertEquals(String.format("The encrypted partial Choice Return Codes size must be equal to psi. [psi: %s]", PSI),
					Throwables.getRootCause(exception).getMessage());

			// Encrypted partial choice return codes of size numberOfSelectableVotingOptions + 1
			final ElGamalMultiRecipientCiphertext tooLongEncryptedPartialChoiceReturnCodes = elGamalGenerator.genRandomCiphertext(PSI + 1);
			doReturn(tooLongEncryptedPartialChoiceReturnCodes).when(decryptPCCInputSpy).getEncryptedPartialChoiceReturnCodes();
			exception = assertThrows(IllegalArgumentException.class,
					() -> decryptPCCAlgorithm.decryptPCC(decryptPCCContext, decryptPCCInputSpy));
			assertEquals(String.format("The encrypted partial Choice Return Codes size must be equal to psi. [psi: %s]", PSI),
					Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("with failing zero knowledge proof validation throws an IllegalStateException")
		void decryptPCCWithFailingZeroKnowledgeProofVerification() {
			doReturn(false).when(zeroKnowledgeProofMock).verifyExponentiation(any(), any(), any(), anyList());

			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> decryptPCCAlgorithm.decryptPCC(decryptPCCContext, decryptPCCInput));
			assertEquals(String.format("The verification of the other control component's exponentiation proof failed [control component: %d]", 2),
					Throwables.getRootCause(exception).getMessage());
		}

	}
}
