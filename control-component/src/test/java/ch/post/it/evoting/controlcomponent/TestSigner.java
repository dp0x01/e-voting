/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.SignatureException;

import ch.post.it.evoting.cryptoprimitives.domain.signature.SignedPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystoreFactory;

/**
 * Test utility to create a signature from any Alias.
 */
public class TestSigner {
	private final SignatureKeystore<Alias> signatureKeystoreService;

	public TestSigner(Path keystoreLocation, Path keyStorePasswordLocation, Alias alias) throws IOException {
		final InputStream keyStoreStream = Files.newInputStream(keystoreLocation);
		final char[] keyStorePassword = new String(Files.readAllBytes(keyStorePasswordLocation)).toCharArray();
		signatureKeystoreService = SignatureKeystoreFactory.createSignatureKeystore(keyStoreStream, "PKCS12", keyStorePassword, keystore -> true, alias);
	}

	public void sign(SignedPayload toSign, Hashable additionalContextData) throws SignatureException {
		final byte[] signature = signatureKeystoreService.generateSignature(toSign, additionalContextData);
		toSign.setSignature(new CryptoPrimitivesSignature(signature));
	}
}
