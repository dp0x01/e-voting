/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.SignatureException;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.annotation.DirtiesContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.CcrjReturnCodesKeys;
import ch.post.it.evoting.controlcomponent.CcrjReturnCodesKeysService;
import ch.post.it.evoting.controlcomponent.ElectionEventService;
import ch.post.it.evoting.controlcomponent.PCCAllowListEntryEntity;
import ch.post.it.evoting.controlcomponent.TestDatabaseCleanUpService;
import ch.post.it.evoting.controlcomponent.MessageBrokerIntegrationTestBase;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;

@DisplayName("GenEncLongCodeSharesProcessor consuming")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class GenEncLongCodeSharesProcessorIT extends MessageBrokerIntegrationTestBase {

	private static byte[] requestPayloadBytes;

	private static SetupComponentVerificationDataPayload setupComponentVerificationDataPayload;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private PCCAllowListEntryService PCCAllowListEntryService;

	@SpyBean
	private GenEncLongCodeSharesAlgorithm genEncLongCodeSharesAlgorithm;

	@MockBean
	private SignatureKeystore<Alias> signatureKeystoreService;

	@Autowired
	private ObjectMapper objectMapper;

	@BeforeAll
	static void setUpAll(
			@Autowired
			final ObjectMapper objectMapper,
			@Autowired
			final ElectionEventService electionEventService,
			@Autowired
			final CcrjReturnCodesKeysService ccrjReturnCodesKeysService
	) throws IOException {

		final Resource payloadsResource = new ClassPathResource(
				"configuration/setupvoting/enclongcodeshares/setupComponentVerificationDataPayload.3.json");

		setupComponentVerificationDataPayload = objectMapper.readValue(payloadsResource.getFile(),
				SetupComponentVerificationDataPayload.class);

		requestPayloadBytes = objectMapper.writeValueAsBytes(setupComponentVerificationDataPayload);

		// Must match the group in the json.
		final GqGroup encryptionGroup = setupComponentVerificationDataPayload.getEncryptionGroup();

		// Save election event.
		final String electionEventId = setupComponentVerificationDataPayload.getElectionEventId();

		electionEventService.save(electionEventId, encryptionGroup);

		final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(encryptionGroup));
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(encryptionGroup);

		final ZqElement ccrKey = zqGroupGenerator.genRandomZqElementMember();
		final ElGamalMultiRecipientKeyPair elGamalMultiRecipientKeyPair = elGamalGenerator.genRandomKeyPair(10);
		ccrjReturnCodesKeysService.save(new CcrjReturnCodesKeys(electionEventId, ccrKey, elGamalMultiRecipientKeyPair));

	}

	@AfterAll
	static void cleanUp(
			@Autowired
			final TestDatabaseCleanUpService testDatabaseCleanUpService) {

		testDatabaseCleanUpService.cleanUp();
	}

	@Test
	@DisplayName("Happy Path Gen Enc Long Code Share Processing")
	void happyPathGenEncLongCodeShareProcessing() throws SignatureException, IOException {
		when(signatureKeystoreService.generateSignature(any(), any())).thenReturn(electionEventId.getBytes(StandardCharsets.UTF_8));
		when(signatureKeystoreService.verifySignature(eq(Alias.SDM_CONFIG), any(), any(), any())).thenReturn(true);

		final MessageProperties messageProperties = new MessageProperties();
		final String correlationId = UUID.randomUUID().toString();
		messageProperties.setCorrelationId(correlationId);

		final Message message = new Message(requestPayloadBytes, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, GEN_ENC_LONG_CODE_SHARES_REQUEST_QUEUE_1, message);

		final Message responseMessage = rabbitTemplate.receive(GEN_ENC_LONG_CODE_SHARES_RESPONSE_QUEUE_1, 30000);

		assertNotNull(responseMessage);

		final byte[] response = responseMessage.getBody();
		final ControlComponentCodeSharesPayload controlComponentCodeSharesPayload = objectMapper.readValue(response,
				ControlComponentCodeSharesPayload.class);

		assertEquals(correlationId, responseMessage.getMessageProperties().getCorrelationId());

		verify(genEncLongCodeSharesAlgorithm, times(1)).genEncLongCodeShares(any(), any());
		verify(signatureKeystoreService, times(1)).verifySignature(eq(Alias.SDM_CONFIG), any(), any(), any());

		assertEquals(setupComponentVerificationDataPayload.getElectionEventId(), controlComponentCodeSharesPayload.getElectionEventId());
		assertEquals(setupComponentVerificationDataPayload.getVerificationCardSetId(), controlComponentCodeSharesPayload.getVerificationCardSetId());
		assertEquals(setupComponentVerificationDataPayload.getChunkId(), controlComponentCodeSharesPayload.getChunkId());
	}

	@Test
	@DisplayName("Happy Path concurrent Gen Enc Long Code Share Processing")
	void happyPathConcurrentGenEncLongCodeShareProcessing() throws SignatureException, IOException {
		when(signatureKeystoreService.generateSignature(any(), any())).thenReturn(electionEventId.getBytes(StandardCharsets.UTF_8));
		when(signatureKeystoreService.verifySignature(eq(Alias.SDM_CONFIG), any(), any(), any())).thenReturn(true);

		final Resource payloadsResource0 = new ClassPathResource(
				"configuration/setupvoting/enclongcodeshares/setupComponentVerificationDataPayload.0.json");

		final Resource payloadsResource1 = new ClassPathResource(
				"configuration/setupvoting/enclongcodeshares/setupComponentVerificationDataPayload.1.json");

		final SetupComponentVerificationDataPayload payload0 = objectMapper.readValue(payloadsResource0.getFile(),
				SetupComponentVerificationDataPayload.class);
		final byte[] requestPayloadBytes0 = objectMapper.writeValueAsBytes(payload0);
		final byte[] requestPayloadBytes1 = objectMapper.writeValueAsBytes(objectMapper.readValue(payloadsResource1.getFile(),
				SetupComponentVerificationDataPayload.class));

		final MessageProperties messageProperties0 = new MessageProperties();
		final String correlationId0 = UUID.randomUUID().toString();
		messageProperties0.setCorrelationId(correlationId0);
		final Message message0 = new Message(requestPayloadBytes0, messageProperties0);

		final MessageProperties messageProperties1 = new MessageProperties();
		final String correlationId1 = UUID.randomUUID().toString();
		messageProperties1.setCorrelationId(correlationId1);
		final Message message1 = new Message(requestPayloadBytes1, messageProperties1);

		// Send two messages roughly at the same time to test that when two chunks are processed at the same time, one will success and one fail
		// to create the verification card set. The failing one will retry once and will be able to process the message. Depending on the thread
		// execution order, it may or may not generate a concurrent creation of the verification card set.
		rabbitTemplate.send(RABBITMQ_EXCHANGE, GEN_ENC_LONG_CODE_SHARES_REQUEST_QUEUE_1, message0);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, GEN_ENC_LONG_CODE_SHARES_REQUEST_QUEUE_1, message1);

		final Message responseMessage0 = rabbitTemplate.receive(GEN_ENC_LONG_CODE_SHARES_RESPONSE_QUEUE_1, 30000);
		final Message responseMessage1 = rabbitTemplate.receive(GEN_ENC_LONG_CODE_SHARES_RESPONSE_QUEUE_1, 30000);

		assertNotNull(responseMessage0);
		assertNotNull(responseMessage1);

		// In the end, two chunks must have been successfully saved, regardless of concurrency.
		final List<PCCAllowListEntryEntity> pccAllowList = PCCAllowListEntryService.getPCCAllowList(payload0.getVerificationCardSetId());
		final List<Integer> chunkIds = pccAllowList.stream()
				.map(PCCAllowListEntryEntity::getChunkId)
				.distinct().toList();
		assertEquals(2, chunkIds.size());
	}

}
