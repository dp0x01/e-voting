/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.confirmvote;

import static ch.post.it.evoting.controlcomponent.TestKeyStoreInitializer.KEYSTORE_FILENAME_PATH;
import static ch.post.it.evoting.controlcomponent.TestKeyStoreInitializer.KEYSTORE_PASSWORD_FILENAME_PATH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.security.SignatureException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.DirtiesContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.BallotBoxService;
import ch.post.it.evoting.controlcomponent.CcrjReturnCodesKeys;
import ch.post.it.evoting.controlcomponent.CcrjReturnCodesKeysService;
import ch.post.it.evoting.controlcomponent.ElectionContextEntity;
import ch.post.it.evoting.controlcomponent.ElectionContextService;
import ch.post.it.evoting.controlcomponent.ElectionEventEntity;
import ch.post.it.evoting.controlcomponent.ElectionEventService;
import ch.post.it.evoting.controlcomponent.MessageBrokerIntegrationTestBase;
import ch.post.it.evoting.controlcomponent.TestDatabaseCleanUpService;
import ch.post.it.evoting.controlcomponent.TestSigner;
import ch.post.it.evoting.controlcomponent.VerificationCard;
import ch.post.it.evoting.controlcomponent.VerificationCardService;
import ch.post.it.evoting.controlcomponent.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponent.VerificationCardSetService;
import ch.post.it.evoting.controlcomponent.VerificationCardStateService;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.domain.voting.confirmvote.ConfirmationKey;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCPayload;
import ch.post.it.evoting.domain.voting.confirmvote.VotingServerConfirmPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class LongVoteCastReturnCodeShareHashProcessorIT extends MessageBrokerIntegrationTestBase {

	private static ContextIds contextIds;
	private static VotingServerConfirmPayload votingServerConfirmPayload;
	private static ElectionEventEntity savedElectionEventEntity;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	@SpyBean
	private SignatureKeystore<Alias> signatureKeystoreService;

	@MockBean
	private ElectionContextService electionContextService;

	@BeforeAll
	static void setUpAll(
			@Autowired
			final BallotBoxService ballotBoxService,
			@Autowired
			final ElectionEventService electionEventService,
			@Autowired
			final VerificationCardSetService verificationCardSetService,
			@Autowired
			final VerificationCardService verificationCardService,
			@Autowired
			final CcrjReturnCodesKeysService ccrjReturnCodesKeysService,
			@Autowired
			final VerificationCardStateService verificationCardStateService) throws IOException, SignatureException {

		contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);

		// Election group.
		final GqGroup gqGroup = GroupTestData.getLargeGqGroup();
		final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(gqGroup));
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);

		// Create election event.
		savedElectionEventEntity = electionEventService.save(electionEventId, gqGroup);

		// Create verification card set.
		final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity(verificationCardSetId, savedElectionEventEntity,
				new CombinedCorrectnessInformation(Collections.emptyList()));
		verificationCardSetService.save(verificationCardSetEntity);

		// Create ballot box.
		final String ballotBoxId = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
		final GroupVector<PrimeGqElement, GqGroup> smallPrimeGroupMembers = PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(
				gqGroup, 1);
		final PrimesMappingTable primesMappingTable = PrimesMappingTable.from(
				List.of(new PrimesMappingTableEntry("actualVotingOption", smallPrimeGroupMembers.get(0), "semantic")));
		ballotBoxService.save(ballotBoxId, verificationCardSetId, true, 1, 1, 900, primesMappingTable);

		// Create verification card.
		final ElGamalMultiRecipientPublicKey publicKey = elGamalGenerator.genRandomPublicKey(1);
		verificationCardService.save(new VerificationCard(verificationCardId, verificationCardSetId, publicKey));

		// Create CCRj election keys.
		final ZqElement ccrKey = zqGroupGenerator.genRandomZqElementMember();
		final ElGamalMultiRecipientKeyPair elGamalMultiRecipientKeyPair = elGamalGenerator.genRandomKeyPair(10);
		ccrjReturnCodesKeysService.save(new CcrjReturnCodesKeys(electionEventId, ccrKey, elGamalMultiRecipientKeyPair));

		//Mark card as LCC share created
		verificationCardStateService.setSentVote(verificationCardId);

		// Generate request payload.
		final GqElement confirmationKeyElement = new GqGroupGenerator(gqGroup).genMember();
		final ConfirmationKey confirmationKey = new ConfirmationKey(contextIds, confirmationKeyElement);

		final TestSigner votingServerSigner = new TestSigner(KEYSTORE_FILENAME_PATH, KEYSTORE_PASSWORD_FILENAME_PATH, Alias.VOTING_SERVER);

		votingServerConfirmPayload = new VotingServerConfirmPayload(gqGroup, confirmationKey);
		votingServerSigner.sign(votingServerConfirmPayload,
				ChannelSecurityContextData.votingServerConfirm(electionEventId, verificationCardSetId, verificationCardId));
	}

	@AfterAll
	static void cleanUp(
			@Autowired
			final TestDatabaseCleanUpService testDatabaseCleanUpService) {
		testDatabaseCleanUpService.cleanUp();
	}

	@Test
	void happyPath() throws IOException, SignatureException {
		doReturn(true).when(signatureKeystoreService).verifySignature(any(), any(), any(), any());

		final LocalDateTime now = LocalDateTime.now();
		final ElectionContextEntity electionContextEntity = new ElectionContextEntity.Builder()
				.setElectionEventEntity(savedElectionEventEntity)
				.setStartTime(now.minusHours(1))
				.setFinishTime(now.plusHours(1))
				.build();
		when(electionContextService.getElectionContextEntity(electionEventId)).thenReturn(electionContextEntity);

		final MessageProperties messageProperties = new MessageProperties();
		final String correlationId = UUID.randomUUID().toString();
		messageProperties.setCorrelationId(correlationId);

		final Message message = new Message(objectMapper.writeValueAsBytes(votingServerConfirmPayload), messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, CREATE_LVCC_SHARE_HASH_REQUEST_QUEUE_1, message);

		final Message response = rabbitTemplate.receive(CREATE_LVCC_SHARE_HASH_RESPONSE_QUEUE_1, 5000);

		assertNotNull(response);

		final ControlComponenthlVCCPayload responsePayload = objectMapper.readValue(response.getBody(), ControlComponenthlVCCPayload.class);

		assertEquals(contextIds, responsePayload.getConfirmationKey().contextIds());
		assertNotNull(responsePayload.getHashLongVoteCastCodeShare());
	}
}

