/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.confirmvote;

import static ch.post.it.evoting.controlcomponent.TestKeyStoreInitializer.KEYSTORE_FILENAME_PATH;
import static ch.post.it.evoting.controlcomponent.TestKeyStoreInitializer.KEYSTORE_PASSWORD_FILENAME_PATH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.security.SignatureException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.DirtiesContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.BallotBoxService;
import ch.post.it.evoting.controlcomponent.ElectionContextEntity;
import ch.post.it.evoting.controlcomponent.ElectionContextService;
import ch.post.it.evoting.controlcomponent.ElectionEventEntity;
import ch.post.it.evoting.controlcomponent.ElectionEventService;
import ch.post.it.evoting.controlcomponent.MessageBrokerIntegrationTestBase;
import ch.post.it.evoting.controlcomponent.TestDatabaseCleanUpService;
import ch.post.it.evoting.controlcomponent.TestSigner;
import ch.post.it.evoting.controlcomponent.VerificationCardEntity;
import ch.post.it.evoting.controlcomponent.VerificationCardRepository;
import ch.post.it.evoting.controlcomponent.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponent.VerificationCardSetService;
import ch.post.it.evoting.controlcomponent.VerificationCardStateEntity;
import ch.post.it.evoting.controlcomponent.VerificationCardStateRepository;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.domain.voting.confirmvote.ConfirmationKey;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponentlVCCSharePayload;
import ch.post.it.evoting.domain.voting.confirmvote.LongVoteCastReturnCodesShare;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class LongVoteCastReturnCodeShareVerifyProcessorIT extends MessageBrokerIntegrationTestBase {

	private static final Random random = RandomFactory.createRandom();
	private static final int HLVCC_LENGTH = 44;

	private static List<ControlComponenthlVCCPayload> controlComponenthlVCCPayloads;
	private static GqGroup encryptionGroup;
	private static GqElement longVoteCastReturnCodeShare;
	private static ExponentiationProof exponentiationProof;
	private static ElectionEventEntity savedElectionEventEntity;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	@SpyBean
	private SignatureKeystore<Alias> signatureKeystoreService;

	@MockBean
	private ElectionContextService electionContextService;

	@BeforeAll
	static void setUpAll(
			@Autowired
			final BallotBoxService ballotBoxService,
			@Autowired
			final ElectionEventService electionEventService,
			@Autowired
			final VerificationCardSetService verificationCardSetService,
			@Autowired
			final VerificationCardRepository verificationCardRepository,
			@Autowired
			final VerificationCardStateRepository verificationCardStateRepository,
			@Autowired
			final Hash hash,
			@Autowired
			final LongVoteCastReturnCodesShareRepository longVoteCastReturnCodesShareRepository,
			@Autowired
			final ObjectMapper objectMapper) throws IOException, SignatureException {

		final String hashedLongVoteCastReturnCode = random.genRandomBase64String(HLVCC_LENGTH);
		final List<String> otherCCRsHashedLongVoteCastReturnCodes = List.of(random.genRandomBase64String(HLVCC_LENGTH),
				random.genRandomBase64String(HLVCC_LENGTH), random.genRandomBase64String(HLVCC_LENGTH));

		final List<String> hlVCC = new ArrayList<>();
		hlVCC.add(hashedLongVoteCastReturnCode);
		hlVCC.addAll(otherCCRsHashedLongVoteCastReturnCodes);

		final List<HashableString> i_aux_list = Stream.of("VerifyLVCCHash", electionEventId, verificationCardSetId, verificationCardId)
				.map(HashableString::from)
				.toList();
		final HashableList i_aux = HashableList.from(i_aux_list);
		final HashableString hlVCC_id_1 = HashableString.from(hlVCC.get(0));
		final HashableString hlVCC_id_2 = HashableString.from(hlVCC.get(1));
		final HashableString hlVCC_id_3 = HashableString.from(hlVCC.get(2));
		final HashableString hlVCC_id_4 = HashableString.from(hlVCC.get(3));
		final String hhlVCC_id = Base64.getEncoder().encodeToString(hash.recursiveHash(i_aux, hlVCC_id_1, hlVCC_id_2, hlVCC_id_3, hlVCC_id_4));

		final List<String> L_lVCC = new ArrayList<>();
		L_lVCC.add(hhlVCC_id);

		encryptionGroup = GroupTestData.getLargeGqGroup();

		savedElectionEventEntity = electionEventService.save(electionEventId, encryptionGroup);
		final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity(verificationCardSetId, savedElectionEventEntity,
				new CombinedCorrectnessInformation(Collections.emptyList()));
		verificationCardSetService.save(verificationCardSetEntity);
		verificationCardSetService.setLongVoteCastReturnCodesAllowList(verificationCardSetId, L_lVCC);

		// Create ballot box.
		final String ballotBoxId = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
		final GroupVector<PrimeGqElement, GqGroup> smallPrimeGroupMembers = PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(
				encryptionGroup, 1);
		final PrimesMappingTable primesMappingTable = PrimesMappingTable.from(
				List.of(new PrimesMappingTableEntry("actualVotingOption", smallPrimeGroupMembers.get(0), "semantic")));
		ballotBoxService.save(ballotBoxId, verificationCardSetId, true, 1, 1, 900, primesMappingTable);

		final VerificationCardEntity verificationCardEntity = new VerificationCardEntity(verificationCardId, verificationCardSetEntity,
				new byte[] {});

		final VerificationCardStateEntity verificationCardStateEntity = new VerificationCardStateEntity();
		verificationCardStateEntity.setLccShareCreated(true);

		// One to one bidirectional mapping.
		verificationCardStateEntity.setVerificationCardEntity(verificationCardEntity);
		verificationCardEntity.setVerificationCardStateEntity(verificationCardStateEntity);

		final GqGroupGenerator gqGroupGenerator = new GqGroupGenerator(encryptionGroup);
		final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(encryptionGroup));

		longVoteCastReturnCodeShare = gqGroupGenerator.genMember();
		exponentiationProof = new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(), zqGroupGenerator.genRandomZqElementMember());

		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		final ConfirmationKey confirmationKey = new ConfirmationKey(contextIds, new GqGroupGenerator(encryptionGroup).genMember());

		final LongVoteCastReturnCodesShareEntity longVoteCastReturnCodesShareEntity =
				new LongVoteCastReturnCodesShareEntity(verificationCardEntity,
						objectMapper.writeValueAsBytes(longVoteCastReturnCodeShare),
						objectMapper.writeValueAsBytes(gqGroupGenerator.genMember()),
						objectMapper.writeValueAsBytes(exponentiationProof),
						confirmationKey.element().getValue().toString());

		verificationCardRepository.save(verificationCardEntity);
		longVoteCastReturnCodesShareRepository.save(longVoteCastReturnCodesShareEntity);
		verificationCardStateRepository.save(verificationCardStateEntity);

		controlComponenthlVCCPayloads = List.of(
				getSignedControlComponenthlVCCPayload(confirmationKey, 1, hashedLongVoteCastReturnCode),
				getSignedControlComponenthlVCCPayload(confirmationKey, 2, otherCCRsHashedLongVoteCastReturnCodes.get(0)),
				getSignedControlComponenthlVCCPayload(confirmationKey, 3, otherCCRsHashedLongVoteCastReturnCodes.get(1)),
				getSignedControlComponenthlVCCPayload(confirmationKey, 4, otherCCRsHashedLongVoteCastReturnCodes.get(2))
		);
	}

	@AfterAll
	static void cleanUp(
			@Autowired
			final TestDatabaseCleanUpService testDatabaseCleanUpService) {
		testDatabaseCleanUpService.cleanUp();
	}

	@Test
	void happyPath() throws IOException, SignatureException {
		doReturn(true).when(signatureKeystoreService).verifySignature(any(), any(), any(), any());

		final LocalDateTime now = LocalDateTime.now();
		final ElectionContextEntity electionContextEntity = new ElectionContextEntity.Builder()
				.setElectionEventEntity(savedElectionEventEntity)
				.setStartTime(now.minusHours(1))
				.setFinishTime(now.plusHours(1))
				.build();
		when(electionContextService.getElectionContextEntity(electionEventId)).thenReturn(electionContextEntity);

		final MessageProperties messageProperties = new MessageProperties();
		final String correlationId = UUID.randomUUID().toString();
		messageProperties.setCorrelationId(correlationId);

		final Message message = new Message(objectMapper.writeValueAsBytes(controlComponenthlVCCPayloads), messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, VERIFY_LVCC_SHARE_HASH_REQUEST_QUEUE_1, message);

		final Message response = rabbitTemplate.receive(VERIFY_LVCC_SHARE_HASH_RESPONSE_QUEUE_1, 5000);

		assertNotNull(response);

		final ControlComponentlVCCSharePayload responsePayload =
				objectMapper.readValue(response.getBody(), ControlComponentlVCCSharePayload.class);

		assertTrue(responsePayload.isVerified());
		assertEquals(encryptionGroup, responsePayload.getEncryptionGroup());
		assertNotNull(responsePayload.getSignature());

		final LongVoteCastReturnCodesShare longVoteCastReturnCodesShare = responsePayload.getLongVoteCastReturnCodesShare()
				.orElseThrow(() -> new IllegalStateException("We checked is verified, shouldn't reach here"));
		assertNotNull(longVoteCastReturnCodesShare);

		assertEquals(1, longVoteCastReturnCodesShare.nodeId());
		assertEquals(electionEventId, longVoteCastReturnCodesShare.electionEventId());
		assertEquals(verificationCardSetId, longVoteCastReturnCodesShare.verificationCardSetId());
		assertEquals(verificationCardId, longVoteCastReturnCodesShare.verificationCardId());
		assertEquals(longVoteCastReturnCodeShare, longVoteCastReturnCodesShare.longVoteCastReturnCodeShare());
		assertEquals(exponentiationProof, longVoteCastReturnCodesShare.exponentiationProof());
	}

	private static ControlComponenthlVCCPayload getSignedControlComponenthlVCCPayload(final ConfirmationKey confirmationKey,
			final int nodeId, final String hashLongVoteCastCodeShare) throws IOException, SignatureException {

		final ControlComponenthlVCCPayload controlComponenthlVCCPayload =
				new ControlComponenthlVCCPayload(encryptionGroup, nodeId, hashLongVoteCastCodeShare, confirmationKey);

		final TestSigner controlComponentByNodeIdSigner = new TestSigner(KEYSTORE_FILENAME_PATH, KEYSTORE_PASSWORD_FILENAME_PATH,
				Alias.getControlComponentByNodeId(nodeId));
		controlComponentByNodeIdSigner.sign(controlComponenthlVCCPayload,
				ChannelSecurityContextData.controlComponenthlVCC(nodeId, electionEventId, verificationCardSetId, verificationCardId));

		return controlComponenthlVCCPayload;
	}
}

