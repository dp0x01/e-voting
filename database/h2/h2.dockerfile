ARG DOCKER_REGISTRY
FROM $DOCKER_REGISTRY/ev/java:17.0.7

ENV HOME=/home/evote/
WORKDIR /home/evote/

COPY ./h2/resources /home/evote/
COPY ./target/sql/schema /home/evote/sql/

RUN dnf install -q -y wget && dnf -q -y clean all

ARG FLYWAY_URL
RUN wget -q -O /home/evote/flyway.tar.gz ${FLYWAY_URL}

RUN dnf install -q -y tar && dnf -q -y clean all

RUN tar -xzf /home/evote/flyway.tar.gz -C /home/evote/ && \
    mv /home/evote/flyway*/ /home/evote/flyway-commandline/ && \
    chmod u+x /home/evote/flyway-commandline/flyway && \
    chown -R evote:evote /home/evote/ && chmod u+wrx /home/evote/*

USER evote
RUN ./create.sh
RUN rm -rf /home/evote/flyway-commandline/

CMD java -cp /data/appl/libs/h2*.jar org.h2.tools.Server -tcp -tcpPort 9093 -pg -web -ifNotExists -tcpAllowOthers
HEALTHCHECK --timeout=3s --retries=4 CMD curl --noproxy localhost -fk http://localhost:8082/ || exit 1